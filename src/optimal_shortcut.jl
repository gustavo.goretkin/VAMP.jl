export shortcutting_algorithm, shortcut

function shortcutting_algorithm(path::AbstractVector{STATE}, valid) where {STATE<:AbstractMotionPlanningState}
  N = length(path)
  instance = LazyAStarInstance(
    (i_to, i_fro) -> distance(path[i_to], path[i_fro]),
    i_s -> distance(path[i_s], path[N]),
    i_s -> i_s == N,
    function (children, s)
      for i = (s+1):N
        push!(children, i)
      end
    end,
    (i_to, i_fro) -> valid(path[i_to], path[i_fro])
  )
  COST = eltype(distance(path[1], path[1])) # arbitrary call to get cost type
  algorithm = LazyAStarAlgorithm(instance, Int64, COST)
  i_start = 1
  setup!(algorithm, i_start, zero(COST))
  return algorithm
end

"return indices into path"
function shortcut(algorithm)

  while isempty(algorithm.goal_node)
    if !step!(algorithm) error("shortcutting search should not fail, but did") end
  end

  return reverse(follow_path(algorithm.closed_parents, algorithm.goal_node[1]))

end

function shortcut_introspect(algorithm)
  queries = []
  while isempty(algorithm.goal_node)
    append!(queries, setdiff(keys(algorithm.valid_cache), queries))
    if !step!(algorithm) error("shortcutting search should not fail, but did") end
  end

  return queries, reverse(follow_path(algorithm.closed_parents, algorithm.goal_node[1]))
end
