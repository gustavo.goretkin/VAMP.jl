abstract type AbstractFrustum end

struct Frustum{T,VT} <: AbstractFrustum
    eye::T
    view::VT
end

struct FrustumPlanes{T, PT} <: AbstractFrustum
  frustum::Frustum{T}
  planes::Vector{PT} # side
  far_plane::PT
end

function FrustumPlanes(frustum::Frustum)
  # TODO frustum.view points do not necessarily lie on far_plane, but should.
  planes = [GGHyperPlane(frustum.eye, vertices(b)...) for b in boundary(frustum.view)]
  center_p = mean(vertices(frustum.view))

  # define all planes to have positive signed distance from points inside the frustum
  planes = map(
    p-> begin
      s = sign(signed_distance(p, center_p))
      GGHyperPlane(s * p.normal, s * p.d)
    end, planes)

  far_plane = GGHyperPlane(Vec(frustum.eye - center_p), center_p)
  return FrustumPlanes(frustum, planes, far_plane)
end

function signed_distance(fp::FrustumPlanes, p::Point)
  return min(
    minimum(signed_distance(plane, p) for plane in fp.planes),
    signed_distance(fp.far_plane, p)
    )
end

function vertices(frustum::Frustum)
  vcat(frustum.view, [frustum.eye])
end

function boundary(frustum::Frustum)
  # TODO only for 2D right now
  return [
    LineSegment(frustum.eye, frustum.view[1]),
    LineSegment(frustum.view[1], frustum.view[2]),
    LineSegment(frustum.view[2], frustum.eye)]
end
