import Plots
include("../perf/figure_maker.jl")


function vis_lra_tree!(plt, alg)
  tree_xy = unique(map(q->tuple(q.e.q...), collect(keys(alg.tree))))
  tree_xy_children = unique(map(tree_node->tuple(tree_node.id.q.e.q...), collect(values(alg.tree))))
  Plots.scatter!(plt, tree_xy, alpha=0.1, color=:grey)
end

function plot_q_trail!(plt, q_path, polys_robot__Frobot)
  N = length(q_path)
  for i in 1:N
      robot = VAMP.get_robot__Fworld(polys_robot__Frobot, q_path[i])
      plot_polyhedron!(plt, robot; color=:grey, fillalpha=0.0, linealpha=0.7)
  end
end


function follow_path_general(parent::Dict{S, V}, end_::S; v_to_k=identity) where {S, V}  # TODO any AbstractDict
  reached = Set{S}()
  r = S[]
  next_ = end_
  while next_ ∉ reached
    push!(reached, next_)
    push!(r, next_)
    node = parent[next_]
    if node.parent == nothing
      break
    end
    next_ = v_to_k(node)
  end
  return r
end



function show_path!(plt, q_path, parameters)
  v_path = VAMP.get_visible_region_geos_path__Fworld(parameters, q_path)
  viol_path = VAMP.get_incremental_violations__Fworld(parameters, q_path; visibility_path__Fworld=v_path)

  plot_frame_viol!(plt, parameters, q_path, v_path, viol_path, length(q_path))
end

function do_ffmpeg(filepath)
  frames_filepath = joinpath(filepath, "frames")
  let frames = joinpath(frames_filepath, "png"), output_no_ext = joinpath(filepath , "from_png")
    run(`ffmpeg -i $(frames)/%04d.png -c:v qtrle $(output_no_ext).mov`)
    run(`ffmpeg -i $(frames)/%04d.png -tune animation -pix_fmt yuv420p $(output_no_ext).mp4`)
  end
end

function do_movie(q_path, parameters, filepath)
  v_path = VAMP.get_visible_region_geos_path__Fworld(parameters, q_path)
  viol_path = VAMP.get_incremental_violations__Fworld(parameters, q_path; visibility_path__Fworld=v_path)

  frames_filepath = joinpath(filepath, "frames")
  plot_frames_viol(parameters, q_path, v_path, viol_path, frames_filepath)

  do_ffmpeg(filepath)
end
