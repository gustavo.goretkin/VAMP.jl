# A tree is a []-able of nodes, which define parent as idx = findparent(node) where tree[idx] gives the parent.
# and tree[1] is the root

function make_forward_accumulate_map(tree, accumulate)
  # assume nodes are topologically sorted
  # assumes first index is root
  d = similar(Array{Int}, indices(tree))
  fill!(d, -typemin(Int))

  d[first(LinearIndices(tree))] = zero(accumulate(tree[1]))
  for i in LinearIndices(tree)[2:end]
      # line error if iteration isn't in toplogical sort order
      d[i] = d[findparent(tree[i])] + accumulate(tree[i])
  end
  return d
end

function make_root_distance_map(tree)
  return make_forward_accumulate_map(tree, x->1)
end

"Includes `node_i` in the path (as last element). If `node_i` is root, return [node_i]``"
function find_root_path(tree, node_i)
  path_indices = [node_i]
  while path_indices[end] ≠ 1
    push!(path_indices, findparent(tree[path_indices[end]]))
  end
  return reverse(path_indices)
end


function make_children_map(tree)
  # assume nodes are topologically sorted
  # assumes first index is root
  T_INDEX = eltype(LinearIndices(tree))

  c = similar(Vector{Vector{T_INDEX}}, indices(tree))
  for i in LinearIndices(c)
    c[i] = Vector{T_INDEX}()
  end

  root_index = first(LinearIndices(tree))

  for i in LinearIndices(tree)[2:end]
      push!(c[findparent(tree[i])], i)
  end

  return c
end

function find_lowest_common_ancestor_and_paths(tree, node_1_index, node_2_index)
  p1 = find_root_path(tree, node_1_index)
  p2 = find_root_path(tree, node_2_index)

  l = min(length(p1), length(p2))
  i = 1
  while p1[i] == p2[i]
    i += 1
    if !(i ≤ l)
      break
    end
  end

  return p1[i-1], p1[i:end], p2[i:end]
end

function find_lowest_common_ancestor_and_path(tree, node_1_index, node_2_index)
  lca, p1, p2 = find_lowest_common_ancestor_and_paths(tree, node_1_index, node_2_index)
  return lca, vcat(reverse(p1), [lca], p2)
end


"If node_i is an ancestor of node_j, return node_i_index (A node is its own ancestor, here.)"
function find_lowest_common_ancestor(tree, node_1_index, node_2_index)
  lca, path = find_lowest_common_ancestor_and_path(tree, node_1_index, node_2_index)
  return lca
end

function length_path(tree, node_1_index, node_2_index)
  lca, path = find_lowest_common_ancestor_and_path(tree, node_1_index, node_2_index)
  return length(path)
end

function path(tree, node_1_index, node_2_index)
  lca, path = find_lowest_common_ancestor_and_path(tree, node_1_index, node_2_index)
  return path
end
