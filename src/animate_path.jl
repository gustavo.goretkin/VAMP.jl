# to be included after vspace_a_star.jl runs.

import Base.convert
USE_GPU = false


if USE_GPU
  using CUDAnative
  using CuArrays
  using CUDAdrv
  const CuArray = CuArrays.CuArray
end

visible_region_params_cache = nothing
viewdata_cache = nothing

function make_frame_visible_region!(plt, visible_region, raster_spec)
  global visible_region_params_cache
  global viewdata_cache
  spec = raster_spec

  if visible_region_params_cache == (visible_region, raster_spec)
    viewdata = viewdata_cache
  else
    pvv = visible_region

    out = nothing
    if USE_GPU
      out = CuArray(zeros(Int16, spec.size...))
    else
      out = zeros(Int16, spec.size...)
    end
    vout = VAMP.RasterVolume(spec, out)

    if USE_GPU
      pvv = VAMP.PackedViewVolumes(CuArray(pvv.arc_regions), CuArray(pvv.shadow_halfspaces))
    end

    VAMP.render!(vout, pvv)

    if USE_GPU
      synchronize()
    end

    # need to transfer GPU->CPU before plotting, otherwise "Bus Error 10"
    viewdata = (Array(vout.data))'
  end
  viewdata_cache = viewdata
  visible_region_params_cache = (visible_region, raster_spec)

  # make 0 plot as white, and anything else yellow-orange
  cmap = Plots.cgrad([:yellow, :orange])
  cmap.values[1] = eps()
  prepend!(cmap.values, [0.0])
  prepend!(cmap.colors, [Plots.RGBA{Float64}(1.0,1.0,1.0,1.0)])

  Plots.heatmap!(plt, VAMP.coordinates(spec)..., viewdata; aspect_ratio=1, c=cmap, colorbar=false)
end

function make_frame!(plt, path)
  tp = map(s->s.q, path)


  flatten_path(path::Array{VAMP.SE2MotionPlanningState{SCALE, T}}) where {SCALE, T} = reshape(reinterpret(T, path),  3, length(path))'

  solvepath = flatten_path(tp);

  for obstacle in polys_obstacles__Fworld.pieces
      plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
  end

  Plots.plot!(plt, solvepath[:,1], solvepath[:,2] ; aspect_ratio = 1)

  convert(::Type{VAMP.MoveAction}, state::SE2_MPState) = VAMP.MoveAction([state.e.q..., state.θ.q.θ])

  p = [convert(VAMP.MoveAction, state) for state in tp] # plot most recent robot conf on top ?maybe

  for i in 1:length(path)
    for p in VAMP.get_robot__Fworld(polys_robot__Frobot, path[i].q).pieces
      if i == length(path)
        plot_polyhedron!(plt, p; color=:blue, fillalpha=0.7)
      else
        plot_polyhedron!(plt, p; color=:grey, fillalpha=0.0, linealpha=0.5)
      end
    end
  end
end

function compute_viewbox(path, parameters)
  @setparam parameters boundingbox_obstacles__Fworld = nothing
  @setparam parameters polys_robot__Frobot = nothing

  bb_empty = BoundingBox([NaN, NaN], [NaN, NaN])

  unlift_q(path) = map(x->x.q, path)

  boundingbox_swept__Fworld = reduce(union,
      (BoundingBox(get_robot__Fworld(polys_robot__Frobot, q)) for q in unlift_q(path)), init=bb_empty)

  bb_view__Fworld = union(boundingbox_obstacles__Fworld, boundingbox_swept__Fworld)
  return bb_view__Fworld
end


function make_animation(path_full, parameters; q_goal=nothing, root="animation", view_bounding_box=compute_viewbox(path_full, parameters))
  frames_filepath = joinpath(root, "frames")
  mkpath(frames_filepath)

  for frame_i = 1:length(path_full)
    plt_animate = Plots.plot(reuse=true, aspect_ratio=1.0)

    make_frame_visible_region!(plt_animate, path_full[frame_i].visible_region, VAMP.RasterVolumeSpec(view_bounding_box, (256, 256)))

    path = path_full[1:frame_i]
    plot_path!(plt_animate, path, parameters, q_goal = q_goal)
    #display(plt_animate)
    frame_str = @sprintf("%05d", frame_i)
    print("$frame_i of $(length(path_full)), ")
    Plots.savefig(plt_animate, "$(frames_filepath)/$(frame_str).png")
  end

  output = joinpath(root, "output.mov")
  run(`ffmpeg -i $(frames_filepath)/%05d.png -c:v qtrle $output`)
  run(`open $output`)

  output = joinpath(root, "output.gif")
  run(`convert $(frames_filepath)/\*.png -fuzz 10% -layers Optimize $output`)
  run(`open $output`)
end

function plot_path!(plt, segment, parameters; q_goal=nothing)
    @setparam parameters polys_robot__Frobot = nothing
    @setparam parameters polys_obstacles__Fworld = nothing
    @setparam parameters boundingbox_workspace__Fworld = nothing

    make_frame_visible_region!(plt, segment[end].visible_region, VAMP.RasterVolumeSpec(boundingbox_workspace__Fworld, (256, 256)))
    if q_goal != nothing
        plot_polyhedron!(plt, VAMP.get_robot__Fworld(polys_robot__Frobot, q_goal), color=:green, fillalpha=0.0, linestyle=:dash)
    end

    tp = map(s->s.q, segment)
    flatten_path(path::Array{VAMP.SE2MotionPlanningState{SCALE, T}}) where {SCALE, T} = reshape(reinterpret(T, path),  3, length(path))'
    path2d = flatten_path(tp);
    Plots.plot!(plt, path2d[:,1], path2d[:,2])

    plot_polyhedron!(plt, polys_obstacles__Fworld, color=:grey, linecolor=nothing)

    for i in 1:length(segment)
        robot = VAMP.get_robot__Fworld(polys_robot__Frobot, segment[i].q)
        if i == length(segment)
          plot_polyhedron!(plt, robot; color=:blue, fillalpha=0.7)
        else
          plot_polyhedron!(plt, robot; color=:grey, fillalpha=0.0, linealpha=0.5)
        end
    end
end

function make_summary_plots(path_segments, parameters; q_goal=nothing, file_prefix="$(hex(rand(UInt))[1:6])_summary")
    for (i_segment, segment) in enumerate(path_segments)
        plt = Plots.plot(;aspect_ratio=1.0)
        plot_path!(plt, segment.path, parameters; q_goal=q_goal)

        frame_str = @sprintf("%02d", i_segment)
        Plots.savefig(plt, "$(file_prefix)_$(frame_str).png")
        #Plots.savefig(plt, "$(file_prefix)_$(frame_str).svg")
    end

end
