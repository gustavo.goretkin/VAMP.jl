import TimerOutputs: @timeit
import TimerOutputs

@with_kw struct TwoPhaseInstance
  timeouts = Dict(
    :explore_any=>(improvement=15.0, failure=1200.0),
    :explore_violation=>(improvement=15.0, failure=30.0)
  )
  parameters = Dict(:execute_move_prefix=>false, :move_safe_preference=>0.001 #=,:do_vavp=>false=#)
  vamp_pieces
  domain_parameters
end

@with_kw mutable struct TwoPhaseAlgorithm{I}
  instance::I
  state = :uninitialized
  path_segments = nothing
  move_alg = nothing
  move_process_result = nothing
  explore_process_result = nothing
  coverage_goal = nothing
  timer = TimerOutputs.TimerOutput()
  explore_search_result = nothing
  move_search_result = nothing
  final_search_summaries = nothing
  domain_parameters = nothing
end


function setup!(alg::TwoPhaseAlgorithm)
  alg.state = :move

  @unpack make_move_alg = alg.instance.vamp_pieces
  @unpack move_vis_successors = alg.instance.vamp_pieces
  @unpack move_safe_preference = alg.instance.parameters
  @unpack vsafe_violations = alg.instance.vamp_pieces
  @unpack mp = alg.instance.domain_parameters
  @unpack starting_visibility__Fworld = alg.instance.domain_parameters
  @unpack SearchState_t = alg.instance.domain_parameters
  @unpack get_visible_region_cached__Fworld = alg.instance.vamp_pieces
  goal = alg.instance.domain_parameters[:goal]

  alg.move_alg = make_move_alg(
    mp,
    goal,
    (s_to, s_fro) -> distance(s_to.q, s_fro.q) + move_safe_preference * vsafe_violations(s_to, s_fro),
    move_vis_successors,
    (s_to, s_fro) -> mp.q_valid_test(s_to.q),
    alg.instance.domain_parameters
  )

  alg.path_segments = Any[
    (
      path = [SearchState_t(mp.q_start, [mp.q_start],
        vcat(
          VAMP.make_packed_view_volume(UnionOfGeometry(starting_visibility__Fworld)),
          get_visible_region_cached__Fworld(mp.q_start)))],
      kind = :initial
    )]

  TimerOutputs.reset_timer!(alg.timer)
  alg.final_search_summaries = []
  return nothing
end


debugshit2 = Dict{Symbol, Any}()
"""
first try to move to the goal, with relaxed.
if path is illegal, try to see illegal part.
if that fails, try to see anything.
"""
function step!(alg::TwoPhaseAlgorithm)
  retval = nothing  # implement try-finally until TimerOutputs does it itself.
  @timeit alg.timer "step" begin
    @unpack prune_visible_region_state = alg.instance.vamp_pieces
    @unpack process_move_path = alg.instance.vamp_pieces
    @unpack execute_move_prefix = alg.instance.parameters
    @unpack do_vavp = alg.instance.parameters
    @unpack visibility_violations_path = alg.instance.vamp_pieces
    @unpack explore_search = alg.instance.vamp_pieces
    @unpack process_explore_path = alg.instance.vamp_pieces
    @unpack workspace_points__Fworld = alg.instance.domain_parameters

    start_state = alg.path_segments[end].path[end]
    @timeit alg.timer "prune_vis" start_state_pruned = prune_visible_region_state(start_state)

    if alg.state == :move
      @timeit alg.timer "$(string(alg.state))_search" alg.move_search_result = move_search_result = move_search!(alg.move_alg, start_state)
      push!(alg.final_search_summaries, move_search_result.search_summaries[end].summary)

      if isempty(alg.move_alg.goal_node)
          error("move search didn't get to goal.")
      end

      @timeit alg.timer "move_process" alg.move_process_result = move_process_result = process_move_path(alg.move_alg, start_state; prune=true)

      if length(move_process_result.move_path_to_add) > 0
        if execute_move_prefix || move_process_result.metadata.reaches_goal
          push!(alg.path_segments,
            (
              path = move_process_result.move_path_to_add,
              kind = alg.state,
              move_process_result = move_process_result,
              search_result = move_search_result
              ))
        end
      end

      if move_process_result.metadata.reaches_goal
        println("Move reached goal in a v-safe way!")
        retval = (keepgoing=false, reason=:success)
      end

      if retval == nothing
        swept_volume_uncovered = visibility_violations_path(move_process_result.move_path_unsafe)
        alg.coverage_goal = swept_volume_uncovered
        alg.state = :explore_violation
        retval = (keepgoing=true, reason=alg.state)
      end
    end

    if retval == nothing && (alg.state == :explore_any || (alg.state == :explore_violation && !do_vavp))
      println("TwoPhase Non-VAVP\n\n\n")
      # we didn't get to the goal yet, because couldn't see some of move_process_result.move_path_unsafe
      @timeit alg.timer "$(string(alg.state))_search" result = explore_search(start_state, alg.coverage_goal; timeouts=alg.instance.timeouts[alg.state], log_header="\n\n$(alg.state)\n")
      #alg.explore_search_result = result
      explore_search_result = result.explore_search_result
      push!(alg.final_search_summaries, explore_search_result.search_summaries[end].summary)

      alg.explore_process_result = explore_process_result = process_explore_path(result.explore_alg, start_state)

      if length(explore_process_result.explore_path_to_add) > 0
        push!(alg.path_segments,
          (
            path = explore_process_result.explore_path_to_add,
            kind = alg.state,
            search_result = explore_search_result
            ))

        alg.state = :move
        retval = (keepgoing=true, reason=alg.state)
      else
        println("$(alg.state) did not see anything.")
        if alg.state == :explore_violation
          alg.state = :explore_any
          workspace_uncovered = difference(Set(workspace_points__Fworld), alg.path_segments[end].path[end].visible_region)
          alg.coverage_goal = workspace_uncovered
          retval = (keepgoing=true, reason=alg.state)
        elseif alg.state == :explore_any
          alg.state = :failure
          retval = (keepgoing=false, reason=alg.state)
        end


      end
    end

    if retval == nothing && alg.state == :explore_violation && do_vavp
      println("TwoPhase VAVP\n\n\n")
      println("state update buggy in two-phase?")
      @show alg.state
      @show do_vavp
      @show retval

      @show length(alg.path_segments)
      @timeit alg.timer "vavp" vavp_search_result = vavp_search(start_state, alg.coverage_goal, alg.instance.vamp_pieces)

      println("\n\n debug shit \n\n")
      #global debugshit2[:vavp_start_state] = start_state
      #global debugshit2[:vavp_coverage_goal] = alg.coverage_goal
      #global debugshit2[:vavp_search_result] = vavp_search_result

      append!(alg.final_search_summaries, vavp_search_result.vavp_alg.final_search_summaries)

      if vavp_search_result.explore_status == :failure
        alg.state = :explore_any
        workspace_uncovered = difference(Set(workspace_points__Fworld), alg.path_segments[end].path[end].visible_region)
        alg.coverage_goal = workspace_uncovered
        retval = (keepgoing=true, reason=alg.state)
      else
        push!(alg.path_segments,
          (
            path = vavp_search_result.explore_path_to_add,
            kind = :vavp,
            search_result = vavp_search_result.search_result))
        alg.state = :move

#=
    (
      explore_path_to_add=vavp_search_result.vavp_alg.path,
      final_search_summaries=alg.final_search_summaries,
      explore_status=vavp_search_result.explore_status,
      vavp_alg=vavp_search_result.vavp_alg)
    end
=#
        retval = (keepgoing=true, reason=alg.state)
      end
    end
    if retval == nothing; error("state machine bork") end
  end
  println("returning from vavp step")
  @show retval
  return retval
end
