# lAzy*
export LazyAStarInstance, LazyAStarAlgorithm, setup!, step!, follow_path
using DataStructures

struct LazyAStarNode{STATE, COST}
  state::STATE
  parent_state::STATE # parent needs to be part of the search node so that validity checking can be performed lazily.
  cost_from_start::COST
end

struct AlwaysValid end

struct LazyAStarInstance{FT,FH,FG,FE,FV}# dispatch <: AbstractAStarInstance
  transition_cost::FT   # (state_to::STATE, state_from::STATE) ↦ edge_cost::COST
  heuristic::FH         # state::STATE ↦ cost_to_goal::COST #arguably belongs in Algorithm, not Instance
  goal_test::FG         # state::STATE ↦ ::Bool
  expand!::FE           # (Set{STATE}, state_from) ↦ ::Nothing
  valid::FV             # (state_to::STATE, state_from::STATE) ↦ ::Bool (pass in AlwaysValid() to make this regular/non-lazy A*)
end

abstract type AbstractDomination end
struct DummyDomination <: AbstractDomination
end

struct LazyAStarAlgorithm{STATE, COST, INSTANCE, DOMINATION}
  instance::INSTANCE
  closed_parents::Dict{STATE, Tuple{STATE, COST}}  # closed set and parents, with cost from start to key state
  agenda::PriorityQueue{LazyAStarNode{STATE, COST}, COST} # lazy agenda
  children::Vector{STATE} # workspace
  goal_node::Vector{STATE}
  valid_cache::Dict{Tuple{STATE, STATE}, Bool}
  domination::DOMINATION
end

function LazyAStarAlgorithm(instance, STATE, COST, domination=DummyDomination())
  closed_parents = Dict{STATE, Tuple{STATE, COST}}()
  agenda = PriorityQueue{LazyAStarNode{STATE, COST}, COST}()
  children = Vector{STATE}()
  goal_node = STATE[]
  valid_cache = Dict{Tuple{STATE, STATE}, Bool}()
  return LazyAStarAlgorithm(instance, closed_parents, agenda, children, goal_node, valid_cache, domination)
end

function setup!(alg::LazyAStarAlgorithm, start_state, start_cost)
  empty!(alg.closed_parents)
  while !isempty(alg.agenda) dequeue!(alg.agenda) end # at time of writing, there is no empty! method
  empty!(alg.children)
  empty!(alg.goal_node)
  empty!(alg.valid_cache)
  empty!(alg.domination)
  enqueue!(alg.agenda, LazyAStarNode(start_state, start_state, start_cost)=>start_cost) # technically should add h too
  return nothing
end

@inline function _isvalid_cached(alg::LazyAStarAlgorithm, s_to_from)
  if typeof(alg.instance.valid) == AlwaysValid # TODO verify this is inferred at compile time
    return true
  end

  #memoize
  if !haskey(alg.valid_cache, s_to_from)
    alg.valid_cache[s_to_from] = alg.instance.valid(s_to_from...)
  end

  if !alg.valid_cache[s_to_from]
    # expandme doesn't actually connect to its parent.
    return false
  end

  return  true
end

closed_check(closed_parents, state) = haskey(closed_parents, state)


import Base.empty!
function empty!(d::AbstractDomination)
  nothing
end

function domination_add!(d::AbstractDomination, search_state)
  nothing
end

function is_dominated(d::AbstractDomination, search_state)
  (false, nothing)
end

@inline function step!(alg::LazyAStarAlgorithm)
  # If laziness is used, there's no possibility of decreasing the cost of a state on the agenda, because
  # the agenda must keep track of the parent to that state, to determine validity lazily. # TODO watch out for domination and laziness
  # also did you need to do this without domination?
  if isempty(alg.agenda)
    return false
  end

  expandme = dequeue!(alg.agenda)

  if closed_check(alg.closed_parents, expandme.state) || is_dominated(alg.domination, expandme.state)[1]
    # Already found the optimal way to get to expandme.state.
    return true
  end

  s_to_from = (expandme.state, expandme.parent_state)

  if !_isvalid_cached(alg, s_to_from)
    # expandme doesn't actually connect to its parent.
    return true
  end

  # Just found the optimal path to expandme.state
  alg.closed_parents[expandme.state] = (expandme.parent_state, expandme.cost_from_start)
  domination_add!(alg.domination, expandme.state)

  if alg.instance.goal_test(expandme.state)
    push!(alg.goal_node, expandme.state)
    return true # don't expand once you've reached the goal
  end

  empty!(alg.children)
  alg.instance.expand!(alg.children, expandme.state)

  for child in alg.children
    if closed_check(alg.closed_parents, child) || is_dominated(alg.domination, child)[1]
      # would get caught above, too, but why bloat agenda?
      continue
    end
    h = alg.instance.heuristic(child)
    edge_cost = alg.instance.transition_cost(child, expandme.state)
    g = expandme.cost_from_start + edge_cost
    queue_value = g + h
    if queue_value == typemax(typeof(queue_value))
      continue
    end
    enqueue!(alg.agenda, LazyAStarNode(child, expandme.state, g)=>queue_value)
  end

  return true
end


function follow_path(parent::Dict{S,Tuple{S,C}}, end_::S) where {S, C}  # TODO any AbstractDict
  reached = Set{S}()
  r = S[]
  next_ = end_
  while next_ ∉ reached
    push!(reached, next_)
    push!(r, next_)
    next_, cost = parent[next_]
  end
  return r
end


function make_a_star_summary(alg)
  if length(alg.agenda) > 0
      (node, f_val) = DataStructures.peek(alg.agenda)
  else
    f_val = nothing
  end

  return (
    agenda_size=length(alg.agenda),
    closed_size=length(alg.closed_parents),
    agenda_top = (f_val=f_val,)
    )
end
