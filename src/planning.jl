export AbstractMotionPlanningGoal, PointEpsilonGoal, HolonomicMotionPlanningInstance, RRTInstance, RRTAlgorithm,
  setup!, step!, steps!, getpath

abstract type AbstractMotionPlanningGoal{STATE} end

# a point goal with an epsilon ball around it.
struct PointEpsilonGoal{STATE, T} <:  AbstractMotionPlanningGoal{STATE}
  q::STATE
  ϵ::T
end


import Base.in
import Base.rand
# requires distance function between STATE
in(q::STATE, g::PointEpsilonGoal{STATE}) where {STATE} = distance(q, g.q) ≤ g.ϵ

# only returns nominal goal
rand(g::PointEpsilonGoal) = g.q # TODO implement rest of `rand` interface

"Single-source motion planning"
struct HolonomicMotionPlanningInstance{STATE<:AbstractMotionPlanningState}
  q_start::STATE
  q_goal::AbstractMotionPlanningGoal{STATE}
  q_valid_test # collision checker
  qq_valid_test
end

abstract type AbstractRRTNode end

struct RRTNode{STATE}
  state::STATE
  parent_index::Int
end

# attempt at making RRTNode and RRTNodeAction look the same
RRTNode{STATE}(state::STATE, action, parent_index::Int) where {STATE} = RRTNode(state, parent_index)

struct RRTNodeAction{STATE, ACTION}
  state::STATE
  action::ACTION  # action taken from parent to get to `state` Could be Nothing for holonomic problems?
  parent_index::Int
end

action(::Type{RRTNode{S}}) where {S} = Nothing
action(::Type{RRTNodeAction{STATE, ACTION}}) where {STATE, ACTION} = ACTION

findparent(node::RRTNode) = node.parent_index
findparent(node::RRTNodeAction) = node.parent_index

struct RRTInstance{STATE}
  mp::HolonomicMotionPlanningInstance{STATE}
  extension_length::Float64
  q_sampler
end

struct RRTAlgorithm{STATE, NODE}
  instance::RRTInstance{STATE}
  tree::Vector{NODE}
  reached_goals::Vector{Int64}  # index into tree
end

function RRTAlgorithm(instance::RRTInstance{STATE}) where {STATE}
  return RRTAlgorithm(instance, RRTNode{STATE}[], Int64[])
end

function RRTAlgorithm(instance::RRTInstance{STATE}, NODE) where {STATE}
  return RRTAlgorithm(instance, NODE[], Int64[])
end

ROOT_SENTINEL=1

get_node_type(alg::RRTAlgorithm{STATE, NODE}) where {STATE, NODE} = NODE
get_state_type(alg::RRTAlgorithm{STATE, NODE}) where {STATE, NODE} = STATE

function setup!(alg::RRTAlgorithm{STATE, NODE}, mem_allocation_n) where {STATE, NODE}
  empty!(alg.tree)
  empty!(alg.reached_goals)
  sizehint!(alg.tree, mem_allocation_n)
  dummy_action = action(NODE) == Nothing ? nothing : zero(action(NODE))
  #@show NODE alg.instance.mp.q_start dummy_action

  push!(alg.tree, NODE(alg.instance.mp.q_start, dummy_action, ROOT_SENTINEL))
end

"if states tie for nearest, choose most savory one"
@inline savoriness(q::Any) = 0 # TODO less stupid name please

@inline function find_nearest(alg::RRTAlgorithm, q)
    T = eltype(distance(q, q)) #TODO nono
    d_nearest = typemax(T)
    savory_nearest = typemin(eltype(savoriness(q)))
    node_nearest_i = -1
    for (node_i, node) in enumerate(alg.tree)
      d = distance(node.state, q) # TODO could be sqdistance
      if d < d_nearest || (d == d_nearest && (savoriness(node.state) > savory_nearest ))
        d_nearest = d
        node_nearest_i = node_i
        savory_nearest = savoriness(node.state)
      end
    end
    return (node_nearest_i, d_nearest)
end

@inline function step!(alg::RRTAlgorithm)
  q_rand = alg.instance.q_sampler()
  step!(alg, q_rand)
end

@inline function step!(alg::RRTAlgorithm{STATE, NODE}, q_rand) where {STATE, NODE}
  node_nearest_i, d_nearest = find_nearest(alg, q_rand)
  q_from = alg.tree[node_nearest_i].state
  a_full = action(q_rand, q_from)
  # don't make long extensions
  l = min(1, alg.instance.extension_length / distance(q_from, q_rand))
  a_partial = fragment(l, a_full)
  q_new = state(q_from, a_partial)
  if !alg.instance.mp.q_valid_test(q_new) return nothing end
  if !alg.instance.mp.qq_valid_test(q_new, q_from) return nothing end
  node_new = NODE(q_new, a_partial, node_nearest_i)
  push!(alg.tree, node_new)

  if q_new ∈ alg.instance.mp.q_goal
    push!(alg.reached_goals, length(alg.tree))
  end
  return nothing
end

function steps!(alg::RRTAlgorithm, n)
  for i = 1:n
    if length(alg.reached_goals) > 0
      break
    end
    step!(alg)
  end
  return nothing
end


function getpath(tree::AbstractVector{NODE}, node::NODE) where {NODE}
  reached = Set{Int}()
  i = node.parent_index
  r = [node.state]
  while i ∉ reached
    push!(r, tree[i].state)
    push!(reached, i)
    i = tree[i].parent_index
  end
  return r
end

function get_state_action_path(tree::AbstractVector{NODE}, node::NODE) where {NODE}
  reached = Set{Int}()
  i = node.parent_index
  states = [node.state]
  actions = [node.action]
  while i ∉ reached
    push!(states, tree[i].state)
    push!(actions, tree[i].action)
    push!(reached, i)
    i = tree[i].parent_index
  end
  return states, actions
end

#=
struct VampState{STATE<:MotionPlanningState, CACHE}
  q::STATE
  visibility_cache::CACHE
end
=#
