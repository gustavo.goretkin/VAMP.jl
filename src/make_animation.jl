
function make_animation(visibility_instance, states_path, actions_path)
  # ignore first action. start from states_path[1] and apply action actions_path[2]
  robot__Frobot = visibility_instance.robot__Frobot
  occluders__Fworld = visibility_instance.occluders__Fworld
  frustum__Fcamera = visibility_instance.camera.frustum__Fcamera


  n = length(states_path)
  for i = 1:(n-1)
    vampstate = states_path[i]
    vampaction = actions_path[i+1]
    robot__Fworld = VAMP.get_robot__Fworld(robot__Frobot, vampstate.robot)
    tx__Fworld__Frobot = VAMP.get_tx__Fworld__Frobot(vampstate.robot)

    plt = plot(;aspect_ratio=1, xlim=(-4,4), ylim=(-4,4))
    for viewvolume in vampstate.viz
      for p in explicit(viewvolume).pieces
        plot_polyhedron!(plt, p; color=:yellow, linealpha=0.0) # linecolor=nothing has issues: https://github.com/JuliaPlots/Plots.jl/issues/1343
      end
    end


    for p in occluders__Fworld.pieces
      plot_polyhedron!(plt, p, color=:brown)
    end


    for p in robot__Fworld.pieces
      plot_polyhedron!(plt, p, color=:purple)
    end
    display(plt)

    for look_action in vampaction.looks
      tx__Frobot__Fcamera = VAMP.get_tx__Frobot__Fcamera(visibility_instance.camera, look_action)
      tx__Fworld__Fcamera = tx__Fworld__Frobot ∘ tx__Frobot__Fcamera
      frustum__Fworld = transform(tx__Fworld__Fcamera, frustum__Fcamera)
      plot_polyhedron!(plt, frustum__Fworld, color=:orange, alpha=0.5)
      display(plt)
    end
    sleep(0.1)
  end

end
