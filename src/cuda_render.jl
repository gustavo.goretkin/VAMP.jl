using CuArrays, CUDAnative
using CUDAnative
import CUDAdrv


function cuda_kernel_render_packed_viewvolume(out, raster_spec, arc_regions, shadow_halfspaces)
  I = CuArrays.@cuindex(out)
  x__c = VAMP.coordinates(raster_spec, I)
  point = Point(x__c[1], x__c[2])
  # accumulate
  out[I...] += count_packed_viewvolume_in(point, arc_regions, shadow_halfspaces)
  return
end


function render!(v::VAMP.RasterVolume{S, A}, packed_viewvolume::PackedViewVolumes) where {S, A <: CuArray}
  extent__c = v.spec.limits.maximum - v.spec.limits.minimum
  size__d = size(v.data)
  return CUDAnative._cuda(size__d, 0, CUDAnative.CuDefaultStream(),
    cuda_kernel_render_packed_viewvolume,
    CUDAnative.cudaconvert(v.data),
    CUDAnative.cudaconvert(v.spec),
    CUDAnative.cudaconvert(packed_viewvolume.arc_regions),
    CUDAnative.cudaconvert(packed_viewvolume.shadow_halfspaces)
  )
end

function render_batch!(v::VAMP.RasterVolume{S, A}, viewvolumes; batchsize=1000) where {S, A <: CuArray}
  n_total = length(viewvolumes.pieces)
  n_batch = Int(ceil(n_total/batchsize))
  for i_batch = 1:n_batch
    selected = ((i_batch - 1)*batchsize + 1):min(i_batch * batchsize, n_total)
    println("process $selected, batch $i_batch of $n_batch")
    pvv = make_packed_view_volume(UnionOfGeometry(uvvs.pieces[selected]))
    gpu_pvv = PackedViewVolumes(CuArray(pvv.arc_regions), CuArray(pvv.shadow_halfspaces))
    render!(v, gpu_pvv)
    CUDAdrv.synchronize()
  end
  return nothing
end
