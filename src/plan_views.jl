#=
function extreme_point(center, centerline, points, direction)
  points = collect(points)
  val, i = findmax([direction * cross(normalize(point - center), normalize(centerline)) for point in points])
  return points[i]
end

function camera_transform(centerline__Fa, centerline__Fb)
  # transform that takes centerline_from to centerline_to
  # while keeping some other subspaces invariant (no subspace in 2D)
  # TODO generalize 3D
  # TODO figure out which subspaces: up stays up. horizontal stays horizontal
  centerline__Fa = normalize(centerline__Fa)
  centerline__Fb = normalize(centerline__Fb)
  c = dot(centerline__Fa, centerline__Fb)
  s = cross(centerline__Fa, centerline__Fb)
  tx__Fb__Fa = SMatrix{2,2}(c, s, -s, c)
  return tx__Fb__Fa
end

function extreme_view(center__Fa, centerline, frustum_depth, frustum_width, point__Fa, direction)
  tx__Fa__Fcamera = camera_transform(Point(frustum_depth, direction * frustum_width/2), (point__Fa - center__Fa))
  mat(θ) = SMatrix{2,2}(cos(θ), sin(θ), -sin(θ), cos(θ))
  offset = mat(deg2rad(1 * direction)) # tolerance
  return AffineMap(tx__Fa__Fcamera * offset, SVector(center__Fa...))
end
=#

#=
function plan_views(center__Fworld, tx__Fworld__Frobot, frustum_depth, frustum_width, points__Fworld, poly_fov__Fcamera)
  # TODO return transforms, not views
  poly_fov__Fcamera = polyhedron( intersect(allhalfspaces(poly_fov__Fcamera)...) ) # TODO be able to transform whatever type this was

  centerline__Fcamera = SVector(1.0, 0.0)
  centerline__Fworld = tx__Fworld__Frobot.m * centerline__Fcamera # only rotate
  views = []
  points__Fworld = collect(points__Fworld)
  while length(points__Fworld) > 0
    point = extreme_point(center__Fworld, centerline__Fworld, points, +1)
    tx__Fworld__Fcamera = extreme_view(center__Fworld, centerline__Fworld, frustum_depth, frustum_width, point, +1)

    view__Fworld = transform(AffineMap(tx__Fworld__Fcamera, SVector(0.0, 0.0)), poly_fov__Fcamera)
    push!(views, view__Fworld)
    newpoints__Fworld = [point__Fworld for point__Fworld in points__Fworld if !isinside(view__Fworld, point__Fworld)]
    if length(newpoints__Fworld) == length(points__Fworld)
      error("infeasible or bug")
    end
    points__Fworld = newpoints__Fworld
  end
  return views
end
=#

function get_discretized_views(q_path, parameters)
  @setparam parameters visibility_instance = nothing
  @setparam parameters frustum_depth = nothing
  @setparam parameters frustum_width = nothing
  @setparam parameters poly_frustum__Fcamera = nothing
  @setparam parameters camera_is_fixed = nothing

  if camera_is_fixed
    θs = [0.0]
  else
    a = acos(visibility_instance.camera.frustum_union__Fcamerabase.cos_half_fov)
    b = atan(frustum_width/2, frustum_depth)
    θs = collect(range(-(a-b), stop=(a-b), length=Int(trunc(a/b * 2)))) # enough to cover the frustum_union__Fcamerabase, and once more.
  end
  return [[
    begin
      tx__Fcamerabase__Fcamera = convert(LinearMap, VAMP.Rot(θ))
      tx__Frobot__Fcamerabase = visibility_instance.camera.camera.tx__Frobot__Fcamerabase
      tx__Fworld__Frobot = get_tx__Fworld__Frobot(q_robot)
      tx__Fworld__Fcamera = tx__Fworld__Frobot ∘ tx__Frobot__Fcamerabase ∘ tx__Fcamerabase__Fcamera
      occluders__Fworld = visibility_instance.occluders__Fworld
      #transform(tx__Fworld__Fcamera, poly_frustum__Fcamera)
      compute_visibility(tx__Fworld__Fcamera, poly_frustum__Fcamera, occluders__Fworld)
    end
    for θ in θs]
      for q_robot in q_path]


  #=
  n = length(q_path)
  point_path = [transform(get_tx__Fworld__Frobot(q_robot), visibility_instance.robot__Frobot) for q_robot in q_path]
  points = vcat(map(collect,point_path)...)
  potential_view_path = [VAMP.get_visible_region__Fworld(visibility_instance, q_robot) for q_robot in q_path]

  txss = [[] for _ = 1:n]
  for i = 1:n
    q_robot = q_path[i]
    tx__Fworld__Frobot = get_tx__Fworld__Frobot(q_robot)
    @show length(points)
    points_within = filter(point -> isinside(potential_view_path[i], point), points)
    @show length(points_within)
    centerline__Fcamera = SVector(1.0, 0.0)
    centerline__Fworld = tx__Fworld__Frobot.m * centerline__Fcamera # only rotate
    center__Fworld = tx__Fworld__Frobot.v

    for point in points_within
      for direction = (-1, 1)
        tx__Fworld__Fcamera = extreme_view(center__Fworld, centerline__Fworld, frustum_depth, frustum_width, point, +1)
        push!(txss[i], tx__Fworld__Fcamera)
      end
    end
  end

  return txss
  =#
end

#=
function find_undominated(sets)
  return [i for i in eachindex(sets) if !any(i != j && (sets[i] ⊊ sets[j]) for j in eachindex(sets))]
end
=#

#=
function compute_membership(points__Fworld, viewss__Fworld, potential_view_path__Fworld)
  # stupid slow.
  txss_membership = [ [
    begin
      print(2)
      Set(i for i = eachindex(points__Fworld) if
          let point__Fworld = points__Fworld[i]
            isinside(potential_view__Fworld, point__Fworld) && isinside(poly_frustum__Fcamera, point__Fworld)
          end
        end)
    end for view__Fworld in views__Fworld ] for (views__Fworld, potential_view__Fworld) in zip(viewss__Fworld, potential_view_path__Fworld)]
  return txss_membership
end
=#
#=
function prune_dominated_views(txss__Fworld__Fcamera, txss_membership)
  # stupid slow.
  txss_pruned = []
  for (txs_membership, txs) in zip(txss_membership, txss__Fworld__Fcamera)
    print(1)
    unique_idxs = unique(i->txs_membership[i], VAMP.find_undominated(txs_membership))
    push!(txss_pruned, txs[unique_idxs])
  end
  return txss_pruned
end
=#

function make_plan_views_model(swept_points_path, discretized_views, starting_visibility__Fworld)
  n = length(swept_points_path)
  k = Base.maximum(map(length, discretized_views)) # TODO handle ragged views (not m at each path step) #TODO deprecation of maximum is messing up namespacing

  m = JuMP.Model()
  JuMP.@variable(m, path_views[1:n], Bin) # whether a point of the path has any views
  JuMP.@variable(m, view_taken[1:n, 1:k], Bin) # whether a view is taken

  for path_i = 1:n
    for point in swept_points_path[path_i]
      if isinside(UnionOfGeometry(starting_visibility__Fworld), point)
        continue
      end

      which_makes_point_visible = 0
      for path_j = 1:(path_i - 1)
        for (view_j, view) = enumerate(discretized_views[path_j])
          if isinside(view, point)
            which_makes_point_visible += view_taken[path_j, view_j]
          end
        end
      end

      JuMP.@constraint(m, which_makes_point_visible ≥ 1)
    end
  end

  for path_i = 1:n
    for view_j = 1:k
      JuMP.@constraint(m, path_views[path_i] ≥ view_taken[path_i, view_j])
    end
  end
  JuMP.@objective(m, Min, 20*sum(path_views[:]) + sum(view_taken[:]))
  JuMP.setsolver(m, Cbc.CbcSolver())
  model = m
  #return (model, path_views, view_taken)
  return Dict(:model=>model, :path_views=>path_views, :view_taken=>view_taken)
end

function difference_list(a, list)
  for el in list
    a = diffrence(a, el)
  end
  return a
end

#=
function plan_views_path(q_path, visibility_instance, starting_visibility__Fworld)
  n = length(q_path)
  point_path = [transform(get_tx__Fworld__Frobot(q_robot), visibility_instance.robot__Frobot) for q_robot in q_path]
  view_set_path = [[] for _ in q_path]
  potential_view_path = [VAMP.get_visible_region__Fworld(visibility_instance, q_robot) for q_robot in q_path]

  for i = 1:n
    while true
      uncovered = difference_list(point_path[i], [starting_visibility__Fworld, vcat(view_set_path[1:i]...))
      if length(uncovered) == 0
        break
      end

      findmax(
        [count(point->isinside(potential_view, point), uncovered)
          for potential_view in potential_view_path[1:i-1]])


      candidate_poses = Set(i for i = 1:n if isinside(potential_view_path[i], point))
      already_have_view = Set(i for i = 1:n if length(view_set_trajectory[i]) > 0)
      preferred = intersect(candidate_poses, already_have_view)

    chosen = if length(preferred) == 0
      minimum(candidate_poses)
    else
      minimum(preferred)
    end
  end


end
=#

difference_path(swept_points_path, view) = [difference(swath, view) for swath in swept_points_path]
length_path(swept_points_path) = sum(map(length, swept_points_path))



"""
first_cost: cost of first view taken at a path_i
rest_cost: cost of any other views taken at the same place.
"""
function choose_greedy(swept_points_path, discretized_views_path, cost_path; cover=[])
    path_N = length(swept_points_path)

    n_covers = Dict()
    n_scores = Dict()

    best_score = 0
    best_view = nothing

    for path_i = 1:(path_N-1)
        for view_i = 1:length(discretized_views_path[path_i])
            if (path_i=path_i, view_i=view_i) in cover; continue end
            view_ = discretized_views_path[path_i][view_i]
            swept_path_i = swept_points_path[(path_i+1):end]
            uncovered_path = difference_path(swept_path_i, view_)
            n_covers[(path_i, view_i)] = length_path(swept_path_i) - length_path(uncovered_path)
            n_scores[(path_i, view_i)] = score = n_covers[(path_i, view_i)] / cost_path[path_i]

            if score > best_score
                best_score = score
                best_view = (path_i=path_i, view_i=view_i)
            end
        end
    end

    if best_score == 0
        error("set cover is not feasible")
    end

    path_i = best_view.path_i
    remaining_swept_points_path_i = difference_path(
        swept_points_path[path_i+1:end],
        discretized_views_path[path_i][best_view.view_i])

    remaining_swept_points_path = vcat(swept_points_path[1:path_i], remaining_swept_points_path_i)
    return (
        remaining_swept_points_path=remaining_swept_points_path,
        best_view=best_view)
end

function greedy_set_cover(swept_points_path, discretized_views_path; first_cost=15.0, rest_cost=1.0)
    cost_path = [first_cost for _ in discretized_views_path]
    tried = Set{Tuple{Int64, Int64}}()
    cover = []

    new_swept_points_path = swept_points_path
    while length_path(new_swept_points_path) > 0
        @show length_path(swept_points_path)

        s1 = reduce(union, swept_points_path)
        s2 = reduce(union, new_swept_points_path)
        whatcovered = setdiff(s1, s2)
        plt = Plots.plot(;aspect_ratio=1.0)
        scatter_points!(plt, s2; color=:red)
        scatter_points!(plt, whatcovered; color=:blue)

        if length(cover) > 0
          println("cover")
          ce = cover[end]
          v = discretized_views_path[ce.path_i][ce.view_i]
          uoc = UnionOfConvex([d for d in explicit(v).pieces])
          plot_polyhedron!(plt, uoc; alpha=0.5, linealpha=0.0, color=:yellow)
        else
          println("no cover yet")
        end
        display(plt)
        println("done plot")

        swept_points_path = new_swept_points_path

        result = choose_greedy(swept_points_path, discretized_views_path, cost_path; cover=cover)
        push!(cover, result.best_view)
        cost_path[result.best_view.path_i] = rest_cost # now that we took a view here, subsequent views can be cheaper
        new_swept_points_path = result.remaining_swept_points_path
    end
    return cover
end

function prune_swept_path(swept_points_path)
  fpf = VAMP.FloatingPointFixer.FPFixer(Set{Point{2, Float64}}(), 1e-1, (a,b)->norm(a-b))
  [[p for p in t if VAMP.FloatingPointFixer.isdistinct!(fpf, p)] for t in swept_points_path]
end

function greedy_view_plan(q_path, parameters, starting_visibility__Fworld)
  @setparam parameters visibility_instance = nothing

  println("Generate Discrete Views")
  discrete_views = get_discretized_views(q_path, parameters)
  println("Generate Swept Path")
  swept_path = [transform(get_tx__Fworld__Frobot(q_robot), visibility_instance.robot__Frobot) for q_robot in q_path];
  swept_path = difference_path(swept_path, starting_visibility__Fworld)
  swept_path_pruned = map(Set, prune_swept_path(swept_path))

  view_plan = greedy_set_cover(swept_path_pruned, discrete_views)
  return (discrete_views=discrete_views, view_plan=view_plan)
end

function vamp_geom_to_plots(vamp_geom)
  # convert it into a Plots.Shape. No more funny geometry stuff. Good for plotting.
  poly_to_geos_coords(poly) = VAMP.LibGEOSInterface.LibGEOS.coordinates(VAMP.LibGEOSInterface.LibGEOS.Polygon(poly))
  geos_coords_to_plots(coords) = Plots.Shape(VAMP.LibGEOSInterface.polygon_coordinates_to_exterior_shape(coords)...)

  poly_to_plots(poly) = geos_coords_to_plots( poly_to_geos_coords(poly))

  return poly_to_plots(vamp_geom)
end
