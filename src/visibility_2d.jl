module Visibility2D
using GeometryTypes

struct Polygon{POINT}
  vertices::Vector{POINT}
end

function cross(a::Vec{2, T}, b::Vec{2, T}) where {T}
  return a[1]b[2] - a[2]b[1]
end

function ⊖(p1::Point{N,T}, p2::Point{N,T}) where {N,T}
  Vec{N,T}(p1 - p2)
end

orthogonal(v) = [v[2], -v[1]]

# N-1 dimensional plane
# dot(plane, normal) = d  #TODO change to dot(plane, normal) + d = 0
type GGHyperPlane{N, T} #Polyhedra.jl defines hyperplane
  normal::Vec{N, T}
  d::T
end


"""
return indices of vertices where adjacent faces change their orientation with respect to `point`
if vertices are ordered CCW, and point is inside poly, then the cross product should be > 0,
  and there will not be a negative transition. There might be a positive transition due to init of
  `last_sense = 0`
"""
function horizon(poly::Polygon, point)
  # assume poly is convex
  N = length(poly.vertices)

  last_sense = 0
  positive_transition = -1
  negative_transition = -1
  for i = 1:N
    edge = (poly.vertices[i], poly.vertices[mod1p(i+1, N)])
    v_poly = edge[2] ⊖ edge[1]
    v_eye = edge[2] ⊖ point # either edge point should work
    this_sense = sign(cross(v_poly, v_eye))
    if this_sense == 0
      continue # an edge aligns with our eye.
    end
    if i == 1
      last_sense = this_sense
      continue
    end

    # in degenerate cases, last_sense can be 0, and we want to report that edge.
    if this_sense > last_sense
      positive_transition = i
    elseif this_sense < last_sense
      negative_transition = i
    end

    last_sense = this_sense
  end
  return (positive_transition, negative_transition)

end

end
