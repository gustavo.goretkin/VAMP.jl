export AbstractMotionPlanningState, AbstractMotionPlanningAction, EuclideanMotionPlanningState, EuclideanMotionPlanningAction,
  E_MPState, E_MPAction, distance, sqdistance, action, state, fragment, SO2MotionPlanningState, SO2MotionPlanningAction,
  SO2_MPState, SO2_MPAction, SE2MotionPlanningState, SE2MotionPlanningAction, SE2_MPState, SE2_MPAction

abstract type AbstractMotionPlanningState end
abstract type AbstractMotionPlanningAction end

struct EuclideanMotionPlanningState{DIM, T} <: AbstractMotionPlanningState
  q::SVector{DIM, T}
end

struct EuclideanMotionPlanningAction{DIM, T} <: AbstractMotionPlanningAction
  a::SVector{DIM, T}
end

const E_MPState = EuclideanMotionPlanningState
const E_MPAction = EuclideanMotionPlanningAction

E_MPState(xargs...) =  E_MPState(SVector(xargs...))
E_MPAction(xargs...) =  E_MPAction(SVector(xargs...))

#=
# Distances doesn't get inlined. avoid call overhead.
using Distances
@inline distance(q1::E_MPState{DIM}, q2::E_MPState{DIM}) where {DIM} = euclidean(q1.q, q2.q)
@inline sqdistance(q1::E_MPState{DIM}, q2::E_MPState{DIM}) where {DIM} = sqeuclidean(q1.q, q2.q)
=#

@inline distance(q1::E_MPState{DIM}, q2::E_MPState{DIM}) where {DIM} = norm(action(q1, q2).a)
@inline sqdistance(q1::E_MPState{DIM}, q2::E_MPState{DIM}) where {DIM} = norm(action(q1, q2).a)^2 #TODO no.

"solve trivial BVP"
@inline action(q_to::E_MPState{DIM}, q_from::E_MPState{DIM}) where {DIM} = E_MPAction(q_to.q - q_from.q)
"solve trivial IVP"
@inline state(q_from::E_MPState{DIM}, a::E_MPAction{DIM}) where {DIM} = E_MPState(q_from.q + a.a)

"0≤α≤1, how much of the action"
fragment(α::Number, a::E_MPAction) = E_MPAction(α * a.a)

import Base.zero
zero(::Type{E_MPAction{DIM, T}}) where {DIM, T} = E_MPAction(zero(SVector{DIM, T}))

struct SO2MotionPlanningState{T} <: AbstractMotionPlanningState
  q::T
end

struct SO2MotionPlanningAction{T} <: AbstractMotionPlanningAction
  a::T
end

const SO2_MPState = SO2MotionPlanningState
const SO2_MPAction = SO2MotionPlanningAction

# Rot as element of group and as group action
SO2_MPState(θ::Number) =  SO2_MPState(Rot(θ))
SO2_MPAction(θ::Number) =  SO2_MPAction(Rot(θ))

import LinearAlgebra: norm  #TODO define norm for the rest of them. Here is used because angle-wrapping computation is reused in distance and also action
@inline norm(a::SO2MotionPlanningAction{Rot{T}}) where {T} = abs(a.a.θ)
@inline distance(q1::SO2_MPState{Rot{T}}, q2::SO2_MPState{Rot{T}}) where {T} = norm(action(q1, q2)) # could be a fallback for it all
@inline sqdistance(q1::SO2_MPState{Rot{T}}, q2::SO2_MPState{Rot{T}}) where {T} = action(q1, q2).a.θ^2
# I think this order is pretty unintuitive, but it matched my interpretation of -
"angular distance computation"
@inline action(q_to::SO2_MPState{Rot{T}}, q_from::SO2_MPState{Rot{T}}) where {T} = SO2_MPAction(Rot(mod2pi((q_to.q.θ - q_from.q.θ) + π) - π))

@inline state(q_from::SO2_MPState{Rot{T}}, a::SO2_MPAction{Rot{T}}) where {T} = SO2_MPState(Rot(q_from.q.θ + a.a.θ))

fragment(α::Number, a::SO2_MPAction{Rot{T}}) where {T} = SO2_MPAction(Rot(α * a.a.θ))

zero(::Type{SO2_MPAction{T}}) where {T} = SO2_MPAction(zero(T))

# By having SCALE as a type parameter, it's known statically.
"SCALE has units of distance / angle"
struct SE2MotionPlanningState{SCALE, T} <: AbstractMotionPlanningState
  e::EuclideanMotionPlanningState{2,T}
  θ::SO2MotionPlanningState{Rot{T}}
end

"SCALE has units of distance / angle"
struct SE2MotionPlanningAction{SCALE, T} <: AbstractMotionPlanningAction
  e::EuclideanMotionPlanningAction{2,T}
  θ::SO2MotionPlanningAction{Rot{T}}
end


const SE2_MPState = SE2MotionPlanningState
const SE2_MPAction = SE2MotionPlanningAction

function SE2_MPState(x, y, θ; SCALE=Any)
  x, y, θ = promote(x, y, θ)
  T = typeof(x)
  return SE2_MPState{SCALE, T}(E_MPState(x,y), SO2_MPState(θ))
end

# allow SE2_MPState{SCALE}(x, y, θ)
function (::Type{SE2_MPState{SCALE, TT} where TT})(x, y, θ) where {SCALE}
  x, y, θ = promote(x, y, θ)
  T = typeof(x)
  SE2_MPState{SCALE, T}(E_MPState(x,y), SO2_MPState(θ))
end

# allow convert(SE2_MPState{SCALE}, q::SE2_MPState{Any, T})
import Base.convert
function convert(::Type{SE2_MPState{SCALE, TT} where TT}, q::SE2_MPState{Any, T}) where {SCALE, T}
  SE2_MPState{SCALE, T}(q.e, q.θ)
end

# allows convert(SE2_MPState{0.5, Float64}, SE2_MPState{Any}(1,2,3.0))
function convert(::Type{SE2_MPState{SCALE, TT}}, q::SE2_MPState{Any, T}) where {SCALE, T, TT}
  SE2_MPState{SCALE, TT}(q.e, q.θ)
end


import Base.show

"print it like you can build it, with compact form."
show(io::IO, s::SE2_MPState{SCALE}) where {SCALE} = print(io, "SE2_MPState{$SCALE}($(s.e.q[1]),$(s.e.q[2]),deg2rad($(rad2deg(s.θ.q.θ))))")

function SE2_MPAction(x, y, θ; SCALE=1.0)
  x, y, θ = promote(x, y, θ)
  T = typeof(x)
  return SE2_MPAction{SCALE, T}(E_MPAction(x,y), SO2_MPAction(θ))
end

@inline sqdistance(q1::SE2_MPState{SCALE}, q2::SE2_MPState{SCALE}) where {SCALE} = sqdistance(q1.e, q2.e) + SCALE^2 * sqdistance(q1.θ, q2.θ)
@inline distance(q1::SE2_MPState{SCALE}, q2::SE2_MPState{SCALE}) where {SCALE} = sqrt(sqdistance(q1, q2)) #could be a fallback with a promote mechanism

@inline action(q_to::SE2_MPState{SCALE, T}, q_from::SE2_MPState{SCALE, T}) where {SCALE, T} = SE2_MPAction{SCALE, T}(action(q_to.e, q_from.e), action(q_to.θ, q_from.θ))
@inline state(q_from::SE2_MPState{SCALE, T}, a::SE2_MPAction{SCALE, T}) where {SCALE, T} = SE2_MPState{SCALE,T}(state(q_from.e, a.e), state(q_from.θ, a.θ))
@inline fragment(α::Number, a::SE2_MPAction{SCALE, T}) where {SCALE, T} = SE2_MPAction{SCALE, T}(fragment(α, a.e), fragment(α, a.θ))

@inline zero(::Type{SE2_MPAction{SCALE, T}}) where {SCALE, T} = SE2_MPAction{SCALE, T}(zero(E_MPAction{2,T}), zero(SO2_MPAction{T}))
