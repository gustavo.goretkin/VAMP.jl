using GeometryTypes
using StaticArrays
using CoordinateTransformations
using Polyhedra
import Base.convert
using NaNMath

using BoundingBoxes: BoundingBox, center, minlimit, maxlimit, dimlimits

import GeometryTypes.vertices
import GeometryTypes.intersects
import GeometryTypes.contains
import GeometryTypes.widths
import GeometryTypes.isinside

export UnionOfConvex, Difference, difference, explicit, cut, ArcRegion, DifferenceGeneral, UnionOfGeometry, cellstatus,
  BoundingBox, ordered_vertices, findnearest, make_aabb_bounded, minlimit, maxlimit, dimlimits, Rot

struct UnionOfConvex{T}
  pieces::Vector{T}
end

struct UnionOfGeometry{T}
  pieces::T
end

struct IntersectionOfGeometry{T}
  pieces::T
end

function isinside(iog::IntersectionOfGeometry{T}, p) where {T}
  for piece in iog.pieces
    if !isinside(piece, p)
      return false
    end
  end
  return true
end

function isinside(uog::Union{UnionOfConvex{T}, UnionOfGeometry{T}}, p) where {T}
  for piece in uog.pieces
    if isinside(piece, p)
      return true
    end
  end
  return false
end

function boundary(g::LineSegment)
  return SVector(vertices(g)...)
end

function boundary(p::FlexibleConvexHull{PT}) where {PT}
  b = LineSegment{PT}[]
  v = vertices(p)

  n = length(v)
  for i = 1:n
    # iterate through cyclic pairs
    i1 = i
    i2 = mod1(i+1, n)
    push!(b, LineSegment(v[i1], v[i2]))
  end
  return b
end

vertices(p::Point) = (p,) #TODO probably should return vector

# get vertices of all the pieces. may have repeated vertices.
function vertices(union::UnionOfConvex)
  PT = eltype(eltype(union.pieces))
  v = PT[]
  for piece in union.pieces
    append!(v, vertices(piece))
  end
  return v
end

abstract type AbstractPolygon end

struct Polygon{T} <: AbstractPolygon
  vertices::Vector{T}
end

struct StaticPolygon{N,T} <: AbstractPolygon
  vertices::SVector{N,T}
end

#=
"""
function jsintersects(a, b)
  (a1, a2) = vertices(a)
  (b1, b2) = vertices(b)
  db = b2 - b1
  da = a2 - a1

	u_b  = db[2] * da[1] - db[1] * da[2]
	if (u_b != 0)
		ua = (db[1] * (a1[2] - b1[2]) - db[2] * (a1[1] - b1[1])) / u_b
		return (true, Point(a1[1] - ua * -da[1], a1[2] - ua * -da[2]))
	end
	return (false, a1)
end
"""
=#

@inline norm22(x) = dot(x,x) # l2 norm squared

# rotate a 2-vector in positive (ccw) direction by a quarter turn.
orthogonal(v::T) where {T<:StaticArray{Tuple{2}}} = T(v[2], -v[1])
orthogonal(v) = [v[2], -v[1]]

# N-1 dimensional plane
# dot(plane, normal) = d  #TODO change to dot(plane, normal) + d = 0
struct GGHyperPlane{N, T} #Polyhedra.jl defines hyperplane
  normal::Vec{N, T}
  d::T
end

#TODO should it work for non-Point?
# make a HyperPlane (line) from two points
function GGHyperPlane(point1::T, point2::T) where {T<:Point{2}}
  v = orthogonal(Vec(point2 - point1))
  d = dot(v, point1)
  return GGHyperPlane(v, d)
end

# make a plane from three points
function GGHyperPlane(point1::T, point2::T, point3::T) where {T<:Point{3}}
  v1 = Vec(point3 - point1)
  v2 = Vec(point2 - point1)
  v = cross(v1, v2)
  return GGHyperPlane(v, point1)
end

function GGHyperPlane(points::Vararg{T,N}) where {N, T<:Point{N}}  # N is number of points and dimensionality of points
  return hyperplane_from_points(collect(points))
end

# line = direction*t + point
struct Line{N,T}
  direction::Vec{N,T}
  point::Point{N,T}
end

# normal vector and point on the plane
function GGHyperPlane(normal::Vec{N}, point::Point{N}) where {N}
  GGHyperPlane(normal, dot(normal, point))
end

function intersection(plane::GGHyperPlane{N}, line::Line{N}, ::Type{Val{:line_parameter}}) where {N}
  # dot(p, plane.normal) = plane.d
  # p = t * line.direction + line.point
  # solve for t
  # returns 1/0 if line and plane are parallel and not complanar (no t is a solution)
  # returns 0 if they are coplanar (any t is a solution)

  # dot(t * line.direction + line.point, plane.normal) = plane.d
  # t * dot(line.direction, plane.normal) + dot(line.point, plane.normal) = plane.d
  den = dot(line.direction, plane.normal)
  num = (plane.d - dot(line.point, plane.normal))
  return if (num==0) num else num / den end
end

function intersection(plane::GGHyperPlane{N}, line::Line{N}) where {N}
  t = intersection(plane, line, Val{:line_parameter})
  return t * line.direction + line.point
end

struct Rot{T} <: AbstractAffineMap
  θ::T
end
import Base.zero
zero(::Type{Rot{T}}) where {T} = Rot(zero(T))

@fastmath function convert(::Type{LinearMap}, r::Rot)
  s = sin(r.θ)
  c = cos(r.θ)
  return LinearMap(SMatrix{2,2}(c, s, -s, c))
end

function convert(::Type{AffineMap}, l::LinearMap)
  return AffineMap(l.linear, zero(SVector{2}))
end

function convert(::Type{AbstractMatrix}, a::AffineMap)
  @warn "copied to Geometry.jl" maxlog=1
  (n1, n2) = size(a.linear)
  M = SMatrix{n1,n2}(a.linear)
  v = SMatrix{n1,1}(a.translation)
  return vcat(
    hcat(M, v),
    hcat(@SMatrix(zeros(Bool, 1, n2)), true))
end


module GGIntersects
using StaticArrays
function param_seg(seg, t)
    p = seg[1]
    v = seg[2] - seg[1]
    return p + t * v
end

function intersects_sys(seg1, seg2)
    p1 = seg1[1]
    p2 = seg2[1]
    v1 = seg1[2] - seg1[1]
    v2 = seg2[2] - seg2[1]

    # solve p1 + v1*t1 = p2 + v2*t2

    A = @SMatrix [
        v1[1]  -v2[1] ;
        v1[2]  -v2[2] ]
    b = SVector(p2 - p1)
    return (A,b)
end

function intersects_ts(seg1, seg2)
    (A, b) = intersects_sys(seg1, seg2)
    ts = A \ b
end

function intersects_ts_test(ts)
  return all(0.0 .≤  ts .≤ 1.0)
end
end

# overwrite the GeometryTypes.intersect, because it's broken: https://github.com/JuliaGeometry/GeometryTypes.jl/issues/133
function intersects(a::LineSegment{Point{N,T}}, b::LineSegment{Point{N,T}}) where {N,T}
  ts = GGIntersects.intersects_ts(a, b)
  return (GGIntersects.intersects_ts_test(ts), GGIntersects.param_seg(a, ts[1]))
end

function intersects(a::LineSegment{Point{N,T}}, b::LineSegment{Point{N,T}}, ::Type{Val{:check_exact_endpoints}}) where {N,T}
  # doesn't separately handle case where Segments are the same, or degenerate
  if a[1] == b[1] || a[1] == b[2]
    return (true, a[1])
  elseif a[2] == b[1] || a[2] == b[2]
    return (true, a[2])
  end
  return intersects(a,b)
end

function signed_distance(h::HalfSpace, point)
  return (dot(h.a, point) - h.β) / norm(h.a)
end

function isinside(h::HalfSpace, point)
  return (dot(h.a, point) - h.β) < 0
end

convert(::Type{HalfSpace}, h::GGHyperPlane) = HalfSpace(h.normal, h.d)
convert(::Type{GGHyperPlane}, h::HalfSpace) = HalfSpace(Vec(h.a...), h.β)

function signed_distance(h::GGHyperPlane, point)
  return signed_distance(convert(HalfSpace, h), point)
end

# hyperplane going through points
function hyperplane_from_points(points::AbstractVector{P}) where {P<:AbstractVector}
  T = eltype(P)
  m = length(points)
  n = length(points[1]) # assume all points are in the same dimension
  @assert (n == m)
  A = Array{T,2}(m, n+1)
  for i in 1:m
    A[i,1:end-1] = points[i]
  end
  A[:,end] = one(T)
  N = nullspace(A)  # SVD
  if size(N, 2) != 1
    error("not unique")
  end
  return GGHyperPlane(Vec(N[1:end-1, 1]...), -N[end, 1])
end


# lots of conversions below, as I learn about the Polyhedra API

is_shitty(bad) =length(nhalfspaces(bad)) == 0 && npoints(bad) > 0

function is_blank(poly::Polyhedron)
  return !(
      (hrepiscomputed(poly) && nallhalfspaces(poly) > 0) ||
      (vrepiscomputed(poly) && npoints(poly) > 0)
    )
end

function is_measure_zero(poly::Polyhedron)
  # any equality (non inequality) in the H representation will give the polygon measure 0.
  # if the H representation is blank, that should be interpreted as an empty polygon (not full)
  # until https://github.com/JuliaPolyhedra/Polyhedra.jl/issues/62 is resolved
  N = fulldim(poly)
  return is_blank(poly) || nhyperplanes(poly) > 0 || npoints(poly) < N
end

# return tuple (inp, outp) which are polyhedra in and out.
function cut(poly::Polyhedron, halfspace::HalfSpace, workaround_blank=true)
  # An empty HRepresentation means the Full Polyhedron
  # An HRepresentation that is impossible to satisfy means Empty Polyhedron
  # If `poly` has one of these, then both of the inh and ouh will also be empty, which is desired.

  # It seems that Polyhedra is not perfect yet at making the distinction between Empty and Full polys
  # so:
  if workaround_blank && is_blank(poly)
    return (deepcopy(poly), deepcopy(poly)) # return empty empty
  end
  inh = intersect(hrep(poly), aSimpleHRepresentation([halfspace]))
  outh = intersect(hrep(poly), aSimpleHRepresentation([-halfspace]))
  in_p = POLY(inh) # ERROR: removehredundancy! not implemented for Polyhedra.LiftedHRepresentation
  out_p = POLY(outh)

  removehredundancy!(in_p)
  removehredundancy!(out_p)
  removevredundancy!(in_p)
  removevredundancy!(out_p)

  @assert(!is_shitty(in_p))
  @assert(!is_shitty(out_p))

  return (in_p, out_p)
end

"compute polyA \\ polyB as a UnionOfConvex"
function difference(polyA::PT, polyB::PT) where {T, PT <: Polyhedron}
  if !overlaps(BoundingBox(polyA), BoundingBox(polyB))
    return UnionOfConvex([polyA])
  end

  result = PT[]
  remaining_piece = polyA
  hsBs = allhalfspaces(polyB)
  for hsB in hsBs
    (remaining_piece, outside_B_piece) = cut(remaining_piece, hsB)
    if !is_measure_zero(outside_B_piece)  # ignore empty pieces (for efficiency, not correctness)
      push!(result, outside_B_piece)
    end
    if is_measure_zero(remaining_piece)
      break # end early, for efficiency, not correctness
    end
  end
  return UnionOfConvex(result)
end

struct DifferenceGeneral{P, N}
  positive::P
  negative::N
end

struct Difference{T <: Polyhedron}
  positive::UnionOfConvex{T}
  negative::UnionOfConvex{T}
end

# get unionofconvex shapes
function explicit(d::Difference)
  positive_pieces = d.positive.pieces
  for negative_piece in d.negative.pieces
      new_positive_pieces = []  # TODO Any
      for positive_piece in positive_pieces
          append!(new_positive_pieces, difference(positive_piece, negative_piece).pieces)
      end
      positive_pieces = new_positive_pieces
  end
  return UnionOfConvex(positive_pieces)
end

function intersection(polyA::PT, polyB::PT) where {PT <: Polyhedron}
  #= optimization removed because I don't know how to represent empty polygon.
  if !overlaps(BoundingBox(polyA), BoundingBox(polyB))
    # TODO should be generic on Poly type
    # return a polygon like 0x + 0y = 1 , i.e. empty
    return SimplePolyhedron{N, T}(MixedMatHRep{N, T}(zeros(T, 1,2), ones(T, 1), IntSet([1])))
  end
  =#
  return polyA ∩ polyB
end

# polyA \  B -> UnionOfConvex
function difference(polyA::PT, B::Difference{PT}) where {PT <: Polyhedron}
  answer_positive = PT[]
  for positive_piece in B.positive.pieces
    append!(answer_positive, difference(polyA, positive_piece).pieces)  # could make a bunch of copies of polyA into answer_positive
  end

  for negative_piece in B.negative.pieces
    minus = intersection(polyA, negative_piece) # a piece in the shadow
    for positive_piece in B.positive.pieces
      minus_actual = intersection(positive_piece, minus) # efficiency not correctness, but avoids geometric growth
      if !is_measure_zero(minus_actual)  # efficiency not correctness
        push!(answer_positive, minus)
      end
    end
  end
  return UnionOfConvex(answer_positive)
end

# TODO should have UnionOfDifference ??? Or GenericUnion{T}
# also Difference{Convex}

# A \ B
function difference(A::UnionOfConvex, B)
  # assume B implements the iterator protocol.
  POLY = eltype(A.pieces)
  result = copy(A.pieces)

  for B_i in B
    new_result = POLY[]

    for p in result
      append!(new_result, difference(p, B_i).pieces)
    end
    result = new_result
    if isempty(result)
      break
    end
  end

  return UnionOfConvex(tighten(result))
end

function difference(A::Set{POINTS}, B) where {POINTS}
  # assume B implements the iterator protocol.
  result = A

  for B_i in B
    result = difference(result, B_i)

    if isempty(result)
      break
    end
  end

  return result
end

function difference(A::Set{POINTS}, B::Vector) where {POINTS}
  # just so we can iterate in reverse, which is more likely to cut away at A if B is a "visible region"
  result = A

  for i in length(B):-1:1
    result = difference(result, B[i])

    if isempty(result)
      break
    end
  end

  return result
end


# construct without needing to specifiy type params.
#= HRepIterator doesn't exist anymore.
function Polyhedra.SimplePolyhedron(it::Polyhedra.HRepIterator)
  T = eltype(it)
  return SimplePolyhedron{T.parameters...}(it)
end
=#


@inline homo(hp::GGHyperPlane) = Vec(hp.normal..., -hp.d)
@inline homo(hp::Polyhedra.HRepElement) = Vec(hp.a..., -hp.β)
@inline homo(hr::LiftedHRepresentation) = hr.A[]

#=
# seems to have since been defined in `Polyhedra`
# /Users/goretkin/.julia/packages/Polyhedra/KKyfQ/src/vecrep.jl:75
import Polyhedra.Intersection
Polyhedra.Intersection(l::Polyhedra.HRepresentation) = Polyhedra.Intersection(hyperplanes(l), halfspaces(l))
=#

import CoordinateTransformations.transform
# apply an affine transformation on HyperPlane
function transform(A::AbstractMatrix, hp::GGHyperPlane{N}) where {N}
  @assert size(A, 2) == N+1
  newh = A' * homo(hp)
  return GGHyperPlane(newh[1:end-1], -newh[end])
end

function transform(A::AbstractMatrix, hp::HT) where {HT<:Polyhedra.HRepElement}
  #TODO should promote
  newh = A' * homo(hp)
  return HT(newh[1:end-1], -newh[end])
end

function transform(A::AbstractMatrix, p::T) where {T<:LiftedHRepresentation}
  # see documentation of LiftedHRepresentation to see how this is stored.
  pA = p.A
  phb = pA[:,1]
  phA = pA[:,2:end] # thought this was negated, but empirically no.
  ph = [phA phb]
  nh = ph * inv(A)

  return T([nh[:,end] nh[:,1:end-1]], p.linset)
end

function transform(A::AbstractMatrix, p::T) where {T<:HRepresentation}
  return convert(T, transform(A, LiftedHRepresentation(p)))
end

function transform(AM::AffineMap, p::T) where {T<:Union{MixedMatHRep, Polyhedra.Polyhedron, Polyhedra.HRepElement, LiftedHRepresentation, GGHyperPlane, Polyhedra.Intersection}}
  return transform(convert(AbstractMatrix, AM), p)
end

function transform(AM::AffineMap, p::T) where {T<:MixedMatVRep}
  V = p.V * AM.linear' .+ AM.translation' # rows are the vectors
  R = p.R * AM.linear' .+ AM.translation'
  return T(V, R, p.Rlinset)
end

function transform(A::AbstractMatrix, p::T) where {T<:Polyhedra.Polyhedron}
  # TODO should transform either/both VRep and HRep
  @warn("don't use transform(AbstractMatrix, Polyhedron) anymore", once=true, key=:warnonce_transform_amp)
  return T(transform(A, hrep(p)))
end

function transform(AM::AffineMap, p::T) where {T<:Polyhedron}
  return T(
    hrepiscomputed(p) ? transform(AM, hrep(p)) : nothing,
    vrepiscomputed(p) ? transform(AM, vrep(p)) : nothing,
    p.solver)
end

"trigger creation of h-representation and v-representation"
function compute!(p::Polyhedra.Polyhedron)
  hrep(p)
  vrep(p)
  nothing
end

# sort points ccw
function convexhull_andrew(points)
  #from Polyhedra.jl/recipe.jl
  counterclockwise(p1, p2) = dot(cross([p1; 0], [p2; 0]), [0, 0, 1])

  points = sort!(points, by = x -> x[1]) # TODO  don't mutate arguments
  return [Polyhedra.getsemihull(points, 1, counterclockwise); Polyhedra.getsemihull(points, -1, counterclockwise)[2:end-1]]
end


struct Bounded{T, B}
  bounding_volume::B
  geometry::T
end

#=
BoundingBox(g::DifferenceGeneral) = BoundingBox(g.positive)

function make_aabb_bounded(geometry)
  return Bounded(BoundingBox(geometry), geometry)
end

isinside(x, y) = contains(x, y)

function contains(bounded_geometry::Bounded, needle)
  if !contains(bounded_geometry.bounding_volume, needle)
    return false
  end
  return contains(bounded_geometry.geometry, needle)
end
=#

function ordered_vertices(bb::BoundingBox{2})
  return [
    Point(bb.minimum[1], bb.minimum[2]),
    Point(bb.minimum[1], bb.maximum[2]),
    Point(bb.maximum[1], bb.maximum[2]),
    Point(bb.maximum[1], bb.minimum[2])
    ]
end

BoundingBox(u::Union{UnionOfConvex, UnionOfGeometry}) = reduce(union, map(BoundingBox, u.pieces))


"like Inf * x, but no NaN"
blowup(x::T) where {T} = x==zero(T) ? zero(T) : typemax(T) * x

function BoundingBox(p::Polyhedron)
  N = fulldim(p)
  T = Polyhedra.coefficient_type(p)
  # work around https://github.com/JuliaPolyhedra/Polyhedra.jl/issues/82 TODO update to latest version
  if is_measure_zero(p)
    return BoundingBox(fill(nan(T), N), fill(nan(T), N))
  end

  points_ = vcat(
    collect(map(r->blowup.(r.a), allrays(p))),
    collect(points(p))
    )
  if length(points_) == 0  # treat as empty polygon
    return BoundingBox(fill(nan(T), N), fill(nan(T), N))
  end
  return BoundingBox(points_)
end

function poly_from_lims(xlims, ylims)
  corners = let (x1, x2) = xlims, (y1, y2) = ylims
                [(x1,y1), (x1,y2), (x2,y1), (x2,y2)]
              end

  return polyhedron(vrep(array(corners)))
end

function convex_interior_refine_slow(vertices::AbstractVector{PT}, isclose, ishalfclose, center) where PT
  # roughly O(n^3 * k) algorithm, where k is length of output points
  # for every pair of vertices, if they're not isclose, then add the middle to the set of vertices. repeat on new vertices
  last_n = 1
  while true
    dobreak = true
    vertices_next = copy(vertices) # indices of `vertices` are valid for `vertices_next`
    n = length(vertices)
    for i=1:n, j=max(i+1, last_n):n  # iterate through all new pairs
      if !isclose(vertices[i], vertices[j])
        c = center(vertices[i], vertices[j])
        if all(!ishalfclose(vertices_next[i], c) for i=1:length(vertices_next)) # if not close to any existing points.
          push!(vertices_next, c)
          dobreak = false
        end
      end
    end
    vertices = vertices_next
    last_n = n
    @show n length(vertices_next)
    if dobreak break end
  end
  return vertices
end

using CDDLib

function fast_simple_poly_from_vertices(vertices)
  T = eltype(vertices)
  return convert(CDDLib.Polyhedron{T}, vrep(vertices))
end

function delaunay(vertices::AbstractVector{PT}, allow_less=false, allow_more=false, skip_error=false) where PT
  # assume length(PT), zero(PT), eltype(PT) is defined
  # convex hull of d-dim-points projected onto (d+1)-dim-paraboloid gives Delaunay triangulation
  vertices_lifted = [Point(p..., norm(p)^2) for p in vertices]
  face_me = Point(zero(PT)..., -one(eltype(PT)))
  D = length(face_me)
  println("make poly")
  hull = fast_simple_poly_from_vertices(array(vertices_lifted))
  fvc = facet_vertex_connectivity(hull, 1e5)

  if !allow_less && !allow_more
    delaunay_triangles = NTuple{3, Int64}[]
  else
    delaunay_triangles = (NTuple{N, Int64} where N)[]
  end

  for face_i in find(facet_visibility_mask(hull, face_me))
    tri = fvc[face_i]
    if (!allow_less && length(tri) <  D) || (!allow_more && length(tri) >  D)
      # assume points are arranged so that there is one unique Delaunay triangulation, and non-subspace
      if !skip_error
        error("delaunay failed: $(tri) has length $(length(tri)). So far: $(delaunay_triangles)")
      end
    else
      push!(delaunay_triangles, tuple(tri...))
    end
  end
  return delaunay_triangles
end

function convex_interior_refine(vertices::AbstractVector{PT}, max_distance, new_only=false) where PT
  # TODO not done
  if length(vertices) == 1
    return vertices
  end

  if length(vertices) == 2
    d = vertices[2] - vertices[1]
    l = norm(d)
    n = Int(ceil(l / max_distance))
    # ensure both endpoints appear exactly
    inbetween = [(1 - (k/n)) * vertices[1] +  (k/n) * vertices[2] for k in 1:(n-1)]
    if new_only
      return inbetween
    end
    return vcat(vertices, inbetween)
  end
end

struct ArcRegion{N, T} # TODO should split up transformable parts and parameters
  radius::T
  cos_half_fov::T # cos of half of the fov angle
  center::Point{N, T}
  direction::Vec{N, T} # assume normalized
end

import GeometryTypes.isinside

@inline function isinside(arc::ArcRegion{N,T}, point::Point{N,T}) where {N, T}
  ray = point - arc.center
  d = norm(ray)
  # avoid NaNs
  return d <= arc.radius && ( d == zero(T) || (dot(normalize(ray), arc.direction) >= arc.cos_half_fov))
end

function transform(AM::AffineMap, arc::ArcRegion)
  return ArcRegion(arc.radius, arc.cos_half_fov, AM(arc.center), AM.linear * arc.direction) # make sure direction doesn't translate. TODO GeometryTypes and CoordinateTransformations should play nicely
end


# to boundary
function signed_distance(poly::Polyhedra.Polyhedron, point)
    #TODO don't ignore hyperplanes
    return maximum(signed_distance(hs, point) for hs in halfspaces(poly))
end

function isinside(poly::Polyhedra.Polyhedron, point)
    #TODO don't ignore hyperplanes
    return all(isinside(hs, point) for hs in halfspaces(poly))
end


@inline function isinside(dog::Union{Difference, DifferenceGeneral}, point)
  #return isinside(dog.positive, point) && !(any(isinside(shadow, point) for shadow in dog.negative.pieces))
  if !isinside(dog.positive,point)
      return false
  end
  for shadow in dog.negative.pieces
    if isinside(shadow, point)
      return false
    end
  end
  return true
end

GEOMETRY = Union{DifferenceGeneral, Difference, Polyhedra.Polyhedron, Polyhedra.HRepElement}
function difference(points::Set{POINT}, geometry::GEOMETRY) where {POINT}
  # Profiling reveals that generator with filter was spending all of its time in "advance_filter" of iterators.jl
  s = Set{POINT}()
  for p in points
    if !isinside(geometry, p)
      push!(s, p)
    end
  end
  return s
end

half_fov(arc::ArcRegion) = acos(arc.cos_half_fov)

function explicitcontour(arc::ArcRegion{2, T}, n) where {T}
  p = [tuple(((convert(LinearMap, Rot(θ)))(arc.radius * arc.direction) + arc.center)...)
    for θ in range(-half_fov(arc), stop=+half_fov(arc), length=n)]

  if arc.cos_half_fov > -1
    prepend!(p, [tuple(arc.center...)])
    push!(p, tuple(arc.center...))
  else # if it's a full circle, don't go back to center.
    push!(p, first(p))
  end
  return p
end

function explicit(arc::ArcRegion{2, T}, n) where {T}
  p = [(convert(LinearMap, Rot(θ)))(arc.radius * arc.direction) + arc.center
    for θ in range(-half_fov(arc), stop=+half_fov(arc), length=n)]

  union = [polyhedron(vrep(array([p[i], p[i+1], arc.center]))) for i=1:(n-1)]
  return UnionOfConvex(union)
end

function explicit(diffarc::DifferenceGeneral{ARC, UnionOfConvex{POLY}}, n) where {ARC<:ArcRegion, POLY<:Polyhedra.Polyhedron}
  positive = explicit(diffarc.positive, n)
  return explicit(Difference(positive, diffarc.negative))
end

"make BoundingBox from ArcRegion"
function BoundingBox(ar::ArcRegion{N,T}) where {N, T}
  # TODO make it tighter (if needed)
  BoundingBox(ar.center .- ar.radius, ar.center .+ ar.radius)
end

function transform(AM::AffineMap, ps::Set{P}) where {P}
  return Set([AM(p) for p in ps])
end

#=
@inline function intersects(poly::POLY, segment::LineSegment{Point{N, T}}) where {POLY<:Polyhedra.Polyhedron, N, T} # TODO make poly and segment share N
  # the segment does not intersect iff both points of line segment are outside the same halfplane, for any given halfplane on the poly
  return !(any(
    all(signed_distance(h, point) <= 0.0 for point in segment) for h in hreps(poly)
  ))
end
=#

@inline function cellstatus(poly::POLY, box::BoundingBox{N, T}) where {POLY<:Polyhedra.Polyhedron, N, T} # TODO make poly and segment share N
  if all(isinside(poly, point) for point in vertices(box))
    return :full
  end
  # if all points of a convex shape are outside any one poly halfspace, then the convex shape does not intersect the poly
  if any(
    all(signed_distance(h, point) >= 0.0 for point in vertices(box)) for h in allhalfspaces(poly)
  )
    return :empty
  end
  return :mixed
end

@inline function contains(arc::ArcRegion{2, T}, segment::LineSegment{Point{2, T}}) where {T}
  if !all(isinside(arc, endpoint) for endpoint in segment)
    return false
  end
  # I tried to think of how to make this generalize to other dimensions, but no luck.
  exists, point = intersects(
    LineSegment(arc.center, arc.center - arc.direction),
    segment)

  if exists
    return false
  end
  return true
end

@inline contains(arc::ArcRegion{N,T}, point::GeometryTypes.Point{N,T}) where {N, T}= isinside(arc, point)

@inline function contains(ball::HyperSphere{N, T}, p::Point{N, T}) where {N, T}
  return norm(p - ball.center) ≤ ball.r
end

@inline function intersects(box::BoundingBox{2, T}, ball::HyperSphere{2, T}) where {T}
  if contains(box, origin(ball))
    return true
  end

  if any([contains(ball, Point(p)) for p in vertices(box)])
    return true
  end

  v = ordered_vertices(box)
  v = [v..., v[1]]
  for i = 1:4
    seg = LineSegment(Point(v[i]), Point(v[i+1]))
    l = norm(findnearest(seg, ball.center) - ball.center)
    if l ≤ ball.r
      return true
    end
  end
  return false
end


@inline function intersects(sector::ArcRegion{N, T}, box::BoundingBox{N, T}) where {N, T}
  if !intersects(box, HyperSphere(sector.center, sector.radius))
    return false
  end
  # check if box exists within the carved out region of ArcRegion
  rays = [v - sector.center for v in vertices(box)] # TODO vertices(box) .- sector.center should have worked
  # TODO want dot.(rays, sector.direction) to work
  return !(all([dot(ray, sector.direction) for ray in normalize.(rays)] .< sector.cos_half_fov))
end

@inline function cellstatus(sector::ArcRegion{N, T}, box::BoundingBox{N, T}) where {N, T}
  if !intersects(sector, box)
    return :empty
  end

  # TODO some computation below is repeated inside the intersects call above
  if all([contains(sector, Point(p)) for p in vertices(box)])
    v = ordered_vertices(box)
    v = [v..., v[1]]
    if all([contains(sector, LineSegment(Point(v[i]), Point(v[i+1]))) for i = 1:4])
      return :full
    end
  end

  return :mixed
end

#= # TODO try to get GJK working with spheres
import GeometryTypes.any_inside
import GeometryTypes.support_vector_max
any_inside(s::HyperSphere) = origin(s)
function support_vector_max(s::HyperSphere{N, T}, d::Vec{N, T}) where {N,T}
  n = norm(d)
  if n > 0 || true
    s.r/n * d
  else
    return normalize(Vec(ones(N)...)) # arbitrary? GeometryTypes.gjk0 calles with direction = -0.0
  end
end

@inline function intersects(box::BoundingBox{N, T}, ball::HyperSphere{N, T}) where {N, T}
  ball_cost = ball.r
  ballc = ball.center
  boxc = Point(center(box))
  D = Vec(ballc - boxc)
  d = norm(D)
  s = findsupport(box, D, boxc)
  box_cost = d * s
  xs = [boxc[1], (s*D + boxc)[1]]
  ys = [boxc[2], (s*D + boxc)[2]]


  objs = filter(x -> x[:get_label]() == "_specialme", ax[:lines])
  if length(objs) > 1
    objs[1][:set_data](xs, ys)
  else
    ax[:plot](xs, ys, "k-", label="_specialme")
  end

  budget = d
  @show box_cost ball_cost budget
  return (budget - ball_cost - box_cost) < 0
end
=#


"""
find nearest point on segment
"""
function findnearest(segment::LineSegment{Point{N, T}}, query::Point{N, T}) where {N, T}
  t = findnearestline(segment..., query)
  tc = clamp(t, 0, 1)
  return lerp(segment, tc)
end


function lerp(segment::LineSegment{Point{N, T}}, t) where {N, T}
  (p1, p2) = segment
  return ((1-t) * p1 + t * p2)
end

"""
given line that goes through p1 and p2, defined parametrically as
l(t) = ((1-t) * p1 + t * p2)
find t such that
norm(l(t) - pq) is minimized
or equivalently
(l(t) - pq)' * (p2 - p1) = 0
"""
function findnearestline(p1::Point{N,T}, p2::Point{N,T}, pq::Point{N,T}) where {N, T}
  d = p2 - p1
  t̄ = ((pq - p1)' * d) / (d' * d)
  return t̄
end

function distance(segment::LineSegment{POINT}, point::POINT) where {N, T, POINT<:Point{N, T}}
  norm(findnearest(segment, point) - point)
end

function distance(seg1::LS, seg2::LS) where {N, T, LS <:LineSegment{Point{N, T}}}
  (exists, point) = intersects(seg1, seg2, Val{:check_exact_endpoints})
  if exists
    return zero(T)
  end
  minimum([distance(seg1, seg2[1]), distance(seg1, seg2[2]), distance(seg2, seg1[1]), distance(seg2, seg1[2])])
end

#=
"""support function in the GJK sense, but return point exactly in the direction v"""
@inline function findsupport(box::BoundingBox{N, T}, v::Vec{N, T}, center::Point{N, T}) where {N, T}
  # scale v so that it is up against the box.
  # v starts at center of box

  ma = box.maximum - center
  mi = box.minimum - center

  s = typemax(T)
  for i = 1:N
    maii = ifelse(v[i] > 0, ma[i], mi[i])
    si = maii / v[i]
    if isnan(si) continue end
    s = min(s, si)
  end
  return s
end

@inline function support(box::BoundingBox{N, T}, v::Vec{N, T}) where {N, T}
  v * findsupport(box, v, center(box))
end
=#
import Base.show
# I'm tired of accidentally printing hundreds of screenfulls.
show(io::IO, uoc::UnionOfConvex{<:Polyhedra.Polyhedron}) = print(io, "Union of $(length(uoc.pieces)) Convex Polyhedra")

# show it how you build it
show(io::IO, point::Point) = print(io, "Point($(join(map(string, point), ", ")))")

"""
return parameter(s) for line which intersect with circle
"""
function find_intersection(line::LineSegment, circle::GeometryTypes.Circle)
  t = findnearestline(line..., circle.center)
  p_n = lerp(line, t)
  d = norm(circle.center - p_n)

  diff_of_sq(a, b) = (a - b) * (a + b) # a^2 - b^2

  if d > circle.r
    return ()
  elseif d == circle.r
    return (t, )
  else
    D = sqrt(diff_of_sq(circle.r, d))
    scale = norm(line[2] - line[1])
    Δt = D / scale
    return (t + Δt, t - Δt)
  end
end

#cross(a::Vec{2}, b::Vec{2}) = (a[1] * b[2]) -  (a[2] * b[1])

function in_circular_order(dir1::Vec{2}, dir2::Vec{2}, dir3::Vec{2})
  cross(dir1, dir2) * cross(dir2, dir3)
end

struct ArcEndpoint{T}
  direction::Int8 # +1 or -1
  unit::Vec{2,T}
end

ArcEndpoint(direction::Signed, unit) = ArcEndpoint(Int8(direction), unit)

function ArcRegion(e1::ArcEndpoint, e2::ArcEndpoint)
  @assert e1.direction * e2.direction < 0
  unit_nosign = normalize(e1.unit + e2.unit)
  s1 = cross(e1.unit, unit_nosign) * e1.direction > 0
  s2 = cross(e2.unit, unit_nosign) * e2.direction > 0
  @assert s1 == s2
  unit = (if s1 1 else -1 end) * unit_nosign
  T1 = one(eltype(unit))
  T0 = zero(eltype(unit))
  cos_half_fov = dot(e1.unit, unit)
  return ArcRegion(T1, cos_half_fov, Point{2}(T0, T0), unit)
end

function rot_mat2(c, s)
  @SMatrix[c -s ; s c]
end


function get_endpoints(ar::ArcRegion{2})
  y = sqrt(1^2 - ar.cos_half_fov^2)
  unit1 = Vec{2}(ar.cos_half_fov, +y)
  unit2 = Vec{2}(ar.cos_half_fov, -y)
  rot = rot_mat2(ar.direction...)
  return (
    ArcEndpoint(+1, rot * unit2),
    ArcEndpoint(-1, rot * unit1)
  )
end

#=
function Base.intersect(a1::ArcRegion{2}, a2::ArcRegion{2})

end
=#
