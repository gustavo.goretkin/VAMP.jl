#=
module VAMP_Pieces



=#

export SO2_Discretization, get_Δθ, angle_to_step, step_to_angle, angle_p_step, on_grid,
ConfigurationDomination, make_vamp_pieces, make_move_heuristic, VAMPSearchState, VAMPSearchStateExplore, move_search!

import Parameters: @pack!, @unpack, @with_kw
using VAMP


mutable struct MarkTime{T}
  t_last::Union{T, Nothing}
  t_first::Union{T, Nothing}
end


function mark!(mt::MarkTime, t)
  mt.t_last = t
  if mt.t_first == nothing
    mt.t_first = t
  end
end


function report_search!(search_summaries, this_time, search_alg; interval=1.0, header="")
  suppress_output = false
  do_print = (!suppress_output || interval <= 0)
  if length(search_summaries) == 0 || this_time - search_summaries[end].time > interval
    summary = VAMP.make_a_star_summary(search_alg)
    if do_print; print(header) end
    if do_print; println(summary) end
    if length(search_summaries) > 0
      d = summary.closed_size - search_summaries[end].summary.closed_size
      if do_print; println("New Closed: $(d)") end
    end

    push!(search_summaries, (time=this_time, summary=summary))
    if length(search_summaries) > 0
      runtime = this_time - first(search_summaries).time
      if do_print; println("Runtime: $(@sprintf("%.2f", runtime))") end
    end
    sleep(0.001)
    return true
  end
  return false
end


struct VAMPSearchState{MP, V}
  q::MP
  q_path::Vector{MP}
  visible_region::V
end

struct VAMPSearchStateExplore{MP, V, NTS}
  q::MP
  q_path::Vector{MP}
  visible_region::V
  need_to_see::NTS
end


function move_search!(move_alg, start_state; move_search_timeout=10^6)
  setup!(move_alg, start_state, 0.0)
  search_summaries = []
  while true
    this_time = time()
    report_search!(search_summaries, this_time, move_alg; header="\n\nmove\n")
    if !isempty(move_alg.goal_node) break end
    if !step!(move_alg); break end

    move_search_clock_time = this_time - first(search_summaries).time

    if move_search_clock_time > move_search_timeout; @warn("move_search! timeout"); break end
  end

  report_search!(search_summaries, time(), move_alg; interval=-1.0, header="move search has terminated\n")
  return (search_summaries=search_summaries,)
end


#==#
@with_kw struct SO2_Discretization{TL}
  Δx::TL
  Δy::TL = Δx
  θ_steps::Int64
end

println(SO2_Discretization)



get_Δθ(::Type{Val{d}}) where {d} = 2π/d.θ_steps
get_Δθ(d::SO2_Discretization) = 2π/d.θ_steps
angle_to_step(d::SO2_Discretization, θ) = Int(round(θ/(get_Δθ(d))))
step_to_angle(d::SO2_Discretization, step) = get_Δθ(d) * (step % d.θ_steps) # makes all floating point angles canonical
angle_p_step(d::SO2_Discretization, θ, Δsteps) = step_to_angle(d, angle_to_step(d, θ) + Δsteps)


function on_grid(d::SO2_Discretization, q)
    all(q.e.q .% [d.Δx, d.Δy] .== 0) && (q.θ.q.θ % get_Δθ(d) == 0)
end

#==#

struct ConfigurationDomination{SET} <: VAMP.AbstractDomination
  closed::SET
end

import VAMP.domination_add!
import VAMP.is_dominated
import Base.empty!

empty!(d::ConfigurationDomination) = empty!(d.closed)

function domination_add!(d::ConfigurationDomination, search_state)
  if search_state.q ∈ d.closed
    #println("errrr")
    #@show search_state.q
  end
  push!(d.closed, search_state.q)
end

function is_dominated(d::ConfigurationDomination, search_state)
  if search_state.q ∈ d.closed
    return (true, nothing)
  end
  return (false, nothing)
end

#==#

"represent the region seen by a robot when traversing q_path"
struct CoveragePath{T, V}
  q_path::T
  visibility::V # maps q-> visible_region
end

struct CoverageGoal{T}
    region::T
end

function VAMP.difference(points::Set{POINTS}, cp::CoveragePath) where {POINTS}
  result = points
  for q in cp.q_path
    visible_region = cp.visibility(q)
    result = difference(result, visible_region)
    if length(result) == 0; break end
  end
  return result
end

#==#

function make_move_heuristic(q_goal, parameters, ::Type{SearchState_t}) where {SearchState_t}
  # do uniform cost search starting from the motion planning goal.
  # this effectively solves all-source-single-goal shortest paths

  @unpack discretization, SE2_MPState_t, mp = parameters
  d = discretization

  ai = LazyAStarInstance(
    (s_to, s_fro) -> distance(s_to, s_fro),
    (s) -> 0, # do uniform cost search
    (s) -> false, # no goal
    function (set, s_fro)
      x, y = s_fro.e.q
      θ = s_fro.θ.q.θ
      q_new = SE2_MPState_t(x, y, angle_p_step(d, θ, +1)); push!(set, q_new)
      q_new = SE2_MPState_t(x, y, angle_p_step(d, θ, -1)); push!(set, q_new)
      q_new = SE2_MPState_t(x, y + d.Δy, θ);    push!(set, q_new)
      q_new = SE2_MPState_t(x, y - d.Δy, θ);    push!(set, q_new)
      q_new = SE2_MPState_t(x + d.Δx, y, θ);    push!(set, q_new)
      q_new = SE2_MPState_t(x - d.Δx, y, θ);    push!(set, q_new)
      nothing
    end,
    (s_to, s_fro) -> mp.q_valid_test(s_to)  # assuming the Δs above are tiny, don't check the whole path.
  )

  alg = LazyAStarAlgorithm(ai, SE2_MPState_t{Float64}, Float64)

  setup!(alg, q_goal, 0.0)
  function heuristic(s::SearchState_t)
    if !mp.q_valid_test(s.q)
        return Inf
    end
    # TODO if s is not reachable from mp.goal, then computation below won't terminate.

    n = length(alg.closed_parents)
    #println("in heuristic: $n")

    # round quantities to the search lattice, in case the heuristic is getting called by something other than A*
    Δxy = SVector(d.Δx, d.Δy)
    q = SE2_MPState_t(
      Δxy .* (round.(s.q.e.q ./ Δxy))...,
      step_to_angle(d, angle_to_step(d, s.q.θ.q.θ))
    )

    while !haskey(alg.closed_parents, q)
      step!(alg)
    end
    m = length(alg.closed_parents)
    (parent, move_cost) = alg.closed_parents[q]

    move_component = (1 + 1e-8) * move_cost
    h = move_component
    return h
  end

  return heuristic
end



function make_vamp_pieces(parameters)
    vamp_pieces = Dict{Symbol,Any}()

    @unpack visibility_instance = parameters
    # const ViewVolume = DifferenceGeneral{}
    # vcache = Dict{SE2_MPState_t, ViewVolume}()
    vcache = Dict{Any, Any}()

    function make_get_visible_region_cached__Fworld(vcache, visibility_instance)
      function get_visible_region_cached__Fworld(q_robot)
        if !haskey(vcache, q_robot)
          visible_region = VAMP.get_visible_region__Fworld(visibility_instance, q_robot)
          vcache[q_robot] = VAMP.make_packed_view_volume(UnionOfGeometry([visible_region]))
        end
        return vcache[q_robot]
      end
      return get_visible_region_cached__Fworld
    end

    get_visible_region_cached__Fworld = make_get_visible_region_cached__Fworld(vcache, visibility_instance)
    @pack! vamp_pieces = get_visible_region_cached__Fworld

    @unpack polys_obstacles__Fworld = parameters
    @unpack workspace_points_raster_spec__Fworld = parameters

    @setparam parameters polys_robot__Frobot = nothing      # only dependency is plotting
    @setparam parameters poly_frustum__Fcamera = nothing    # vi
    @setparam parameters is_collision_free = nothing        # mp
    @setparam parameters workspace_points__Fworld = nothing # two phase see something new definition
    @setparam parameters boundingbox_workspace__Fworld = nothing
    @setparam parameters workspace_points_raster_spec__Fworld = nothing # execute move if it sees something new workspace_points_raster_spec__Fworld
    # higher resolution for making sure we don't prune poorly.
    @setparam parameters prune_test_raster_spec__Fworld = VAMP.RasterVolumeSpec(workspace_points_raster_spec__Fworld.limits, 1 .* workspace_points_raster_spec__Fworld.size)



    @unpack discretization = parameters
    @unpack mp = parameters # motion planning problem

    @unpack SE2_MPState_t = parameters
    @unpack SearchState_t = parameters
    @unpack SearchStateExplore_t = parameters


    move_vis_successors = let d = discretization
        function move_vis_successors(set, s_fro)
          x, y = s_fro.q.e.q
          θ = s_fro.q.θ.q.θ
          q_news = (
            SE2_MPState_t(x, y, angle_p_step(d, θ, +1)),
            SE2_MPState_t(x, y, angle_p_step(d, θ, -1)),
            SE2_MPState_t(x, y + d.Δy, θ),
            SE2_MPState_t(x, y - d.Δy, θ),
            SE2_MPState_t(x + d.Δx, y, θ),
            SE2_MPState_t(x - d.Δx, y, θ),
          )

          for q_new in q_news
            if !(q_new in s_fro.q_path)
              try
                push!(set, SearchState_t(q_new, vcat(s_fro.q_path, q_new), vcat(s_fro.visible_region, get_visible_region_cached__Fworld(q_new)) ))
              catch err
                if isa(err, VAMP.ShadowEyeCollision)
                  # don't add this, since it's in collision (and we didn't detect due to being lazy w.r.t. collision checks)
                  nothing
                else
                  rethrow(err)
                end
              end
            else
              push!(set, SearchState_t(q_new, vcat(s_fro.q_path, q_new), s_fro.visible_region))
            end
          end
          nothing
        end
    end

    @pack! vamp_pieces = move_vis_successors



    explore_vis_successors = let d = discretization
      function explore_vis_successors(set, s_fro)
        x, y = s_fro.q.e.q
        θ = s_fro.q.θ.q.θ
        q_news = (
          SE2_MPState_t(x, y, angle_p_step(d, θ, +1)),
          SE2_MPState_t(x, y, angle_p_step(d, θ, -1)),
          SE2_MPState_t(x, y + d.Δy, θ),
          SE2_MPState_t(x, y - d.Δy, θ),
          SE2_MPState_t(x + d.Δx, y, θ),
          SE2_MPState_t(x - d.Δx, y, θ),
        )

        for q_new in q_news
          if !(q_new in s_fro.q_path)
            new_visibility = get_visible_region_cached__Fworld(q_new)
            new_need_to_see = difference(s_fro.need_to_see, new_visibility)

            push!(set, SearchStateExplore_t(
              q_new,
              vcat(s_fro.q_path, q_new),
              vcat(s_fro.visible_region, new_visibility),
              new_need_to_see))
          else
            push!(set, SearchStateExplore_t(
              q_new,
              vcat(s_fro.q_path, q_new),
              s_fro.visible_region,
              s_fro.need_to_see))
            end
        end
        nothing
      end
    end

    @pack! vamp_pieces = explore_vis_successors



    vsafe_violations = function vsafe_violations(s_to, s_fro)
      # s_to.q is seen by s_fro.q_path
      if s_to.q in s_fro.q_path
        return 0
      end
      uncovered_pieces__Fworld = VAMP.get_uncovered_pieces__Fworld(visibility_instance, s_to.q, s_fro.visible_region)
      n = length(uncovered_pieces__Fworld)
      #=
      if n > 0
        println("vsafe returned: $n")
      end
      =#
      return n
    end
    @pack! vamp_pieces = vsafe_violations

    vsafe_test = function vsafe_test(s_to, s_fro)
      if !mp.q_valid_test(s_to.q) # assuming the Δs above are tiny, don't check the whole path.
        return false
      end

      return vsafe_violations(s_to, s_fro) == 0
    end
    @pack! vamp_pieces = vsafe_test



    vsafe_split_path = function vsafe_split_path(path)
      if length(path) == 0
        return (path[[]], path[[]])
      end
      safe_path = [path[1]]

      for i = 2:length(path)
        if vsafe_test(path[i], path[i-1])
          push!(safe_path, path[i])
        else
          break
        end
      end
      # if path is 1-2-3-4-5-6
      # and vsafe_test fails at 4-5
      # then safe_path is 1-2-3-4
      # and unsafe_path is 4-5-6
      # 4 is repeated twice
      # this ensures that visibility_violations_path(path) == visibility_violations_path(unsafe_path)
      unsafe_path = path[(length(safe_path)):end]
      return (safe_path, unsafe_path)
    end
    @pack! vamp_pieces = vsafe_split_path


    make_move_alg = function make_move_alg(mp #=TODO not used=#, configuration_goal, transition_cost, successors, edge_test, parameters;
      SearchState_t_=SearchState_t,
      cost_t=Float64,
      domination=ConfigurationDomination(Set{SE2_MPState_t{Float64}}()),
      heuristic=make_move_heuristic(rand(configuration_goal), parameters, SearchState_t_)
      )

      move_ai = LazyAStarInstance(
        transition_cost,
        heuristic,
        (s) -> s.q ∈ configuration_goal,
        successors,
        edge_test
      )
      move_alg = LazyAStarAlgorithm(move_ai, SearchState_t_, cost_t, domination)
      return move_alg
    end
    @pack! vamp_pieces = make_move_alg



    make_explore_alg = function make_explore_alg(mp #=TODO not used=#, coverage_goal, transition_cost, successors, edge_test;
      SearchState_t=SearchStateExplore_t,
      cost_t=Float64,
      domination=ConfigurationDomination(Set{SE2_MPState_t{Float64}}()),
      heuristic=(function () # try to make a closure so that explore_alg.instance.heuristic.h_view exists
        h_view = VAMP.make_coverage_heuristic(
          coverage_goal, polys_obstacles__Fworld, workspace_points_raster_spec__Fworld)
        function h(s)
          return 50.0 * h_view(get_visible_region_cached__Fworld(s.q))
        end
        return h
      end)(),
      improved_solution=(goal_test_closure)->nothing
      )

      s_that_saw_the_most = nothing
      s_that_saw_the_most_remaining = length(coverage_goal)

      explore_ai = LazyAStarInstance(
        transition_cost,
        #(s) -> 0.0,
        heuristic,
        #(s) -> length(s.need_to_see),
        function goal_test(s)
          if length(s.need_to_see) < s_that_saw_the_most_remaining
              s_that_saw_the_most = s
              s_that_saw_the_most_remaining = length(s.need_to_see)
              improved_solution(goal_test)
          end
          return length(s.need_to_see) == 0 # in general, search may be terminated before this goal
        end,
        successors,
        edge_test
      )
      explore_alg = LazyAStarAlgorithm(explore_ai, SearchState_t, Float64, domination)
      return explore_alg
    end
    @pack! vamp_pieces = make_explore_alg



    explore_search = function explore_search(start_state, coverage_goal, prohibited=coverage_goal, relaxtion::Type{Val{RELAX}}=Val{false};
      timeouts=(improvement=15.0, failure=1e9), log_header="explore\n", safe_preference=0.001) where {RELAX}
      mark_time = MarkTime{Float64}(nothing, nothing)

      mark_improvement(goal_test_closure) = begin
        #println("coverage increased");
        #@show length(goal_test_closure.s_that_saw_the_most.contents.need_to_see);
        mark!(mark_time, time());
      end

      prohibited__Fworld = [p for p in prohibited]
      robot_outer_radius = parameters[:robot_width]/2 * sqrt(2)
      function coverage_collision(q)
        # make bounding volume for robot.
        robot_center__Fworld = q.e.q
        if all(norm.(prohibited__Fworld .- [robot_center__Fworld]) .> robot_outer_radius)
          return false # outside bounding circle of robot.
        end

        # TODO maybe move points, not polygon.
        polyr = VAMP.get_robot__Fworld(parameters[:polys_robot__Frobot], q).pieces[1]
        for p in prohibited__Fworld
          if VAMP.signed_distance(polyr, p) < 100eps()
            return true
          end
        end
        return false
      end

      if !RELAX
        explore_alg = make_explore_alg(
          mp,
          coverage_goal,
          (s_to, s_fro) -> distance(s_to.q, s_fro.q),
          explore_vis_successors,
          vsafe_test,
          improved_solution=mark_improvement
        )
      else
        explore_alg = make_explore_alg(
          mp,
          coverage_goal,
          (s_to, s_fro) -> distance(s_to.q, s_fro.q) + safe_preference * vsafe_violations(s_to, s_fro),
          explore_vis_successors,
          (s_to, s_fro) -> mp.q_valid_test(s_to.q) && !coverage_collision(s_to.q) , # motions are not constrained to visible region alone.,
          improved_solution=mark_improvement
        )
      end
      explore_search_result = explore_search!(explore_alg, start_state, coverage_goal, mark_time, timeouts; log_header=log_header)
      return (explore_alg=explore_alg, relax=RELAX, explore_search_result=explore_search_result)
    end
    @pack! vamp_pieces = explore_search


    explore_search! = function explore_search!(explore_alg, start_state, coverage_goal, improvement_time, timeouts; log_header="explore\n")
      println("explore_search!")
      @show length(coverage_goal)
      println("\n")
      # lift the start state to the exploration search space (add coverage)
      start_state_explore = SearchStateExplore_t(start_state.q, start_state.q_path, start_state.visible_region, coverage_goal)
      setup!(explore_alg, start_state_explore, 0.0)

      explore_status = nothing
      search_summaries = []
      last_n_goal = nothing
      while true
        this_time = time()
        if report_search!(search_summaries, this_time, explore_alg; header=log_header)
          if last_n_goal != explore_alg.instance.goal_test.s_that_saw_the_most_remaining
            last_n_goal = explore_alg.instance.goal_test.s_that_saw_the_most_remaining
            println("Goal improved: $(last_n_goal)")
          else
            #println("Goal no improve.")
          end
        end
        #global explore_trace
        #peekit = DataStructures.peek(explore_alg.agenda)
        #push!(explore_trace, (peekit.first.state.q_path, peekit.second))

        if !isempty(explore_alg.goal_node); println("have goal"); break end
        if !step!(explore_alg); println("step says we're done, with no goal"); explore_status = :explore_fail; break end


        run_time = this_time - first(search_summaries).time
        # time-out is bigger if we've been working harder
        if improvement_time.t_last != nothing && time() - improvement_time.t_last > max(timeouts.improvement, .10 * run_time)
          println("\n\nexplore improvement timeout:")
          best_state = explore_alg.instance.goal_test.s_that_saw_the_most.contents
          @show length(best_state.need_to_see)
          explore_status = :explore_some_success
          break
        end

        if improvement_time.t_last == nothing && time() -  first(search_summaries).time > timeouts.failure
          println("\n\nexplore timeout, seen nothing.")
          explore_status = :explore_fail
          break
        end

        if !isempty(explore_alg.goal_node)
          explore_status = :explore_full_success
        end
      end
      report_search!(search_summaries, time(), explore_alg; interval=-1.0, header="explore search has terminated\n")
      return (
        search_summaries=search_summaries,
        explore_status=explore_status,
        #alg=strip_search_alg(explore_alg),
        coverage_goal=coverage_goal)
    end
    @pack! vamp_pieces = explore_search!


    process_move_path = function process_move_path(move_alg, starting_state; prune=true)
      move_path = reverse(follow_path(move_alg.closed_parents, move_alg.goal_node[1]))
      move_path_prefix, move_path_unsafe = vsafe_split_path(move_path)

      if length(move_path_unsafe) > 0
        println("move path has v-violations")
      end

      pruning_info = nothing
      move_path_to_add = SearchState_t[]

      if length(move_path_prefix) == length(move_path)
        move_path_to_add = move_path_prefix # it gets to the goal. add the whole path
      elseif prune
        # did not reach the goal.
        # compute the prefix of the path that sees something new.
        # As a quick proxy, if it "stands somewhere new"
        i_novel = VAMP.find_novel_prefix(unlift_q(move_path_prefix), Set(starting_state.q_path))

        if length(i_novel) < length(move_path_prefix)
          println("shortcutted move path that visits no new configurations.")
        end
        move_path_prefix_novel = move_path_prefix[i_novel]

        # Now slower check based on visibility.
        ref_pvv = starting_state.visible_region
        move_path_visibility = map(get_visible_region_cached__Fworld, unlift_q(move_path_prefix_novel));

        i_novel_vis = VAMP.find_novel_prefix_visibility(move_path_visibility, ref_pvv, workspace_points_raster_spec__Fworld)

        if length(i_novel_vis) < length(move_path_prefix_novel)
          println("shortcutted move path that achieves no new visibility")
        end

        move_path_to_add = move_path_prefix_novel[i_novel_vis]

        pruning_info = (
          i_novel_q=i_novel,
          i_novel_vis=i_novel_vis
        )
      end

      @assert(move_path_to_add != 1)

      return (
        move_path_to_add=move_path_to_add,
        move_path_prefix=move_path_prefix,
        move_path_unsafe=move_path_unsafe,
        move_path=move_path,
        metadata=(
          reaches_goal=(length(move_path) == length(move_path_to_add)),
          d="move_path",
          #alg=strip_search_alg(move_alg),
          pruning_info=pruning_info
        )
      )
    end
    @pack! vamp_pieces = process_move_path


    process_explore_path = function process_explore_path(explore_alg, starting_state)
        explore_goal_node = explore_alg.instance.goal_test.s_that_saw_the_most.contents
      if !isempty(explore_alg.goal_node)
        @assert(explore_goal_node != nothing)
      end

      explore_path_ss = SearchState_t[]
      if explore_goal_node != nothing
        explore_path = reverse(follow_path(explore_alg.closed_parents, explore_goal_node))
        explore_path_ss = [SearchState_t(ss.q, ss.q_path, ss.visible_region) for ss in explore_path]
      end

      return (
        explore_path_to_add=explore_path_ss,
        metadata=(placeholder=nothing,))
    end
    @pack! vamp_pieces = process_explore_path


    prune_visible_region_state = function prune_visible_region_state(state)
      s = state
      println("Prune Visible Region")
      prunemask = VAMP.find_prunable_visible_region(
        s.visible_region, prune_test_raster_spec__Fworld)
      println("Keeping $(sum(prunemask)) / $(length(prunemask))")

      new_visible_region = VAMP.cat_pvv(
        VAMP.split_pvv(VAMP.index_packed_viewvolume(s.visible_region))[prunemask])
      SearchState_t(s.q, s.q_path, new_visible_region)
    end
    @pack! vamp_pieces = prune_visible_region_state

    #Profile.init(10^8, 0.001)
    #Profile.clear()

    """for a given path, return the points on the swept volume that, if they were observed,
    would make the path feasible"""
    visibility_violations_path = function visibility_violations_path(path)
      onestep(s_from, s_to) = VAMP.get_uncovered_pieces__Fworld(visibility_instance, s_to.q, s_from.visible_region)

      empty = Set{Point{2, Float64}}() # need to see this
      swept_volume_uncovered = reduce(union, (onestep(s_from, s_to) for (s_from, s_to) in VAMP.iter2(path)), init=empty)
      return swept_volume_uncovered
    end
    @pack! vamp_pieces = visibility_violations_path



    return vamp_pieces
end

function make_visibility_instance(parameters)
  @setparam parameters visibility_instance = let
      @setparam parameters tx__Frobot__Fcamerabase = nothing
      @setparam parameters poly_frustum__Fcamera = nothing
      @setparam parameters all_view__Fcamerabase = nothing
      @setparam parameters polys_obstacles__Fworld = nothing
      @setparam parameters points_robot__Frobot = nothing

      camera = PanSteerableCamera(tx__Frobot__Fcamerabase, poly_frustum__Fcamera)
      potential_camera = VAMP.PotentialCamera(camera, all_view__Fcamerabase)

      VisibilityInstance(
        Set(points_robot__Frobot),
        polys_obstacles__Fworld,
        potential_camera,
        q->UnionOfConvex([]));
  end;
  return nothing
end

function do_parameters(parameters)
  @setparam parameters is_collision_free = nothing        # mp
  @setparam parameters boundingbox_workspace__Fworld = nothing
  @setparam parameters workspace_points_raster_spec__Fworld = nothing # execute move if it sees something new workspace_points_raster_spec__Fworld
  # higher resolution for making sure we don't prune poorly.
  @setparam parameters prune_test_raster_spec__Fworld = VAMP.RasterVolumeSpec(workspace_points_raster_spec__Fworld.limits, 1 .* workspace_points_raster_spec__Fworld.size)

  VAMP.make_visibility_instance(parameters)

  # carefully chosen to be exactly representable in floating point and also include the goal, because of equality testing
  @setparam parameters discretization = SO2_Discretization(Δx = .125, θ_steps=16)


  @setparam parameters SE2_MPState_t = SE2_MPState{parameters[:robot_width]/2} # reasonable scaling to measure distance
  @setparam parameters VisibleRegion_t = VAMP.PackedViewVolumes{Array{VAMP.ArcRegion{2,Float64},1},Array{StaticArrays.SArray{Tuple{3},Float64,1,3},1}}
  @setparam parameters SearchState_t = VAMPSearchState{SE2_MPState_t{Float64}, VisibleRegion_t}
  @setparam parameters SearchStateExplore_t = VAMPSearchStateExplore{SE2_MPState_t{Float64}, VisibleRegion_t, Set{Any}}

  @setparam parameters q_start = nothing
  @setparam parameters goal = nothing
  @setparam parameters starting_visibility__Fworld = nothing

  q_start = convert(SE2_MPState_t, q_start)
  goal = PointEpsilonGoal(convert(SE2_MPState_t, rand(goal)), goal.ϵ) # TODO clunky
  @pack! parameters = goal
  @pack! parameters = q_start

  @setparam parameters mp = HolonomicMotionPlanningInstance(
    q_start,
    goal,
    q->is_collision_free(q) && contains(boundingbox_workspace__Fworld, Point(q.e.q...)), # cheaply bound the workspace to help have a finite search space.
    (state1,state2)->is_path_free(state1,state2)
  )

  for q in [rand(goal), q_start]
      @assert mp.q_valid_test(q)
      @assert on_grid(discretization, q)
  end
  return nothing
end
#=
end
=#
