module MeshVisibility


import GeometryTypes: vertices, faces, vertextype, facetype, Face, OffsetInteger, norm, normalize, dot
import GeometryTypes
import EnhancedGJK

include("/Users/goretkin/projects/VAMP.jl/src/mesh_utils.jl")


function compute_silhouette_cap(mesh, eyeball)
  signed_distance(face) = EnhancedGJK.ReferenceDistance.interior_distance( vertices(mesh)[face], eyeball)
  mesh_con = MeshUtils.mesh_connection(mesh)

  # two faces for every edge
  # @assert unique(map(length, values(mesh_con.faces_by_edge))) == [2]

  sds = signed_distance.(faces(mesh))
  if any(sds .== 0)
    @warn("you needed to handle this. two silhoette edges on a face")
  end

  signed_distance_map = Dict(face => sd for(face, sd) in zip(faces(mesh), sds))

  function is_silhouette_edge(edge)
    if length(mesh_con.faces_by_edge[edge]) == 1
      return true # TODO is this a good idea?
    end
    (face1, face2) = mesh_con.faces_by_edge[edge]
    sd1 = signed_distance_map[face1]
    sd2 = signed_distance_map[face2]
    return sd1 * sd2 <= 0
  end

  silhouette_edges = [edge for edge in mesh_con.edges if is_silhouette_edge(edge)]

  cap_faces = [face for face in faces(mesh) if signed_distance_map[face] >= 0]

  return (silhouette_edges=silhouette_edges, cap_faces=cap_faces)
end

function compute_shadow_volume(mesh, eyeball, infinity)
  r = compute_silhouette_cap(mesh, eyeball)
  new_vertices = copy(vertices(mesh))
  new_faces = r.cap_faces

  FACE = facetype(mesh)
  VERTEX_I = eltype(FACE)
  silhouette_vertices_i = Set{VERTEX_I}()

  for edge in r.silhouette_edges
    for v in edge; push!(silhouette_vertices_i, v); end
  end

  silhouette_vertices_i = collect(silhouette_vertices_i)

  # add vertices to mesh that are at "infinity"
  # keep track of which indices project to which indices
  project_i = Dict()
  for vert_i in silhouette_vertices_i
    ray = new_vertices[vert_i] - eyeball
    to_go = infinity - norm(ray)
    #if to_go <= 0; push!(vert_i_out_of_range, vert_i); continue end
    to_go = max(to_go, 0) # not sure how to handle this better.
    new_vert = to_go * normalize(ray) + new_vertices[vert_i]

    push!(new_vertices, new_vert)
    new_vert_unoffset_i = length(new_vertices)
    new_vert_i = VERTEX_I(new_vert_unoffset_i)
    project_i[vert_i] = new_vert_i
  end
  normal = EnhancedGJK.ReferenceDistance.normal

  quads = []
  for edge in r.silhouette_edges
    (vni1, vni2) = edge # vertices near index
    vfi1 = project_i[vni1]
    vfi2 = project_i[vni2]

    face1 = FACE(vni1, vni2, vfi1)
    face2 = FACE(vni2, vfi1, vfi2)

    these_new_faces = [face1, face2]

    test_face = (eltype(new_vertices)(eyeball...), new_vertices[vni1], new_vertices[vni2])
    desired_normal = normal(test_face)

    for i in 1:2
      if dot(desired_normal, normal(new_vertices[these_new_faces[i]])) < 0
        flipped_face = these_new_faces[i]
        these_new_faces[i] = FACE(flipped_face[2], flipped_face[1], flipped_face[3])
      end
    end

    append!(new_faces, these_new_faces)
  end

  return GeometryTypes.HomogenousMesh(faces=new_faces, vertices=new_vertices)
end



end