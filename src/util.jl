import MacroTools
export countall, @setparam, unlift_q, safe_make_path

# Good for converting a list of Points into a Matrix
array(x::AbstractVector{T}) where {T<:AbstractVector{S}} where {S} = Array(hcat(x...)')
array(x::AbstractVector{T}) where {T<:Tuple} = Array(hcat([[xi...] for xi in x]...)')

# tigthen(Any[1,2.0]) == Float64[1,2]
tighten(a::AT) where {N, T, AT<:AbstractArray{T, N}} = convert(Array{reduce(promote_type, Union{}, typeof.(a)), N}, a) # TODO don't always make it an Array
nan(T::Union{AbstractFloat, Type{<:AbstractFloat}}) = zero(T)/zero(T) # TODO better way?

unlift_q(path) = map(x->x.q, path)
iter2(a) = zip(a[1:end-1], a[2:end])

get_path_length(q_path) = sum(map(x->distance(x...), iter2(q_path)))

"""
visit_has_novelty()[1,2,3], 2) is true because last time 2 was visited, you had only seen 1. now you've also seen 3.
visit_has_novelty()[1,2,3,2], 3) is false
"""
function visit_has_novelty(path, element)
  n = length(path)
  if n == 0
    return true
  end

  maybe_new = Set{eltype(path)}()
  seen = Set{eltype(path)}()

  for i = n:-1:1
    if path[i] == element
      for j = 1:i
        push!(seen, path[j])
      end
      break
    end
    push!(maybe_new, path[i])
  end

  return length(setdiff(maybe_new, seen)) > 0
end

"`path` is made up of `elements`"
function find_novel_prefix(path, visited)
  #This should work even if array has funky indices
  I = axes(path)[1]
  for (i, pi) = enumerate(reverse(I))
    if !(path[pi] in visited)
        return I[1:end-(i-1)]
    end
  end
  return I[1:0] # should be an empty range no matter what.
end

"""
pvv is PackedViewVolumes
"""
function find_novel_prefix_visibility(visibility_path, visibility_old, raster_spec)
    ref = zeros(Int16, raster_spec.size)
    test = zeros(Int16, raster_spec.size)

    RV_ref = VAMP.RasterVolume(raster_spec, ref)
    RV_test = VAMP.RasterVolume(raster_spec, test)

    render!(RV_ref, visibility_old)
    RV_ref.data .= RV_ref.data .> 0
    # requires array to have standard indexing
    N = length(visibility_path)
    for i = N:-1:1
        render!(RV_test, visibility_path[i])
        RV_test.data .= RV_test.data .> 0

        none_new = all(RV_ref.data .≥ RV_test.data)
        if !none_new
            return 1:i
        end
    end
    return 1:0 # empty range
end

function fully_connected_ngrid(n)
  center = ntuple(x->0, n)
  grid_1d = (-1, 0, 1)
  hypergrid = Iterators.product(Iterators.repeated(grid_1d, n)...)
  return Iterators.filter(x->x != center, hypergrid)
end

function limited_connected_ngrid(n, k)
  # at most k coordinates change
  return Iterators.filter(x->sum(x .!= 0) <= k, fully_connected_ngrid(n) )
end

function countall(iter)
  histogram = Dict{eltype(iter)}{Int64}()
  for element in iter
    if !haskey(histogram, element)
      histogram[element] = 0
    end
    histogram[element] += 1
  end
  return histogram
end

# only set if there isn't already a key
function setindex_default!(dict, value, key)
  if haskey(dict, key) && dict[key] != value && value != nothing
    @warn("Ignoring parameter: $(key)=>$(value). Existing value: $(dict[key])")
  end
  if !haskey(dict, key)
    dict[key] = value
  end
end

"""
```
Sets parameters and makes them available in the
d = Dict()
d[:z] = "already has a setting"

@setparam d x = 3
@setparam d y = 2x
println(x, y)
@setparam d z = x + y

will execute

d[:x] = 3
x = d[:x]
d[:y] = 2x
y = d[:y]
z = x + y
```
"""
macro setparam(parameters, ex)
  if !MacroTools.@capture(ex, lhs_Symbol = rhs_)
    if !MacroTools.@capture(ex, lhs_Symbol)
      error("Must be an assignment statement (and left-hand side must be a Symbol), or just a Symbol.")
    else
      rhs = lhs
    end
  end

  quote
    setindex_default!($(esc(parameters)), $(esc(rhs)), $(QuoteNode(lhs)))
    $(esc(lhs)) = ($(esc(parameters)))[$(QuoteNode(lhs))]
  end
end

module FloatingPointFixer
struct FPFixer{S, R, D}
    seen::S
    r::R
    distance::D
end

function fix!(fpf::FPFixer, input)
    # snap to existing points.
    best = nothing
    d = Inf
    for seen in fpf.seen
        dd = fpf.distance(input, seen)
        if dd < d
            best = seen
            d = dd
        end
    end

    if d < fpf.r
        return best
    else
        push!(fpf.seen, input)
        return input
    end
end

function isdistinct!(fpf::FPFixer, input)
    # snap to existing points.
    best = nothing
    d = Inf
    for seen in fpf.seen
        dd = fpf.distance(input, seen)
        if dd < d
            best = seen
            d = dd
        end
    end

    if d < fpf.r
        return false
    else
        push!(fpf.seen, input)
        return true
    end
end


function fix_list_of_list_of_points(input, threshold)
  fpf = FloatingPointFixer.FPFixer(Any[], threshold, (a,b) -> norm(a-b))

  return [[FloatingPointFixer.fix!(fpf, pt) for pt in pc] for pc in input];
end

end

module LibGEOSInterface
import VAMP
import VAMP: BoundingBox, minlimit, maxlimit, dimlimits, poly_from_lims, Difference, DifferenceGeneral,
  explicitcontour, ArcRegion, UnionOfConvex, UnionOfGeometry

import LibGEOS
import Polyhedra

arraycycle(v) = vcat(v, [v[1]])

to_libgeos_poly(points) =  LibGEOS.Polygon([arraycycle(VAMP.convexhull_andrew(points))])


function to_multipoly_2d(pieces)
    answer = to_libgeos_poly(pieces[1])
    for i = 2:length(pieces)
        answer = LibGEOS.union(answer, to_libgeos_poly(pieces[i]))
    end
    return answer
end

import LibGEOS.Polygon
function LibGEOS.Polygon(bb::VAMP.BoundingBox{2, T}) where {T}
  return Polygon([
        arraycycle(map(Array, VAMP.ordered_vertices(bb)))
        ])
end

function LibGEOS.Polygon(poly::P) where {P<:Polyhedra.Polyhedron}
  @assert Polyhedra.fulldim(poly) == 2
  if Polyhedra.hasrays(poly)
    error("not yet")
  end
  if Polyhedra.npoints(poly) > 1
    LibGEOS.Polygon([arraycycle(VAMP.convexhull_andrew(collect(Polyhedra.points(poly))))])
  else
    nothing # how to construct an empty LibGEOS Polygon?
  end
end

function to_geos(polypieces)
  reduce(LibGEOS.union, map(VAMP.LibGEOSInterface.LibGEOS.Polygon, polypieces))
end

function polygon_coordinates_to_exterior_shape(coords)
  exterior = coords[1]
  (map(x->[x...], zip(exterior...))...,)
end

function exteriors(mp::LibGEOS.MultiPolygon)
  map(polygon_coordinates_to_exterior_shape, LibGEOS.coordinates(mp))
end

function LibGEOS.Polygon(d::Union{UnionOfConvex, UnionOfGeometry})
  return reduce(LibGEOS.union, map(LibGEOS.Polygon, d.pieces))
end

function LibGEOS.Polygon(d::Union{Difference, DifferenceGeneral}) # should restrict type to 2D
  box = BoundingBox(d.positive)
  box = BoundingBox(minlimit(box) .- 1.0, maxlimit(box) .+ 1.0)
  box_poly = VAMP.poly_from_lims(dimlimits(box)...)
  pos_geos = LibGEOS.Polygon(d.positive)

  #result = deepcopy(pos_geos)  # seems to be responsible for segfaults
  result = pos_geos
  for neg in d.negative.pieces
    # the Polyhedra.Polyhedron may be unbounded. Bound it.
    neg_geos = LibGEOS.Polygon(intersect(neg, box_poly))
    if neg_geos != nothing # unnecessary if there's a LibGEOS representation of empty polygon.
      result = LibGEOS.difference(result, neg_geos)
    end
  end
  return result
end

function LibGEOS.Polygon(ar::ArcRegion{2})
    LibGEOS.Polygon([map(x->[x...], explicitcontour(ar, 50))])
end

function has_holes(p::LibGEOS.Polygon)
  return length(LibGEOS.coordinates(p)) > 1
end

function has_holes(p::LibGEOS.MultiPolygon)
  return any(map(poly_coords -> length(poly_coords) > 1, LibGEOS.coordinates(p)))
end

function has_holes(p::LibGEOS.GeometryCollection)
  if length(LibGEOS.geometries(p)) == 0
    return false
  else
    error("TODO iterate through geometries")
  end
end


"""
take union so long as no holes are introduced. Because Plots doesn't plot holes.
"""
function union_no_hole(d::UnionOfGeometry{Vector{T}}, results=[]) where T <: LibGEOS.AbstractGeometry
  for pg in d.pieces
    if length(results) == 0
      push!(results, pg)
      continue
    end

    found_a_home = false
    for i = 1:length(results)
      maybe = LibGEOS.union(results[i], pg)
      if !has_holes(maybe)
        results[i] = maybe
        found_a_home = true
        break
      end
    end
    if !found_a_home
      push!(results, pg)
    end
  end
  return results
end

function to_geos_with_no_holes(d::Union{UnionOfConvex, UnionOfGeometry}, results=[])
  return union_no_hole(UnionOfConvex([LibGEOS.Polygon(p) for p in d.pieces]), result)
end

end #LibGEOSInterface


"""
calculate a mask into visible_regions, 0 means that element can be ignored without
changing coverage, up to the raster.
"""
function find_prunable_visible_region(visible_regions, raster_spec)
  N = length(visible_regions.arc_regions)

  R = zeros(Int16, raster_spec.size)
  T = zeros(Int16, raster_spec.size)
  RV = VAMP.RasterVolume(raster_spec, R)
  RV_T = VAMP.RasterVolume(raster_spec, T)

  pvv = visible_regions
  mask = ones(Bool, N)

  render!(RV_T, pvv)
  RV_T.data .= RV_T.data .> 0

  for i = 1:N
    RV.data[:] .= 0
    mask[i] = 0
    render!(RV, pvv, mask)
    RV.data .= RV.data .> 0
    if !all(RV.data .≥ RV_T.data)
      mask[i] = 1 # keep i
    end
  end
  return mask
end

module Profiling
  import JLD2
  #import ProfileView # causing 0.6.3 to segfault with some inference issue.
  function sanitize_stackframe(sf)
    # `Core.MethodInstance` has pointers and cannot be serialized. mark `linfo` as missing.
    linfo = Nullable{Core.MethodInstance}()
    StackFrame(sf.func, sf.file, sf.line, linfo, sf.from_c, sf.inlined, sf.pointer)
  end


  function sanitize_lidict(lidict)
    Dict(k => map(sanitize_stackframe, sfs) for (k, sfs) in lidict)
  end

  function save_profiling(filename_prefix)
    (li, lidict) = Profile.retrieve()
    @show length(li)
    unsanitized_lidict = lidict
    lidict = sanitize_lidict(unsanitized_lidict)
    JLD2.@save "$(filename_prefix).jlprof.jld2" li lidict
    #ProfileView.svgwrite("$(filename_prefix).svg", li, lidict)
  end
end


function ishalted()
  r = false
  try
    open("halt") do f
      s = readstring(f)
      r =  startswith(s, "halt")
    end
  catch
  end
  return r
end

function safe_make_path(path)
  if isdir(path)
    return false
  end
  mkpath(path)
  return true
end

function get_vamp_repo_path()
  path_of_me = @__FILE__
  path_of_vamp = realpath(joinpath(path_of_me, "..", ".."))
  return path_of_vamp
end

"""
Get path of the root of the latex document repository, if we are running in the document
"""
function get_document_repo_path()
  try
    path_of_vamp = get_vamp_repo_path()
    parent_of_vamp = realpath(joinpath(path_of_vamp, ".."))
    if basename(parent_of_vamp) == "code"
      path_of_doc = realpath(joinpath(parent_of_vamp, ".."))
      return path_of_doc
    end
  catch
    nothing
  end
  return nothing
end
