struct PackedViewVolumes{AR, AS}
  arc_regions::AR # Vector{ArcRegion}
  shadow_halfspaces::AS # Vector{SVector{d, T}
end

struct IndexPackedViewVolume{P}
  pvv::P
  indices::Vector{Int64} # into shadow_halfspaces
end

function index_packed_viewvolume(pvv::PackedViewVolumes)
  idxs_shadow_halfsapces = Int64[1]

  n_arc_regions = length(pvv.arc_regions)
  shadow_halfspaces = pvv.shadow_halfspaces
  i_arc_regions = 1
  i_shadow_halfspaces = 1

  while i_arc_regions <= n_arc_regions
    while true  # through all shadows of arc_region
      @inbounds shadow_halfspace = shadow_halfspaces[i_shadow_halfspaces]
      if !isnan(shadow_halfspace[3])
        while true # through all halfspaces of shadow_halfspace
          @inbounds shadow_halfspace = shadow_halfspaces[i_shadow_halfspaces]
          thisnan = isnan(shadow_halfspace[3])
           i_shadow_halfspaces += 1
           if thisnan
             # done with halfspaces
             @goto break_halfspace
           end
        end
        @label break_halfspace
      else
        i_shadow_halfspaces += 1
        i_arc_regions += 1
        push!(idxs_shadow_halfsapces, i_shadow_halfspaces)
        break
      end
    end
  end

  return IndexPackedViewVolume(pvv, idxs_shadow_halfsapces)
end

function split_pvv(ipvv::IndexPackedViewVolume)
  idxs = ipvv.indices
  intervals = [idxs[i]:(idxs[i+1]-1) for i = 1:(length(idxs)-1)]

  return [PackedViewVolumes(
    ipvv.pvv.arc_regions[[i]],
    ipvv.pvv.shadow_halfspaces[interval]) for (i, interval) in enumerate(intervals)]
end


function Base.vcat(pvv1::PVV, pvv2::PVV) where {PVV <: PackedViewVolumes}
  PackedViewVolumes(
    vcat(pvv1.arc_regions, pvv2.arc_regions),
    vcat(pvv1.shadow_halfspaces, pvv2.shadow_halfspaces)
  )
end


function cat_pvv(pvvss::Vector{PackedViewVolumes{AR, AS}}) where {AR, AS}
  ar = AR()
  as = AS()

  for pvvs in pvvss
    append!(ar, pvvs.arc_regions)
    append!(as, pvvs.shadow_halfspaces)
  end
  return PackedViewVolumes(ar, as)
end


function difference(points::Set{POINTS}, pvvs::PackedViewVolumes) where {POINTS}
  result = Set{POINTS}()
  for point in points
    if !packed_viewvolume_in(point, pvvs.arc_regions, pvvs.shadow_halfspaces)
      push!(result, point)
    end
  end
  return result
end

isinside(pvvs::PackedViewVolumes, point) = packed_viewvolume_in(point, pvvs.arc_regions, pvvs.shadow_halfspaces)

function packed_viewvolume_in(point, arc_regions, shadow_halfspaces, mask=nothing)
  T = eltype(point)
  point_homo = Point(point[1], point[2], -one(T)) # TODO make dimensionally generic
  count::Int = 0
  i_arc_regions = 1
  i_shadow_halfspaces = 1
  n_arc_regions = length(arc_regions)
  while i_arc_regions <= n_arc_regions
    @inbounds  arc_region = arc_regions[i_arc_regions]
    in_arc = isinside(arc_region, point)
    out_all_shadow = true
    while true  # through all shadows of arc_region
      @inbounds shadow_halfspace = shadow_halfspaces[i_shadow_halfspaces]
      if !isnan(shadow_halfspace[3])
        out_this_shadow = false
        while true # through all halfspaces of shadow_halfspace
          @inbounds shadow_halfspace = shadow_halfspaces[i_shadow_halfspaces]
          thisnan = isnan(shadow_halfspace[3])
           if !thisnan
             out_this_shadow |= (dot(shadow_halfspace, point_homo) > 0) # have to be out of at least one halfspace to be out of this shadow
           end
           i_shadow_halfspaces += 1
           if thisnan
             # done with halfspaces
             #@cuprintf("HS,")
             @goto break_halfspace
           end
        end
        @label break_halfspace
        out_all_shadow &= out_this_shadow
      else
        i_shadow_halfspaces += 1
        i_arc_regions += 1
        #@cuprintf("AR,")
        break
      end
    end
    #out_all_shadow = true # ignore shadow
    m = mask == nothing ? true : (mask[i_arc_regions-1] > 0)
    if m && (in_arc & out_all_shadow)
      return true
    end
  end
  return false
end


function count_packed_viewvolume_in(point, arc_regions, shadow_halfspaces, mask=nothing)
  T = eltype(point)
  point_homo = Point(point[1], point[2], -one(T)) # TODO make dimensionally generic
  count::Int = 0
  i_arc_regions = 1
  i_shadow_halfspaces = 1
  n_arc_regions = length(arc_regions)
  while i_arc_regions <= n_arc_regions
    @inbounds  arc_region = arc_regions[i_arc_regions]
    in_arc = isinside(arc_region, point)
    out_all_shadow = true
    while true  # through all shadows of arc_region
      @inbounds shadow_halfspace = shadow_halfspaces[i_shadow_halfspaces]
      if !isnan(shadow_halfspace[3])
        out_this_shadow = false
        while true # through all halfspaces of shadow_halfspace
          @inbounds shadow_halfspace = shadow_halfspaces[i_shadow_halfspaces]
          thisnan = isnan(shadow_halfspace[3])
           if !thisnan
             out_this_shadow |= (dot(shadow_halfspace, point_homo) > 0) # have to be out of at least one halfspace to be out of this shadow
           end
           i_shadow_halfspaces += 1
           if thisnan
             # done with halfspaces
             #@cuprintf("HS,")
             @goto break_halfspace
           end
        end
        @label break_halfspace
        out_all_shadow &= out_this_shadow
      else
        i_shadow_halfspaces += 1
        i_arc_regions += 1
        #@cuprintf("AR,")
        break
      end
    end
    #out_all_shadow = true # ignore shadow
    m = mask == nothing ? 1 : mask[i_arc_regions-1]
    count += (in_arc & out_all_shadow) * m
  end
  return count
end


"""
pack visibility information into flat arrays that can be sent to the GPU
"""
function make_packed_view_volume(uniondiffarc::UnionOfGeometry{<:AbstractArray{DifferenceGeneral{ARC, UnionOfConvex{POLY}}}}, shadow_indices=Int64[]) where {ARC<:ArcRegion, POLY<:Polyhedra.Polyhedron}
  # TODO generalize types
  N = 3
  T = Float64
  NAN = zero(T) / zero(T)
  halfspace_marker = SVector(ntuple(i->NAN, N)...)

  arc_regions = ARC[]
  shadow_halfspaces = SVector{N, T}[]
  #shadow_indices is an 'outparam' just so I don't have to fix old call sites
  #shadow_indices = Int64[]  #arc_region[k]'s shadows begin at shadow_halfspaces[shadow_indices[k]]
  for diff in uniondiffarc.pieces
    push!(arc_regions, diff.positive)
    push!(shadow_indices, length(shadow_halfspaces) + 1)
    for shadow in diff.negative.pieces
      for halfspace in allhalfspaces(shadow)
        new_halfspace = SVector(halfspace.a..., halfspace.β)
        if isnan(new_halfspace)
          error("can't have any nans")
        end
        push!(shadow_halfspaces, new_halfspace)
      end
      push!(shadow_halfspaces, halfspace_marker)
    end
    push!(shadow_halfspaces, halfspace_marker)
  end
  return PackedViewVolumes(arc_regions, shadow_halfspaces)
end

# non CUDA fallback
function render!(v::VAMP.RasterVolume{S, A}, packed_viewvolume::PackedViewVolumes, mask=nothing) where {S, A}
  for subs__d in CartesianIndices(v.data)
    x__c = coordinates(v.spec, Tuple(subs__d))
    point =  Point(x__c[1], x__c[2])
    v.data[subs__d] += count_packed_viewvolume_in(point, packed_viewvolume.arc_regions, packed_viewvolume.shadow_halfspaces, mask)
  end
end

"takes a view volume and makes a data structure that encodes all the order-invariance as Sets
allows for hashing and quick comparisons, hopefully"
function unpacked_viewvolume_set(pvv)
  error("untested. appears to be broken")
  arc_regions = pvv.arc_regions
  shadow_halfspaces = pvv.shadow_halfspaces

  i_arc_regions = 1
  i_shadow_halfspaces = 1
  n_arc_regions = length(arc_regions)

  POSITIVE = eltype(arc_regions)
  HALFSPACE = eltype(shadow_halfspaces)

  SHADOW = Set{HALFSPACE}
  SHADOWS = Set{SHADOW}
  VIEWVOLUME = DifferenceGeneral{POSITIVE, SHADOWS}
  VIEWVOLUMES = Set{VIEWVOLUME}

  set_vv = VIEWVOLUMES()
  indices = Int64[-1 for i = 1:n_arc_regions]

  while i_arc_regions <= n_arc_regions # through all view volumes
    indices[i_arc_regions] = i_shadow_halfspaces

    @inbounds arc_region = arc_regions[i_arc_regions]
    shadows = SHADOWS()
    while true  # through all shadows of arc_region
      shadow = SHADOW()
      @inbounds shadow_halfspace = shadow_halfspaces[i_shadow_halfspaces]
      if !isnan(shadow_halfspace[3])
        while true # through all halfspaces of shadow_halfspace
          @inbounds shadow_halfspace = shadow_halfspaces[i_shadow_halfspaces]
          thisnan = isnan(shadow_halfspace[3])
           if !thisnan
             push!(shadow, shadow_halfspace)
           else
             # done with halfspaces for this shadow
             push!(shadows, shadow)
             @goto break_halfspace
           end
           i_shadow_halfspaces += 1
        end
        @label break_halfspace

      else
        # done with shadows for this view volume
        view_volume = DifferenceGeneral(arc_region, shadows)
        push!(set_vv, view_volume)

        i_shadow_halfspaces += 1
        i_arc_regions += 1
        break
      end
    end
  end
  return set_vv
end


"""consumes output of unpacked_viewvolume_set"""
function unpacked_viewvolume_set_to_poly(unpacked_view_volume)
    return UnionOfGeometry([DifferenceGeneral(
        diff.positive,
        UnionOfConvex(
            [Polyhedra.polyhedron(reduce(∩, Polyhedra.HalfSpace(v[1:2], v[3]) for v in shadow))
                for shadow in diff.negative])
    ) for diff in unpacked_view_volume])
end
