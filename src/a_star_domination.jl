export AStarInstance, AStarAlgorithm, setup!, step!, follow_path
using DataStructures

struct AStarNode{STATE, COST}
  state::STATE
  parent_state::STATE
  cost_from_start::COST
end

struct AlwaysValid end

struct AStarInstance{FT,FH,FG,FE,FA,FD}
  transition_cost::FT   # (state_to::STATE, state_from::STATE) → edge_cost::COST
  heuristic::FH         # state::STATE → cost_to_goal::COST
  goal_test::FG         # state::STATE → ::Bool
  expand!::FE           # (Set{STATE}, state_from) → ::Nothing
  alias::FA             # state::STATE → STATE_ALIAS
  dominates::FD         # (s1::STATE, s2::STATE) ⇢ Bool  s1 dominates s2
end

struct AStarAlgorithm{STATE, STATE_ALIAS, COST, INSTANCE}
  instance::INSTANCE
  closed_parents::Dict{STATE_ALIAS, AStarNode{STATE, COST}} # closed set and back pointers
  agenda::PriorityQueue{AStarNode{STATE, COST}, COST}
  children::Set{STATE} # workspace
  goal_node::Vector{STATE}
end

function AStarAlgorithm(instance, STATE, STATE_ALIAS, COST)
  closed_parents = Dict{STATE_ALIAS, AStarNode{STATE, COST}}()
  agenda = PriorityQueue{AStarNode{STATE, COST}, COST}()
  children = Set{STATE}()
  goal_node = STATE[]
  return AStarAlgorithm(instance, closed_parents, agenda, children, goal_node)
end

function setup!(alg::AStarAlgorithm, start_state, start_cost)
  empty!(alg.closed_parents)
  while !isempty(alg.agenda) dequeue!(alg.agenda) end # at time of writing, there is no empty! method
  empty!(alg.children)
  empty!(alg.goal_node)
  enqueue!(alg.agenda, AStarNode(start_state, start_state, start_cost)=>start_cost) # technically should add h too
  return nothing
end

closed_check(closed_parents, state) = haskey(closed_parents, state)

@inline function step!(alg::AStarAlgorithm)
  if isempty(alg.agenda)
    return false
  end

  expandme = dequeue!(alg.agenda)

  expandme_alias = alg.instance.alias(expandme.state)
  if closed_check(alg.closed_parents, expandme_alias)
    if alg.instance.dominates(alg.closed_parents[expandme_alias].state, expandme.state)
      return true # already got to this state (alias) in a "better" way.
    end
  end

  # Just found the optimal path (modulo aliasing and domination) to expandme.state
  alg.closed_parents[expandme_alias] = AStarNode(expandme.state, expandme.parent_state, expandme.cost_from_start)

  if alg.instance.goal_test(expandme.state)
    push!(alg.goal_node, expandme.state)
    return true # don't expand once you've reached the goal
  end

  empty!(alg.children)
  alg.instance.expand!(alg.children, expandme.state)

  for child in alg.children
    h = alg.instance.heuristic(child)
    edge_cost = alg.instance.transition_cost(child, expandme.state)
    g = expandme.cost_from_start + edge_cost
    queue_value = g + h
    if queue_value == typemax(typeof(queue_value))
      continue
    end
    enqueue!(alg.agenda, AStarNode(child, expandme.state, g)=>queue_value)
  end

  return true
end


function follow_path(parents::Dict{AS,AStarNode{S,C}}, end_::S, alias) where {AS, S, C}  # TODO any AbstractDict
  reached = Set{S}()
  r = S[]
  next_ = end_
  while next_ ∉ reached
    push!(reached, next_)
    push!(r, next_)
    next_ = parents[alias(next_)].parent
  end
  return r
end
