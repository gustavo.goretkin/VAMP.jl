# would really like JLD2 to have this functionality
function migrate_ArcRgion(v)
  # assume typeparams are determined by field types
  samefieldnames = [:radius, :center, :direction]
  newfieldmap = Dict{Symbol, Any}(:cos_half_fov=>cos(getfield(v, :halffov)))

  for fn in samefieldnames
    newfieldmap[fn] = getfield(v, fn)
  end

  newfieldnames = fieldnames(VAMP.ArcRegion)
  params = [newfieldmap[fn] for fn in newfieldnames]
  return VAMP.ArcRegion(params...)
end

function migrate_visible_region(vs)
  return [VAMP.DifferenceGeneral(migrate_ArcRgion(v.positive), v.negative) for v in vs]
end

function migrate_RetroVampRRTAlgorithm(v)
  rrt = v.rrt
  pc = v.visibility_instance.camera
  pcnew = VAMP.PotentialCamera(pc.camera, migrate_ArcRgion(pc.frustum_union__Fcamerabase))
  vi = VAMP.VisibilityInstance(
    v.visibility_instance.robot__Frobot,
    v.visibility_instance.occluders__Fworld,
    pcnew,
    q->UnionOfConvex([])
  )
  vr = migrate_visible_region(v.visible_region)
  return VAMP.RetroVampRRTAlgorithm(rrt, vi, vr)
end
