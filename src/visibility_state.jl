export VampState, VampAction, SO2LookAction, PanSteerableCamera, VisibilityInstance, VampRRTAlgorithm,
  get_tx__Frobot__Fcamera, POLY, ROBOT_S, VISIBILITY_S, ROBOT_A, VISIBILITY_A, VAMPSTATE, VAMPACTION

struct VampState{ROBOT, VIZ} <: AbstractMotionPlanningState
  robot::ROBOT
  viz::Vector{VIZ}  # __Fworld
end

struct VampAction{ROBOT, VIZ}
  # __Frobot
  robot::ROBOT
  looks::Vector{VIZ}  #take these views, then move
end

struct SO2LookAction{T}
  θ::T
end

struct PanSteerableCamera{TX, POLY}
  tx__Frobot__Fcamerabase::TX
  frustum__Fcamera::POLY
end

struct PotentialCamera{CAM, POT}
  camera::CAM
  frustum_union__Fcamerabase::POT # everything the camera could see.
end

struct VisibilityInstance{CAM, POLY, ROBOT, FO}
  #mp_instance::HolonomicMotionPlanningInstance
  robot__Frobot::ROBOT
  occluders__Fworld::UnionOfConvex{POLY}
  camera::CAM
  get_robot_occluders__Fworld::FO
end

function get_tx__Frobot__Fcamera(pcs::PanSteerableCamera, θ__Frobot::SO2LookAction)
  tx__Fcamerabase__Fcamera = convert(LinearMap, θ__Frobot.θ)
  tx__Frobot__Fcamera = pcs.tx__Frobot__Fcamerabase ∘ tx__Fcamerabase__Fcamera
  return tx__Frobot__Fcamera
end

POLY = let vr = vrep(SVector{2, Float64}[])
  typeof(polyhedron(vr, Polyhedra.default_library(2, Float64)))
end
ROBOT_S = SE2MotionPlanningState{0.2, Float64}
VISIBILITY_S = Difference{POLY}

ROBOT_A = SE2MotionPlanningAction{0.2, Float64}
VISIBILITY_A = SO2LookAction{Rot{Float64}}

VAMPSTATE = VampState{ROBOT_S, VISIBILITY_S}
VAMPACTION = VampAction{ROBOT_A, VISIBILITY_A}



# lift some <:AbstractMotionPlanningState into this VampState
@inline sqdistance(q1::VampState{ROBOT, VIZ}, q2::VampState{ROBOT, VIZ}) where {ROBOT,  VIZ} = sqdistance(q1.robot, q2.robot)
@inline distance(q1, q2) = sqrt(sqdistance(q1, q2))

@inline action(q_to::VampState{ROBOT, VIZ}, q_from::VampState{ROBOT, VIZ}) where {ROBOT,  VIZ} =
  VampAction(action(q_to.robot, q_from.robot), VISIBILITY_A[])  # TODO `VISIBILITY_A` not generic, visibility dynamics

# method does not do visibility dynamics
@inline state(q_from::VampState, a::VampAction) =
  VampState(state(q_from.robot, a.robot), deepcopy(q_from.viz))

# method does do visibility dynamics
function state(q_from::VampState, action::VampAction, vi::VisibilityInstance)
  viz = deepcopy(q_from.viz)

  tx__Fworld__Frobot = get_tx__Fworld__Frobot(q_from.robot)

  for camera_look in action.looks
    tx__Frobot__Fcamera = get_tx__Frobot__Fcamera(vi.camera, camera_look)
    tx__Fworld__Fcamera = tx__Fworld__Frobot ∘ tx__Frobot__Fcamera
    visible_region = compute_visibility(tx__Fworld__Fcamera, vi.camera.frustum__Fcamera, vi.occluders__Fworld)
    push!(viz, visible_region)
  end

  VampState(state(q_from.robot, action.robot), viz)
end


@inline fragment(α::Number, a::VampAction{ROBOT, VIZ}) where {ROBOT, VIZ} = VampAction(fragment(α, a.robot), a.looks)

# prefer states with more views things
@inline savoriness(q::VampState) = length(q.viz)

zero(::Type{VampAction{ROBOT, VIZ}}) where {ROBOT, VIZ} = VampAction(zero(ROBOT), VISIBILITY_A[])

#= ugh what am i doing
# return robot pieces that are not covered by the visibility region.
# visibility state is not in VampState, but in path to root.
function get_uncovered_robot__Fworld(
    vi::VampInstance,
    start::VampState{ROBOTSTATE, VIZSTATE},
    actions::AbstractVector{VampAction{ROBOTACT, VIZACT}},
    ) where {ROBOTSTATE, VIZSTATE, ROBOTACT, VIZACT}

  # Checks if Moves (including the 1st) are covered by Looks.
  # TODO check swept volumes
  # TODO get POLY from `start`
  uncovered_robot__Fworld = UnionOfConvex(POLY[])
  q_this = start
  for action in actions
    v_next = state(q_this, action, vi)  # TODO use mutating version to add to accumulated_visibility
    accumulated_visibility = v_next.viz
    robot__Fworld = get_robot__Fworld(vi.robot__Frobot, v_next.robot)
    this_uncovered = difference(robot__Fworld, accumulated_visibility)
    append!(uncovered_robot__Fworld.pieces, this_uncovered.pieces)
  end
  return uncovered_robot__Fworld
end

# different computational perspective, where robot pieces are accumulated in reverse
function get_uncovered(
  vi::VampInstance,
  query_poly__Fworld::UnionOfConvex
  end_node::NODE,
  tree::AbstractVector{NODE}) where {NODE}

  remaining_query_poly__Fworld = query_poly__Fworld
  this_node = end_node
  visited = Set{Int64}()
  while true
    prev_node = tree[this_node.parent_index]
    tx__Fworld__Frobot = get_tx__Fworld__Frobot(prev_node.robot)
    action = this_node.action

    for camera_look in action.looks
      tx__Frobot__Fcamera = get_tx__Frobot__Fcamera(vi.camera, camera_look)
      tx__Fworld__Fcamera = tx__Fworld__Frobot ∘ tx__Frobot__Fcamera
      visible_region = compute_visibility(tx__Fworld__Fcamera, vi.camera.frustum__Fcamera, vi.occluders__Fworld)
      push!(viz.pieces, visible_region)
    end

  remaining_query_poly__Fworld = difference(remaining_query_poly__Fworld, )

  robot__Fworld = get_robot__Fworld(vi.robot__Frobot, v_next.robot)
end
=#


struct VampRRTAlgorithm{STATE, TX, POLY}
  rrt::RRTAlgorithm{STATE}
  visibility_instance::VisibilityInstance{TX, POLY}
end

#= ugh
function indices_from_root(tree, node_index)
  indexs = Int64[]
  push!(indexs, node_index)
  while true
    i = tree[indexs[end]].parent_index
    if i in indexs # TODO make O(1)
      break
    end
    push!(indexs, i)
  end
  return reverse(indexs)
end
=#

@inline function step!(alg::VampRRTAlgorithm, q_rand, plt)
  node_nearest_i, d_nearest = find_nearest(alg.rrt, q_rand)
  q_from = alg.rrt.tree[node_nearest_i].state
  a_full = action(q_rand, q_from)
  # don't make long extensions
  l = min(1, alg.rrt.instance.extension_length / distance(q_from, q_rand))
  a_partial = fragment(l, a_full)
  q_new = state(q_from, a_partial, alg.visibility_instance)
  # check collisions
  if !alg.rrt.instance.mp.q_valid_test(q_new) return nothing end
  if !alg.rrt.instance.mp.qq_valid_test(q_new, q_from) return nothing end

  # check visibility
  robot__Fworld = get_robot__Fworld(alg.visibility_instance.robot__Frobot, q_new.robot)
  uncovered_pieces__Fworld = difference(robot__Fworld, q_new.viz)

  if length(uncovered_pieces__Fworld.pieces) > 0
    # the movement is not vision safe
    rand_uncovered_piece__Fworld = rand(uncovered_pieces__Fworld.pieces)
    uncovered_centroid__Fworld = mean(collect(points(rand_uncovered_piece__Fworld))) # arbitrary. not really center
    if plt != nothing
      plot_polyhedron!(plt, rand_uncovered_piece__Fworld; color=:red)
      scatter!(plt, [(uncovered_centroid__Fworld...,)]; marker=:o, color=:green)
    end
    tx__Frobot__Fworld = inv(get_tx__Fworld__Frobot(q_from.robot))
    tx__Fcamerabase__Frobot = inv(alg.visibility_instance.camera.tx__Frobot__Fcamerabase)

    uncovered_centroid__Fcamerabase = tx__Fcamerabase__Frobot(tx__Frobot__Fworld(uncovered_centroid__Fworld))
    θ = atan(uncovered_centroid__Fcamerabase[2], uncovered_centroid__Fcamerabase[1])
    a_retry = zero(typeof(a_partial))
    push!(a_retry.looks, SO2LookAction(Rot(θ)))
    q_retry = state(q_from, a_retry, alg.visibility_instance)

    # TODO now try to move!
    node_new = RRTNodeAction(q_retry, a_retry, node_nearest_i)
    push!(alg.rrt.tree, node_new)
  else
    # it is a vision-safe movement
    node_new = RRTNodeAction(q_new, a_partial, node_nearest_i)
    push!(alg.rrt.tree, node_new)
    if q_new ∈ alg.rrt.instance.mp.q_goal
      push!(alg.rrt.reached_goals, length(alg.rrt.tree))
    end
  end

  return nothing
end



struct RetroVampRRTAlgorithm{STATE, CAM, POLY, ROBOT, VR}
  rrt::RRTAlgorithm{STATE}  # STATE is q of robot only, not visiblity state.
  visibility_instance::VisibilityInstance{CAM, POLY, ROBOT}
  visible_region::VR  # the whole tree has a visible region.
end

function setup!(alg::RetroVampRRTAlgorithm, n)
  setup!(alg.rrt, n)
  empty!(alg.rrt.tree) # setup!(::RRTAlgorithm) adds initial node. We want to add it in our way.
  empty!(alg.visible_region)
  NODE = get_node_type(alg.rrt)

  dummy_action = action(NODE) == Nothing ? nothing : zero(action(NODE))

  add_configuration!(alg, NODE(alg.rrt.instance.mp.q_start, dummy_action, ROOT_SENTINEL))
end

#= TODO specialize on CAM type so that it doesn't need to be commented out
@inline function add_configuration!(alg::RetroVampRRTAlgorithm, node_new)
  # maintain invariants of algorithm
  vi = alg.visibility_instance

  push!(alg.rrt.tree, node_new)
  q_robot = node_new.state

  tx__Fworld__Frobot = get_tx__Fworld__Frobot(q_robot)
  tx__Frobot__Fcamerabase = vi.camera.camera.tx__Frobot__Fcamerabase
  frustum_union__Fcamerabase = vi.camera.frustum_union__Fcamerabase

  tx__Fworld__Fcamerabase = tx__Fworld__Frobot ∘ tx__Frobot__Fcamerabase
  @assert length(frustum_union__Fcamerabase.pieces) == 1 # TODO generalize this
  potential__Fcamerabase = frustum_union__Fcamerabase.pieces[1]

  visible_region = compute_visibility(tx__Fworld__Fcamerabase, potential__Fcamerabase, vi.occluders__Fworld)
  push!(alg.visible_region, visible_region) # should be AbstractVector{Difference}, in this case
end
=#

@inline function add_configuration!(alg::RetroVampRRTAlgorithm, node_new)
  # maintain invariants of algorithm
  vi = alg.visibility_instance

  push!(alg.rrt.tree, node_new)
  q_robot = node_new.state

  tx__Fworld__Frobot = get_tx__Fworld__Frobot(q_robot)
  tx__Frobot__Fcamerabase = vi.camera.camera.tx__Frobot__Fcamerabase
  frustum_union__Fcamerabase = vi.camera.frustum_union__Fcamerabase

  tx__Fworld__Fcamerabase = tx__Fworld__Frobot ∘ tx__Frobot__Fcamerabase
  potential__Fcamerabase = frustum_union__Fcamerabase

  visible_region = compute_visibility(tx__Fworld__Fcamerabase, potential__Fcamerabase, vi.occluders__Fworld)
  push!(alg.visible_region, visible_region) # should be AbstractVector{Difference}, in this case
end

function get_visible_region__Fworld(visibility_instance, q_robot)
  vi = visibility_instance
  tx__Frobot__Fcamerabase = vi.camera.camera.tx__Frobot__Fcamerabase
  frustum_union__Fcamerabase = vi.camera.frustum_union__Fcamerabase

  tx__Fworld__Frobot = get_tx__Fworld__Frobot(q_robot)
  tx__Fworld__Fcamerabase = tx__Fworld__Frobot ∘ tx__Frobot__Fcamerabase
  potential__Fcamerabase = frustum_union__Fcamerabase

  visible_region = compute_visibility(tx__Fworld__Fcamerabase, potential__Fcamerabase,
    UnionOfConvex(vcat(
      vi.occluders__Fworld.pieces,
      vi.get_robot_occluders__Fworld(q_robot).pieces)))
  return visible_region
end

function get_generator_visible_region__Fworld(visibility_instance, qs_covering)
  visible_regions__Fworld = (
    get_visible_region__Fworld(visibility_instance, q_robot)
    for q_robot in qs_covering
  )
  return visible_regions__Fworld
end

# so long as difference is defined appropriately, visible_region can be an iterable of regions or just one.
function get_uncovered_pieces__Fworld(visibility_instance, q, visible_region)
  robot__Fworld = transform(get_tx__Fworld__Frobot(q), visibility_instance.robot__Frobot)
  uncovered_pieces__Fworld = difference(robot__Fworld, visible_region)
  return uncovered_pieces__Fworld
end

# only hacked together for debugging
function get_covered_pieces__Fworld(visibility_instance, q, visible_region)
  robot__Fworld = transform(get_tx__Fworld__Frobot(q), visibility_instance.robot__Frobot)
  uncovered_pieces__Fworld = difference(robot__Fworld, visible_region) # should be intersection
  return setdiff(robot__Fworld, uncovered_pieces__Fworld)
end


# TODO use bounding volumes to quickly reject configurations that don't cover q
function get_uncovered_pieces__Fworld_from_qs_covering(visibility_instance, q, qs_covering, initial_visibility=[])
  generator_visible_region_covering__Fworld = get_generator_visible_region__Fworld(visibility_instance, qs_covering)
  generator_visible_region__Fworld = IterTools.chain(initial_visibility, generator_visible_region_covering__Fworld)
  return get_uncovered_pieces__Fworld(visibility_instance, q, generator_visible_region__Fworld)
end

@inline function step!(alg::RetroVampRRTAlgorithm, q_rand, plt)
  node_nearest_i, d_nearest = find_nearest(alg.rrt, q_rand)
  NODE = get_node_type(alg.rrt)
  q_from = alg.rrt.tree[node_nearest_i].state
  a_full = action(q_rand, q_from)
  # don't make long extensions
  l = min(1, alg.rrt.instance.extension_length / distance(q_from, q_rand))
  a_partial = fragment(l, a_full)
  q_new = state(q_from, a_partial)
  # check collisions
  if !alg.rrt.instance.mp.q_valid_test(q_new) return nothing end
  if !alg.rrt.instance.mp.qq_valid_test(q_new, q_from) return nothing end

  # check visibility
  #robot__Fworld = get_robot__Fworld(alg.visibility_instance.robot__Frobot, q_new.robot)
  #uncovered_pieces__Fworld = difference(robot__Fworld, alg.visible_region)  #TODO check in smarter order

  #if length(uncovered_pieces__Fworld.pieces) > 0
  uncovered_pieces__Fworld = get_uncovered_pieces__Fworld(alg.visibility_instance, q_new, alg.visible_region)
  if !isempty(uncovered_pieces__Fworld)
    # the movement is not vision safe
    rand_uncovered_piece__Fworld = rand(uncovered_pieces__Fworld)
    uncovered_centroid__Fworld = rand_uncovered_piece__Fworld
    if plt != nothing
      plot_polyhedron!(plt, rand_uncovered_piece__Fworld; color=:red)
      scatter!(plt, [(uncovered_centroid__Fworld...,)]; marker=:o, color=:green)
    end
    return nothing
  else
    # it is a vision-safe movement
    node_new = NODE(q_new, a_partial, node_nearest_i)
    add_configuration!(alg, node_new) # only mutating step
    if q_new ∈ alg.rrt.instance.mp.q_goal
      push!(alg.rrt.reached_goals, length(alg.rrt.tree))
    end
  end

  return nothing
end


function get_visible_region_geos_path__Fworld(parameters, q_path)
  LibGEOS = VAMP.LibGEOSInterface.LibGEOS
  @unpack visibility_instance = parameters
  return [LibGEOS.Polygon(VAMP.get_visible_region__Fworld(visibility_instance, q_see)) for q_see in q_path]
end
"""
produce sequence of LibGEOS AbstractGeometry corresponding to v-safe violations of a path
workspace must be 2D
"""
function get_incremental_violations__Fworld(parameters, q_path; visibility_path__Fworld=get_visible_region_geos_path__Fworld(parameters, q_path))
    LibGEOS = VAMP.LibGEOSInterface.LibGEOS

    @unpack polys_robot__Frobot = parameters
    @unpack visibility_instance = parameters

    # we will be creating the visible region from the starting_visibility and configuration path
    # this is because the planner prunes (compresses) visible regions based on approximations
    # so we really should re-create.
    @unpack starting_visibility__Fworld = parameters
    @assert length(starting_visibility__Fworld) == 1
    starting_visibility__Fworld = starting_visibility__Fworld[1]
    starting_visibility_geos__Fworld = LibGEOS.Polygon(starting_visibility__Fworld)

    get_incremental_violations__Fworld = Any[nothing for i = 1:length(q_path)]
    for i = 1:length(q_path)
        q_be = q_path[i]
        poly_robot__Fworld = VAMP.get_robot__Fworld(polys_robot__Frobot, q_be).pieces[1]
        polygeos_robot__Fworld = LibGEOS.Polygon(poly_robot__Fworld)

        uncovered = LibGEOS.difference(polygeos_robot__Fworld, starting_visibility_geos__Fworld)
        for j = 1:i
            uncovered = LibGEOS.difference(uncovered, visibility_path__Fworld[j])
        end
        get_incremental_violations__Fworld[i] = uncovered
    end
    return get_incremental_violations__Fworld
end