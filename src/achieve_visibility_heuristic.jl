function polys_as_line_segments(polys_obstacles)
  segments = LineSegment{Point{2, Float64}}[]

  for poly in polys_obstacles
    vertices = VAMP.convexhull_andrew(collect(points(poly)))
    n = length(vertices)
    for i = 1:n
      push!(segments, LineSegment(vertices[i], vertices[mod1(i + 1, n)]))
    end
  end

  return segments
end


"""
solve Eikonol equation on 2D grid, subject to obstacles

env_segments is a list of segments.
starting_set is a set of points, assumed to not be in collision.

returns Dict{Point, Cost}
"""
function floodfill(starting_set, raster_spec, env_segments; smooth_connected=false)
  starting_set_d = [begin
    Iu = VAMP.coordinates_inv(raster_spec, point)
    I = Int.(round.(Iu)) # TODO enforce rounding up to ϵ away, otherwise error
    (I...,)
  end for point in starting_set]

  function d_to_c(d)  # grid to continuous
    T = VAMP.coordinates(raster_spec)
    #Point(T[1][d[1]], T[2][d[2]])
    Point((Base.unsafe_getindex(T[i], d[i]) for i in 1:2)...)
  end

  ai = LazyAStarInstance(
    (s_to, s_fro) -> norm(d_to_c(s_to) - d_to_c(s_fro)),
    (s) -> 0.0, # uniform cost search
    (s) -> false, # search will terminate by emptying agenda
    function (set, s_fro)
      x, y = s_fro
      q_news = (
        (x, y + 1),
        (x, y - 1),
        (x + 1, y),
        (x - 1, y),
      )
      if smooth_connected
        q_news =(q_news...,
                (x + 1, y + 1),
                (x - 1, y + 1),
                (x + 1, y - 1),
                (x - 1, y - 1),

                (x + 2, y + 1),
                (x - 2, y + 1),
                (x + 2, y - 1),
                (x - 2, y - 1),

                (x + 1, y + 2),
                (x - 1, y + 2),
                (x + 1, y - 2),
                (x - 1, y - 2))
        end

      for s_to in q_news
        test_segment = LineSegment(d_to_c(s_fro), d_to_c(s_to))
        clear = true
        if !contains(raster_spec.limits, d_to_c(s_to))
          continue
        end

        for env_segment in env_segments
          if intersects(env_segment, test_segment)[1]
            clear = false
            break
          end
        end

        if clear
          push!(set, s_to)
        end
      end
      nothing
    end,
    VAMP.AlwaysValid()
  )


  alg = LazyAStarAlgorithm(ai, NTuple{2, Int64}, Float64)

  # setup other stuff, but not agenda.
  setup!(alg, first(starting_set_d), 0.0)

  while !isempty(alg.agenda) DataStructures.dequeue!(alg.agenda) end # at time of writing, there is no empty! method
  for p in unique(starting_set_d) # because of discretization, starting_set_d could contain duplicates
    DataStructures.enqueue!(alg.agenda, VAMP.LazyAStarNode(p, p, 0.0)=>0.0)
  end

  while !isempty(alg.agenda)
    # println(length(alg.closed_parents), " ",length(alg.agenda))
    step!(alg)
  end

  F = Dict(d_to_c(pt)=>cost for (pt, (pt_pre, cost)) in alg.closed_parents)
  return F
end


function grid_render!(raster_volume, dict)
  for (point, value) in dict
    Iu = VAMP.coordinates_inv(raster_volume.spec, point)
    I = Int.(round.(Iu)) # TODO error on big rounding error
    if !all(raster_volume.spec.size .≥ I .≥ [1,1]); continue; end
    raster_volume.data[I...] = value
  end
  nothing
end

function make_coverage_heuristic(workspace_uncovered, polys_obstacles__Fworld, workspace_points_raster_spec__Fworld;
  smooth_connected=false)
  eikonal = floodfill(
    workspace_uncovered, workspace_points_raster_spec__Fworld,
    polys_as_line_segments(polys_obstacles__Fworld.pieces), smooth_connected=smooth_connected)

  function evaluate_view(packed_view)
    best = Inf
    for point in keys(eikonal)
      #if isinside(view, point)
      if VAMP.count_packed_viewvolume_in(point, packed_view.arc_regions, packed_view.shadow_halfspaces) > 0
        # when visibility needs to bend a corner, the best eikonal point can remain the same, even as the robot moves closer to it.
        # also it would make sense to favor view angles that align with the gradient of the eikonal field.
        d = norm(point - packed_view.arc_regions[1].center) # assume there's only one arc region.
        best = min(best, eikonal[point] + 1e-4*d)
      end
    end
    return best
  end

  function profiled_evaluate_view(view)
    try
      status = Base.Profile.start_timer()
      if status < 0
        Base.Profile.error(Base.Profile.error_codes[status])
      end
      return evaluate_view(view)
    finally
      Base.Profile.stop_timer()
    end
  end

  return evaluate_view
end

#=


let plt = Plots.plot(;aspect_ratio=1, reuse=false)

  rv = VAMP.RasterVolume(workspace_points_raster_spec__Fworld, zeros(workspace_points_raster_spec__Fworld.size...))
  rv.data[:] = Inf

  grid_render!(rv, h_view.eikonal)
  Plots.heatmap!(plt, VAMP.coordinates(rv.spec)..., rv.data')

  for obstacle in polys_obstacles__Fworld.pieces
      plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
  end

  display(plt)
end
=#
