export VAVPInstance, VAVPAlgorithm
@with_kw struct VAVPInstance
  timeouts = (improvement=4.0, failure=600.0)
  start_state
  vamp_pieces
end

@with_kw mutable struct VAVPAlgorithm
  instance
  stack = []
  final_search_summaries = []
  path = nothing
  timer = TimerOutputs.TimerOutput()
  search_result = nothing
end

function setup!(alg::VAVPAlgorithm, coverage_goal)
  empty!(alg.stack)
  empty!(alg.final_search_summaries)
  push!(alg.stack, coverage_goal)
  global debugshit[:vavp_stack_provenance] = Any["given_goal"]
  TimerOutputs.reset_timer!(alg.timer)
end

debugshit = Dict{Symbol, Any}()

function step!(alg::VAVPAlgorithm)
  println("vavp step begin!")
  @show length(alg.stack)
  global debugshit
  recursion_depth = length(length(alg.stack)) - 1
  if recursion_depth > 10
    return error("seems like step! vavp is looping.")
  end
  retval = nothing
  @timeit alg.timer "step!" begin
    @timeit alg.timer "depth: $(recursion_depth)" begin
      @unpack explore_search = alg.instance.vamp_pieces
      @unpack process_explore_path = alg.instance.vamp_pieces
      @unpack vsafe_split_path = alg.instance.vamp_pieces
      @unpack visibility_violations_path = alg.instance.vamp_pieces
      @unpack get_visible_region_cached__Fworld = alg.instance.vamp_pieces

      this_coverage_goal_region = alg.stack[end]
      prohibited = reduce(union, alg.stack)

      start_state = alg.instance.start_state
      explore_search_result = explore_search(start_state, this_coverage_goal_region, prohibited, Val{true}; timeouts=alg.instance.timeouts, log_header="\n\nexplore vavp relaxed: $(recursion_depth)\n")
      push!(alg.final_search_summaries, explore_search_result.explore_search_result.search_summaries[end].summary)

      alg.search_result = explore_search_result.explore_search_result
      #@pack! debugshit = explore_search_result
      # explore_search_result could certainly fail if `prohibited` gets big enough to cause blockage
      if explore_search_result.explore_search_result.explore_status == :explore_fail
        retval = (keepgoing=false, reason=:failure)
      else
        explore_process_result = process_explore_path(explore_search_result.explore_alg, start_state)
        #@pack! debugshit = alg
        #debugshit[:path_to_add_vavp] = explore_process_result.explore_path_to_add
        (explore_safe, explore_unsafe) = vsafe_split_path(explore_process_result.explore_path_to_add);

        coverage_goal_remaining = difference(this_coverage_goal_region, CoveragePath(unlift_q(explore_safe), get_visible_region_cached__Fworld))

        if length(coverage_goal_remaining) < length(this_coverage_goal_region)
          alg.path = explore_safe # this path sees some of what we wanted, or enables seeing some of what we wanted
          retval = (keepgoing=false, reason=:done)
        else
          new_coverage_goal_region = visibility_violations_path(explore_process_result.explore_path_to_add)
          push!(alg.stack, new_coverage_goal_region)
          # used by test/vavp.jl for generating figure.
          global debugshit
          push!(debugshit[:vavp_stack_provenance], explore_process_result.explore_path_to_add)
          #@pack! debugshit = new_coverage_goal_region
          retval = (keepgoing=true, reason=:more_goal)
        end
      end
    end
  end
  @assert retval != nothing
  println("vavp step end")
  @show retval
  return retval
end


function vavp_search(start_state, coverage_goal, vamp_pieces)
  vavpi = VAVPInstance(vamp_pieces=vamp_pieces, start_state=start_state)
  vavpa = VAVPAlgorithm(instance=vavpi)
  setup!(vavpa, coverage_goal)

  r = nothing
  while true
    r = step!(vavpa)
    if !r.keepgoing
      break
    end
  end
  println("vavp done\n")
  explore_path_to_add = vavpa.path

  return (
    explore_path_to_add=vavpa.path,
    final_search_summaries=vavpa.final_search_summaries,
    explore_status=r.reason,
    coverage_goal=coverage_goal,
    vavp_alg=vavpa,
    search_result=vavpa.search_result)
end
