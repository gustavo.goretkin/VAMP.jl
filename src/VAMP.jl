module VAMP
using Nullables

using Reexport
@reexport using GeometryTypes
@reexport using StaticArrays
@reexport using CoordinateTransformations
@reexport using Polyhedra
using DataStructures

import JuMP
import Cbc

import Parameters: @pack!, @unpack, @with_kw


using LinearAlgebra
using Statistics
using Printf

include("geometry.jl")
include("util.jl")
include("motion_planning_state.jl")
include("planning.jl")
include("a_star.jl")
include("optimal_shortcut.jl")
include("visibility.jl")
include("visibility_state.jl")

include("frustum.jl")

include("plot.jl")
include("plot_geometry.jl")

include("vamp_domain.jl")

include("make_animation.jl")
include("animate_path.jl")

include("tree.jl")
include("tree_search.jl")

include("render.jl")
#include("cuda_render.jl")

include("plan_views.jl")

include("achieve_visibility_heuristic.jl")
include("vamp_pieces.jl")
include("two_phase.jl")
include("vavp.jl")
include("g_star.jl")
include("g_star_tree_vis.jl")
end
