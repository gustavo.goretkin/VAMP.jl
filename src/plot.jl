export plot_polyhedron!, plot_segment!, scatter_points!, plot_raster!, visibility_colorbar

using RecipesBase
using IterTools
using Plots

@recipe function f(::HalfSpace)
  [:t, :tr, :r, :br, :b, :bl, :l, :tl]
  [:t, :tr, :r, :br, :b, :bl, :l, :tl]

end

function bounded_segment(a, b, c, xlim, ylim)
  #points on the line ax + by = c, on xlim and ylim
  # TODO promotion issues
  if a == 0
    x1, x2 = xlim
    y1 = y2 = c/b
    return [(x1, y1), (x2, y2)]
  elseif b ==0
    y1, y2 = ylim
    x1 = x2 = c/a
    return [(x1, y1), (x2, y2)]
  else
    yox = x -> (c - a*x) / b
    xoy = y -> (c - b*y) / a

    x1x, x2x = xlim
    y1x, y2x = yox.(xlim)

    y1y, y2y = ylim
    x1y, x2y = xoy.(ylim)

    is_inside = (x, y) -> xlim[1] ≤ x ≤ xlim[2] && ylim[1] ≤ y ≤ ylim[2]
    inside = filter(xy->is_inside(xy...), [(x1x, y1x), (x2x, y2x), (x1y, y1y), (x2y, y2y)])
    if length(inside) < 2
      # segment not visible in current limits
      return [(x1x, y1x), (x2x, y2x)] # arbitrary
    end
    return [inside[1], inside[2]]
  end
end

#TODO keyword argument `aspect_ratio` not working on those
"Adds ax+by=c... straight line over the current plot"
function abcline!(plt::Plots.Plot, a, b, c; kw...)
    segment = bounded_segment(a, b, c, xlims(plt), ylims(plt))
    plot!(plt, segment; kw...)
end

abcline!(args...; kw...) = abcline!(current(), args...; kw...)

function halfspace!(plt::Plots.Plot, a, b, c; kw...)
  segment = bounded_segment(a, b, c, xlims(plt), ylims(plt))
  plot!(plt, segment; kw...)
  # tuples aren't vectors. plot doesn't plot vectors as points. need to convert between.
  ((x1, y1), (x2, y2)) = segment
  center = [mean([x1,x2]), mean([y1,y2])]
  normal = [a, b] # assumes aspect_ratio=1, otherwise "normal" won't be normal.
  tip = center + normalize(normal) * .1 * (xlims(plt)[2] - xlims(plt)[1])
  plot!(plt, [tuple(center...), tuple(tip...)]; kw...) #TODO separate kwargs
end

halfspace!(args...; kw...) = halfspace!(current(), args...; kw...)


"returns contiguous parts where its inside,"
function contiguous_segments(collection, isinside)
  segments = []
  push!(segments, [])
  last === nothing
  for point in collection
    if isempty(segments[end]) # haven't started yet
      if isinside(point)
        if last != nothing
          push!(segments[end], last)
        end
        push!(segments[end], point)
      end
    else
      if !isinside(point)
        push!(segments[end], point)
        push!(segments, []) #  start a new thing.
      end
    end
    last = point
  end
  return segments
end

"Take a sequence of integers, and compress into ranges."
function compress_range(collection)
  ranges = UnitRange{Int64}[]

  for p in collection
    if isempty(ranges)
      push!(ranges, p:p)
      continue
    end
    current_range = ranges[end]
    if p == last(current_range) + 1
      ranges[end] = first(current_range):p
    else
      push!(ranges, p:p)
    end
  end
  return ranges
end

function render(plt::Plots.Plot, p::Polyhedron)
  @assert fulldim(p) == 2
  ((xhull, yhull), (xline, yline)) = renderclip(xlims(plt), ylims(plt), p)
  return (Shape(xhull, yhull), (xline, yline))
end

function renderclip(xlims, ylims, p::Polyhedron)
  @assert fulldim(p) == 2
  # make a clipping polygon to turn the unbounded polygon into a bounded one.
  # this will introduce points on the rays (and also points at the corner of the view window...)
  xlim_poly = extrema(map(xy->xy[1], points(p)))
  ylim_poly = extrema(map(xy->xy[2], points(p)))
  xspan_poly = xlim_poly[2] - xlim_poly[1]
  yspan_poly = ylim_poly[2] - ylim_poly[1]

  # expand for two reasons:
  # 1 don't introduce new points except for on rays (don't get close to other corners)
  # 2 guarantee a little bit of the ray is shown.
  W = one(Polyhedra.coefficient_type(p)) / 10
  xlim_poly_expand = (xlim_poly[1] - W*xspan_poly, xlim_poly[2] + W*xspan_poly)
  ylim_poly_expand = (ylim_poly[1] - W*yspan_poly, ylim_poly[2] + W*yspan_poly)

  # the only points outside this limit will be the ray points (or spurious points from clip_poly)
  xlim_poly_mid = (xlim_poly[1] - W/2*xspan_poly, xlim_poly[2] + W/2*xspan_poly)
  ylim_poly_mid = (ylim_poly[1] - W/2*yspan_poly, ylim_poly[2] + W/2*yspan_poly)

  x_clip = extrema(tuple(xlims..., xlim_poly_expand...))
  y_clip = extrema(tuple(ylims..., ylim_poly_expand...))

  # clip poly is slightly bigger than test poly which is slightly bigger than the view port..
  test_poly = poly_from_lims(xlim_poly_mid, ylim_poly_mid)
  clip_poly = poly_from_lims(x_clip, y_clip)

  clipped_poly = polyhedron(intersect(hrep(clip_poly), hrep(p)))

  clipped_points = collect(points(clipped_poly))
  hull_fill = map(x->[x...], convexhull_andrew(map(x->Point(x...), clipped_points)))

  xhull, yhull = [p[1] for p in hull_fill], [p[2] for p in hull_fill]

  isinside(point) = ininterior(point, hrep(test_poly))

  w = 1:length(hull_fill)
  mask =  findall(map(isinside, hull_fill))
  ranges = compress_range(mask)

  # expand each range 1 in each direction, if possible
  for i =  1:length(ranges)
    r = ranges[i]
    ranges[i] = max(first(r)-1, first(w)):min(last(r)+1, last(w))
  end

  # interrupt line with nans, so that all strokes are on same series, with same color.
  NaNpoint = (NaN, NaN)

  line = []
  for r in ranges
    for i in r
      push!(line, tuple(hull_fill[i]...))
    end
    push!(line, NaNpoint)
  end

  return ((xhull, yhull), ([t[1] for t in line], [t[2] for t in line]))
end

function plot_polyhedron!(plt::Plots.Plot, p::Polyhedron; kw...)
  @assert fulldim(p) == 2
  # the check for an empty polygon is not useful unless the representation is minimal.
  p = deepcopy(p)
  removehredundancy!(p)
  removevredundancy!(p)

  if length(points(p)) == 0
    return plt
  end

  if hasrays(p)
    (shape, (x,y)) = render(plt, p)
    plot!(plt, shape; linecolor=nothing, kw...)
    plot!(plt, x, y; kw...)
  else
    plot!(plt, p; kw...)
  end
  plt
end

function plot_polyhedron(p::Polyhedron; kw...)
  @assert fulldim(p) == 2
  plot_polyhedron!(current(), p; kw...)
end

function plot_polyhedron!(plt, ps::UnionOfConvex{POLY}; kw...) where {POLY <: Polyhedron}
    for p in ps.pieces
        plot_polyhedron!(plt, p; kw...)
    end
end

function plot_segment!(plt, segment::LineSegment{Point{2, T}}; kw...) where {T}
  Plots.plot!(plt, map(x->tuple(x...), collect(segment)); legend=false, kw...)
end

function scatter_points!(plt, points; kw...)
  Plots.scatter!(plt, collect(map(x->tuple(x...), points)); kw...)
end

function scatter_points!(plt, points::Set; kw...)
  scatter_points!(plt, collect(points); kw...)
end

arraycycle(v) = vcat(v, [v[1]])

 aaaaaaaa = 3
function plot_polyhedron_mpl!(ax, ps::UnionOfConvex{POLY}; kw...) where {POLY <: Polyhedron}
  for poly in ps.pieces
    @assert fulldim(poly) == 2
    vertices = arraycycle(VAMP.convexhull_andrew(collect(Polyhedra.points(poly))))
    global aaaaaaaa = vertices
    patch = to_mpl_patch(Plots.Shape(map(x->[x...], zip(vertices...))...); kw...)
    ax.add_patch(patch)
  end
end

function visibility_colorbar()
  # make 0 plot as white, and anything else yellow-orange
  cmap = Plots.cgrad([:yellow, :orange])
  cmap.values[1] = eps()
  prepend!(cmap.values, [0.0])
  prepend!(cmap.colors, [Plots.RGBA{Float64}(1.0,1.0,1.0,1.0)])
  return cmap
end


function plot_raster!(plt, raster_volume; kwargs...)
  Plots.heatmap!(plt, VAMP.coordinates(raster_volume.spec)..., raster_volume.data'; colorbar=false, kwargs...)
end


import Base.convert
function convert(::Type{Val{Symbol("PyPlots.PathPatch")}}, shape::Plots.Shape;)
  # assumes PyPlot backend is loaded in Plots
  path = Plots.pypath["Path"](hcat(shape.x, shape.y))
  patches = Plots.pypatches["PathPatch"](path)
  return patches
end


function to_mpl_patch(shape::Plots.Shape; kw...)
  # assumes PyPlot backend is loaded in Plots
  path = Plots.pypath["Path"](hcat(shape.x, shape.y))
  patches = Plots.pypatches["PathPatch"](path; kw...)
  return patches
end

function pyplot_polyhedron3d!(ax, plt::Plots.Plot{Plots.PyPlotBackend}, p::Polyhedron, plot_kwargs=Dict(), lift3d_kwargs=Dict())
  @assert fulldim(p) == 2
  #display(plt) # doesn't work unless displayed
  (shape, (x,y)) = render(plt, p)
  pathpatch = convert(Val{Symbol("PyPlots.PathPatch")}, shape)
  ax[:add_patch](pathpatch; plot_kwargs...)
  PyPlot.mplot3d[:art3d][:pathpatch_2d_to_3d](pathpatch; lift3d_kwargs...)
  return nothing
end
