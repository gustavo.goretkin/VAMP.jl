# plot VAMP things. broken out of plot.jl so that plot.jl could be imported without VAMP

# depends on VAMP.ArcRegion
function render(arc::ArcRegion{2, T}, n) where {T}
  half_fov = acos(arc.cos_half_fov)
  p = [(convert(LinearMap, VAMP.Rot(θ)))(arc.radius * arc.direction) + arc.center
    for θ in range(-half_fov, stop=+half_fov, length=n)]
  push!(p, arc.center)
  t = map(x->tuple(x...), p)
  return Shape(t)
end

# relies on definition of SE2State
function plot_tree_se2!(plt, tree)
  plot3d!(;
    xlabel="x (m)",
    ylabel="y (m)",
    zlabel="θ (°)",
    zlim=(0.0, 360.0),
    legend=nothing)

  flatten(state) = tuple(state.e.q..., rad2deg(state.θ.q.θ))

  for node in tree
    to_state = node.state
    from_state = tree[node.parent_index].state
    # tree edge
    plot3d!(plt,
      [flatten(node) for node in [from_state, to_state]], color=:black)
  end

  scatter3d!(plt, [flatten(node.state) for node in tree], color=:blue, alpha=0.2)

  return plt
end

# depends on VAMP.BoundingBox
struct RasterVolumeSpec{N, T}
  limits::BoundingBox{N, T}
  size::NTuple{N, Int64}
end

struct RasterVolume{S, A}
  spec::S
  data::A
end


function coordinates(v::RasterVolumeSpec{N}) where {N}
  return tuple([range(v.limits.minimum[i], stop=v.limits.maximum[i], length=v.size[i]) for i=1:N]...)
end

@inline function coordinates(v::RasterVolumeSpec, I)
  extent__c = v.limits.maximum - v.limits.minimum
  # other choices possible. this is consistent with Plots.heatmap
  x__f = (SVector(I...) .- 1) ./ (SVector(v.size...) .- 1)
  x__c = x__f .* extent__c + v.limits.minimum
  return x__c
end

"doesn't return integers"
@inline function coordinates_inv(v::RasterVolumeSpec, x__c)
  extent__c = v.limits.maximum - v.limits.minimum
  x__f = (x__c - v.limits.minimum) ./ extent__c

  # other choices possible. this is consistent with Plots.heatmap
  I = ((x__f .* (SVector(v.size...) .- 1)) .+ 1)
  return I
end

"""
f takes continuous value as SVector, and current v.data, and returns what can be written into v.data
v.data must be at least length 1 in each dimension
"""
function render!(v::RasterVolume, f, f_params=())
  extent__c = v.spec.limits.maximum - v.spec.limits.minimum

  for subs__d in CartesianIndices(v.data)
    x__c = coordinates(v.spec, Tuple(subs__d))
    v.data[subs__d] = f(x__c, v.data[subs__d], f_params...)
  end
end
