using DataStructures

myeps(::Type{T}) where {T} = zero(T)
myeps(::Type{T}) where {T<:AbstractFloat} = eps(T)

#bandaid for not understanding Polyhedra.jl API
simple_hrep(h::HRepresentation) = MixedMatHRep(h)

memoize_1 = Dict{Tuple{Polyhedron, Number}, Dict{Int, Vector{Int}}}()
memoize_2 = Dict{Any, Any}()

# return a dictionary D such that D[face_index] = [vertex_indices]
# which is a representation of the bi-partite graph of face and vertex incidence
function facet_vertex_connectivity(poly::Polyhedron, eps_scale=1)
  if haskey(memoize_1, (poly, eps_scale)); return memoize_1[(poly, eps_scale)]; end
  println("COMPUTING FACET VERTEX CONNECTIVITY")
  # this is not the best way to get this information, and it could probably fail due to numerical issues
  #d = dim(poly)
  h = simple_hrep(hrep(poly))
  v = vrep(poly)
  # each vertex in v is the result of intersecting d or more hyperplanes in h
  # if poly is unbounded, then it has rays. not sure what each ray is.

  # m[i,j] is the signed distance between the ith halfspace and jth vertex
  m = (h.A * v.V') .- h.b
  b = (abs.(m) .<= eps_scale * myeps(Polyhedra.coefficient_type(poly)))

  facet_to_vertices = Dict{Int, Vector{Int}}()
  for i = 1:size(m,1)
    facet_to_vertices[i] = findall(b[i,:])
  end
  memoize_1[(poly, eps_scale)] = facet_to_vertices
  return facet_to_vertices
end

# given an adjacency list, find nodes that share exactly m neighbors
# and produce a graph where those nodes are adjacent. Also annotate the edge with neighbors
function facet_facet_connectivity(facet_to_vertices::AbstractDict{FT, LVT}, m) where {FT, LVT}
  facets = keys(facet_to_vertices)
  facet_to_facets_edgets = DefaultDict(Vector{Tuple{FT, LVT}}) # the edgets are all made of m points, and simplices (points for polygons, edges for polyhedra, ...)
  for f1 in facets, f2 in facets
    if f1 == f2
        continue
    end
    v1 = facet_to_vertices[f1]
    v2 = facet_to_vertices[f2]
    v_common = intersect(Set(v1), Set(v2))
    n_common = length(v_common)
    # n_common should not be bigger than dim(poly) - 1 if this is a polytope. Could check this.
    if n_common == m
      push!(facet_to_facets_edgets[f1], (f2, sort(collect(v_common)))) # sort so that edgets are canonical
    end
  end
  return facet_to_facets_edgets
end

# returns mask such that mask[face_index] = true iff face is visible from point
function facet_visibility_mask(poly::Polyhedron, point)
  # TODO don't just hope the order of the HalfSpaces is preserved
  h = simple_hrep(hrep(poly))
  v = vrep(poly)
  visible = (h.A * point) .- h.b .>= 0 # rows of h.A point into polyhedron
  return visible
end

function facet_facet_connectivity(poly::Polyhedron)
  if haskey(memoize_2, poly); return memoize_2[poly]; end
  r = facet_facet_connectivity(facet_vertex_connectivity(poly), dim(poly) - 1)
  memoize_2[poly] = r
  return r
end

function vertex_pairs_on_face(poly::Polyhedron)
  # return pairs of vertices on same face grouped by facets
  fvc = facet_vertex_connectivity(poly)
  s = Set{Tuple{Int64, Int64}}()
  for vs in collect(values(fvc))
    n = length(vs)
    for i=1:n, j=(i+1):n
      push!(s, minmax(vs[i], vs[j]))
    end
  end
  return s
end

function horizon(facet_to_facets_edgets::AbstractDict, facet_visibility)
  # if all the faces are visible, then there's nothing to be done
  # if some face is not visible, and is neighbors with a visible face, that edge is part of the horizon

  #If I am visible, and all my facet neighbors are visible, then I don't contribute to the horizon

  horizon_edget = Set()
  for invisible_facet in Iterators.filter(fi->!facet_visibility[fi], keys(facet_to_facets_edgets))
    horizon_facet_edgets = Iterators.filter(x->facet_visibility[x[1]], facet_to_facets_edgets[invisible_facet])
    for horizon_facet_edget in horizon_facet_edgets
      push!(horizon_edget, horizon_facet_edget[2])
    end
  end
  return horizon_edget
end

function horizon(poly::Polyhedron, point)
  return horizon(facet_facet_connectivity(poly), facet_visibility_mask(poly, point))
end

# point is inside polyhedron. cannot compute a shadow
struct ShadowEyeCollision <: Exception end

function shadow(poly::Polyhedron, point)
  visibility_mask = facet_visibility_mask(poly, point)
  fvc = facet_vertex_connectivity(poly)
  vertices = collect(points(poly))
  visible_vertices_i = vcat(map(x->fvc[x], findall(visibility_mask))...) #duplicates, whatever.
  if length(visible_vertices_i) == 0
    throw(ShadowEyeCollision())
  end
  center_visible_region = mean(vertices[visible_vertices_i]) # only used to check to set the orientation of hyperplanes below

  # IGNORE HYPERPLANES TODO???
  shadow_h_list = collect(halfspaces(poly))[visibility_mask]
  horizon_ = horizon(facet_facet_connectivity(poly), visibility_mask)

  for edget in horizon_
    hp = GGHyperPlane(map(x->Point(x...), vertices[[edget...]])..., point)
    sense = -sign(signed_distance(hp, center_visible_region))
    push!(shadow_h_list, sense * HalfSpace(hp.normal, hp.d))
  end
  #@show map(typeof, shadow_h_list)
  return polyhedron(hrep(shadow_h_list))
end


function compute_visibility(tx__Fworld__Fcamera, poly_frustum__Fcamera, obstacles__Fworld)
  PT = eltype(obstacles__Fworld.pieces) # good if world is empty of obstacles.
  ###@show PT
  eye__Fworld = tx__Fworld__Fcamera(Point(0.0, 0.0)) # assume camera eye is at origin of Fcamera frame
  positive = transform(tx__Fworld__Fcamera, poly_frustum__Fcamera)
  bb = BoundingBox(positive)
  negatives = UnionOfConvex(
    #filter( # only consider shadows that have a chance of overlapping with the view frustum.
     # sh->overlaps(bb, BoundingBox(sh)),
      [shadow(obstacle, eye__Fworld) for obstacle in obstacles__Fworld.pieces]
      #)
      ) # should mark [] with type PT

  ###@show map(typeof, negatives.pieces)
  #negatives = UnionOfConvex(map(Polyhedra.polyhedron, negatives.pieces))
  ###@show map(typeof, negatives.pieces)
  negatives = UnionOfConvex(PT[n for n in negatives.pieces])
  ###@show map(typeof, negatives.pieces)
  ###@show eltype(negatives.pieces)
  return Difference(UnionOfConvex([positive]), negatives)
  #return Difference(negatives, UnionOfConvex(PT[]))
end

function compute_visibility(arc__Fworld::ArcRegion, obstacles__Fworld)
  PT = eltype(obstacles__Fworld.pieces) # good if world is empty of obstacles.
  ###@show PT
  eye__Fworld = arc__Fworld.center
  negatives = UnionOfConvex(  # TODO prune out shadows that don't intersect arc__Fworld region
      [shadow(obstacle, eye__Fworld) for obstacle in obstacles__Fworld.pieces]) # TODO add PT back in in case Array is empty.
  ###@show eltype(negatives.pieces)
  return DifferenceGeneral(arc__Fworld, negatives)
end

function compute_visibility(tx__Fworld__Fcamera, arc__Fcamera::ArcRegion, obstacles__Fworld)
  arc__Fworld = transform(tx__Fworld__Fcamera, arc__Fcamera)
  compute_visibility(arc__Fworld, obstacles__Fworld)
end
