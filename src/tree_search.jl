# Search for a path within a tree that satisfies a global property (visibility).
# The path may traverse the same edge in the tree more than once.

"""
Solution paths must start from tree[1] and end at tree[goal_index]
and must satisfy

isvalid(state_from::TreeSearchNode, new_edge::Tuple{INDEX, INDEX})
"""
struct TreeSearchInstance{TREE, INDEX, SAFE}
  tree::TREE
  goal_index::INDEX
  isvalid::SAFE
end


struct TreeSearchNode{P, V}
  path::P  # compressed path of tuples (i,j) which are indices into tree
  visited::V  # Set of indices into tree
end

import Base: ==, hash
# https://github.com/JuliaLang/julia/issues/4648
==(tsn1::TreeSearchNode{P, V}, tsn2::TreeSearchNode{P, V}) where {P,V} = (tsn1.path == tsn2.path && tsn1.visited == tsn2.visited)
# https://github.com/JuliaLang/julia/issues/12198
hash(tsn::TreeSearchNode) = xor(hash(tsn.path), hash(tsn.visited))

function make_a_star_instance(tsi::TreeSearchInstance)
  tree_children = make_children_map(tsi.tree)

  function expand!(out_children, state_from)
    path_pair = state_from.path[end]
    scheduled_visit = Set{Int}()

    for tree_child in tree_children[last(path_pair)]
      @assert tree_child ∉ state_from.visited
      # extend path down tree
      new_edge = (last(path_pair), tree_child)
      extended_edge = (first(path_pair), tree_child)
      tentative_path = vcat(state_from.path[1:end-1], [extended_edge])
      if tsi.isvalid(state_from, new_edge)
        push!(out_children,
          TreeSearchNode(
            tentative_path,
            union(state_from.visited, Set([tree_child]))
          )
        )
        push!(scheduled_visit, tree_child)
      end
    end

    # TODO really unsure about this
    for path_pair in state_from.path
      for tree_parent in state_from.visited, tree_child in tree_children[tree_parent]
          if tree_child ∉ state_from.visited && tree_child ∉ scheduled_visit
            # back up to previously visited state, and descend one, to a new state
            coming_from = last(state_from.path[end])
            common_ancestor = find_lowest_common_ancestor(tsi.tree, coming_from, tree_parent)
            new_edge = (common_ancestor, tree_child)
            tentative_path = vcat(state_from.path[1:end], [new_edge])

            if tsi.isvalid(state_from, new_edge)
              push!(out_children,
                TreeSearchNode(
                  tentative_path,
                  union(state_from.visited, Set([tree_child]))
                )
              )
            end
          end
      end
    end
    return nothing
  end

  asi = LazyAStarInstance(
    (state_to, state_from) -> (length(state_to.path) - length(state_from.path)),  # transition_cost, favor paths with less retro.
    (state) -> length_path(tsi.tree, last(state.path[end]), tsi.goal_index), # heuristic. tree distance to goal (with unit edge cost)
    (state) -> (last(state.path[end]) == tsi.goal_index), # goal test
    expand!,  # connectivity
    AlwaysValid() # Lazy A* valid test, use eager A*
  )
  return asi
end


function make_a_star_algorithm(tsi::TreeSearchInstance, asi::LazyAStarInstance)
  root_index = 1
  path_pair = (root_index, root_index)
  path = [path_pair]
  start_state = TreeSearchNode(path, Set([root_index]))

  asa = LazyAStarAlgorithm(asi, typeof(start_state), Int)
  setup!(asa, start_state, 0)
  return asa
end
