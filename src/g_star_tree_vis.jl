function make_g_star_vis(parameters, vamp_pieces)
  @unpack discretization = parameters
  get_visible_region_cached__Fworld = vamp_pieces[:get_visible_region_cached__Fworld]

  @unpack visibility_instance, starting_visibility__Fworld, SE2_MPState_t = parameters

  mp = parameters[:mp]

  function tree_vis_successors!(result, s_fro)
    x, y = s_fro.e.q
    θ = s_fro.θ.q.θ

    q_news = let d = discretization
      (
        SE2_MPState_t(x, y, angle_p_step(d, θ, +1)),
        SE2_MPState_t(x, y, angle_p_step(d, θ, -1)),
        SE2_MPState_t(x, y + d.Δy, θ),
        SE2_MPState_t(x, y - d.Δy, θ),
        SE2_MPState_t(x + d.Δx, y, θ),
        SE2_MPState_t(x - d.Δx, y, θ),
      )
    end

    for s_to in q_news
      d = if mp.q_valid_test(s_to)
            distance(s_to, s_fro)
          else
            Inf
          end
      push!(result, GStar.GStarSuccessor(s_to, d))
    end

    nothing
  end


  function tree_vis_valid(s_to, s_fro, alg)
    robot__Fworld = transform(VAMP.get_tx__Fworld__Frobot(s_to), visibility_instance.robot__Frobot)

    uncovered_pieces__Fworld = robot__Fworld

    uncovered_pieces__Fworld = difference(uncovered_pieces__Fworld, starting_visibility__Fworld)
    if !mp.q_valid_test(s_to)
      return false
    end

    for q in keys(alg.closed)
      uncovered_pieces__Fworld = difference(uncovered_pieces__Fworld, get_visible_region_cached__Fworld(q))
      if length(uncovered_pieces__Fworld) == 0
        return true
      end
    end
    return false
  end

  return (tree_vis_valid = tree_vis_valid, tree_vis_successors! = tree_vis_successors!)
end
