using GeometryTypes
#using GLVisualize, GLAbstraction, Reactive, GLWindow, GLFW, Colors

using CoordinateTransformations
using StaticArrays

export get_tx__Fworld__Frobot, get_robot__Fworld

struct Transformed{T}
  geometry::T
  transform
end


#= not needed, unless refactor of `compute_visibility`
function get_frustum__Fworld(vi::VampInstance, robot_state, camera_state)
  tx__Fworld__Frobot = get_tx__Fworld__Frobot(robot_state)
  tx__Frobot__Fcamera = get_tx__Frobot__Fcamera(vi.camera, camera_state)
  return transform(tx__Fworld__Frobot ∘ tx__Frobot__Fcamera, vi.frustum__Fcamera)
end
=#

struct MoveAction{T}
  q::T # go to
end

struct LookAction{T}
  q_camera::T
end

#const Action{M,L} = Union{MoveAction{M}, LookAction{L}}

struct Path{A}
  path::Vector{A}
end

"tx__Fworld__Frobot"
function tx_base(q_base)  # TODO convert uses to get_tx__Fworld__Frobot
  t = Translation(q_base[1], q_base[2])
  r = convert(LinearMap, Rot(q_base[3]))
  return compose(t, r)
end

# TODO this name
@inline function get_tx__Fworld__Frobot(s::SE2_MPState)
  t = Translation(s.e.q...,)
  r = convert(LinearMap, s.θ.q) # already a Rot
  return compose(t, r)
end

# place the robot pieces in the world
function pose_robot(robot_spec, q)
  tx__Fworld__Frobot = tx_base(q)
  return UnionOfConvex([transform(tx__Fworld__Frobot, poly) for poly in robot_spec.pieces])
end

# TODO d.r.y.
function get_robot__Fworld(robot__Frobot, q)
  tx__Fworld__Frobot = get_tx__Fworld__Frobot(q)
  return UnionOfConvex([transform(tx__Fworld__Frobot, poly) for poly in robot__Frobot.pieces])
end


"produce a bunch of stuff that is all expressed in the world frame.
1. :visibility for objects (Difference) that represent visible regions
2. :robot for objects (UnionOfConvex) that represent robot"
function worldgrounded_path(path, robot_spec, frumstum_spec, obstacles_spec)
  stuff__Fworld = []

  last_q = nothing
  for action in path
    if typeof(action) <: MoveAction
      posed = pose_robot(robot_spec, action.q)
      push!(stuff__Fworld, (:robot, posed))
      last_q = action.q
    end

    if typeof(action) <: LookAction
      tx__Fworld__Frobot = tx_base(last_q)
      tx__Fcamerabase__Fcamera = convert(LinearMap, Rot(action.q_camera))
      tx__Fworld__Fcamera = tx__Fworld__Frobot ∘ tx__Frobot__Fcamerabase ∘ tx__Fcamerabase__Fcamera
      visible_region = compute_visibility(tx__Fworld__Fcamera, frumstum_spec, obstacles_spec)

      push!(stuff__Fworld, (:visibility, visible_region))
    end
  end
  return stuff__Fworld
end


function plot_path!(plt, stuff__Fworld)
  last_q = nothing
  for (type_, thing) in stuff__Fworld
    if type_ == :robot
      for piece in thing.pieces
        plot_polyhedron!(plt, piece)
      end
    end

    if type_ == :visibility
      for piece in explicit(thing).pieces
        plot_polyhedron!(plt, piece, alpha=0.3, color=:yellow)
      end
    end
  end
  return plt
end

# return robot pieces that are not covered by the visibility region.
function compute_path_validity(stuff__Fworld, starting_visibility)
  # Checks if Moves (including the 1st) are covered by Looks.
  # TODO check swept volumes
  PT = eltype(starting_visibility.pieces)
  accumulated_visibility = [Difference(deepcopy(starting_visibility), UnionOfConvex(PT[]))]
  uncovered_robot__Fworld = UnionOfConvex(PT[])

  for (type_, thing) in stuff__Fworld
    if type_ == :visibility
      visibility_volume = thing
      push!(accumulated_visibility, visibility_volume)
    end

    if type_ == :robot
      robot__Fworld = thing
      this_uncovered = difference(robot__Fworld, accumulated_visibility)
      append!(uncovered_robot__Fworld.pieces, this_uncovered.pieces)
    end

  end
  return uncovered_robot__Fworld
end


function visible_region_to_libgeos(visible_region)
    points_visible_region = vcat([
      [collect(points(poly)) for poly in explicit(chunk, 15).pieces] for chunk in visible_region]...)
    points_visible_region = filter(x->length(x) ≥ 3, points_visible_region) # remove degenerates
    fixed = FloatingPointFixer.fix_list_of_list_of_points(points_visible_region, 1e-4)
    fixed = filter(x->length(unique(x)) ≥ 3, fixed) # remove new degenerates from Fixing process.
    multipolygon = LibGEOSInterface.to_multipoly_2d(fixed)
    return multipolygon
end



function make_novel_visibility_goal(visibility_instance, visible_region_multipolygon, n_arc_discretization=15)
  function novel_region(q)
    new_vis = explicit(get_visible_region__Fworld(visibility_instance, q),  n_arc_discretization)
    points_to_fix = [collect(points(poly)) for poly in new_vis.pieces]
    fixed = FloatingPointFixer.fix_list_of_list_of_points(points_to_fix, 1e-4)
    new_vis_mp = LibGEOSInterface.to_multipoly_2d(fixed)
    return LibGEOSInterface.LibGEOS.difference(new_vis_mp, visible_region_multipolygon)
  end

  function novel_area(q)
    return LibGEOSInterface.LibGEOS.area(novel_region(q))
  end

  return (novel_area=novel_area, novel_region=novel_region)
end
