include("../test/experiment_setup.jl")
include("../perf/figure_maker.jl")

function doit(domain_setting, search_parameters, file_output_path, path_i_frames)
  parameters = make_complete_parameters(domain_setting)
  vamp_pieces = make_vamp_pieces(parameters)

  @unpack goal = parameters

  tpi = VAMP.TwoPhaseInstance(vamp_pieces=vamp_pieces, domain_parameters=parameters)
  merge!(tpi.parameters, search_parameters)
  tpa = VAMP.TwoPhaseAlgorithm(instance=tpi)
  setup!(tpa)

  while true
    if !step!(tpa).keepgoing
        break
    end
  end
  println("two-phase search is done.")
  path_segments = tpa.path_segments

  q_path_segments = [unlift_q(segment.path) for segment in path_segments]
  kind_segments = [segment.kind for segment in path_segments]

  # searching
  # ------------------
  # plotting

  path = vcat(map(x->x.path, path_segments)...)

  path_i_transitions = cumsum(map(x->length(x.path), path_segments))
  path_i_to_segment_i(path_i) = first(searchsorted(path_i_transitions, path_i))

  views_path = [ [] for _ in path] # all views, indexed by path_i
  path_i_views = [] # path_i's that have views

  visibility_instance = parameters[:visibility_instance]
  V(q_robot) = VAMP.get_visible_region__Fworld(visibility_instance, q_robot)

  for i = 1:length(path)
      push!(views_path[i], VAMP.LibGEOSInterface.Polygon(V(path[i].q)))
      push!(path_i_views, i)
  end

  plot_frames(parameters, path, path_segments, views_path, path_i_views, path_i_frames, file_output_path)
  return tpa
end


# Make a figure
domain_setting = Dict{Symbol, Any}(
  :all_field_of_view=>200.0,
  :domain1_goal=>:hard,
  :domain_id=>:world1
)
search_parameters=Dict(
  :do_vavp=>false,
  :move_safe_preference=>0.001
)
path_i_frames = [01, 47, 65, 85]
file_output_path = if VAMP.get_document_repo_path() != nothing
  joinpath(VAMP.get_document_repo_path(), "figs/one-hallway-good")
else
  "vamp_out/figs/one-hallway-good"
end

tpa = doit(domain_setting, search_parameters, file_output_path, path_i_frames)


# Make a figure
domain_setting = Dict{Symbol, Any}(
  :all_field_of_view=>:narrow,
  :domain1_goal=>:easy,
  :domain_id=>:world1
)
search_parameters=Dict(
  :do_vavp=>false,
  :move_safe_preference=>0.05
)
path_i_frames = [01, 40, 62, 95]
file_output_path = if VAMP.get_document_repo_path() != nothing
  joinpath(VAMP.get_document_repo_path(), "figs/one-hallway-narrow-view-staging")
else
  "vamp_out/figs/one-hallway-narrow-view-staging"
end

tpa = doit(domain_setting, search_parameters, file_output_path, path_i_frames)

#=
include("../test/do_algs.jl")
function doit_debug(domain_setting, search_parameters, file_output_path)
  parameters = make_complete_parameters(domain_setting)
  do_two_phase(file_output_path, parameters; search_parameters=search_parameters)
end
=#

#tpa = doit_debug(domain_setting, search_parameters, "/tmp/vampdebug")
