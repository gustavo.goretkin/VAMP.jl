using VAMP
using Parameters
import Plots
Plots.pyplot()


println("`using VAMP` completed")

include("../test/domain_robot.jl")
include("../test/domain_world.jl")
include("../test/domain_world2.jl")
include("../test/domain_motion_planning_collision.jl") # not needed but things are coupled

println("`include(domain_*)` completed")

function make_domain_world_generic(parameters)
  @unpack domain_id = parameters
  if domain_id == :world1
    make_domain_world(parameters)
  elseif domain_id == :world2
    make_domain_world2(parameters)
  end
end

parameters = Dict{Symbol, Any}()

setparams = [Dict{Symbol, Any}()]

new_setparams = []
for domain_id in [:world1, :world2]
  for old_setparam in setparams
    old_setparam = copy(old_setparam)
    @pack! old_setparam = domain_id
    push!(new_setparams, old_setparam)
  end
end
setparams = new_setparams


new_setparams = []
for old_setparam in setparams
  if old_setparam[:domain_id] == :world1
    for g in [:easy, :hard]
      old_setparam = copy(old_setparam)
      old_setparam[:domain1_goal] = g
      push!(new_setparams, old_setparam)
    end
  else
    push!(new_setparams, old_setparam)
  end
end
setparams = new_setparams

outdir = "outs/domains"

for (param_i, setparam) in enumerate(setparams)
  parameters = copy(setparam)

  make_robot(parameters)
  make_robot_vis_target(parameters)
  make_domain_world_generic(parameters)
  make_domain_motion_planning_collision(parameters)

  VAMP.do_parameters(parameters)

  @unpack polys_obstacles__Fworld = parameters
  @unpack polys_robot__Frobot = parameters

  @unpack boundingbox_workspace__Fworld = parameters


  (xlim, ylim) = dimlimits(boundingbox_workspace__Fworld)
  plt = Plots.plot(;aspect_ratio=1.0, xlim=xlim, ylim=ylim)

  plot_polyhedron!(plt, polys_obstacles__Fworld, linealpha=0.0, color=:grey)

  @unpack q_start = parameters
  @unpack goal = parameters
  q_goal = rand(goal)

  @unpack starting_visibility__Fworld = parameters

  sv = VAMP.LibGEOSInterface.LibGEOS.Polygon(UnionOfGeometry(starting_visibility__Fworld))
  Plots.plot!(plt, sv, color=:yellow, alpha=0.7)

  plot_polyhedron!(plt,
    VAMP.get_robot__Fworld(polys_robot__Frobot, q_start); fillalpha=0.0)

  Plots.annotate!(plt, q_start.e.q[1], q_start.e.q[2], Plots.text("s", 16, :black, :center))

  plot_polyhedron!(plt,
    VAMP.get_robot__Fworld(polys_robot__Frobot, q_goal); fillalpha=0.0)

  Plots.annotate!(plt, q_goal.e.q[1], q_goal.e.q[2], Plots.text("g", 16, :black, :center))

  # TODO plot workspace border

  for ext in ["png", "svg", "pdf"]
    dir = joinpath(outdir, ext)
    dirmpl = dir * "mpl"
    mkpath(dir)
    mkpath(dirmpl)
    Plots.savefig(plt, joinpath(dir, "$(param_i).$(ext)"))
    plt.o[:savefig](joinpath(dirmpl, "$(param_i).$(ext)"), bbox_inches="tight")
  end

end
