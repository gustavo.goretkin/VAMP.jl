import Plots
Plots.pyplot()
import Parameters: @pack!, @unpack, @with_kw
using VAMP


println("`using VAMP` completed")

include("../test/domain_robot.jl")
include("../test/domain_world.jl")
include("../test/domain_motion_planning_collision.jl") # Not needed in this test.
println("`include(domain_*)` completed")

function make_generic_fov(parameters)
  if parameters[:all_field_of_view] == :narrow
    delete!(parameters, :all_field_of_view)
    make_narrow_cone(parameters)
  end
end



for fov in [360.0, 200.0, 100.0, 50.0, :narrow], position = [:away, :by]
  parameters = Dict{Symbol, Any}()
  parameters[:all_field_of_view] = fov

  make_generic_fov(parameters)
  make_robot(parameters)
  make_robot_vis_target(parameters)
  make_domain_world(parameters)
  make_domain_motion_planning_collision(parameters)

  VAMP.do_parameters(parameters)

  @unpack SE2_MPState_t = parameters
  @unpack visibility_instance = parameters
  @unpack polys_robot__Frobot = parameters
  @unpack polys_obstacles__Fworld = parameters

  V(q_robot) = VAMP.get_visible_region__Fworld(visibility_instance, q_robot)
  q = if position == :away
    SE2_MPState_t(10.0,10.0,0.0)
  elseif position == :by
    # parameters[:q_start]
    SE2_MPState{0.5}(-0.5,-1.0,deg2rad(0.0))
  end

  v = V(q)


  plt = Plots.plot(; aspect_ratio=1.0, border=false, ticks=false, grid=false, margin=0.0Plots.mm)

  if position == :by
    for p in polys_obstacles__Fworld.pieces
      plot_polyhedron!(plt, p; fillcolor=:brown)
    end
  end

  plot_polyhedron!(plt, VAMP.get_robot__Fworld(polys_robot__Frobot, q), color=Plots.RGB(((26, 156, 247)./255)...))
  Plots.plot!(plt, VAMP.LibGEOSInterface.Polygon(v); color=:yellow, fillalpha=0.7)

  outdir = joinpath("outs", "sensor_configurations")
  mkpath(outdir)

  for ext in ["png", "svg", "pdf"]
    dir = joinpath(outdir, ext)
    mkpath(dir)
    Plots.savefig(plt, joinpath(dir, "$(fov)-$(position).$(ext)"))
  end

  for ext in ["png", "svg", "pdf"]
    dir = joinpath(outdir, ext*"mpl")
    mkpath(dir)
    plt.o[:gca]()[:patch][:set_visible](false)
    plt.o[:gca]()[:axis]("off")
    #plt.o[:figure][:patch][:set_visible](false)
    plt.o[:savefig](joinpath(dir, "$(fov)-$(position).$(ext)"), bbox_inches="tight")
  end



end
