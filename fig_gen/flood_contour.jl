import PyPlot

using VAMP
using Contour

println("`using VAMP` completed")

include("../test/domain_robot.jl")
include("../test/domain_world.jl")
include("../test/domain_world2.jl")
include("../test/domain_motion_planning_collision.jl")
println("`include(domain_*)` completed")

# https://stackoverflow.com/a/48110626/415404
# make SVGs reproducible
# PyCall makes copies of py dicts, so access the dict as follows:
# https://github.com/JuliaPy/PyPlot.jl/issues/417
PyPlot.PyDict(PyPlot.matplotlib."rcParams")["svg.hashsalt"] = "vamp"
# Make PDF/PS creation date reproducible
# https://matplotlib.org/2.1.1/users/whats_new.html#reproducible-ps-pdf-and-svg-output
# https://github.com/JuliaPy/PyCall.jl/issues/698
PyPlot.PyDict(PyPlot.PyCall.pyimport("os").environ)["SOURCE_DATE_EPOCH"] = "0"

results_path = if VAMP.get_document_repo_path() != nothing
  joinpath(VAMP.get_document_repo_path(), "figs/achieve_vis_heuristic_floodfill")
else
  "vamp_out/flood_contour"
end
mkpath(results_path)
@show results_path

parameters = Dict()

make_robot(parameters)
make_robot_vis_target(parameters)
make_domain_world2(parameters)
make_domain_motion_planning_collision(parameters)


@setparam parameters polys_robot__Frobot = nothing
@setparam parameters poly_frustum__Fcamera = nothing
@setparam parameters polys_obstacles__Fworld = nothing
@setparam parameters is_collision_free = nothing
@setparam parameters workspace_points__Fworld = nothing
@setparam parameters boundingbox_workspace__Fworld = nothing
@setparam parameters workspace_points_raster_spec__Fworld = nothing
# higher resolution for making sure we don't prune poorly.
@setparam parameters prune_test_raster_spec__Fworld = VAMP.RasterVolumeSpec(workspace_points_raster_spec__Fworld.limits, 1 .* workspace_points_raster_spec__Fworld.size)


coverage_goal = Set(GeometryTypes.Point{2,Float64}[Point(0.625, 2.125), Point(1.025, 2.5), Point(0.3249999999999999, 2.25), Point(0.125, 2.525), Point(1.125, 2.525), Point(0.42500000000000004, 2.0), Point(0.925, 2.375), Point(0.22499999999999992, 2.375), Point(1.125, 2.35), Point(0.825, 2.125), Point(0.525, 2.375), Point(0.22499999999999992, 2.5), Point(1.025, 2.25), Point(0.125, 2.3), Point(0.525, 2.125), Point(0.625, 2.0), Point(0.3249999999999999, 2.5), Point(0.525, 2.25), Point(1.125, 2.325), Point(0.825, 2.5), Point(0.625, 2.25), Point(0.3249999999999999, 2.0), Point(0.125, 2.375), Point(0.525, 2.5), Point(0.925, 2.125), Point(1.025, 2.375), Point(0.125, 2.325), Point(0.825, 2.375), Point(0.22499999999999992, 2.25), Point(0.125, 2.45), Point(1.125, 2.2), Point(0.125, 2.2), Point(0.125, 2.35), Point(0.125, 2.425), Point(0.125, 2.5), Point(0.125, 2.0), Point(0.725, 2.125), Point(1.125, 2.475), Point(0.125, 2.125), Point(0.725, 2.5), Point(0.725, 2.375), Point(1.125, 2.225), Point(0.42500000000000004, 2.375), Point(1.125, 2.5), Point(1.125, 2.4), Point(0.42500000000000004, 2.25), Point(0.42500000000000004, 2.125), Point(0.125, 2.25), Point(0.125, 2.1), Point(0.3249999999999999, 2.125), Point(0.125, 2.225), Point(1.125, 2.45), Point(0.825, 2.0), Point(1.125, 2.25), Point(1.125, 2.375), Point(0.125, 2.4), Point(1.125, 2.3), Point(1.025, 2.125), Point(0.925, 2.0), Point(1.025, 2.0), Point(1.125, 2.1), Point(0.825, 2.25), Point(0.725, 2.0), Point(0.625, 2.375), Point(0.725, 2.25), Point(0.42500000000000004, 2.5), Point(0.525, 2.0), Point(0.125, 2.475), Point(0.3249999999999999, 2.375), Point(0.925, 2.25), Point(1.125, 2.425), Point(1.125, 2.125), Point(0.22499999999999992, 2.0), Point(1.125, 2.0), Point(0.925, 2.5), Point(0.22499999999999992, 2.125)])

flood_raster_spec = workspace_points_raster_spec__Fworld
#flood_raster_spec = VAMP.RasterVolumeSpec(workspace_points_raster_spec__Fworld.limits, (256, 256))
h_view = VAMP.make_coverage_heuristic(
  coverage_goal, polys_obstacles__Fworld, flood_raster_spec; smooth_connected=true)

costfield_d = zeros(flood_raster_spec.size)
costfield = VAMP.RasterVolume(flood_raster_spec, costfield_d)

VAMP.grid_render!(costfield, h_view.eikonal)

function distance_to_obstacle_boundary(point)
  minimum([abs(VAMP.signed_distance(poly, point)) for poly in polys_obstacles__Fworld.pieces])
end

#- how to get matplotlib to show -#
# - just plot obstacles and points -#

fig, ax = PyPlot.subplots()

mpl_extent = vcat(map(x->[x...], dimlimits(costfield.spec.limits))...)
cax = ax.imshow(costfield.data', extent=mpl_extent, origin="lower")
ax.figure.colorbar(cax)

#Plots.plot!(plt;colorbar=true)
#Plots.heatmap!(plt, VAMP.coordinates(costfield.spec)..., costfield.data')
#Plots.contour!(plt, VAMP.coordinates(costfield.spec)..., costfield.data'; linewidth=4.0)

contouring = contours(VAMP.coordinates(costfield.spec)..., costfield.data, collect(0.1:0.5:8.6))
clines = levels(contouring)

# the scalar field is discontinuous at the obstacle boundaries. don't plot the broken contourlines 
for lineset in clines
  for line in lineset.lines
    points = map(x->Point(x...), line.vertices)
    masked = [if distance_to_obstacle_boundary(point) > 1e-1; tuple(point...); else (nothing, nothing); end for point in points]
    xy = zip(masked...)
    ax.plot(xy...; color="white")
  end
end

#plot_polyhedron!(plt, polys_obstacles__Fworld; color=:grey, linealpha=0.0)
VAMP.plot_polyhedron_mpl!(ax, polys_obstacles__Fworld; edgecolor="none", facecolor="grey")

ax.plot(zip(coverage_goal...)..., linestyle="None", marker="x", color="white", markersize=1.5)
#scatter_points!(plt, coverage_goal; color=:black, linealpha=0.0, markersize=1.0)

for ext in ["png", "svg", "pdf"]
    dir = results_path
    #Plots.savefig(plt, joinpath(dir, "figure.$(ext)"))
    ax.figure.savefig(joinpath(dir, "figure.$(ext)"), bbox_inches="tight")
end
