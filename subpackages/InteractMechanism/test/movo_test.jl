import RobotModeling
import KinovaMovoRobot
import InteractMechanism

mechanism = RobotModeling.get_robot_mechanism(KinovaMovoRobot.urdfpath())
widgets = InteractMechanism.manipulate!(x->(), RobotModeling.rbd.MechanismState(mechanism))

import Blink
window = Blink.Window()

Blink.body!(window, widgets)
