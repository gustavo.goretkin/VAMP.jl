module InteractMechanism
# copied from https://github.com/JuliaRobotics/MeshCatMechanisms.jl/blob/master/src/manipulate.jl

import RigidBodyDynamics
const rbd = RigidBodyDynamics

import RigidBodyDynamics.Rotations
import UnitfulAngles
import InteractBase

using InteractBase: slider, Widget, observe, vbox
using OrderedCollections: OrderedDict


function remove_infs(b::rbd.Bounds, default=Float64(π))
    rbd.Bounds(isfinite(rbd.lower(b)) ? rbd.lower(b) : -default,
           isfinite(rbd.upper(b)) ? rbd.upper(b) : default)
end

slider_range(joint::rbd.Joint) = remove_infs.(rbd.position_bounds(joint))
function slider_range(joint::rbd.Joint{T, <: rbd.QuaternionFloating}) where {T}
    defaults = [1., 1, 1, 1, 10, 10, 10]
    remove_infs.(rbd.position_bounds(joint), defaults)
end

slider_labels(joint::rbd.Joint) = [string("q", i) for i in 1:rbd.num_positions(joint)]
slider_labels(joint::rbd.Joint{T, <:rbd.QuaternionFloating}) where {T} = ["rw", "rx", "ry", "rz", "x", "y", "z"]

function sliders(joint::rbd.Joint, values=clamp.(zeros(rbd.num_positions(joint)), rbd.position_bounds(joint));
                 bounds=slider_range(joint),
                 labels=slider_labels(joint),
                 resolution=0.01, prefix="")
    map(bounds, labels, values) do b, label, value
        num_steps = ceil(Int, (rbd.upper(b) - rbd.lower(b)) / resolution)
        r = range(rbd.lower(b), stop=rbd.upper(b), length=num_steps)
        slider(range(rbd.lower(b), stop=rbd.upper(b), length=num_steps),
               value=clamp(value, first(r), last(r)),
               label=string(prefix, label))
    end
end

function combined_observable(joint::rbd.Joint, sliders::AbstractVector)
    map(observe.(sliders)...) do args...
        q = vcat(args...)
        rbd.normalize_configuration!(q, joint)
        q
    end
end

function widget(joint::rbd.Joint{T, <:rbd.Fixed}, args...) where T
    Widget{:rbd_joint}()
end

function widget(joint::rbd.Joint, initial_value=clamp.(zeros(rbd.num_positions(joint)), rbd.position_bounds(joint)); prefix=string(joint, '.'))
    s = sliders(joint, initial_value, prefix=prefix)
    keys = Symbol.(slider_labels(joint))
    w = Widget{:rbd_joint}(OrderedDict(zip(keys, s)))
    w.output = combined_observable(joint, s)
    w.layout = x -> vbox(s...)
    w
end

function manipulate!(callback::Function, state::rbd.MechanismState)
    joint_list = rbd.joints(state.mechanism)
    widgets = widget.(joint_list, rbd.configuration.(state, joint_list))
    keys = Symbol.(joint_list)
    w = Widget{:rbd_manipulator}(OrderedDict(zip(keys, widgets)))
    w.output = map(observe.(widgets)...) do signals...
        for i in 1:length(joint_list)
            if rbd.num_positions(joint_list[i]) > 0
              rbd.set_configuration!(state, joint_list[i], signals[i])
            end
        end
        callback(state)
    end
    w.layout = x -> vbox(widgets...)
    w
end

end