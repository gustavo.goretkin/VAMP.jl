# Geometry.jl

![Lifecycle](https://img.shields.io/badge/lifecycle-experimental-orange.svg)<!--
![Lifecycle](https://img.shields.io/badge/lifecycle-maturing-blue.svg)
![Lifecycle](https://img.shields.io/badge/lifecycle-stable-green.svg)
![Lifecycle](https://img.shields.io/badge/lifecycle-retired-orange.svg)
![Lifecycle](https://img.shields.io/badge/lifecycle-archived-red.svg)
![Lifecycle](https://img.shields.io/badge/lifecycle-dormant-blue.svg) -->
[![Build Status](https://travis-ci.com/goretkin/Geometry.jl.svg?branch=master)](https://travis-ci.com/goretkin/Geometry.jl)
[![codecov.io](http://codecov.io/github/goretkin/Geometry.jl/coverage.svg?branch=master)](http://codecov.io/github/goretkin/Geometry.jl?branch=master)

pieces of generic geometry stuff that likely belong in separate packages, or in GeometryTypes
