using Geometry
using Test

import GeometryTypes
@testset "construct PointCloud" begin
    pc = Geometry.PointCloud([Geometry.Point(1,2.0)])
    @test length(pc.points) == 1
end

@testset "convert HyperRectangle" begin
     h = GeometryTypes.HyperRectangle(0,0,1,1)
     h_constructor = HyperRectangle{2, Float32}(h)
     h_convert = convert(HyperRectangle{2, Float32}, h)
     @test h_constructor == h_convert
     h_convert2 = convert(HyperRectangle{N, Float32} where N, h)
     @test h_constructor == h_convert2
end

include("tx_convert.jl")
