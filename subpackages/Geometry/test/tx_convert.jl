import CoordinateTransformations
import StaticArrays
import LinearAlgebra

const coordtx = CoordinateTransformations
const SA = StaticArrays

@testset "glue transformations based on StaticArrays" begin
    R = SA.SMatrix{3,3,Float64}(1:9)
    t = SA.SVector{3, Float64}((1:3).*10)
    tx = coordtx.AffineMap(R, t)
    tx_mat = convert(AbstractMatrix, tx)
    @test tx_mat isa SA.StaticArray

    # these won't be `StaticArray` since the type would depend
    R_roundtrip = tx_mat[1:3, 1:3]
    t_roundtrip = tx_mat[1:3, 4]

    @test R_roundtrip == R
    @test t_roundtrip == t
end
