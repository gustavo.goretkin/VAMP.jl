import StaticArrays: SMatrix, @SMatrix, @SArray
import StaticArrays
const SA = StaticArrays

function Base.convert(::Type{AbstractMatrix}, a::coordtx.AffineMap)
  _zeros_row = zero(a.linear[1,:]')
  _one_row = @SArray [1,]
  last_row = hcat(_zeros_row, _one_row)
  return vcat(
    hcat(a.linear, a.translation),
    last_row
  )
end

Base.convert(::Type{AbstractArray}, a::coordtx.AffineMap) = convert(AbstractMatrix, a)

function Base.convert(::Type{Homogeneous{coordtx.LinearMap}}, a::coordtx.AffineMap)
  Homogeneous(coordtx.LinearMap(convert(AbstractMatrix, a)))
end

function Base.convert(::Type{coordtx.AffineMap}, ha::Homogeneous{<:AbstractMatrix})
  a = ha.value
  (n1, n2) = size(a)
  if !(all(a[n1, 1:n2-1] .== 0)) || !(a[n1, n2] == 1)
    throw(InexactError("does not have [0, ⋯ 0, 1] in last row"))
  end
  linear = a[1:n1-1, 1:n2-1]
  trans = a[1:n1-1, n2]
  coordtx.AffineMap(linear, trans)
end

function Base.convert(::Type{coordtx.AffineMap}, ha::Homogeneous{<:SA.StaticMatrix})
  a = ha.value
  (n1, n2) = size(a)
  if !(all(a[n1, 1:n2-1] .== 0)) || !(a[n1, n2] == 1)
    throw(InexactError("does not have [0, ⋯ 0, 1] in last row"))
  end
  # TODO check that this function is specialized for specific values of n1, n2
  linear = a[SA.SOneTo(n1-1), SA.SOneTo(n2-1)]
  trans = a[SA.SOneTo(n1-1), n2]

  coordtx.AffineMap(linear, trans)
end
