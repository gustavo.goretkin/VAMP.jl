module MollerTriangleIntersection
"""
return plane of the triangle
"""

function tri_plane(tri)
    N = cross(tri[2] - tri[1], tri[3] - tri[1])
    d = -dot(N, tri[1])
    return (N, d)
end


plane_dist(N, d, V) = dot(N, V) + d         # eq 2

"""
A Fast Triangle-Triangle Intersection Test - Tomas Möller
https://web.stanford.edu/class/cs277/resources/papers/Moller1997b.pdf
"""
function triangle3_triangle3_do_intersect(tri1, tri2)
    (N2, d2) = tri_plane(tri2)              # eq 1
    d_V1 = plane_dist.(Ref(N2), Ref(d2), tri1) # eq 2

    if d_V1 .> 0 || d_V1 .< 0
        # triangle 1 is completely to one side of triangle 2
        return false
    end

    (N1, d1) = tri_plane(tri1)

    d_V2 = plane_dist.(Ref(N1), Ref(d1), tri2)

    if d_V2 .> 0 || d_V2 .< 0
        # triangle 2 is completely to one side of triangle 1
        return false
    end

    if d_V1 .== 0
        # coplaner, not handeled yet
        error()
    end

    D = cross(N1, N2)

    p_V1 = dot(Ref(D), tri1)        #eq 5

end
end
