module Geometry

using GeometryTypes: GeometryTypes, Simplex, Point, facetype, AbstractMesh, OffsetInteger, vertices
import GeometryTypes
const gt = GeometryTypes
using LinearAlgebra: norm

import LazyArrays
import CoordinateTransformations
const coordtx = CoordinateTransformations

const TX_I = coordtx.IdentityTransformation

"""
Make a mesh out of a simplex
Only works for tris and tets.
"""
function (::Type{M})(s::Simplex{N, P}) where {M <: AbstractMesh, P <: Point, N}
    FT = facetype(M)
    @assert 3 == length(FT)
    OI = OffsetInteger{-1}

    faces = if N == 4
        [
          FT(OI(1), OI(2), OI(3)),
          FT(OI(2), OI(3), OI(4)),
          FT(OI(3), OI(4), OI(1)),
          FT(OI(4), OI(1), OI(2)),
        ]
    elseif N == 3
        [FT(OI(1), OI(2), OI(3))]
    else
        error()
    end
    # VT = vertextype(M)  # TODO should coerce vertices?
    # `vertices(::Simplex)` can return a tuple. TODO should always return a ::StaticArray array?
    v = collect(vertices(s))
    M(faces=faces, vertices=v)
end

# conversion logic in a constructor in `GeometryTypes/src/hyperrectangles.jl`
using GeometryTypes: HyperRectangle
Base.convert(::Type{HyperRectangle{N2, T} where N2}, h::HyperRectangle{N, T2}) where {T, N, T2} = convert(HyperRectangle{N, T}, h)
Base.convert(H::Type{HyperRectangle{N, T}}, h::HyperRectangle{N, T2}) where {T, N, T2} = H(h)

struct PointCloud{DIM, NUM, DATA} <: GeometryTypes.AbstractGeometry{DIM, NUM}
    points::DATA
end

function PointCloud(points)
    PT = eltype(points)
    DIM = size(PT)      # ambient dimension # TODO this "trait" is not defined for a lot of types.
    NUM = eltype(PT)    # e.g. Float32
    PointCloud{DIM, NUM, typeof(points)}(points)
end

# TODO would be good to use an iterator interface to PointCloud so there's no question about array-of-struct v struct-of-array / array layout
gt.vertices(pc::PointCloud) = pc.points

# TODO add additional refinement strategies
PointCloud(m::AbstractMesh) = PointCloud(vertices(m))


function boundingvolume(bounder_type, mesh::gt.AbstractMesh, args...)
    # boundingvolume would never care about faces, unless there's some kind of subdivision surface / spline thing.
    boundingvolume(bounder_type, PointCloud(mesh), args...)
end

"""
Compute a bounding volume expressed in coordinates Fb, of points expressed in Fa
"""
function boundingvolume(THyperSphere__Fb::Type{gt.HyperSphere}, pc__Fa::PointCloud, tx__Fb_Fa=TX_I())
    # TODO generators don't infer so well?
    # TODO norm / HyperSphere is rotation invariant so don't bother doing rotation
    # TODO clarify meaning of parametrization that includes just the translation, yet is fully expressive
    radius = maximum(norm(tx__Fb_Fa(p)) for p in vertices(pc__Fa))
    z = zero(radius)
    origin__Fb = gt.Point(z, z, z)
    bound__Fb = THyperSphere__Fb(origin__Fb, radius)
end

function boundingvolume(THyperRectangle__Fb::Type{gt.HyperRectangle}, pc__Fa::PointCloud, tx__Fb_Fa=TX_I())
    # `gt.HyperRectangle` accepts an `AbstractArray` of `Point`s.
    # it would better take an iterator of Points, but since it doesn't use LazyArrays to avoid allocating a new array, and transform points on the fly.
    points__Fa = gt.vertices(pc__Fa)
    # this doesn't seem to be inferring. It's not correct to rely on inference anyway...
    #=
    points__Fb = LazyArrays.BroadcastArray(
        #point->tx__Fb_Fa(point),   # not inferring? redundant syntax, anyway
        tx__Fb_Fa,
        points__Fa
    )
    =#
    points__Fb = tx__Fb_Fa.(points__Fa)
    bound__Fb = THyperRectangle__Fb(points__Fb)
end

function boundingvolume(THyperSphere__Fb::Type{gt.HyperSphere}, s__Fa::gt.HyperSphere, tx__Fb_Fa=TX_I())
    Oa = gt.Point(0, 0, 0) # TODO make dimensionally generic
    Oa__Fb = tx__Fb_Fa(Oa)

    radius_tx = norm(Oa__Fb)
    radius_s = gt.radius(s__Fa)
    radius = radius_tx + radius_s
    z = zero(radius)
    origin__Fb = gt.Point(z, z, z)
    bound__Fb = THyperSphere__Fb(origin__Fb, radius)
end

#=
import SimpleTraits: @traitfn
import SimpleTraits.BaseTraits: IsIterator

@traitfn boundingvolume(::Type{T}, iterable::I) where {T, I; IsIterator{I}} = boundingvolume_from_iterator(T, I)

# until Traits are part of the julia language definition, it might be good to have an escape hatch to call the implementation directly
function boundingvolume_from_iterator(H::Type{HyperRectangle}, iterable)
    # will error unless `eltype(iterable)` gives e.g. gt.Point
    H(ArrayDisguisedIterator(iterable))
end
=#



# just to signify that we've got a Homogeneous object.
# e.g ::Homogeneous{StaticMatrix{4, 4}} * Point(0,0,0) could be made to work
struct Homogeneous{T}
    value::T
end

include("tx_convert.jl")

end # module
