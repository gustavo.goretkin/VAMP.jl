using Documenter, Geometry

makedocs(
    modules = [Geometry],
    format = Documenter.HTML(),
    checkdocs = :exports,
    sitename = "Geometry.jl",
    pages = Any["index.md"]
)

deploydocs(
    repo = "github.com/goretkin/Geometry.jl.git",
)
