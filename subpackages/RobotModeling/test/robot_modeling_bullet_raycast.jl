import RobotModeling
use_makie = false
if use_makie; include("../src/makie_robot_modeling.jl"); end
#import Bullet
import RigidBodyDynamics
const rbd = RigidBodyDynamics
import GeometryTypes

import UnitfulAngles: turn


import CoordinateTransformations.Rotations: RotZYX
import CoordinateTransformations: compose, AffineMap
import StaticArrays: SVector
import Setfield: @set

import ColorTypes
import CoordinateTransformations
import MeshCatMechanisms
import MeshCat
import MechanismGeometries

import KinovaMovoRobot

using AssignOnce: @assignonce
@assignonce robot_model = RobotModeling.get_robot(;urdf_path=KinovaMovoRobot.urdfpath(), asset_dir=KinovaMovoRobot.packagepath(), target_detail_radius=0.02)


# makes for a nice tab-completion experience in the REPL, too
movo_joints = Dict(joint.name=>joint for joint in rbd.joints(robot_model.mechanism))

see_state = RigidBodyDynamics.MechanismState(robot_model.mechanism)
target_state = RigidBodyDynamics.MechanismState(robot_model.mechanism)

rbd.set_configuration!(see_state, movo_joints["left_shoulder_lift_joint"], +0.4)
rbd.set_configuration!(see_state, movo_joints["floating_joint"],
  rbd.Transform3D(rbd.frame_after(movo_joints["floating_joint"]), rbd.frame_before(movo_joints["floating_joint"]),
    RotZYX(-0.0turn, 0.0, 0.0),
    SVector(0.0, 0.0, 0.0)
  )
)

tx__Fworld__Fbase = rbd.Transform3D(rbd.frame_after(movo_joints["floating_joint"]), rbd.frame_before(movo_joints["floating_joint"]),
  RotZYX(0.1turn, 0.0, 0.0),
  SVector(2.0, 0.1, 0.0)
)

rbd.set_configuration!(target_state, movo_joints["right_shoulder_lift_joint"], 0.1)
rbd.set_configuration!(target_state, movo_joints["floating_joint"], tx__Fworld__Fbase)

if false
sm = Bullet.connect(
#  ;kind=:gui
)
# Bullet.setup_gui_timer()

#bullet_movo_id = Bullet.load_urdf(sm, KinovaMovoRobot.urdfpath())

handles = (bullet_sm=sm, bullet_robot_id=bullet_movo_id, robot_model=robot_model)
end
function do_visibility_check(handles, see_state, target_points__Fworld)
  camera_optical_link_name = "kinect2_ir_optical_frame"
  camera_optical_frame = RobotModeling.get_frame_by_link_name(robot_model.mechanism, camera_optical_link_name)

  tx__Fworld__Feye = RigidBodyDynamics.transform_to_root(see_state, camera_optical_frame)

  eye__Fworld = tx__Fworld__Feye * GeometryTypes.Point(0,0,0)

  # make a flat array. # TODO use reinterpret
  target_array__Fworld = zeros(3, length(target_points__Fworld))
  for i=1:length(target_points__Fworld); target_array__Fworld[:,i] .= target_points__Fworld[i] end

  source_array__Fworld = zeros(3, length(target_points__Fworld))
  for i=1:length(target_points__Fworld); source_array__Fworld[:,i] .= eye__Fworld end

  Bullet.RigidBodyDynamicsAdapter.update_state(handles.bullet_sm, handles.bullet_robot_id, see_state)
  BATCH_SIZE = Bullet.Raw.MAX_RAY_INTERSECTION_BATCH_SIZE
  N = length(target_points__Fworld)

  nan_hit = Bullet.Raw.b3RayHitInfo(NaN, -100, -100, (NaN, NaN, NaN), (NaN, NaN, NaN))
  hit_batch_image = fill(nan_hit, (N, ))

  for batch_i in 1:(Int(round(N / BATCH_SIZE, RoundUp)))
    batch_i0 = batch_i - 1
    batch_range = (batch_i0 * BATCH_SIZE + 1):min((batch_i * BATCH_SIZE), N)
    batch_range_no_offset = batch_range .- batch_i0 * BATCH_SIZE
    raycast_info = Bullet.raycast_batch(handles.bullet_sm, source_array__Fworld[:, batch_range], target_array__Fworld[:, batch_range])

    hits_batch = [unsafe_load(raycast_info.m_rayHits, i) for i in batch_range_no_offset]
    hit_batch_image[batch_range] = hits_batch
  end
  return hit_batch_image
end

function visualize_visibility_check(handles, see_state, target_state)
  target_points__Fworld = RobotModeling.get_target_pointcloud_Fworld(handles.robot_model, target_state)
  hit_batch_image = do_visibility_check(handles, see_state, target_points__Fworld)

  things_hit = Set((hit.m_hitObjectUniqueId, hit.m_hitObjectLinkIndex) for hit in hit_batch_image)
  @show things_hit

  s = Makie.Scene()
  find_visible = getfield.(hit_batch_image, :m_hitObjectUniqueId) .== Bullet.LINK_ID_NONE
  Makie.scatter!(s, target_points__Fworld[find_visible], markersize=0.02, color=:yellow)
  Makie.scatter!(s, target_points__Fworld[.!find_visible], markersize=0.03, color=:red)

  plot_bot!(s, robot_model, robot_model.collision_geometries, see_state; plot_cmd! = (s, b)->Makie.mesh!(s, b; color=:grey))

  return s
end

# assume all of the fields are observables
get_value_dict(o::T) where {T} = Dict(fn => getfield(o, fn)[] for fn in fieldnames(T))

function set_from_value_dict(o::T, value_dict) where {T}
  for fn in fieldnames(Makie.AbstractPlotting.Camera3D)
    getfield(o, fn)[] = value_dict[fn]
  end
end

# different from RBD.num_bodies
num_link_geometries(robot_model::RobotModeling.RobotModel) = length(robot_model.geometry_info)

import GeometryTypes: GLPlainMesh, HyperRectangle
import GeometryTypes: faces
import GeometryTypes: vertices
import BoundingBoxes

bb_to_mesh(hr::HyperRectangle) = RobotModeling.MeshUtils.deduplicate_vertices(GLPlainMesh(hr))
bb_to_mesh(bb::BoundingBoxes.BoundingBox) = bb_to_mesh(convert(HyperRectangle, bb))
GLPlainMesh(bb::BoundingBoxes.BoundingBox) = bb_to_mesh(bb)


import GeometryReferenceGenericSolver
function get_link_bounding__Fworld(state, link_i)
  tx__Fworld__Flink = RigidBodyDynamics.transform_to_root(state, robot_model.geometry_info[link_i].frame)
  tx__Fworld__Fgeo = tx__Fworld__Flink # * geometry_info.transform TODO

  bb = robot_model.bounding_geometries[link_i]
  bb_mesh = GLPlainMesh(bb)
  vertices__Fgeo = vertices(bb_mesh)

  vertices__Fworld = Ref(tx__Fworld__Fgeo) .* vertices__Fgeo
  return GLPlainMesh(vertices=vertices__Fworld, faces=faces(bb_mesh))
end

function get_eye__Fworld(see_state)
  camera_optical_link_name = "kinect2_ir_optical_frame"
  camera_optical_frame = RobotModeling.get_frame_by_link_name(robot_model.mechanism, camera_optical_link_name)
  tx__Fworld__Feye = RigidBodyDynamics.transform_to_root(see_state, camera_optical_frame)
  eye__Fworld = tx__Fworld__Feye * GeometryTypes.Point(0,0,0)
end

function get_tets__Fworld(eye__Fworld, target_state, link_i)
  target_bb_mesh__Fworld = get_link_bounding__Fworld(target_state, link_i)
  target_vertices__Fworld = vertices(target_bb_mesh__Fworld)

  [
    map(GeometryTypes.Point{3, Float64}, tuple(target_vertices__Fworld[target_face]..., eye__Fworld))
    for target_face = faces(target_bb_mesh__Fworld)
  ]
end

function get_tris__Fworld(see_state, link_i)
  occluder_bb_mesh__Fworld = get_link_bounding__Fworld(see_state, link_i)
  occluder_vertices__Fworld = vertices(occluder_bb_mesh__Fworld)

  [
    tuple(occluder_vertices__Fworld[occluder_face]...)
    for occluder_face in faces(occluder_bb_mesh__Fworld)
  ]
end

"""
iterate through tets and tris
"""
function link_bounding_visibility_check2(see_state, target_state; scene=nothing)
  eye__Fworld = get_eye__Fworld(see_state)

  n = num_link_geometries(robot_model)
  n = min(4, n)
  interaction = zeros(n, n)
  for target_i = 1:n
    for occluder_i = 1:n
      tx__Fworld__Ftarget_link = RigidBodyDynamics.transform_to_root(target_state, robot_model.geometry_info[target_i].frame)
      tx__Fworld__Ftarget_geo = tx__Fworld__Ftarget_link # * geometry_info.transform TODO

      tx__Fworld__Foccluder_link = RigidBodyDynamics.transform_to_root(see_state, robot_model.geometry_info[target_i].frame)
      tx__Fworld__Foccluder_geo = tx__Fworld__Foccluder_link # * geometry_info.transform TODO

      target_bb = robot_model.bounding_geometries[target_i]
      occluder_bb = robot_model.bounding_geometries[occluder_i]
      target_vertices__Ftarget_geo = vertices(target_bb)
      occluder_vertices__Foccluder_geo = vertices(occluder_bb)

      target_vertices__Fworld = Ref(tx__Fworld__Ftarget_geo) .* target_vertices__Ftarget_geo
      occluder_vertices__Fworld = Ref(tx__Fworld__Ftarget_geo) .* occluder_vertices__Foccluder_geo
      @show (target_i, occluder_i)
      d_sq_min = Inf
      for target_face = faces(target_bb)
        for occluder_face = faces(occluder_bb)
          tet__Fworld = tuple(target_vertices__Fworld[target_face]..., eye__Fworld)
          tri__Fworld = tuple(occluder_vertices__Fworld[occluder_face]...)

          if scene !== nothing
            Makie.mesh!(scene, GLPlainMesh(Simplex(tet__Fworld)); color=:blue)
            Makie.mesh!(scene, GLPlainMesh(Simplex(tri__Fworld)); color=:red)
            return scene
          end
          r = GeometryReferenceGenericSolver.find_minimum_distance(
            tet__Fworld,
            tri__Fworld)
          d_sq_min = min(r.d_sq, d_sq_min)
        end
      end
      interaction[target_i, occluder_i] = d_sq_min
    end
  end
  return interaction
end


function tri_tet_sq_distance_qp(tri, tet)
  r = GeometryReferenceGenericSolver.find_minimum_distance(
    tet,
    tri)
  return r.d_sq
end

"""
iterate through tets and tris
"""
function link_bounding_visibility_check(see_state, target_state, tri_tet_sq_distance; scene=nothing)
  eye__Fworld = get_eye__Fworld(see_state)

  n = num_link_geometries(robot_model)
  #n = min(4, n)
  interaction = zeros(n, n)
  for target_i = 1:n
    @show target_i
    for occluder_i = 1:n
      #@show occluder_i
      tets = get_tets__Fworld(eye__Fworld, target_state, target_i)
      tris = get_tris__Fworld(see_state, occluder_i)

      d_sq_min = Inf
      for tet in tets
        for tri in tris
          d_sq = tri_tet_sq_distance(tri ,tet)
          d_sq_min = min(d_sq, d_sq_min)
        end
      end
      interaction[target_i, occluder_i] = d_sq_min
    end
  end
  return interaction
end



#-------------
# TODO eww
function convert_tx(::Type{AffineMap}, FloatType, rbd_tx::RigidBodyDynamics.Spatial.Transform3D)
  if FloatType === Any
    error()
  end
  Rm = convert(AbstractArray{FloatType}, RigidBodyDynamics.rotation(rbd_tx).mat)
  R = CoordinateTransformations.RotMatrix(Rm)
  t = convert(AbstractArray{FloatType}, RigidBodyDynamics.translation(rbd_tx))
  AffineMap(R, t)
end

"""
iterate through ray-group and occluders (both convex)
"""
function link_bounding_visibility_check_unify(see_state, target_state, gjk_unified, queries=nothing; gjk_type=Float64)
  T = gjk_type # for GJK
  eye__Fworld = get_eye__Fworld(see_state)

  n = num_link_geometries(robot_model)
  #n = min(4, n)
  interaction = zeros(T, n, n)
  # 8 points for the bounding box, one point for the eye.
  # GJK should just care about convex hull.
  frustum__Ftarget_geo = Vector{GeometryTypes.Vec{3, T}}(undef, 9)

  for target_i = 1:n
    #@show target_i
    tx_target__Fworld__Flink = RigidBodyDynamics.transform_to_root(target_state, robot_model.geometry_info[target_i].frame)
    tx_target__Fworld__Fgeo = tx_target__Fworld__Flink # * geometry_info.transform TODO

    eye__Ftarget_geo = inv(tx_target__Fworld__Fgeo) * eye__Fworld

    hr_target__Fgeo = convert(HyperRectangle{3, T}, robot_model.bounding_geometries[target_i].geometry)
    frustum__Ftarget_geo[1] = eye__Ftarget_geo
    frustum__Ftarget_geo[2:9] .= vertices(hr_target__Fgeo)

    for occluder_i = 1:n
      tx_see__Fworld__Flink = RigidBodyDynamics.transform_to_root(see_state, robot_model.geometry_info[occluder_i].frame)
      tx_see__Fworld__Fgeo = tx_see__Fworld__Flink # * geometry_info.transform TODO

      hr_occluder__Fgeo = convert(HyperRectangle{3, T}, robot_model.bounding_geometries[occluder_i].geometry)

      gjk_args = (
        geo1 = hr_occluder__Fgeo,
        pose1 = tx_see__Fworld__Fgeo,
        geo2 = GeometryTypes.FlexibleConvexHull(frustum__Ftarget_geo),
        pose2 = tx_target__Fworld__Fgeo
      )
      result = gjk_unified(gjk_args...)
      if queries != nothing
        queries[(target_i, occluder_i)] = (
          query=(
            geo1 = gjk_args.geo1,
            pose1 = gjk_args.pose1,
            geo2 = copy(gjk_args.geo2),
            pose2 = gjk_args.pose2,
          ),
          result=result
        )
      end

      interaction[target_i, occluder_i] = result.dist_sq
    end
  end
  return interaction
end


# these gjk_unify_* routines should have the same interface
import EnhancedGJK
import GeometryTypes: vertextype, FlexibleConvexHull
GeometryTypes.vertextype(::HyperRectangle{N, T}) where {N, T} = Point{N, T}
GeometryTypes.vertextype(::FlexibleConvexHull{PT}) where {PT} = PT

gjk_number_type(geo1, geo2) = promote_type(eltype(vertextype(geo1)), eltype(vertextype(geo2)))
function gjk_unify_egjk(geo1__Fa, tx__Fw__Fa, geo2__Fb, tx__Fw__Fb)
  T = gjk_number_type(geo1__Fa, geo2__Fb)
  pose1 = convert_tx(AffineMap, T, tx__Fw__Fa)
  pose2 = convert_tx(AffineMap, T, tx__Fw__Fb)

  args = (geo1__Fa, geo2__Fb, pose1, pose2)
  result = EnhancedGJK.gjk(args...)

  signed_distance_proxy = if result.in_collision
    -EnhancedGJK.simplex_penetration_distance(result)
  else
    EnhancedGJK.separation_distance(result)
  end

  sep_dist_sq = if result.in_collision
    0.0f0
  else
    EnhancedGJK.separation_distance(result)^2
  end

  return (
    dist_sq = sep_dist_sq,
    witness_point_1 = pose1(result.closest_point_in_body.a),
    witness_point_2 = pose2(result.closest_point_in_body.b)
  )
end

import GJK_Reference
function gjk_unify_gjk_ref(geo1__Fa, tx__Fw__Fa, geo2__Fb, tx__Fw__Fb)
  # TODO allow coercing to any type?
  T = gjk_number_type(geo1__Fa, geo2__Fb)
  pose1 = convert_tx(AffineMap, T, tx__Fw__Fa)
  pose2 = convert_tx(AffineMap, T, tx__Fw__Fb)

  tr1 = GJK_Reference.allocate_gjk_tr(3, T)
  tr2 = GJK_Reference.allocate_gjk_tr(3, T)
  GJK_Reference.gjk_tr!(tr1, 3, pose1)
  GJK_Reference.gjk_tr!(tr2, 3, pose2)
  args = (collect(vertices(geo1__Fa)), tr1, collect(vertices(geo2__Fb)), tr2)
  result = GJK_Reference.gjk_distance(args...)

  return (dist_sq = result.sq_dist, witness_point_1 = result.wpt1, witness_point_2 = result.wpt2)
end

function gjk_unify_gjk_ref2_args(geo1__Fa, tx__Fw__Fa, geo2__Fb, tx__Fw__Fb)
  pose1 = convert_tx(AffineMap, Float32, tx__Fw__Fa)
  pose2 = convert_tx(AffineMap, Float32, tx__Fw__Fb)

  geo1__Fw = collect(map(pose1, vertices(geo1__Fa)))
  geo2__Fw = collect(map(pose2, vertices(geo2__Fb)))

  (
    collect(geo1__Fw), nothing,
    collect(geo2__Fw), nothing
  )
end

function gjk_unify_gjk_ref2(inargs...)
  gjk_args = gjk_unify_gjk_ref2_args(inargs...)
  result = GJK_Reference.gjk_distance(gjk_args...)
  return (dist_sq = result.sq_dist, witness_point_1=result.wpt1, witness_point_2=result.wpt2)
end


function gjk_unify_qp(geo1__Fa, tx__Fw__Fa, geo2__Fb, tx__Fw__Fb)
  pose1 = convert_tx(AffineMap, Float32, tx__Fw__Fa)
  pose2 = convert_tx(AffineMap, Float32, tx__Fw__Fb)

  geo1_Fw = collect(map(pose1, vertices(geo1__Fa)))
  geo2_Fw = collect(map(pose2, vertices(geo2__Fb)))
  r = GeometryReferenceGenericSolver.find_minimum_distance(
    geo1_Fw,
    geo2_Fw)
  return (dist_sq=r.d_sq, witness_point_1 = r.p1, witness_point_2 = r.p2)
end


#-------------

rbd.set_configuration!(see_state, movo_joints["left_shoulder_lift_joint"], +0.5)
if false
scene = visualize_visibility_check(handles, see_state, target_state)
display(scene)
end
#interaction = link_bounding_visibility_check(see_state, target_state, tri_tet_sq_distance_qp)

function make_bb_vis(robot_model, vis; color=ColorTypes.RGBA{Float32}(0.5, 0.5, 0.9, 0.4))
  mvis_bb = MeshCatMechanisms.MechanismVisualizer(robot_model.mechanism, MeshCatMechanisms.Skeleton(inertias=false), vis[:robot_link_aabb])

  bb_visual_elements = [@set x.color = color for x in robot_model.bounding_geometries]
  MeshCatMechanisms._set_mechanism!(mvis_bb, bb_visual_elements)
  return mvis_bb
end

function visualize_query()
  vis = MeshCat.Visualizer()
  open(vis)

  mvis_see = make_bb_vis(robot_model, vis[:bb_see])
  mvis_target = make_bb_vis(robot_model, vis[:bb_target]; color=ColorTypes.RGBA{Float32}(0.5, 0.9, 0.5, 0.4))

  # TODO silly to need the .q
  MeshCatMechanisms.set_configuration!(mvis_see, see_state.q)
  MeshCatMechanisms.set_configuration!(mvis_target, target_state.q)
  return vis
end

@assignonce vis = visualize_query()

function run_one(gjk_unified)
  debug_gjk = Dict{Any, Any}()
  interaction = link_bounding_visibility_check_unify(see_state, target_state, gjk_unified, debug_gjk);
  return (interaction, debug_gjk)
end



using Polyhedra
convex_mesh(points) = Polyhedra.Mesh(Polyhedra.polyhedron(Polyhedra.vrep(map(Array, points))))

using GeometryTypes: HyperSphere, Point, Vec
function debug_broadphase_vis(vis, gjk_query_result)
  debug_gjk_query(vis, gjk_query_result.query)
  debug_gjk_result(vis, gjk_query_result.result)
end

function debug_gjk_query(vis, gjk_query)
  MeshCat.setobject!(vis[:frustum], convex_mesh(gjk_query.geo2.vertices),
    MeshCat.MeshPhongMaterial(color=MeshCat.RGBA{Float32}(0, 1, 0, 0.5))
  )
  MeshCat.settransform!(vis[:frustum], convert_tx(AffineMap, Float32, gjk_query.pose2))

  MeshCat.setobject!(vis[:occluder], gjk_query.geo1,
    MeshCat.MeshPhongMaterial(color=MeshCat.RGBA{Float32}(0, 0, 1, 0.5))
  )
  MeshCat.settransform!(vis[:occluder], convert_tx(AffineMap, Float32, gjk_query.pose1))
end

function debug_gjk_result(vis, gjk_result)
  function p(vis, point)
    MeshCat.setobject!(vis,
      HyperSphere(Point(point), convert(eltype(point), 0.01)),
      MeshCat.MeshPhongMaterial(color=MeshCat.RGBA{Float32}(1, 0, 0, 0.5))
    )
  end
  p(vis["witness1"], Point(gjk_result.witness_point_1...))
  p(vis["witness2"], Point(gjk_result.witness_point_2...))
end

(interaction_egjk, debug_egjk) = run_one(gjk_unify_egjk)
(interaction_gjk_ref, debug_gjk_ref) = run_one(gjk_unify_gjk_ref)
(interaction_qp, debug_qp) = run_one(gjk_unify_qp)
#interaction_qp = link_bounding_visibility_check(see_state, target_state, tri_tet_sq_distance_qp)

function debug_broadphase_vis_all(idx)
  debug_gjk_query(vis[:debug_gjk_query], debug_egjk[idx].query)
  debug_gjk_result(vis[:debug_egjk], debug_egjk[idx].result)
  debug_gjk_result(vis[:debug_gjk_ref], debug_gjk_ref[idx].result)
  debug_gjk_result(vis[:debug_qp], debug_qp[idx].result)
  println("\n--")
  @show idx
  dsq_d(dsq) = (dsq, sqrt(max(0,dsq)))
  @show dsq_d(interaction_egjk[idx...])
  @show dsq_d(interaction_gjk_ref[idx...])
  @show dsq_d(interaction_qp[idx...])
end

sep_dist_sq = 1e-6  # distance above which "no collision"
sep_dist = sqrt(sep_dist_sq)
@show sep_dist
egjk_qp_discrepencies = findall(.!((interaction_egjk .> sep_dist_sq ) .== (interaction_qp .> sep_dist_sq)))

gjk_ref_qp_discrepencies = findall(.!((interaction_gjk_ref .> sep_dist_sq ) .== (interaction_qp .> sep_dist_sq)))

# these are link indices for which the bounding box for the link contains the optical origin.
problematic_occluders = [3,8,9]

filter_discrep(d) = [e for e in d if e[2] ∉ problematic_occluders]
egjk_qp_discrepencies2 = filter_discrep(egjk_qp_discrepencies)
if length(egjk_qp_discrepencies2) > 1
  debug_broadphase_vis_all(Tuple(
    first(egjk_qp_discrepencies2)
  ))
end

if false
  link_i = 10
  eye__Fworld = get_eye__Fworld(see_state)
  bb_mesh_target__Fworld = get_link_bounding__Fworld(target_state, link_i)
  bb_mesh_see__Fworld = get_link_bounding__Fworld(see_state, link_i)

  tets__Fworld = get_tets__Fworld(eye__Fworld, target_state, link_i)
  tris__Fworld = get_tris__Fworld(see_state, link_i)

  s = Makie.Scene()
  Makie.wireframe!(s, bb_mesh_target__Fworld; color=:green, linewidth=1.0)
  Makie.wireframe!(s, bb_mesh_see__Fworld; color=:red, linewidth=1.0)

  tet__Fworld = tets__Fworld[1]
  tri__Fworld = tris__Fworld[1]

  Makie.wireframe!(s, GLPlainMesh(Simplex(tet__Fworld)); color=:gray, alpha=0.5, linewidth=2.0)
  Makie.wireframe!(s, GLPlainMesh(Simplex(tri__Fworld)); color=:gray, alpha=0.5, linewidth=2.0)
end
