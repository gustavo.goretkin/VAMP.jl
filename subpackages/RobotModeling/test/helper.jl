import RobotModeling

import RigidBodyDynamics
import MechanismGeometries
import MeshCatMechanisms
import CoordinateTransformations
import GeometryTypes
import Geometry

const rbd = RigidBodyDynamics
const mg = MechanismGeometries
const mcm = MeshCatMechanisms
const gt = GeometryTypes
const Geo = Geometry

const coordtx = CoordinateTransformations
import Geometry

import MeshCat
import KinovaMovoRobot

using AssignOnce: @assignonce
@assignonce robot_model = begin
    empty!(rbd.Spatial.frame_names)
    RobotModeling.get_robot(;urdf_path=KinovaMovoRobot.urdfpath(), asset_dir=KinovaMovoRobot.packagepath(), target_detail_radius=0.02)
end


@assignonce vis = begin
    vis = MeshCat.Visualizer()
    open(vis)
    vis
end

function MechanismVisualizer_helper(mechanism_state, visualizer, geometry_element)
    delete!(visualizer)
    mech_vis = mcm.MechanismVisualizer(mechanism_state, visualizer)
    #rbd.zero_configuration!(mech_vis.state)
    mcm._set_mechanism!(mech_vis, geometry_element)
    mcm._render_state!(mech_vis)
    return mech_vis
end
