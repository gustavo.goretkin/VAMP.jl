include("helper.jl")

conf = let state = rbd.MechanismState(robot_model.mechanism)
    rbd.zero_configuration!(state)
    rbd.configuration(state)
end

state =rbd.MechanismState(robot_model.mechanism)
rbd.zero_configuration!(state)

robot =  RobotModeling.PosedGeometricMechanism{Float64}(
    robot_model.mechanism,
    robot_model.collision_geometries,
    conf
)

# configuration bounding volume, for setting the base cases and for memoization
qbv_dict_manual = Dict{Any, Any}()

# step 1, just put a static-configuration bounding element on the grippers
for link_name in ["right_gripper_base_link", "left_gripper_base_link"]
    body = rbd.findbody(robot.mechanism, link_name)
    subrobot = RobotModeling.subrobot(robot, body)
    Fbase = rbd.root_frame(subrobot.mechanism)
    @assert Fbase == rbd.default_frame(body)

    aabb__Fbase = Geo.boundingvolume(gt.HyperRectangle, subrobot, Fbase)


    geo_el__Fbase = mg.VisualElement(Fbase, aabb__Fbase,
        mg.RGBA{Float32}(1.0,1.0,0.0,0.5), coordtx.IdentityTransformation())

    qbv_dict_manual[body] = [geo_el__Fbase]
end


if false
mechanism = robot_model.mechanism
# TODO do a topological sort
leafs = []
for body in rbd.Graphs.vertices(mechanism.tree)
    etc = rbd.Graphs.edges_to_children(body, mechanism.tree)
    if length(etc) == 0
        push!(leafs, body)
    end
end

a_leaf = leafs[6]

ancestor(vertex, tree, i=1) = rbd.Graphs.ancestors(a_leaf, tree)[1+i]

# test base case
qbv_1 = RobotModeling.get_configuration_bounding_element(robot_model.mechanism, robot_model.collision_geometries, a_leaf)

# one up, Fixed
a_body_fixed = ancestor(a_leaf, mechanism.tree)
qbv_2 = RobotModeling.get_configuration_bounding_element(mechanism, robot_model.collision_geometries, a_body_fixed;
    base_case_override=qbv_dict_manual)

a_body_behind_revolute = ancestor(a_body_fixed, mechanism.tree, 2)
qbv_3 = RobotModeling.get_configuration_bounding_element(mechanism, robot_model.collision_geometries, a_body_behind_revolute;
    base_case_override=qbv_dict_manual)


qbv_s = vcat(qbv_1, qbv_2, qbv_3)
qbv_s = [qbv_s...] # make concrete TODO fix above

mcm._set_mechanism!(right_hand_vis_qbv, qbv_s)
end

qbve = RobotModeling.get_configuration_bounding_element(
    robot_model.mechanism,
    robot_model.collision_geometries,
    rbd.findbody(robot_model.mechanism, "left_wrist_3_link");
    base_case_override=qbv_dict_manual)

qbve = [qbve...]

import Setfield: @set
bv_color = mg.RGBA{Float32}(0.0,1.0,0.0,0.5)
qbv_color = mg.RGBA{Float32}(0.0,0.0,1.0,0.5)
mechanism_vis = MechanismVisualizer_helper(state, vis[:mechanism_bv],
    [@set ge.color = bv_color for ge in robot_model.bounding_geometries])

mechanism_vis_qbv = MechanismVisualizer_helper(state, vis[:mechanism_qbve],
    [@set ge.color = qbv_color for ge in qbve])

mechanism_vis_manul_qbv = MechanismVisualizer_helper(state, vis[:mechanism_manual_qbv],
    vcat(collect(values(qbv_dict_manual))...))
