include("helper.jl")

right_hand_mechanism = rbd.submechanism(robot_model.mechanism, rbd.findbody(robot_model.mechanism, "right_ee_link"))

fingers_mechanism = rbd.submechanism(right_hand_mechanism, rbd.findbody(right_hand_mechanism, "right_gripper_base_link"))

conf = let state = rbd.MechanismState(robot_model.mechanism)
    rbd.zero_configuration!(state)
    rbd.configuration(state)
end

fingers_robot = RobotModeling.subrobot(
    RobotModeling.PosedGeometricMechanism{Float64}(
        robot_model.mechanism,
        robot_model.collision_geometries,
        conf
    ),
    rbd.findbody(robot_model.mechanism, "right_gripper_base_link")
)

Froot = rbd.root_frame(fingers_robot.mechanism)

fingers_geo = fingers_robot.geometry_elements

bv_fingers_Fbase = Geo.boundingvolume(
    gt.HyperRectangle,
    fingers_robot,
    Froot
)


bv_ve = mg.VisualElement(Froot, bv_fingers_Fbase,
    mg.RGBA{Float32}(0.0,1.0,0.0,0.5), coordtx.IdentityTransformation())

fingers_mechanism_state = rbd.MechanismState(fingers_mechanism)
rbd.zero_configuration!(fingers_mechanism_state)

fingers_vis = MechanismVisualizer_helper(fingers_mechanism_state, vis[:fingers], fingers_geo)
fingers_bv_vis = MechanismVisualizer_helper(fingers_mechanism_state, vis[:fingers_single_aabb], [bv_ve])

fingers_geo_Froot = [RobotModeling.visual_element_in_frame(geo_el, fingers_mechanism_state, Froot) for geo_el in fingers_geo]

fingers_vis_base = MechanismVisualizer_helper(fingers_mechanism_state, vis[:fingers_base], fingers_geo_Froot)


bvs = [Geo.boundingvolume(gt.HyperRectangle, Geo.PointCloud(ve.geometry), ve.transform) for ve in fingers_geo_Froot]

bvs_ve = [mg.VisualElement(Froot, geometry, mg.RGBA{Float32}(0.0,1.0,0.0,0.5), coordtx.IdentityTransformation())
    for geometry in bvs]

fingers_vis_base_aabb = MechanismVisualizer_helper(fingers_mechanism, vis[:fingers_base_aabb], bvs_ve)
