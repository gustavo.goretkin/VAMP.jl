# I think CGAL needs to be loaded first, because it sets rtti for the C++ compiler, otherwise
#/Users/goretkin/projects/CGAL.jl/deps/cgal/STL_Extension/include/CGAL/Object.h:130:16: error: cannot use typeid with -fno-rtti
#        return typeid(void);


using CGAL
using CGAL.Cxx

cxx"""
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef K::Point_3          Point_3;
typedef K::Vector_3         Vector_3;
typedef K::Triangle_3       Triangle_3;
typedef K::Tetrahedron_3    Tetrahedron_3;
"""

"doesn't actually return sq distance, but 0.0 or 1.0 depending on intersection test"
function tri_tet_sq_distance_cgal_bin(tri, tet)
  tetc = @cxx Tetrahedron_3(
    (@cxx Point_3(tet[1][1], tet[1][2], tet[1][3])),
    (@cxx Point_3(tet[2][1], tet[2][2], tet[2][3])),
    (@cxx Point_3(tet[3][1], tet[3][2], tet[3][3])),
    (@cxx Point_3(tet[4][1], tet[4][2], tet[4][3])),
  )
  tric = @cxx Triangle_3(
    (@cxx Point_3(tri[1][1], tri[1][2], tri[1][3])),
    (@cxx Point_3(tri[2][1], tri[2][2], tri[2][3])),
    (@cxx Point_3(tri[3][1], tri[3][2], tri[3][3])),
  )
  i = (@cxx CGAL::do_intersect(tetc, tric))
  return (if i; 0.0 else 1.0 end;)
end

cgal_point(point) = icxx"Point_3($(point[1]), $(point[2]), $(point[3]));"

function make_tet(tet)
  p1 = cgal_point(tet[1])
  p2 = cgal_point(tet[2])
  p3 = cgal_point(tet[3])
  p4 = cgal_point(tet[4])

  tetc = icxx"Tetrahedron_3($p1, $p2, $p3, $p4);"
  return tetc
end

function make_tri(tri)
  p1 = cgal_point(tri[1])
  p2 = cgal_point(tri[2])
  p3 = cgal_point(tri[3])

  tric = icxx"Triangle_3($p1, $p2, $p3);"
  return tric
end


function tri_tet_sq_distance_cgal_bin3(tri, tet)
  tetc = make_tet(tet)
  tric = make_tri(tri)
  i = (@cxx CGAL::do_intersect(tetc, tric))
  return (if i; 0.0 else 1.0 end;)
end


function tri_tet_sq_distance_cgal_bin2(tri, tet)
  tetc = icxx"""Tetrahedron_3(
    (Point_3($(tet[1][1]), $(tet[1][2]), $(tet[1][3]))),
    (Point_3($(tet[2][1]), $(tet[2][2]), $(tet[2][3]))),
    (Point_3($(tet[3][1]), $(tet[3][2]), $(tet[3][3]))),
    (Point_3($(tet[4][1]), $(tet[4][2]), $(tet[4][3]))),
  );"""
  tric = icxx"""Triangle_3(
    (Point_3($(tri[1][1]), $(tri[1][2]), $(tri[1][3]))),
    (Point_3($(tri[2][1]), $(tri[2][2]), $(tri[2][3]))),
    (Point_3($(tri[3][1]), $(tri[3][2]), $(tri[3][3]))),
  );"""
  i = (@cxx CGAL::do_intersect(tetc, tric))
  return (if i; 0.0 else 1.0 end;)
end
