using GeometryTypes: HyperRectangle, FlexibleConvexHull, Vec
using CoordinateTransformations: AffineMap
using StaticArrays
using Polyhedra

convex_hull_gg(vertices) = collect(points(vrep(polyhedron(hrep(polyhedron(vrep(vertices)))))))

staticfy(tx) = AffineMap(SMatrix{3,3}(tx.linear), SVector{3}(tx.translation))

function to_convex_world(gjk_call)
    geo1__Fgeo1 = gjk_call.geo1
    geo2__Fgeo2 = gjk_call.geo2
    tx__Fworld__Fgeo1 = staticfy(gjk_call.pose1)
    tx__Fworld__Fgeo2 = staticfy(gjk_call.pose2)

    geo1__Fworld = FlexibleConvexHull(map(tx__Fworld__Fgeo1, SVector.(vertices(geo1__Fgeo1))))

    geo2__Fworld_nonconvex = FlexibleConvexHull(map(tx__Fworld__Fgeo2,
            SVector.(
            map(x->map(y->convert(Float32, y), x),
                (SVector.(vertices(geo2__Fgeo2)))
            )
            )
        ))

    geo2__Fworld = FlexibleConvexHull(map(tx__Fworld__Fgeo2,
        SVector.(
        map(x->map(y->convert(Float32, y), x),
            convex_hull_gg(SVector.(vertices(geo2__Fgeo2)))
        )
        )
    ))
    return (geo1__Fworld, geo2__Fworld, geo2__Fworld_nonconvex)
end
