using StaticArrays
using GeometryTypes
using EnhancedGJK
# https://github.com/goretkin/GeometryReferenceGenericSolver
using GeometryReferenceGenericSolver

colliding_pairs = [
(FlexibleConvexHull{SArray{Tuple{3},Float32,1,3}}(SArray{Tuple{3},Float32,1,3}[[0.9740448, -0.10185, 0.7140564], [0.9740448, -0.10185, 0.7931733], [1.1091698, -0.10185, 0.7140564], [1.1091698, -0.10185, 0.7931733], [0.9740448, -0.16485, 0.7140564], [0.9740448, -0.16485, 0.7931733], [1.1091698, -0.16485, 0.7140564], [1.1091698, -0.16485, 0.7931733]]), FlexibleConvexHull{SArray{Tuple{3},Float32,1,3}}(SArray{Tuple{3},Float32,1,3}[[1.9418844, -0.2089226, 0.36910376], [1.6882384, 0.14019117, 0.36910376], [1.9418844, -0.2089226, 1.1028918], [2.3888526, 0.11581881, 0.36910376], [2.3888526, 0.11581881, 1.1028918], [2.1352067, 0.46493256, 0.36910376], [0.3095777, -0.033249497, 1.0613251], [2.1352067, 0.46493256, 1.1028918], [1.6882384, 0.14019117, 1.1028918]])),

(FlexibleConvexHull{SArray{Tuple{3},Float32,1,3}}(SArray{Tuple{3},Float32,1,3}[[0.7356198, -0.1746, 0.79131997], [0.4894948, -0.1746, 0.79131997], [0.7356198, -0.1746, 0.70645565], [0.4894948, -0.1746, 0.70645565], [0.7356198, -0.092099994, 0.79131997], [0.4894948, -0.092099994, 0.79131997], [0.7356198, -0.092099994, 0.70645565], [0.4894948, -0.092099994, 0.70645565]]), FlexibleConvexHull{SArray{Tuple{3},Float32,1,3}}(SArray{Tuple{3},Float32,1,3}[[1.8752792, -0.28443995, 0.280728], [1.8752792, -0.28443995, 0.0], [1.595835, 0.1001819, 0.0], [2.4041271, 0.09979049, 0.280728], [2.4041271, 0.09979049, 0.0], [0.3095777, -0.03324946, 1.0613251], [2.124683, 0.48441234, 0.0], [2.124683, 0.48441234, 0.280728]])),

(FlexibleConvexHull{SArray{Tuple{3},Float32,1,3}}(SArray{Tuple{3},Float32,1,3}[[0.19948179, -0.12499971, 1.0193152], [0.19948179, -0.12499962, 1.0898153], [0.19948146, 0.1250003, 1.019315], [0.19948146, 0.12500039, 1.0898149], [0.3170779, -0.124999546, 1.0193152], [0.3170779, -0.12499945, 1.0898153], [0.31707758, 0.12500045, 1.019315], [0.31707758, 0.12500054, 1.0898149]]), FlexibleConvexHull{SArray{Tuple{3},Float32,1,3}}(SArray{Tuple{3},Float32,1,3}[[2.9813108, 0.9511965, 0.8458499], [2.994311, 0.9333032, 0.8417178], [2.955267, 0.9049359, 0.8417178], [2.9772167, 0.9568318, 0.8085657], [2.990217, 0.9389385, 0.8044336], [0.3095777, -0.033249438, 1.0613252], [2.9511726, 0.9105712, 0.8044336], [2.9381723, 0.9284645, 0.8085657]])),
]


signed_distance_proxy(result) = if result.in_collision
        -EnhancedGJK.simplex_penetration_distance(result)
      else
        EnhancedGJK.separation_distance(result)
      end

for pair in colliding_pairs
    qp_result = GeometryReferenceGenericSolver.find_minimum_distance(vertices.(pair)...)
    gjk_result = EnhancedGJK.gjk(pair...)
    println("---------")
    @show qp_result.d_sq
    @show gjk_result.in_collision
    @show signed_distance_proxy(gjk_result)
    @show signed_distance_proxy(gjk_result)^2
end
