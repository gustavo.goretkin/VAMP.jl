using StaticArrays
using GeometryTypes
using EnhancedGJK
# https://github.com/goretkin/GeometryReferenceGenericSolver
using GeometryReferenceGenericSolver

signed_distance_proxy(result) = if result.in_collision
        -EnhancedGJK.simplex_penetration_distance(result)
      else
        EnhancedGJK.separation_distance(result)
      end


import Combinatorics: permutations, multiset_permutations
p3 = [rand(SArray{Tuple{3}, Float64}) for _ in 1:3]

bad_p3s = [
@SArray SArray{Tuple{3},Float32,1,3}[[0.52585113, 0.33685374, 0.58089995], [0.6485164, 0.6289742, 0.84126365], [0.27516925, 0.9550772, 0.44672143]]
,
@SArray SArray{Tuple{3},Float64,1,3}[[0.9349150115935445, 0.38289685050819466, 0.2554398465769967], [0.3427108043006297, 0.5461113044612809, 0.12919637095807368], [0.4718816481224819, 0.6704336007734557, 0.2565579020929114]]
]


p3 = bad_p3s[2]
try_simplex = @SArray [p3[1], p3[2], p3[2], p3[3]]
try_weights = EnhancedGJK.projection_weights_reference(try_simplex)
@show try_weights


# simplex with one repeated point
s_base = [p3[1], p3[2], p3[3]]
weightss = [
    begin
        s = s_base[permutation]
        try_simplex = @SArray [s[1], s[2], s[3], s[4]]
        try_weights = EnhancedGJK.projection_weights_reference(try_simplex)
        (weights=try_weights, p=permutation)
    end
    for permutation in multiset_permutations([1,2,2,3], 4)
]

resultss = [
    begin
        s = s_base[permutation]
        try_simplex = @SArray [s[1], s[2], s[3], s[4]]
        r = EnhancedGJK.gjk(
        make_dummy_neighbor_mesh(
        #FlexibleConvexHull(
            collect(try_simplex)),
        FlexibleConvexHull([@SArray[0, 0, 0]]))
        (r, signed_distance_proxy(r), r.iterations)
    end
    for permutation in multiset_permutations([1,2,2,3], 4)
]
