using StaticArrays
using GeometryTypes
using EnhancedGJK
# https://github.com/goretkin/GeometryReferenceGenericSolver
using GeometryReferenceGenericSolver

good_pair =
(FlexibleConvexHull{SArray{Tuple{3},Float32,1,3}}(SArray{Tuple{3},Float32,1,3}[[0.4893698, -0.1746, 0.70658064], [0.4893698, -0.1746, 0.79144496], [0.24324478, -0.1746, 0.70658064], [0.24324478, -0.1746, 0.79144496], [0.4893698, -0.092099994, 0.70658064], [0.4893698, -0.092099994, 0.79144496], [0.24324478, -0.092099994, 0.70658064], [0.24324478, -0.092099994, 0.79144496]]), FlexibleConvexHull{SArray{Tuple{3},Float32,1,3}}(SArray{Tuple{3},Float32,1,3}[[2.6326709, 0.76342815, 0.7298917], [2.7419894, 0.84285265, 0.7298917], [2.7419894, 0.84285265, 0.80900866], [2.6697016, 0.7124601, 0.7298917], [2.7790198, 0.7918846, 0.7298917], [0.3095777, -0.033249497, 1.0613251], [2.7790198, 0.7918846, 0.80900866], [2.6697016, 0.7124601, 0.80900866]]))

# good_pair is convex hull of nan_pair

nan_pair = (FlexibleConvexHull{SArray{Tuple{3},Float32,1,3}}(SArray{Tuple{3},Float32,1,3}[[0.4893698, -0.1746, 0.70658064], [0.4893698, -0.1746, 0.79144496], [0.24324478, -0.1746, 0.70658064], [0.24324478, -0.1746, 0.79144496], [0.4893698, -0.092099994, 0.70658064], [0.4893698, -0.092099994, 0.79144496], [0.24324478, -0.092099994, 0.70658064], [0.24324478, -0.092099994, 0.79144496]]), FlexibleConvexHull{SArray{Tuple{3},Float32,1,3}}(SArray{Tuple{3},Float32,1,3}[[0.3095777, -0.033249497, 1.0613251], [2.7419894, 0.84285265, 0.7298917], [2.6326709, 0.76342815, 0.7298917], [2.7419894, 0.84285265, 0.80900866], [2.6326709, 0.76342815, 0.80900866], [2.7790198, 0.7918846, 0.7298917], [2.6697016, 0.7124601, 0.7298917], [2.7790198, 0.7918846, 0.80900866], [2.6697016, 0.7124601, 0.80900866]]))


@assert Set(vertices(nan_pair[1])) ==  Set(vertices(good_pair[1]))
extra_nan_points = setdiff(Set(vertices(nan_pair[2])),  Set(vertices(good_pair[2])))
@assert 1 == length(extra_nan_points)
extra_nan_point = first(extra_nan_points)
@assert 0 == length(setdiff(Set(vertices(good_pair[2])),  Set(vertices(nan_pair[2]))))
extra_idxs = findall(vertices(nan_pair[2]) .== Ref(extra_nan_point))
@assert 1 == length(extra_idxs)
# index of point in `nan_pair` not in `good_pair`
extra_idx = first(extra_idxs)

good_result = EnhancedGJK.gjk(good_pair...)
nan_result = EnhancedGJK.gjk(nan_pair...)

nan_simplex = simplex = @SArray (SArray{Tuple{3},Float32,1,3}[[0.1797921, -0.058850497, -0.26988012], [-0.06633292, -0.058850497, -0.26988012], [-0.06633292, -0.058850497, -0.26988012], [-2.4264567, -0.80456007, 0.06155324]])

nan_weights = EnhancedGJK.projection_weights_reference(nan_simplex)

import Combinatorics: permutations
p3 = [rand(SArray{Tuple{3}, Float32}) for _ in 1:3]

bad_p3 = @SArray SArray{Tuple{3},Float32,1,3}[[0.52585113, 0.33685374, 0.58089995], [0.6485164, 0.6289742, 0.84126365], [0.27516925, 0.9550772, 0.44672143]]

p3 = bad_p3
try_simplex = @SArray [p3[1], p3[2], p3[2], p3[3]]
try_weights = EnhancedGJK.projection_weights_reference(try_simplex)
@show try_weights

# simplex with one repeated point
s_base = [p3[1], p3[1], p3[2], p3[3]]
weightss = [
    begin
        s = s_base[permutation]
        try_simplex = @SArray [s[1], s[2], s[3], s[4]]
        try_weights = EnhancedGJK.projection_weights_reference(try_simplex)
        (weights=try_weights, p=permutation)
    end
    for permutation in permutations(1:4, 4)
]
show(IOContext(stdout, :limit=>false), MIME"text/plain"(), weightss)

using LinearAlgebra: dot, cross
"""
if origin is closer to point(q1) than to any other feature of the simplex
pg. 403 of Ericson Real-Time Collision Detection
"""
function is_closest_point(q1, q2, q3, q4)
    v2 = q2 - q1
    v3 = q3 - q1
    v4 = q4 - q1
    d = (#=p=# - q1)
    return dot(d, v2) ≤ 0 && dot(d, v3) ≤ 0 && dot(d, v3) ≤ 0
end

"""
if origin is closer to edge(q1, q2) than to any other feature of the simplex
pg. 403 of Ericson Real-Time Collision Detection
"""
function is_closest_edge(q1, q2, q3, q4)
    d1 = (#=p=# - q1)
    d2 = (#=p=# - q2)
    n_123 = cross(q2 - q1, q3 - q1)
    n_142 = cross(q4 - q1, q2 - q1)
    return (
        dot(d1, q2 - q1) ≥ 0 &&
        dot(d2, q1 - q2) ≥ 0 &&
        dot(d1, cross(q2 - q1, n_123)) ≥ 0 &&
        dot(d1, cross(n_142, q2 - q1)) ≥ 0
    )
end

"""
if origin is closer to face(q1, q2, q3) than to any other feature of the simplex
pg. 403 of Ericson Real-Time Collision Detection
"""
function is_closest_face(q1, q2, q3, q4)
    n_123 = cross(q2 - q1, q3 - q1)
    d1 = (#=p=# - q1)
    return dot(d1, n_123) * dot(q4 - q1, n_123) < 0
end

function find_closest_origin(ps...)
    n = length(ps)
    M = hcat((p - ps[end] for p in ps[1:end-1])...)
    weights_m1 = (inv(M' * M) * M') * ps[end]
    weight_end = 1 - sum(weights_m1)
    weights = vcat(weights_m1, (weight_end))
    return weights
end

function has_closest_point(q1, q2, q3, q4)
    if is_closest_point(q1, q2, q3, q4)
        return (1,)
    elseif is_closest_point(q2, q1, q3, q4)
        return (2,)
    elseif is_closest_point(q3, q1, q2, q4)
        return (3,)
    elseif is_closest_point(q4, q1, q2, q3)
        return (4,)
    end
    return nothing
end

function has_closest_edge(q1, q2, q3, q4)
    if is_closest_edge(q1, q2, q3, q4)
        return (1,2)
    elseif is_closest_edge(q1, q3, q2, q4)
        return (1,3)
    elseif is_closest_edge(q1, q4, q2, q3)
        return (1,4)
    elseif is_closest_edge(q2, q3, q1, q4)
        return (2,3)
    elseif is_closest_edge(q2, q4, q1, q3)
        return (2,4)
    elseif is_closest_edge(q3, q4, q1, q2)
        return (3,4)
    end
    return nothing
end

function has_closest_face(q1, q2, q3, q4)
    if is_closest_face(q1, q2, q3, q4)
        return (1,2,3)
    elseif is_closest_face(q1, q2, q4, q3)
        return (1,2,4)
    elseif is_closest_face(q1, q3, q4, q2)
        return (1,3,4)
    elseif is_closest_face(q2, q3, q4, q1)
        return (2,3,4)
    end
    return nothing
end

function find_closest_feature(q1, q2, q3, q4)
    p = has_closest_point(simplex...)
    if p != nothing
        return p
    end
    p = has_closest_edge(simplex...)
    if p != nothing
        return p
    end
    p = has_closest_face(simplex...)
    if p != nothing
        return p
    end
    return (1,2,3,4)
end


function make_dummy_neighbor_mesh(points)
    m = HomogenousMesh(points,[],[],[], Nothing, Nothing, [])
    NeighborMesh(m, let n = length(vertices(m)); [Set(1:n) for _ in 1:n]; end)
end

nan_pair_nm = make_dummy_neighbor_mesh.(vertices.(nan_pair));

using Polyhedra
convex_hull_gg(vertices) = collect(points(vrep(polyhedron(hrep(polyhedron(vrep(vertices)))))))


function all_pairs_diff(pointset1, pointset2)
    pointsetdiff = [p1 - p2 for p1 in pointset1 for p2 in pointset2]
end


using EnhancedGJK
using MeshCat
using Polyhedra
using StaticArrays: SVector
using GeometryTypes: HyperSphere, Point

function visualize_simplex(vis::Visualizer, simplex)
    p = polyhedron(vrep(simplex))
    setobject!(vis[:simplex], Polyhedra.Mesh(p))
    for (i, point) in enumerate(simplex)
        setobject!(vis["p$i"], HyperSphere(Point(point), convert(eltype(point), 0.03)))
    end
end

vis = Visualizer()
open(vis)



convex_mesh(points) = Polyhedra.Mesh(Polyhedra.polyhedron(Polyhedra.vrep(map(Array, points))))

import QHull
function convex_hull_point_iter(point_iter)
    ch = QHull.chull(Array(reduce(hcat, point_iter)'))
end

function convex_mesh2(points)
    ch = convex_hull_point_iter(points)
    # SimpleMesh(apd, ch.facets)
    HomogenousMesh(map(Point, points), map(x->Face(x...), ch.simplices), [], [], nothing, nothing, [])
end

import MeshUtils
apd = all_pairs_diff(vertices.(nan_pair)...)
#diff_1_2 = convex_mesh(apd);
diff_1_2_ = convex_mesh2(apd)
diff_1_2__ = HomogenousMesh(diff_1_2_.vertices, diff_1_2_.faces)
diff_1_2 = MeshUtils.remove_unused_vertices(diff_1_2_);

setobject!(vis[:geo1], convex_mesh(vertices(nan_pair[1])))
setobject!(vis[:geo2], convex_mesh(vertices(nan_pair[2])))

const mc = MeshCat

"""low-level access to three js JSON interface
See https://github.com/mrdoob/three.js/blob/master/src/loaders/MaterialLoader.js"""
mutable struct OverlayMaterial{T<:mc.AbstractMaterial, OVT} <: mc.AbstractMaterial
  underlying::T
  overlay::Dict{String, OVT}
end

function mc.lower(material::OverlayMaterial)
  merge(mc.lower(material.underlying), material.overlay)
end

wireframe_material = OverlayMaterial(
    mc.MeshPhongMaterial(color=mc.RGBA{Float32}(0.8, 0.8, 0.8, 1)),
    Dict{String, Any}("wireframe"=>true))


setobject!(vis[:geo_1_2], MeshUtils.fix_convex_mesh_face_orientation(diff_1_2), wireframe_material)
#setobject!(vis[:geo_1_2_full], diff_1_2__)

setobject!(vis[:geo_1_2_pointcloud_support], PointCloud(vertices(diff_1_2)))
#visualize_simplex(vis[:simplex], result.simplex)

Tmapmap(T, a) = map(x->map(T,x), a)
nan_pair_f64 = Tmapmap.(Float64, vertices.(nan_pair))

# notice, the example works great in Float64 arithmetic, but not Float32
EnhancedGJK.gjk(FlexibleConvexHull.(nan_pair_f64)...)
