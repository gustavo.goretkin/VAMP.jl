import RobotModeling
import KinovaMovoRobot
include("../src/makie_robot_modeling.jl")
import GeometryTypes: HyperRectangle, GLPlainMesh
#import JLD

using AssignOnce: @assignonce

function make_movo_robot_model()
  if false
    filename = "cache_robot_model.jld2"
    robot_model = try
      JLD.load(filename, "robot_model")
    catch err
      println("Error loading cached file. Recreating.")
      @show err

      println("make movo model")
      robot_model = RobotModeling.get_robot(;urdf_path=KinovaMovoRobot.urdfpath(), asset_dir=KinovaMovoRobot.packagepath(), target_detail_radius=0.02)
      println("done with movo model")
      JLD.save(filename, "robot_model", robot_model)
      robot_model
    end
  end

  println("make movo model")
  robot_model = RobotModeling.get_robot(;urdf_path=KinovaMovoRobot.urdfpath(), asset_dir=KinovaMovoRobot.packagepath(), target_detail_radius=0.02)
  println("done with movo model")
  return robot_model
end

@assignonce robot_model = make_movo_robot_model()

s = Makie.Scene()

plot_bot!(s, robot_model.collision_geometries, rbd.MechanismState(robot_model.mechanism))
plot_bot!(s, robot_model.target_geometries, rbd.MechanismState(robot_model.mechanism); plot_cmd! = (s, b)->Makie.scatter!(s, b.points; markersize=0.01, color=:yellow))
plot_bot!(s, robot_model.bounding_geometries, rbd.MechanismState(robot_model.mechanism);
  plot_cmd! = (s, b)->Makie.wireframe!(s,
    GLPlainMesh(convert(HyperRectangle, b)); alpha=0.5, color=:red))

import CoordinateTransformations.Rotations: RotZYX
import StaticArrays: SVector

floating_joint = rbd.findjoint(robot_model.mechanism, "floating_joint")
tx__Fworld__Fbase = rbd.Transform3D(rbd.frame_after(floating_joint), rbd.frame_before(floating_joint),
  RotZYX(0.1, 0.0, 0.0),
  SVector(2.0, 1.0, 0.0)
)

ms_move = rbd.MechanismState(robot_model.mechanism)
rbd.set_configuration!(ms_move, rbd.findjoint(ms_move.mechanism, "right_shoulder_lift_joint"), 0.1)
rbd.set_configuration!(ms_move, floating_joint, tx__Fworld__Fbase)


plot_bot!(s, robot_model.target_geometries, ms_move; plot_cmd! = (s, b)->Makie.scatter!(s, b.points; markersize=0.01, color=:blue))
display(s)
