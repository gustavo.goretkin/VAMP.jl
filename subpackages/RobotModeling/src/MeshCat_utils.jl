function foreachpath(f::Function, s::mc.SceneTrees.SceneNode, path=[])
  f(s, path)
  for (k, v) in s.children
    foreachpath(f, v, vcat(path, [k]))
  end
end

"""given a MeshCat.SceneTrees.SceneNode, e.g. mechvis.visualizer.core.tree, get paths to all leaves"""
function leaf_paths(scene_node)
  paths_to_leaves = []
  function add_if_leaf(scene_node, path)
    if length(scene_node.children) == 0
      push!(paths_to_leaves, path)
    end
  end

  foreachpath(add_if_leaf, scene_node)
  return paths_to_leaves
end



"""visual elements with material definition"""
mutable struct MaterialVisualElement{VE, M}
  visual_element::VE
  material::M
end


function mcm.setelement!(mvis::mcm.MechanismVisualizer, element::MaterialVisualElement, name::AbstractString="<element>")
  mcm.setelement!(mvis, element.visual_element.frame, element.visual_element.geometry, element.material, name)
  mcm.settransform!(mvis[element.visual_element.frame][name], element.visual_element.transform)
end

function mcm._set_mechanism!(mvis::mcm.MechanismVisualizer, elements::AbstractVector{<:MaterialVisualElement}, name="geometry_")
  for (i, element) in enumerate(elements)
      mcm.setelement!(mvis, element, name * "$i")
  end
end


"""low-level access to three js JSON Material interface
See https://github.com/mrdoob/three.js/blob/master/src/loaders/MaterialLoader.js"""
mutable struct OverlayMaterial{T<:mc.AbstractMaterial, OVT} <: mc.AbstractMaterial
  underlying::T
  overlay::Dict{String, OVT}
end

function mc.lower(material::OverlayMaterial)
  merge(mc.lower(material.underlying), material.overlay)
end
