#=
struct SweptVolume#=__Fa=#{S, V#=<:AbstractGeometry=#} #= <: AbstractGeometry=#
    sweeper#=tx__Fa_Fb=#::S,
    volume#=__Fb=#::V
end
=#

struct JointBoundType
end

function proximal_joint_bounding_volume(
    ::Val{:sphere_at_joint_center},
    distal_visual_element,
    joint::rbd.Joint{T, rbd.Revolute{T}}
) where {T}
    # TODO decide how to access fields, through getters or rebeliously

    # TODO use position bounds, for smaller volumes
    #rbd.position_bounds(joint)
    # TODO revolve only about jt.axis, for smaller volumes
    #joint.joint_type.axis # expressed in `rbd.frame_before(joint)`

    @assert distal_visual_element.frame == joint.frame_after

    distal_geo__Fgeo = distal_visual_element.geometry
    tx__Fjointafter_Fgeo = distal_visual_element.transform

    proximal_bound_Fjointafter = Geometry.boundingvolume(
        gt.HyperSphere,
        distal_geo__Fgeo,
        tx__Fjointafter_Fgeo)
    proximal_bound_Fjointbefore = proximal_bound_Fjointafter

    proximal_bound = (@set (@set (@set (distal_visual_element
    ).frame = joint.frame_before
    ).transform = coordtx.IdentityTransformation()
    ).geometry = proximal_bound_Fjointbefore
    )
    proximal_bound
end

"""
Produce a geometry that can be attached to `rbd.frame_before(joint)`, which bounds all bodies in the subtree , `rbd.frame_after(joint)`, taking into account the joint limits
"""
function get_configuration_bounding_element(mechanism, geometry_elements, body; base_case_override=Dict())
    distal_joints = rbd.Graphs.edges_to_children(body, mechanism.tree)
    @show body
    if haskey(base_case_override, body)
        @info "base_case_override" body
        return base_case_override[body]
    end

    if length(distal_joints) == 0
        # leaf base case

        frames = RobotModeling.frames_of_body(body)
        return map(
            # fix coordinate frame so that elements are attached to `rbd.default_frame(body)`
            ge -> begin
                tx = rbd.frame_definition(body, ge.frame)
                RobotModeling.reframe_visual_element(tx, ge)
            end,
            filter(
                # if there are multiple frames attached to this body, check geometry elements for all of them
                ge->ge.frame in frames,
                geometry_elements)
        )
    end

    result = [] # TODO eltype
    for joint in distal_joints
        jt = rbd.joint_type(joint)
        distal_body = rbd.Graphs.target(joint, mechanism.tree)
        #@show body
        #@show joint
        #@show distal_body
        distal_bound = get_configuration_bounding_element(mechanism, geometry_elements, distal_body)
        if length(distal_bound) == 0
            continue
        end
        if jt isa rbd.Fixed
            # TODO fix coordinate frame so that elements are attached to `rbd.default_frame(body)`
            tx_fixed_joint = rbd.joint_transform(joint, [])

            append!(result,
                map(
                    visual_element->begin
                        tx_body = rbd.frame_definition(body, tx_fixed_joint.to)
                        tx = tx_body * tx_fixed_joint
                        ve = RobotModeling.reframe_visual_element(tx, visual_element)
                        @assert ve.frame == rbd.default_frame(body)
                        ve
                    end,
                    distal_bound
                )
            )
        elseif jt isa rbd.Revolute
            for distal_bound_ in distal_bound
                tx_Fjoint = rbd.fixed_transform(distal_body, rbd.frame_after(joint), distal_bound_.frame)
                distal_bound_Fjoint = RobotModeling.reframe_visual_element(tx_Fjoint, distal_bound_)
                proximal_bound__Fany = proximal_joint_bounding_volume(
                    Val(:sphere_at_joint_center), distal_bound_Fjoint, joint)
                tx__Fbody_Fany = rbd.frame_definition(body, proximal_bound__Fany.frame)
                proximal_bound__Fbody = RobotModeling.reframe_visual_element(tx__Fbody_Fany, proximal_bound__Fany)
                @assert proximal_bound__Fbody.frame == rbd.default_frame(body)

                append!(result, [proximal_bound__Fbody])
            end
        else
            error("not yet implemented: $joint")
        end
    end
    return result
end
