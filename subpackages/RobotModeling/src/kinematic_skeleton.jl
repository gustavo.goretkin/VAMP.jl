# copied from https://github.com/JuliaRobotics/MechanismGeometries.jl/blob/32a0b593067a9c214e901f3423b0935cc9a25d5a/src/skeleton.jl#L101

function visual_elements(mechanism::Mechanism, source::Skeleton)
    body_fixed_joint_frames = Dict(body => begin
        [map(frame_before, out_joints(body, mechanism)); map(frame_after, in_joints(body, mechanism))]
    end for body in bodies(mechanism))

    box_width = 0.05 * maximum_link_length(body_fixed_joint_frames)
    elements = Vector{VisualElement}()

    for body in bodies(mechanism)
        if source.inertias && has_defined_inertia(body) && spatial_inertia(body).mass >= 1e-3
            push!(elements, inertial_ellipsoid(body))
        else
            for joint in out_joints(body, mechanism)
                if !iszero(box_width)
                    push!(elements, VisualElement(
                        frame_before(joint),
                        HyperSphere{3, Float64}(zero(Point{3, Float64}), box_width),
                        DEFAULT_COLOR,
                        IdentityTransformation()
                    ))
                end
            end
        end
        frames = body_fixed_joint_frames[body]
        for (i, framei) in enumerate(frames)
            for j = i + 1 : length(frames)
                framej = frames[j]
                joint_to_joint = fixed_transform(mechanism, framei, framej)
                push!(elements, create_frame_to_frame_geometry(
                    joint_to_joint,
                    box_width / 2
                ))
            end
        end
    end
    if source.randomize_colors
        for element in elements
            element.color = RGBA{Float32}(rand(), rand(), rand(), 0.5)
        end
    end
    elements
end
