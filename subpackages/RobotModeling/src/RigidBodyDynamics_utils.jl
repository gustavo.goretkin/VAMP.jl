"""
Set of frames, where each frame's coordinate transformation from the root is determined by the mechanism configuration
"""
function frames(m::rbd.Mechanism)
  Set(
    f
    for joint in rbd.joints(m)
      for f in (joint.frame_before, joint.frame_after)
  )
end

import CoordinateTransformations
const coordtx = CoordinateTransformations
"""
interop between CoordinateTransformations and RigidBodyDynamics
"""
function Base.convert(::Type{coordtx.Transformation}, rbd_tx::rbd.Spatial.Transform3D)
  coordtx.AffineMap(
    rbd.rotation(rbd_tx),
    rbd.translation(rbd_tx)
  )
end

function get_by_after_to_skip_fixed_joints(mechanism)
    error("does not work")
    fixedjoints = filter(j -> rbd.joint_type(j) isa rbd.Fixed, rbd.tree_joints(mechanism))

    by_after = Dict()
    # assume the "after_" frames are the `from` frames
    for fixedjoint in fixedjoints
        j_tx = rbd.joint_transform(fixedjoint, [])
        if haskey(by_after, j_tx.from)
            error("unexpected topology?")
        end
        by_after[j_tx.from] = j_tx
    end

    for k in keys(by_after)
        j_tx = by_after[k]
        if haskey(by_after, j_tx.to)
            error("not implemented. multiple consecutive fixed joints")
        end
    end
    return by_after
end

"""
Frames that are attached to a body (not taking into consideration fixed joints)
"""
function frames_of_body(body)
    Set(frame
        for frame_def in rbd.frame_definitions(body)
            for frame in (frame_def.to, frame_def.from)
    )
end

function get_frame_to_body_map(mechanism)
    result = Dict()
    for body in rbd.bodies(mechanism)
        for frame in frames_of_body(body)
            @assert !haskey(result, frame)
            result[frame] = body
        end
    end
    return result
end

# Really a MechanismGeometries util:
function reframe_visual_element(tx_fixed_joint, visual_element)
    # e.g. visual_element is attached to after_foo". Emit a new VisualElement that is attached to "before_foo"
    # geom_F{visual_element.frame} = visual_element.transform * visual_element.geometry
    # geom_F{after_x} = visual_element.transform * visual_element.geometry
    # geom_F{before_x} = tx__F{before_x}__F{after_x} * visual_element.transform * visual_element.geometry
    @assert tx_fixed_joint.from == visual_element.frame

    new_transform = convert(CoordinateTransformations.Transformation, tx_fixed_joint) ∘ visual_element.transform
    new_frame = tx_fixed_joint.to

    # TODO upate both fields one go.
    s1 = (@set visual_element.transform = new_transform)
    s2 = (@set s1.frame = new_frame)
    @assert s2.frame == new_frame
    return s2
end

function remap_visual_element(by_after, visual_element)
    if !haskey(by_after, visual_element.frame)
        return visual_element
    end

    tx_fixed_joint = by_after[visual_element.frame]
    return reframe_visual_element(tx_fixed_joint, visual_element)
end

function visual_elements_skip_fixed_joints(mechanism, visual_elements)
    error("does not work")
    by_after = get_by_after_to_skip_fixed_joints(mechanism)
    return map(ve->remap_visual_element(by_after, ve), visual_elements)
end

function visual_element_in_frame(geometry_element, mechanism_state, Ftarget=rbd.root_frame(mechanism_state.mechanism))
    rbd.update_transforms!(mechanism_state) # TODO consider hoisting out

    tx__Froot__Ftarget = rbd.transform_to_root(mechanism_state, Ftarget)
    tx__Ftarget__Flink_i = inv(tx__Froot__Ftarget) * rbd.transform_to_root(mechanism_state, geometry_element.frame)
    tx__Flink_i__Fgeo_i = geometry_element.transform
    tx__Flink_i__Fgeo_i_mat = convert(AbstractMatrix, tx__Flink_i__Fgeo_i)
    tx__Ftarget__Fgeo_i = tx__Ftarget__Flink_i.mat * tx__Flink_i__Fgeo_i_mat
    tx__Ftarget__Fgeo_i_AffineMap = convert(coordtx.AffineMap, Geometry.Homogeneous(tx__Ftarget__Fgeo_i))

    (@set (@set (geometry_element
    ).frame = Ftarget
    ).transform = tx__Ftarget__Fgeo_i_AffineMap
    )
end


"""
in RigidBodyDynamics, a `state` (configuration, velocity, etc), contains a reference to a mechanism.
And the mechanism contains many stateful components. This is an attempt at a more decoupled interface, at the cost of lower performance and subtle state bugs :-(
"""
struct PosedGeometricMechanism{PointNumT, MECH, GEOMS, CONF} <: gt.AbstractGeometry{3, PointNumT}
    mechanism::MECH
    geometry_elements::GEOMS
    configuration::CONF
end

function PosedGeometricMechanism{PointNumT}(
    mechanism::MECH, geometry_elements::GEOMS, configuration::CONF
) where {PointNumT, MECH, GEOMS, CONF}
    PosedGeometricMechanism{PointNumT, MECH, GEOMS, CONF}(mechanism, geometry_elements, configuration)
end

function subrobot(robot::PosedGeometricMechanism{PointNumT}, new_root::rbd.RigidBody) where PointNumT
    frames_super = RobotModeling.frames(robot.mechanism)
    jointmap = let T = Float64
        Dict{rbd.Joint{T}, rbd.Joint{T}}()
    end
    submechanism = rbd.submechanism(robot.mechanism, new_root; jointmap=jointmap)
    frames_sub =  RobotModeling.frames(submechanism)

    # should not introduce new frames
    @assert length(setdiff(frames_sub, frames_super)) == 0

    function is_in_sub(frame)
        if !(frame in frames_super)
            error("Geometry must be attached to frame: $(frame), but not found in mechanism.")
        end
        return (frame in frames_sub)
    end

    sub_geometry_elements = filter(el->is_in_sub(el.frame), robot.geometry_elements)

    # not sure how to deal with configurations directly, so go through `MechanismState`
    supstate = rbd.MechanismState(robot.mechanism)
    rbd.set_configuration!(supstate, robot.configuration)
    substate = rbd.MechanismState(submechanism)

    for (supjoint, subjoint) in jointmap
        q_joint = rbd.configuration(supstate, supjoint)
        rbd.set_configuration!(substate, subjoint, q_joint)
    end
    subconfiguration = rbd.configuration(substate)
    PosedGeometricMechanism{PointNumT}(submechanism, sub_geometry_elements, subconfiguration)
end
