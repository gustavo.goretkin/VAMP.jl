module RobotModeling

import RobotModeling
import RigidBodyDynamics
import MechanismGeometries
import MeshCatMechanisms
import CoordinateTransformations

import Parameters: @with_kw
import GeometryTypes
import StaticArrays
import Setfield: @set

import GeometryTypes: GLPlainMesh, AbstractMesh
import MeshUtils

const rbd = RigidBodyDynamics
const mg = MechanismGeometries
const mcm = MeshCatMechanisms
const gt = GeometryTypes
const coordtx = CoordinateTransformations

using BoundingBoxes: BoundingBox
import Geometry
include("RigidBodyDynamics_utils.jl")
include("configuration_bounding_volume.jl")

import FileIO: load
@with_kw struct RobotModel
  mechanism
  geometry_info
  visualization_geometries
  collision_geometries
  occlusion_geometries
  target_geometries
  bounding_geometries
end

@with_kw struct GeometryInfo{F, TX}
  frame::F
  transform::TX
end

function make_point_geometry(geometry, detail_radius)
  mesh = GLPlainMesh(geometry)
  mesh_fine = MeshUtils.refine_triangle(mesh, detail_radius)
  vi = MeshUtils.find_prune_vertices(mesh_fine.vertices, detail_radius)
  return mesh_fine.vertices[vi]
end

function get_robot_mechanism(urdf_path)
  robot_mechanism = rbd.parse_urdf(urdf_path, remove_fixed_tree_joints=false)

  world = rbd.RigidBody{Float64}("world")
  _mechanism = rbd.Mechanism(world)
  body_root_frame = rbd.CartesianFrame3D()
  body_root = rbd.RigidBody("body_root", rand(rbd.SpatialInertia{Float64}, body_root_frame))
  floatingjoint = rbd.Joint("floating_joint", rbd.QuaternionFloating{Float64}())
  rbd.attach!(_mechanism, world, body_root, floatingjoint)

  floating_mechanism = rbd.attach!(_mechanism, body_root, robot_mechanism)
  return floating_mechanism
end

function get_robot(;urdf_path, asset_dir, target_detail_radius)
  urdf_collision_vis = mg.URDFVisuals(
    urdf_path,
    tag="collision",
    package_path=[asset_dir])

  floating_mechanism = get_robot_mechanism(urdf_path)
  collision_visual_elements_ = mg.visual_elements(floating_mechanism, urdf_collision_vis)

  load_geo(a::MechanismGeometries.MeshFile) = load(a.filename)
  load_geo(a) = a
  # movo mesh is full of duplicate vertices
  function fix_mesh(g_in::AbstractMesh)
    MeshUtils.check_dangling_missing_vertices(g_in)
    g_out = MeshUtils.deduplicate_vertices(g_in)
    MeshUtils.check_dangling_missing_vertices(g_out)
    return g_out
  end
  fix_mesh(any) = any

  collision_geometries = map(
    x -> (@set x.geometry = fix_mesh(load_geo(x.geometry))),
    collision_visual_elements_)

  # make a struct of arrays
  geometry_info = [GeometryInfo(element.frame, element.transform) for element in collision_visual_elements_]

  # collision_visual_elements_wireframe_material = [jru.MaterialVisualElement(ve, wireframe_material) for ve in collision_visual_elements]
  # mcm._set_mechanism!(mechvis, collision_visual_elements_wireframe_material, "geometry_wireframe_")

  target_elements = map(
    x -> (@set x.geometry = Geometry.PointCloud(make_point_geometry(x.geometry, target_detail_radius))),
    collision_geometries)

  bb(m::AbstractMesh) = GeometryTypes.HyperRectangle(m.vertices)
  bb(g::GeometryTypes.HyperRectangle) = g
  bounding_elements = map(
    x -> (@set x.geometry = bb(x.geometry)),
    collision_geometries)

  return RobotModel(
    mechanism=floating_mechanism,
    geometry_info=geometry_info,
    visualization_geometries=collision_geometries,
    occlusion_geometries=collision_geometries,
    collision_geometries=collision_geometries,
    target_geometries=target_elements,
    bounding_geometries=bounding_elements,
    )
end

# interop between GeometryTypes and RigidBodyDynamics
function Base.:*(rbd_tx::RigidBodyDynamics.Spatial.Transform3D, point::RobotModeling.GeometryTypes.Point{3})
  point_homo = StaticArrays.SVector(point..., 1)  # TODO hope this splatting is okay.
  tx_point_homo = rbd_tx.mat * point_homo
  RobotModeling.GeometryTypes.Point(tx_point_homo[1:3]...)
end

function get_target_pointcloud_Fworld(robot_model::RobotModel, mechanism_state)
  rbd.update_transforms!(mechanism_state)
  points__Fworld = []
  for (geo_i, geometry_info) in enumerate(robot_model.geometry_info)
    tx__Froot__Flink_i = rbd.transform_to_root(mechanism_state, geometry_info.frame)
    tx__Flink_i__Fgeo_i = geometry_info.transform
    tx__Froot__Fgeo_i = tx__Froot__Flink_i #* tx__Flink_i__Fgeo_i  # TODO make RigidBodyDynamics.Spatial and CoordinateTransformations get along.

    link_points__Fgeo_i = robot_model.target_geometries[geo_i]

    link_points__Fworld = Ref(tx__Froot__Fgeo_i) .* link_points__Fgeo_i
    append!(points__Fworld, link_points__Fworld)
  end
  # TODO unfortunately not inferring well
  points__Fworld = [p for p in points__Fworld]
  return points__Fworld
end

function get_frame_by_link_name(mechanism, camera_optical_link_name)
  camera_optical_link = rbd.findbody(mechanism, camera_optical_link_name)
  Fcamera = rbd.default_frame(camera_optical_link)
  return Fcamera
end


"""
The third argument here isn't really generic. In other methods, it's a transform. here it's a frame
# TODO, treat the robot as __Froot and then have a tx__Fbase_Froot.
"""
function Geometry.boundingvolume(
    T__Fbase::Type{<:gt.AbstractGeometry},
    robot::PosedGeometricMechanism,
    Fbase = rbd.root_frame(robot.mechanism)
)
    state = rbd.MechanismState(robot.mechanism)
    rbd.set_configuration!(state, robot.configuration)

    # Step 1. make a bunch of individual bounding elements, expressed in the same frame
    bvs__Froot = (
        let geo__Froot = RobotModeling.visual_element_in_frame(geo_el, state, Fbase)
            Geometry.boundingvolume(
              T__Fbase, Geometry.PointCloud(geo__Froot.geometry), geo__Froot.transform
            )
        end
        for geo_el in robot.geometry_elements
    )

    # Step 2. union those individual bounding elements
    return reduce(union, bvs__Froot)
end

end
