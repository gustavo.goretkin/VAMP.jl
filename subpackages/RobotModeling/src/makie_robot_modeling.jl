import Makie
import RigidBodyDynamics
import CoordinateTransformations
import CoordinateTransformations: AffineMap

import Geometry # Base.convert(::AbstractArray, AffineMap)

const rbd = RigidBodyDynamics

function Base.convert(::Type{Makie.Quaternion}, rot::CoordinateTransformations.Rotation)
  q = CoordinateTransformations.Quat(rot)
  return Makie.Quaternion(q.w, q.x, q.y, q.z)
end

function Makie.transform!(scene, tx::AffineMap)
  translation = transformation.translation
  rotation = transformation.linear
  Makie.translate!(scene, translation...)
  Makie.rotate!(scene,
    Base.convert(Makie.Quaternion, rotation)
  )
end

function plot_bot!(s, geometry_elements, mechanism_state; plot_cmd! = Makie.mesh!)
  rbd.update_transforms!(mechanism_state)
  for geo_el in geometry_elements
    posed_geo_el = RobotModeling.visual_element_in_frame(geo_el, mechanism_state)
    plot_cmd!(s, posed_geo_el.geometry)
    link_plot = s.plots[end]  #TODO make more robust
    tx__Froot__Fgeo_i = convert(AbstractMatrix, posed_geo_el.transform)
    Makie.AbstractPlotting.transformationmatrix(link_plot)[] = tx__Froot__Fgeo_i
  end
end
