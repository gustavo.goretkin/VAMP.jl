using Test

import MeshUtils
import GeometryTypes: Point, Face, vertices, faces, GLPlainMesh, HomogenousMesh


@testset "find_unique_map" begin
    find_unique(a) = unique(i->a[i], 1:length(a))

    function test_against_base(a)
        (unique_a_i, old_to_new) = MeshUtils.find_unique_map(a)
        unique_a_i2 = find_unique(a)
        unique_a = unique(a)

        @test Set(unique_a) == Set(a[unique_a_i])
        @test unique_a_i2 == unique_a_i
        @test a == (a[unique_a_i])[old_to_new]
    end

    a = [8, 10, 5, 2, 1, 4, 4, 4, 6, 3, 6, 3, 8, 7, 10, 6, 9, 5, 1, 1] # string(rand(1:10,20))

    test_against_base(a)
    test_against_base(Float64[])
    test_against_base([])
    test_against_base([1])
    test_against_base([1, 1])
end

#=
# data below is generated with this
using GeometryTypes: GLPlainMesh, HyperRectangle, Vec, faces, vertices
mesh_repeated_vertices = GLPlainMesh(HyperRectangle(Vec(-1.0,-1.0,-1.0),Vec(2,2,2)))

fstring = string(map(x->Face((x.+1)...), reinterpret(NTuple{3, Int32}, faces(mesh_repeated_vertices))))

vstring = string(vertices(mesh_repeated_vertices))
=#

v = Point{3,Float32}[[-1.0, -1.0, 1.0], [-1.0, 1.0, 1.0], [1.0, 1.0, 1.0], [1.0, -1.0, 1.0], [-1.0, -1.0, -1.0], [1.0, -1.0, -1.0], [1.0, 1.0, -1.0], [-1.0, 1.0, -1.0], [1.0, -1.0, -1.0], [1.0, -1.0, 1.0], [1.0, 1.0, 1.0], [1.0, 1.0, -1.0], [-1.0, -1.0, -1.0], [-1.0, 1.0, -1.0], [-1.0, 1.0, 1.0], [-1.0, -1.0, 1.0], [-1.0, -1.0, -1.0], [-1.0, -1.0, 1.0], [1.0, -1.0, 1.0], [1.0, -1.0, -1.0], [-1.0, 1.0, -1.0], [1.0, 1.0, -1.0], [1.0, 1.0, 1.0], [-1.0, 1.0, 1.0]]

f = Face{3,Int64}[[1, 2, 3], [3, 4, 1], [5, 6, 7], [7, 8, 5], [9, 10, 11], [11, 12, 9], [13, 14, 15], [15, 16, 13], [17, 18, 19], [19, 20, 17], [21, 22, 23], [23, 24, 21]]

# not with offset integers
mesh_repeated_vertices = GLPlainMesh(v, f)

mesh_repeated_vertices_homo = HomogenousMesh(v, f, [], [], nothing, nothing, [])

@testset "deduplicate_vertices" begin
    dedup1 = MeshUtils.deduplicate_vertices(mesh_repeated_vertices)
    dedup2 = MeshUtils.deduplicate_vertices(mesh_repeated_vertices_homo)
    @test length(vertices(dedup1)) == 8
    @test length(vertices(dedup2)) == 8
end

@testset "remove_missing" begin
    f = Face{3,Int64}[[1, 2, 3], [3, 4, 1], [5, 6, 7], [7, 8, 5], [9, 10, 11], [11, 12, 9], [13, 14, 15], [15, 16, 13], [17, 18, 19], [19, 20, 17], [21, 22, 23]]
    new_mesh = MeshUtils.remove_unused_vertices(GLPlainMesh(v, f))
    @test length(vertices(new_mesh)) == 23
end
