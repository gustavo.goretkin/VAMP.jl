module MeshUtils

import GeometryTypes
import GeometryTypes: vertices, faces, vertextype, facetype, Face, OffsetInteger

import DataStructures: DefaultDict
import LinearAlgebra

"""
`find_unique_map(a)` returns `(a_unique_indices, old_to_new)` where
`a_unique = a[a_unique_indices] == unique(a)` (possibly up to permutation)
and `old_to_new` is the many-to-one mapping from indices in a to indices in a_unique.
`a == a_unique[old_to_new]`

Note that
`a_unique_indices == unique(i->a[i], 1:length(a))
"""
function find_unique_map(a)
    IndexType = Int64
    new_index = Dict{eltype(a), IndexType}()
    new_a_i = IndexType[]
    old_to_new = Vector{IndexType}(undef, length(a))
    # TODO `old_to_new` should be whatever AbstractArray that supports the same indices as `a` (so possibly an OffsetArray)
    for (i, v) in enumerate(a)
        if !haskey(new_index, v)
            new_index[v] = length(new_index) + 1
            push!(new_a_i, i)
        end
        old_to_new[i] = new_index[v]
    end
    return (new_a_i, old_to_new)
end

function _relabel_faces(faces, old_to_new)
    new_faces = [Face(map(old_to_new, face)...) for face in faces]
end
"""
If it were just a permutation, would not need `old_to_new_idx`. But the relabeling is allowed to be many-to-one.

TODO keep all mesh attributes, not just vertices and faces
"""
function relabel_vertices(mesh, new_vertices_i::AbstractVector{VertexIndexT}, old_to_new_idx::AbstractVector{VertexIndexT}) where {VertexIndexT}
  old_vertices = vertices(mesh)
  new_vertices = old_vertices[new_vertices_i]
  IndexType = eltype(facetype(mesh))
  # Possibly convert back to offset indices
  old_to_new_idx_offset = x->IndexType(old_to_new_idx[x])
  new_faces = _relabel_faces(faces(mesh), old_to_new_idx_offset)
  return GeometryTypes.homogenousmesh(Dict{Symbol, Any}(:faces=>new_faces, :vertices=>new_vertices))
end

"""remove duplicate vertices from mesh"""
function deduplicate_vertices(mesh)
    old_vertices = vertices(mesh)
    (new_vertices_i, old_to_new_idx) = find_unique_map(old_vertices)
    relabel_vertices(mesh, new_vertices_i, old_to_new_idx)
end

"""
Make a new mesh where there are only vertices mentioned by the faces.

TODO keep all mesh attributes, not just vertices and faces
"""
function remove_unused_vertices(mesh)
  vertex_indices_from_face = find_vertex_indices_from_face(mesh.faces)
  vi = sort(collect(vertex_indices_from_face))

  old_to_new_d = Dict(vi[i] => i for i = 1:length(vi))
  IndexType = eltype(facetype(mesh))
  # Possibly convert back to offset indices
  f = i->IndexType(old_to_new_d[Int(i)])
  new_faces = _relabel_faces(faces(mesh), f)
  new_vertices = vertices(mesh)[vi]
return GeometryTypes.homogenousmesh(Dict{Symbol, Any}(:faces=>new_faces, :vertices=>new_vertices))
end


function canonical_edge(edge)
  return (if edge[1].i > edge[2].i
    (edge[2], edge[1])
  else
    (edge[1], edge[2])
  end)
end

function face_edges(face)
  # edges canonical
  cycle(i) = mod1(i, length(face))
  (canonical_edge((face[i], face[cycle(i + 1)])) for i in 1:length(face))
end


# allow offset integer to be support `hash`
Base.trailing_zeros(oi::OffsetInteger) = Base.trailing_zeros(oi.i)
Base.leading_zeros(oi::OffsetInteger) = Base.leading_zeros(oi.i)
Base.bit_ndigits0z(oi::OffsetInteger) = Base.bit_ndigits0z(oi.i)
#Base.ndigits0zpb(oi::OffsetInteger, args...) = Base.ndigits0zpb(oi.i, args...)
Base.:<<(oi::OffsetInteger, s::Int64) = Base.:<<(oi.i, s)
Base.:>>(oi::OffsetInteger, s::Int64) = Base.:>>(oi.i, s)


function mesh_connection(mesh)
  FACE = facetype(mesh)
  VERTEX = eltype(FACE)
  EDGE = NTuple{2, VERTEX}

  edges = Set{EDGE}()
  faces_by_edge = DefaultDict{EDGE, Vector{FACE}}(Vector{FACE})
  edges_by_vertex = DefaultDict{VERTEX, Vector{EDGE}}(Vector{EDGE})

  for face in faces(mesh)
    for edge in face_edges(face)
      push!(faces_by_edge[edge], face)
      push!(edges, edge)
    end
  end

  for edge in edges
    for vertex in edge
      push!(edges_by_vertex[vertex], edge)
    end
  end

  return (faces_by_edge=faces_by_edge, edges_by_vertex=edges_by_vertex, edges=edges)
end

import Distances: euclidean

function find_prune_vertices(vertices, eps)
  keep = [firstindex(vertices)]
  function find_nearest_in_kept(query)
    best_i = nothing
    best = Inf
    for i in eachindex(keep)
      d = euclidean(vertices[keep[i]], query)
      if d < best
        best = d
        best_i = i
      end
    end
    return (best, best_i)
  end

  for v_i in eachindex(vertices)
    (d, closest_kept_i) = find_nearest_in_kept(vertices[v_i])
    if d > eps
      push!(keep, v_i)
    end
  end
  return keep
end

import GeometryTypes: facetype, vertextype, HomogenousMesh
import Distances: euclidean

function face_diameter(face, vertices)
  vs = map(i->vertices[face[i]], 1:3)
  return maximum((
    euclidean(vs[1], vs[2]),
    euclidean(vs[1], vs[3]),
    euclidean(vs[2], vs[3]),))
end

avg(v1, v2) = 0.5 * v1 + 0.5 * v2

function push_get_i!(array, element)
  push!(array, element)
  return lastindex(array)
end

function refine_triangle(mesh, max_diameter)
  FACE = facetype(mesh)
  faces_agenda = copy(mesh.faces)
  vertices = copy(mesh.vertices)
  faces_out = FACE[]

  while length(faces_agenda) > 0
    face_in = pop!(faces_agenda)
    if face_diameter(face_in, vertices) > max_diameter
      vs = map(i->vertices[face_in[i]], 1:3)
      nvi12 = push_get_i!(vertices, avg(vs[1], vs[2]))
      nvi23 = push_get_i!(vertices, avg(vs[2], vs[3]))
      nvi31 = push_get_i!(vertices, avg(vs[3], vs[1]))

      append!(faces_agenda, (
        FACE(face_in[1], nvi12, nvi31),
        FACE(face_in[2], nvi23, nvi12),
        FACE(face_in[3], nvi31, nvi23),
        FACE(nvi12, nvi23, nvi31),
      ))
    else
      push!(faces_out, face_in)
    end
  end
  return HomogenousMesh(vertices, faces_out, [], [], nothing, nothing, [])
end

function find_vertex_indices_from_face(faces)
  vertex_indices_from_face = Set{Int64}()
  for face in faces
    for vertex_i in face
      push!(vertex_indices_from_face, Int(vertex_i)) # explicitly get rid of offset integer
    end
  end
  return vertex_indices_from_face
end

"""
Return vertex indices that aren't mentioned by any face (dangling) and indices that are mentioned by a face but not present in the vertices of the mesh (missing)

Returned indices are not of type `GeometryTypes.OffsetInteger`
"""
function find_dangling_missing_vertices(mesh)
  vertex_indices = Set(LinearIndices(mesh.vertices))

  vertex_indices_from_face = find_vertex_indices_from_face(mesh.faces)

  dangling_vertices = setdiff(vertex_indices, vertex_indices_from_face)
  missing_vertices = setdiff(vertex_indices_from_face, vertex_indices)
  (dangling_vertices=dangling_vertices, missing_vertices=missing_vertices)
end

function check_dangling_missing_vertices(mesh)
  r = find_dangling_missing_vertices(mesh)
  if length(r.dangling_vertices) != 0 || length(r.missing_vertices) != 0
    error("mesh dangling and/or missing vertices: $r")
  end
end



function _convex_simplex_orientation_matrix(simplex, point)
    d = simplex[2:end] .- Ref(simplex[1])
    d_end = point - simplex[1]
    m = hcat(d..., d_end)
end

"""
Return orientation of a simplex (e.g. a face in 3D, an edge in 2D) with respect to a point

See https://en.wikipedia.org/wiki/Cross_product#Multilinear_algebra
"""
convex_simplex_orientation(simplex, point) =    LinearAlgebra.det(_convex_simplex_orientation_matrix(simplex, point))

function find_convex_mesh_face_orientation(face, mesh_vertices; rtol_factor=100)
    face_ = map(Int, face)
    simplex = mesh_vertices[face]
    d_max = 0
    d_min = 0
    d_max_i = nothing
    d_min_i = nothing
    for (vertex_i, v) in enumerate(mesh_vertices)
        # if vertex_i in face_; continue; end
        # check all points except those on face
        d = convex_simplex_orientation(simplex, v)
        #=
        if d > 0 && orientation ≥ 0
            orientation = 1
        elseif d < 0 && orientation ≤ 0
            orientation = -1
        elseif d != 0
            # inconsistent orientation
            @show face_i # investigating this face
            @show vertex_i
            error("mesh is not convex")
        end
        =#
        # d_max = max(d_max, d)
        # d_min = max(d_min, d)
        if d > d_max
            d_max = d
            d_max_i = vertex_i
        end

        if d < d_min
            d_min = d
            d_min_i = vertex_i
        end
    end
    pos = d_max > -d_min && d_min < rtol_factor*eps(d_max)
    neg = d_min < -d_max && d_max > -rtol_factor*eps(d_min)
    if pos && neg
        @show d_min_i, d_max_i
        @show d_min, d_max
        error("unexpected")
    end
    if !pos && !neg
        @show d_min_i, d_max_i
        error("mesh is not convex, up to tolerance")
    end
    (pos - neg)
end


"""
For a convex mesh, generate a mesh so that all faces have a normal that points outward.
(should work for "faces" in all dimensions)
TODO: preserve mesh type and attributes
"""
function fix_convex_mesh_face_orientation(mesh)
    function flip(f::FT) where {FT <: Face}
        FT(f[2], f[1], f[3:end]...)
    end

    function fixed_face(face, mesh_vertices)
        o = find_convex_mesh_face_orientation(face, mesh_vertices)
        if o == 1
            face
        elseif o == -1
            flip(face)
        else
            error("")
        end
    end

    HomogenousMesh(
        vertices(mesh),
        [fixed_face(face, vertices(mesh)) for face in faces(mesh)]
    )
end
end
