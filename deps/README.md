The Dockerfile is for making the docker image that can be used in the CI server to avoid installing and building dependencies for each CI run.

# Steps for building and deploying image
```
cd VAMP.jl
docker build -t vamp-test-ci -f ./deps/Dockerfile .
docker login
docker tag vamp-test-ci gustavogoretkin/vamp-test-ci
docker push gustavogoretkin/vamp-test-ci
```

# Steps for Running VAMP in image
```
docker run -it -v /dir/to/VAMP.jl/:/VAMP.jl vamp-test-ci
import Pkg; Pkg.activate("VAMP.jl"); Pkg.test()
```