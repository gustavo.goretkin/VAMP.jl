
AG = Union{AbstractGeometry, AbstractFlexibleGeometry}
intersects(a::AG, b::AG) = min_euclidean(a, b) <= 0
intersects(a::Transformed, b::Transformed) = EnhancedGJK.gjk(a.geometry, b.geometry, a.transform, b.transform).signed_distance <= 0




# vertices need to be GeometryTypes.Vec{2,T}}
function polygon_mesh(vertices, z=0)
  # mesh with fixed z
  fs = polygon2faces(map(Point, vertices))
  vs = map(x->Point{3}(x[1],x[2],z), vertices)
  #GLNormalColorMesh doesn't support Signal color
  return GLNormalMesh(vertices=vs, faces=fs)
end

function plot(mesh, model_matrix=eye(SMatrix{4,4}), color=RGBA(1.0, 1.0, 1.0, 1.0))
  @show model_matrix
  @show color
  vis = visualize(mesh, model=model_matrix, color=color)
  _view(vis, window, camera = :orthographic_pixel)
  return vis
end


function find_intersections(geom1, geoms::UnionOfConvex, t1=IdentityTransformation(), t2=IdentityTransformation())
  # returns index into geoms, representing bodies in geoms that intersect with geom1
  r = Int[]
  for idx = indices(geoms.pieces, 1)
    sd = EnhancedGJK.gjk(geom1, geoms.pieces[idx], t1, t2).signed_distance
    if sd <= 0.0
      push!(r, idx)
    end
  end
  return r
end

function affine_map_to_gl_matrix(am)
  g = eye(4)
  g[1:2,1:2] = am.m
  g[1:2,4] = am.v
  return SMatrix{4,4}(g)
end
