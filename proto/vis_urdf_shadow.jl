
module VisShadow
import .RobotModeling
import RigidBodyDynamics
const rbd = RigidBodyDynamics

import MeshCatMechanisms
const mcm = MeshCatMechanisms

import MeshCat
const mc = MeshCat

import MechanismGeometries
const mg = MechanismGeometries

import Polyhedra
const Poly = Polyhedra

using GeometryTypes
const GeomT = GeometryTypes


include("/Users/goretkin/projects/VAMP.jl/src/mesh_utils.jl")
include("/Users/goretkin/projects/VAMP.jl/src/mesh_visibility.jl")
include("/Users/goretkin/projects/VAMP.jl/src/RobotModeling/RobotModeling.jl")

const jru = RobotModeling

fov_x = 70.6 # degrees
fov_y = 60.0 # degrees
ir_range = 7.0 # meters. # 8 according to spec

frustum_poly = let dxdr = tan(deg2rad(fov_x/2)), dydr = tan(deg2rad(fov_y/2))
  dx = dxdr * ir_range
  dy = dxdr * ir_range

  frustum_points = [
    Point(0.0, 0.0, 0.0),
    Point(dx, dy, ir_range),
    Point(-dx, dy, ir_range),
    Point(dx, -dy, ir_range),
    Point(-dx, -dy, ir_range)]

  Poly.polyhedron(Poly.vrep(map(Array, frustum_points)))
end

floor_extent = 10.0


wireframe_material = jru.OverlayMaterial(
    mc.MeshPhongMaterial(color=mc.RGBA{Float32}(0.8, 0.8, 0.8, 1)),
    Dict{String, Any}("wireframe"=>true))

highlight_material = mc.MeshPhongMaterial(color=mc.RGBA{Float32}(1.0, 1.0, 0.0, 0.8))

frustum_material = mc.MeshPhongMaterial(color=mc.RGBA{Float32}(0.7, 0.7, 0, 0.5))

floor_material = mc.MeshPhongMaterial(color=mc.RGBA{Float32}(([118, 182, 235] ./ 255)..., 1.0))

shadow_material = mc.MeshPhongMaterial(color=mc.RGBA{Float32}(0.9, 0.1, 0.1, 0.3))



urdf_path = "/Users/goretkin/projects/VAMP.jl/data/movo.urdf"
asset_dir = "/Users/goretkin/projects/VAMP.jl/data"

urdf_collision_vis = nothing # moved

function get_robot_model(pos...;kwargs...)
  error("moved")
end

function get_frame_by_link_name(mechanism, camera_optical_link_name)
  error("moved")
end

camera_optical_link_name = "kinect2_ir_optical_frame"

function setup_vis!(mechvis; Fcamera=nothing)
  mechanism = mechvis.state.mechanism

  # Froot = rbd.root_frame(mechanism)
  Fworld = rbd.root_frame(mechanism)

  mcm.setelement!(mechvis, Fcamera, mc.Triad(0.5), "optical_axes")
  mcm.setelement!(mechvis, Fcamera, Poly.Mesh(frustum_poly), frustum_material, "frustum")

  floor_vis = mcm.setelement!(mechvis, Fworld,
    GeomT.HyperRectangle(-floor_extent/2, -floor_extent/2, -0.1, floor_extent, floor_extent, 0.1), floor_material, "floor")
  return nothing
end
#open(mechvis.visualizer)




#=
frame_list = map(v->v.frame, collision_visual_elements)

function findframe(list, name)
  for frame in list
    if string(frame) == name
      return frame
    end
  end
  return nothing
end

function find_visual_element(visual_elements, frame)
  for ve in visual_elements
    if ve.frame == frame; return ve end
  end
  return nothing
end
=#


function delete_shadows!(mechvis)
  shadow_paths = filter(x->occursin("shadow", x), map(x->joinpath((x[2:end])...), jru.leaf_paths(mechvis.visualizer.core.tree)))

  for p in shadow_paths
  #  delete!(mechvis.visualizer.core.tree, p)
    delete!(mechvis[p])
  end
end

import VAMP: isinside

function do_shadows!(mechvis, frustum_poly, Fcamera, visual_elements)
  for (i, visual_element) in enumerate(visual_elements)
    Fsome = visual_element.frame
    mesh = visual_element.geometry

    @show i
    @show Fsome

    if occursin("kinect", string(Fsome))
      println("skip kinect")
      continue
    end

    tx__Fsome__Fcamera = mcm.to_affine_map(rbd.relative_transform(mechvis.state, Fcamera, Fsome))
    frustum_poly__Fsome = Poly.polyhedron(Poly.vrep(tx__Fsome__Fcamera.(Poly.points(frustum_poly))))

    if !any(isinside(frustum_poly__Fsome, p) for p in vertices(mesh))
      println("skip")
      continue
    end
    eyeball_Fcamera = Point(0.0,0.0,0.0)
    eyeball_Fsome = tx__Fsome__Fcamera(eyeball_Fcamera)

    shadow = MeshVisibility.compute_shadow_volume(mesh, eyeball_Fsome, 2 * ir_range)

    select_vis = mcm.setelement!(mechvis, Fsome, shadow, shadow_material, "shadow_$i")
  end
end

end