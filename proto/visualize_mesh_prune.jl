import ColorTypes: RGBA
import Makie;
include("vis_urdf_shadow.jl")
robot = VisShadow.get_robot()

mesh = robot.collision_visual_elements[20].geometry

function vis_prune(mesh, detail_radius)
  mesh_fine = VisShadow.MeshUtils.refine_triangle(mesh, detail_radius)
  s = Makie.mesh(mesh)
  if length(mesh_fine.vertices) > length(mesh.vertices)
    new_vertices = mesh_fine.vertices[length(mesh.vertices)+1:end]
    @show length(new_vertices)
    # added vertices
    Makie.scatter!(s, new_vertices; markersize=0.005, color=RGBA(0.0, 1.0, 0.0, 0.8))
  end

  vi = VisShadow.MeshUtils.find_prune_vertices(mesh_fine.vertices, detail_radius)
  @show length(vi)
  # starting color
  Makie.scatter!(s, mesh.vertices; markersize=0.005, color=:yellow)
  # ending color
  Makie.scatter!(s, mesh_fine.vertices[vi]; markersize=0.008, color=:red)
  return s
end


display(vis_prune(mesh, 0.01))

import Plots
f(prune_radius) = length(VisShadow.MeshUtils.find_prune_vertices(mesh.vertices, prune_radius))
# just out of curiosity
Plots.plot(f, 0.0, 0.02)
