include("../src/g_star.jl")

import GStar

gi = GStar.GStarInstance(
  push_successors! = function push!_successors!(result, state)
      for Δ = [-1, +1]
          push!(result, GStar.GStarSuccessor(state + Δ, 1.0))
      end
  end,
  transition_valid = function transition_valid(s_to, s_from, alg)
    if s_to < 5
      return true
    end

    return GStar.isclosed(alg, -10) # transitions above 5 are only validated if -10 is reachable and path has been determined to it
  end
  )

ga = GStar.GStarAlgorithm{Int64, Float64, typeof(gi)}(instance=gi)


GStar.setup!(ga, 0)
GStar.step!(ga)

for i = 1:25
  GStar.step!(ga)
end
