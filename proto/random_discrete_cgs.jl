# attempt at making a discrete coverage-constrained search problem.
# equivalent to how Kris Hauser has a discrete MCR problem

include("util.jl")
include("a_star_domination.jl")

def = @NT(n_grid = 5)

NODE = Tuple{Int64, Int64}
function make_problem(def)
  max_n_cov_required = 5
  ors = 3
  states = vcat([[(i,j) for i=1:def.n_grid] for j=1:def.n_grid]...)
  STATE = eltype(states)
  PRECOVER = Set{Set{STATE}}
  precover = Dict{STATE, PRECOVER}()

  for state in states
    precover[state] = PRECOVER() # init
    other_states = filter(s -> s != state, states)
    for i = 1:ors
      select = randperm(length(other_states))[1:rand(0:max_n_cov_required)]
      push!(precover[state], Set(other_states[select]))
    end
  end
  return states, precover
end

states, precover = make_problem(def)

struct CoverGraphSearchState{NODE}
  node::NODE
  cover::Set{NODE}
end

function expand!(children, parent, precover, def)
  neighbors_full = [((a+b for (a,b) in zip(parent.node, Δ))...) for Δ in limited_connected_ngrid(2, 1)]
  neighbors_boundary = filter(s -> all(def.n_grid .≥ s .≥ 1), neighbors_full)
  neighbors = filter(state -> any(s ⊆ parent.cover for s in precover[state]), neighbors_boundary)
  for neighbor in neighbors
    push!(children, CoverGraphSearchState(neighbor, Set((neighbor,)) ∪ parent.cover))
  end
  nothing
end

goal_node = (4, 4)
l1_distance(x1, x2) = sum(abs(a-b) for (a,b) in zip(x1, x2))

ai = let goal_node = goal_node, precover = precover, def = def
  AStarInstance(
    (s2, s1) -> 1,                            # (state_to::STATE, state_from::STATE) → edge_cost::COST
    (s) -> l1_distance(s.node, goal_node),         # state::STATE → cost_to_goal::COST
    (s) -> s.node == goal_node,              # state::STATE → ::Bool
    (o, s) -> expand!(o, s, precover, def),   # (Set{STATE}, state_from) → ::Void
    (s) -> s.node,                            # state::STATE → STATE_ALIAS
    (s1, s2) -> s2.cover ⊆ s1.cover && (length(s2.cover) < length(s1.cover)),          # (old::STATE, new::STATE) ⇢ Bool  true means don't consider this state
  )
end

alg = let STATE = CoverGraphSearchState{NODE}, COST = Int64, STATE_ALIAS = NODE
  AStarAlgorithm(ai, STATE, STATE_ALIAS, COST)
end

setup!(alg, CoverGraphSearchState((2,2), Set([(1,1), (2,2), (1,3)])), 0)

start_time = time()
while length(alg.goal_node) == 0 && time() - start_time  < 30e10
  if !step!(alg)
    break
  end
end
