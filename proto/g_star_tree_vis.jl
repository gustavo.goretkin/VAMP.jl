using VAMP
using Parameters
import VAMP.GStar

include("../test/domain_robot.jl")
include("../test/domain_world.jl")
include("../test/domain_world2.jl")
include("../test/domain_motion_planning_collision.jl")

println("`include(domain_*)` completed")

parameters = Dict{Symbol, Any}()
make_robot(parameters)
make_robot_vis_target(parameters)
make_domain_world(parameters)
make_domain_motion_planning_collision(parameters)

VAMP.do_parameters(parameters)
vamp_pieces = make_vamp_pieces(parameters)


gstar_pieces = VAMP.make_gstar_vis(parameters, vamp_pieces)

tree_vis_successors! = gstar_pieces.tree_vis_successors!
tree_vis_valid = gstar_pieces.tree_vis_valid


gi = GStar.GStarInstance(
  push_successors! = tree_vis_successors!,
  transition_valid = tree_vis_valid,
  goal_test = function goal_test(s); s ∈ goal; end
  )

@unpack SE2_MPState_t = parameters
@unpack mp = parameters
@unpack goal = parameters

ga = GStar.GStarAlgorithm{SE2_MPState_t{Float64}, Float64, typeof(gi)}(instance=gi)



function make_g_star_summary(alg)
  if length(alg.open) > 0
      (node, f_val) = DataStructures.peek(alg.open)
  else
    f_val = nothing
  end

  return @NT(
    open_size=length(alg.open),
    closed_size=length(alg.closed),
    preopen_size=length(alg.preopen),
    agenda_top = @NT(f_val=f_val)
    )
end

function report_search!(search_summaries, this_time, search_alg; interval=1.0, header="")
  if length(search_summaries) == 0 || this_time - search_summaries[end].time > interval
    summary = make_g_star_summary(search_alg)
    print(header)
    println(summary)
    if length(search_summaries) > 0
      d = summary.closed_size - search_summaries[end].summary.closed_size
      println("New Closed: $(d)")
    end

    push!(search_summaries, @NT(time=this_time, summary=summary))
    if length(search_summaries) > 0
      runtime = this_time - first(search_summaries).time
      println("Runtime: $(@sprintf("%.2f", runtime))")
    end
    sleep(0.001)
  end
end


GStar.setup!(ga, mp.q_start)
GStar.step!(ga)

search_summaries = []

Profile.init(10^8, 0.005)
Profile.clear()

while true
  while true
    result = GStar.step!(ga)
    if !result.keepgoing
      @show result
      break
    end
    report_search!(search_summaries, time(), ga)
  end

  if !(isnull(ga.goal_state[]))
    println("found goal")
    break
  end
  @profile GStar.step_preopen!(ga)

  if length(ga.open) == 0
    println("after preopen, no news.")
    break
  end
end

qp1 = reverse(GStar.get_path(ga, get(ga.goal_state[])))

@unpack get_visible_region_cached__Fworld = vamp_pieces
@unpack SearchState_t = parameters
@unpack starting_visibility__Fworld = parameters
@unpack visibility_violations_path = vamp_pieces

function induce_visibility(initial_visibility, q_path, get_visible_region_cached__Fworld)
  s1 = SearchState_t(q_path[1], [q_path[1]], VAMP.make_packed_view_volume(UnionOfGeometry(initial_visibility)))

  function succ(s1, q2)
    SearchState_t(q2, vcat(s1.q_path, q2), vcat(s1.visible_region, get_visible_region_cached__Fworld(q2)))
  end

  ss = [s1]

  for q in q_path[2:end]
    push!(ss, succ(ss[end], q))
  end
  return ss
end

vp1 = induce_visibility(starting_visibility__Fworld, qp1, get_visible_region_cached__Fworld)

needtosee = visibility_violations_path(vp1)
nothing
