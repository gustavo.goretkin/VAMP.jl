struct ArmRobot
  base
  arm
  forearm
end

struct ArmRobotMotionPlanningState{SCALE_BASE, SCALE_ARM, T} <: AbstractMotionPlanningState
  base::SE2MotionPlanningState{SCALE_BASE, T}
  armθ::SO2MotionPlanningState{Rot{T}}
end

const ArmRobot_MPState = ArmRobotMotionPlanningState

# allow ArmRobot_MPState{SCALE_BASE, SCALE_ARM}(x, y, θ, armθ)
function (::Type{ArmRobot_MPState{SCALE_B, SCALE_A, TT} where TT})(x, y, θ, armθ) where {SCALE_B, SCALE_A}
  x, y, θ, armθ = promote(x, y, θ, armθ)
  T = typeof(x)
  ArmRobot_MPState{SCALE_B, SCALE_A, T}(SE2_MPState{SCALE_B}(x, y, θ), SO2_MPState(armθ))
end


function make_arm_robot(parameters)
  p = parameters
  @setparam p robot_width = nothing
  @setparam p arm_width = robot_width/10
  @setparam p arm_length = robot_width
  @setparam p polys_robot__Frobot = nothing

  @setparam p polys_arm__Farm = let
    w = arm_width/2
    l = arm_length

    points_arm_link = [
      Point(-w, -w),
      Point(-w, +w),
      Point(l-w, +w),
      Point(l-w, -w)
    ]
    arm_link__Farm0 = polyhedron(vrep(VAMP.array(points_arm_link)))
    UnionOfConvex([arm_link__Farm0])
  end

  @setparam p polys_forearm__Fforearm = let
    w = arm_width/2
    l = arm_length

    points_arm_link = [
      Point(-w, -w),
      Point(-w, +w),
      Point(l-w, +w),
      Point(l-w, -w)
    ]

    forearm_link__Fforearm0 = polyhedron(vrep(VAMP.array(points_arm_link)))
    UnionOfConvex([forearm_link__Fforearm0])
  end

  @setparam p robot = ArmRobot(polys_robot__Frobot, polys_arm__Farm, polys_forearm__Fforearm)

  #@setparam p get_tx__Fworld__Frobot = function (s) p[:get_tx__Fworld__Frobot](s.base) end # TODO gnarly way to inherit behavior
  @setparam p get_tx__Fworld__Frobot = function get_tx__Fworld__Frobot(s) VAMP.get_tx__Fworld__Frobot(s.base) end

  @setparam p get_tx__Frobot__Farm = function get_tx__Frobot__Farm(s::ArmRobot_MPState)
    t = Translation(0.2, -0.4)
    r = convert(LinearMap, s.armθ.q) # already a Rot
    return compose(t, r)
  end

  @setparam p get_tx__Farm__Fforearm = function get_tx__Farm__Fforearm(s::ArmRobot_MPState)
    l = arm_length
    w = arm_width / 2

    t = Translation(l - 2w, 0)
    θa = s.armθ.q.θ
    θfa = -0.5 * (s.armθ.q.θ - deg2rad(-180)) - deg2rad(180)
    r = convert(LinearMap, Rot(θfa)) # elbow bend
    return compose(t, r)
  end

  @setparam p get_robot__Fworld = function get_robot__Fworld(arm_robot, q)
    tx__Fworld__Frobot = get_tx__Fworld__Frobot(q)
    tx__Frobot__Farm = get_tx__Frobot__Farm(q)
    tx__Farm__Fforearm = get_tx__Farm__Fforearm(q)
    return UnionOfConvex([
      [transform(tx__Fworld__Frobot, poly) for poly in arm_robot.base.pieces]...,
      [transform(tx__Fworld__Frobot ∘ tx__Frobot__Farm, poly) for poly in arm_robot.arm.pieces]...,
      [transform(tx__Fworld__Frobot ∘ tx__Frobot__Farm ∘ tx__Farm__Fforearm, poly) for poly in arm_robot.forearm.pieces]...
    ])
  end


  @setparam p get_robot_occluders__Fworld = function get_robot_occluders__Fworld(q)
    l = arm_length
    w = arm_width / 2

    tx__Fworld__Frobot = get_tx__Fworld__Frobot(q)
    tx__Frobot__Farm = get_tx__Frobot__Farm(q)
    tx__Farm__Fforearm = get_tx__Farm__Fforearm(q)
    tx__Fforearm__Ftip = Translation(l - 2w, 0)
    tx__Fworld__Ftip = tx__Fworld__Frobot ∘ tx__Frobot__Farm ∘ tx__Farm__Fforearm ∘ tx__Fforearm__Ftip

    square_points = 0.2 * [-1.0 -1.0; -1.0 1.0; 1.0 -1.0; 1.0 1.0]

    poly_sq = polyhedron(vrep(square_points))

    return UnionOfConvex([transform(tx__Fworld__Ftip, poly_sq)])
  end

  return nothing
end
