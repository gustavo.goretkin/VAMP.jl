using VAMP
using Logging
include("VAMPlragraph.jl")

struct LazySP
    instance
    goal_node
    policy
    rmEdgesFrom
    rmEdgesTo
    start_state
    start_cost
    evaluatedEdges
end

function updateTreeNode(tree, node::TNode)
    #println(node)
    @assert node.cost !=Inf
    if (haskey(tree, node.id) && tree[node.id].cost <= node.cost)
        return false
    end
    tree[node.id] = node
    return true
end

function getNewNode(algo, parNode::TNode, id)
    # @assert parNode.budget < algo.alpha "node $(id) budget exceeded"
    # @assert lazyEval(graph, edge) != Inf
    return TNode(id,
                parNode.id,
                parNode.cost + lazyEval(algo, parNode.id, id),
                0.0,
                algo.instance.heuristic(id),
                parNode.budget + 1)
end

# policy take a path and give out an edge.
# policy should return nothing for a completely evaluated path.
function ConstructLazySPAlgorithm(instance, nodeType, weightType, policy)
    return LazySP(
        instance,
        nodeType[],
        policy,
        Dict{nodeType, Set}(),
        Dict{nodeType, Set}(),
        [], # a place holder for start_state
        [],  # a place holder for start_cost
        Dict()
    )
end

function realEval(algo, from, to)
    real = algo.instance.real_transition_cost(to, from)
    if (real == Inf)
        if (!haskey(algo.rmEdgesFrom, from))
            algo.rmEdgesFrom[from] = Set()
        end
        if (!haskey(algo.rmEdgesTo, to))
            algo.rmEdgesTo[to] = Set()
        end
        push!(algo.rmEdgesFrom[from], to)
        push!(algo.rmEdgesTo[to], from)

        #println("removed $(from) $(to)")
    end
    size = length(algo.evaluatedEdges)
    algo.evaluatedEdges[(from, to)] = size
    algo.evaluatedEdges[(to, from)] = size
    return real
end

function VAMP.setup!(algo::LazySP, start_state, start_cost)
    append!(algo.start_state, start_state)
    append!(algo.start_cost, start_cost)
end

function VAMP.step!(algo::LazySP)::Bool
    path = astar(algo)
    edge_to_eval = algo.policy(algo, path) # edge is a tuple

    if (edge_to_eval == nothing)
        @assert length(path) > 1
        append!(algo.goal_node, path[1])
        println("find shortest path, exiting")
        return true
    end
    #println("finish a step")
    @info("edge to eval from:$(edge_to_eval[1]) to:$(edge_to_eval[2]) weight:$(realEval(algo, edge_to_eval...))") # we expand it here.
    return false
end

function astar(algo)
    agenda = SortedSet{TNode}()
    tree = Dict()

    @assert length(algo.start_state) == 1
    start_state = algo.start_state[1]
    start_cost = algo.start_cost[1]
    # we would only be using cost in astar
    start_node = TNode(start_state,
    nothing,
    0.0,
    0.0,
    algo.instance.heuristic(start_state),
    0)

    updateTreeNode(tree, start_node)
    push!(agenda, start_node)

    while (!isempty(agenda))
        #println(length(agenda))
        node = pop!(agenda)
        if (haskey(tree, node.id) && node.cost > tree[node.id].cost)
            continue
        end
        if (algo.instance.goal_test(node.id))
            function ConstructPath(algo, goal)
                target = goal
                path = [goal]
                while (target != start_state)
                    target = tree[target].parent
                    append!(path, target)
                end
                return path
            end
            @info("path length is $(node.cost)")
            return ConstructPath(algo, node.id)
        end

        succ = getSucc(algo, node.id)
        #algo.instance.succ!(succ, node.id)
        #println("succ is $(succ)")
        for v in succ
            leval = lazyEval(algo, node.id, v)
            if (leval == Inf)
                continue
            end

            newChildNode = getNewNode(algo, node, v)
            updateTreeNode(tree, newChildNode)
            push!(agenda, newChildNode)
        end
    end
    []
end

# forward policy picks the closest unevaluated edge to the start-state.
# path is an array stores node from terminal state back to start state.
function forwardPolicy(algo, path)
    @info(path)
    for idx in length(path):-1:2
        source = path[idx]
        target = path[idx-1]
        if (!haskey(algo.evaluatedEdges, (source, target)))
            return (source, target)
        end
    end
    nothing
end

# TODO:Add more policy here.

function getGraph()
    open("input.txt") do file
        open("graph.dot","w") do output
            redirect_stdout(output)
            println("strict graph {")
            println("rankdir=LR;")

            nV = parse(Int, readline(file))
            nE = parse(Int, readline(file))
            dept = parse(Int, readline(file))
            dest = parse(Int, readline(file))
            alpha = parse(Int, readline(file))
            lazyWgt = Dict()
            realWgt = Dict()
            edges = Dict()

            println("label=\"lazySP----dept:$(dept), dest:$(dest)\";")

            for i in 1:nE
                from = parse(Int, readline(file))
                to = parse(Int, readline(file))
                real = parse(Float64, readline(file))
                lazy = parse(Float64, readline(file))

                r = Int(real) == 0x3f3f3f3f ? "inf" : string(real)
                println("$(from) -- $(to) [color = \"green\";label=\"real:$(r),lazy:$(lazy)\"];")

                if (Int(real) == 0x3f3f3f3f)
                    real = Inf
                end
                lazyWgt[(from, to)] = lazy
                lazyWgt[(to, from)] = lazy
                realWgt[(from, to)] = real
                realWgt[(to, from)] = real
                if (!haskey(edges, from))
                    #println(from)
                    edges[from] = Set()
                end
                if (!haskey(edges, to))
                    #println(to)
                    edges[to] = Set()
                end
                push!(edges[from], to)
                push!(edges[to], from)
                @assert haskey(edges, from)
                @assert haskey(edges, to)
            end

            edgeFunc = (container,source) ->
                for target in edges[source]
                    push!(container, target)
                end

            lrai = LRAInstance(
                (from, to) -> lazyWgt[(from, to)],
                (from, to) -> realWgt[(from, to)],
                x -> 0.0,
                x -> x == dest,
                edgeFunc,
                edgeFunc
            )

            algo = ConstructLazySPAlgorithm(
                lrai,
                Int,
                Float64,
                forwardPolicy
            )

            return (dept, algo)
        end
    end
end


function main()
    io = open("log.txt", "w")
    logger = SimpleLogger(io)
    global_logger(logger)

    (dept, algo) = getGraph()
    #h nmprintln("loading successful")
    setup!(algo, dept, 0.0)
    cnt = 0
    println("start searching")
    while (!step!(algo))
        cnt = cnt + 1
        # for (x, y) in algo.tree
        #     println(y)
        # end
        #sleep(3)
    end
    # println("finished")
    # # for (x, y) in algo.tree
    # #     println(y)
    # # end
    #
     #assert(isInTree(algo, algo.goal_node[1]))
     #println(getNode(algo, algo.goal_node[1]))
     drawHeatGraph(algo)
     run(`dot -Tpng graph.dot -o lazysp.png`)
     close(io)
end

function drawHeatGraph(algo)
    open("graph.dot", "a") do io
        redirect_stdout(io)
# cout << "graph ER {" << endl;
# cout << "rankdir=LR;" << endl;
#
# cin >> n >> m;
#
# cin >> a >> b >> alpha;
# for (int i = 1; i <= m; ++i) {
# int u, v, w, z;
# cin >> u >> v >> w >> z;
# printf("%d -- %d [label=\"%d,%d\"]%c\n", u, v, w, z, i != m ? ';' : ';');
# }
# printf("label=\"dept:%d, dest:%d\"}", a, b);
#
# fclose(stdout);
# system("dot -Tpng g.dot -o g.png");
        size = length(algo.evaluatedEdges)
        cnt = 0
        for (from, to) in keys(algo.evaluatedEdges)
            println("$(from) -- $(to) [color=\"0.00 0.00 $(1 - algo.evaluatedEdges[(from, to)] / size)\"]")
        end
        println("}")
    end
end
