using Plots
using Polyhedra

using VAMP

# make an unbounded polygon
shr = hrep(
        -float.([  1 0;
                  0 1;
                  1 2;
                  2 1]),
        -[0, 0, 2, 2])
#=
plot(;xlim=(-.1,3), ylim=(-.1,3), aspect_ratio=1, legend=false)
for hs in hreps(shr)
  halfspace!(hs.a..., hs.β)
end

plot!(x->-10) # trigger some drawing??
=#
poly = polyhedron(shr, SimplePolyhedraLibrary{Float64}())
vrep(poly)

@show vrep(poly)

plot()

plot!(;xlim=(-5,5), ylim=(-5,5), aspect_ratio=1.0)

plot_polyhedron!(current(), poly; linewidth=2.0, fillcolor=:purple, linecolor=:green)
