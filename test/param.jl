parameters = Dict()

let p = parameters
  @setparam p x = 3
end

@test parameters[:x] == 3

let p = parameters
  @setparam p x = 3
  @setparam p y = 2x
end

@test parameters[:y] == 6

let p = parameters
  z = 4
  @setparam p z
end

@test parameters[:z] == 4

let p = parameters
  @setparam p x = 4
end

@test parameters[:x] == 3

function f(parameters, i)
  p = parameters
  @setparam p fx = i
end


f(parameters, 3)
@test parameters[:fx] == 3
