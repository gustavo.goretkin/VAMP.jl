import Parameters: @pack!, @unpack, @with_kw

using VAMP
using Plots

println("`using VAMP` completed")

include("domain_robot.jl")
include("domain_world.jl")
include("domain_world2.jl")
include("domain_motion_planning_collision.jl")
include("do_algs.jl")
println("`include(domain_*)` completed")
include("VAMPastarNoPred.jl")
println("`include(VAMPastarNoPred.jl)` completed")

"""
Make all the domain-specific pieces that would be used by a lazy search algorithm.
Namely, the transition cost is split up into multiple pieces.
"""
function make_lazy_search_pieces()
  parameters = Dict{Symbol, Any}()
  parameters[:all_field_of_view] = 200.0


  make_robot(parameters)
  make_robot_vis_target(parameters)
  make_domain_world(parameters)
  make_domain_motion_planning_collision(parameters)

  VAMP.do_parameters(parameters)


  vamp_pieces = make_vamp_pieces(parameters)

  goal_q = parameters[:goal]
  goal_test(s) = s.q ∈ goal_q

  tpi = VAMP.TwoPhaseInstance(vamp_pieces=vamp_pieces, domain_parameters=parameters)

  tpa = VAMP.TwoPhaseAlgorithm(instance=tpi)
  setup!(tpa)

  # search start state
  @setparam parameters q_start = nothing
  @setparam parameters starting_visibility__Fworld = nothing
  vamp_start_state = tpa.path_segments[end][:path][end]

  @unpack vsafe_violations = tpi.vamp_pieces

  move_safe_preference = 0.001

  # transition costs
  function vsafe_penalty(s_to, s_fro)
    try
      return move_safe_preference * vsafe_violations(s_to, s_fro)
    catch err
      if isa(err, VAMP.ShadowEyeCollision)
        return Inf
      else
        rethrow(err)
      end
    end
  end

  motion_cost(s_to, s_fro) = distance(s_to.q, s_fro.q)
  collision_test = tpa.move_alg.instance.valid

  heuristic = tpa.move_alg.instance.heuristic
  successors! = tpa.move_alg.instance.expand!

  return (
    successors! = successors!,
    heuristic = heuristic,
    vsafe_penalty = vsafe_penalty,
    motion_cost = motion_cost,
    collision_test = collision_test,
    vamp_start_state = vamp_start_state,
    goal_test = goal_test,
    parameters = parameters
  )
end

function test_lazy_search_pieces()
  lsp = make_lazy_search_pieces()

  s_tos = Set()
  s_fro = lsp.vamp_start_state
  lsp.successors!(s_tos, s_fro)

  for s_to in s_tos
    @show s_to.q

    @show lsp.vsafe_penalty(s_to, s_fro)
    @show lsp.motion_cost(s_to, s_fro)
    @show lsp.heuristic(s_to)
    @show lsp.collision_test(s_to, s_fro)
    @show lsp.goal_test(s_to)
  end
end


include("../src/visualize_lra_search.jl")



function main()
  global plt
  global start_time
  global α
  α=3
  gr()
  lsp = make_lazy_search_pieces()
  alg = ConstructLRAAlgorithm(lsp.successors!,
                              lsp.heuristic,
                              lsp.vsafe_penalty,
                              lsp.motion_cost,
                              lsp.collision_test,
                              lsp.goal_test,
                              α)
  timerInit()
  setup!(alg, lsp.vamp_start_state, 0.0)
  plt = Plots.plot(;aspect_ratio=1.0,reuse=true)
  plot_polyhedron!(plt, lsp.parameters[:polys_obstacles__Fworld], color=:grey, linecolor=nothing)
  display(plt)

  println("start search")
  start_time = time()
  while isempty(alg.goal_node)
    res = step!(alg)
    #vis_lra_tree!(plt, alg)
    #display(plt)
    if res break end

    # if (time() - start_time) > 1.0
    #     println("frontier size is $(length(alg.frontier))")
    #     println("tree size is $(length(alg.tree))")
    #     start_time = time()
    # end
  end
  println("done searching")
  println("alpha is $(alg.alpha)\n")
  println("tree size is $(length(alg.tree))")
  println(lsp.vamp_start_state.q)

  q_path = reverse(follow_path_general(alg.tree, alg.goal_node[1].q; v_to_k=(v->v.parent.q)))
  @show length(q_path)

  show_path!(plt, q_path, lsp.parameters)
  plot_q_trail!(plt, q_path, lsp.parameters[:polys_robot__Frobot])

  vis_lra_tree!(plt, alg)
  display(plt)
  println("make movie")
  do_movie(q_path, lsp.parameters, "lazy_vamp_movie")


  #head = keys(alg.tree).head
  # res = Set()
  # lsp.successors!(res, lsp.vamp_start_state)
  # println(length(res))
  # for x in res
  #   println("collision is $(lsp.collision_test(x, lsp.vamp_start_state))")
  # end

  println("distance to target is $(getNode(alg,alg.goal_node[1]).cost)")
  print(to)
  # append!(benchmarkStat, [to])
  print("\n\n\n")
end

#main()
