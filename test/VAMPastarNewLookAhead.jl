using VAMP
using TimerOutputs
include("VAMPlragraphNewLookAhead.jl")

to = TimerOutput()

debug = Dict()
@timeit to function evaluate!(algo::LRAAlgorithm, node::TNode)
    global debug
    debug[:algo] = algo
    debug[:node] = node
    #print("...")
    parNode = getParNode(algo, node.id)
    while (parNode.lazy != 0)
        #println(node)
        node = parNode
        parNode = getParNode(algo, node.id)
        debug[:parNode] = parNode
    end

    realEdgCost = realEval(algo, node.parent, node.id)

    if (realEdgCost != Inf)
        newChildNode = TNode(node.id, node.parent, parNode.cost+realEdgCost, 0.0, algo.instance.heuristic(node.id), 0)
        updateTreeNode!(algo, newChildNode)
        push!(algo.update, newChildNode)
        succ = false
        if (algo.instance.goal_test(newChildNode.id))
            push!(algo.goal_node, newChildNode.id)
            succ=true
        end
        return (succ, [])
    else
        #println(algo.tree)
        treeRewire = takeOut!(algo, node.id, node.parent)
        return (false, treeRewire)
    end
    return (false, [])
end

@timeit to function DataStructures.update!(algo::LRAAlgorithm)
    while (!isempty(algo.update))
        node = pop!(algo.update)
        @assert isInTree(algo, node.id)

        succ = getSucc(algo,node.id)
        childVtxs = [v for v in succ
                        if (isInTree(algo, v) &&
                            getNode(algo, v).parent == node.id)]

        if (length(childVtxs) == 0 || algo.instance.goal_test(node.id))
            @assert isInTree(algo, node.id)
            push!(algo.extend, node)
        else
            for v in childVtxs
                childNode = getNode(algo, v)
                if (childNode.lazy > algo.alpha)
                    remove!(algo.frontier, childNode)
                end
                newChildNode = getNewNode(algo, node, v)
                updateTreeNode!(algo, newChildNode)
                if (newChildNode.lazy <= algo.alpha)
                    push!(algo.update, newChildNode)
                end
            end
        end
    end
end

@timeit to function rewire!(algo::LRAAlgorithm, treeRewire::Set)
    # if you run this part, you won't get any error.
    # this part is used to testify that ignore rewiring is feasible.
    # for v in treeRewire
    #     pred = getPred(algo, v)
    #     for p in pred
    #         if !isInTree(algo, p) continue end
    #         pNode = getNode(algo, p)
    #         if (pNode.lazy <= algo.alpha)
    #             push!(algo.extend, pNode)
    #         end
    #     end
    # end

    # below is a regular rewiring procedure.
    # TODO we haven't turn this part to accomodate definition change of α
    for v in treeRewire
        pred = getPred(algo, v)
        #algo.instance.pred!(pred, v)
        newNodes = [getNewNode(algo, getNode(algo, u), v)
                    for u in pred
                    if (isInTree(algo, u) &&
                        !in(u, treeRewire) &&
                        lazyEval(algo, u, v) != Inf &&
                        getNode(algo, u).lazy <= algo.alpha)]

        if (length(newNodes) == 0)
            continue
        end
        minNewNode = minimum(newNodes)
        if (!isInTree(algo, v) || minNewNode < getNode(algo, v))
            #println("rewired node $(minNewNode)")
            updateTreeNode!(algo, minNewNode)
            push!(algo.rewire, minNewNode)
        end
    end

    while (!isempty(algo.rewire))
        node = pop!(algo.rewire)

        if (node.lazy > algo.alpha || algo.instance.goal_test(node.id))
            @assert isInTree(algo, node.id)
            push!(algo.frontier, node)
            continue
        end

        if (node.lazy <= algo.alpha)
            push!(algo.extend, node)
        else
            succ = getSucc(algo, node.id)
            for v in succ
                if (lazyEval(algo, node.id, v) == Inf || !in(v, treeRewire))
                    continue
                end
                newNode = getNewNode(algo, node, v)
                inTree = isInTree(algo, v)
                if (!inTree || newNode < getNode(algo, v))
                    updateTreeNode!(algo, newNode)
                    push!(algo.rewire, newNode)
                end
            end
        end
    end
end

@timeit to function extend!(algo::LRAAlgorithm)
    loop_counter = 0    # this does nothing except for prevent an issue like https://github.com/JuliaLang/julia/issues/30093
    while (!isempty(algo.extend))
        loop_counter += 1
        node = pop!(algo.extend)
        #println(node)
        if (algo.instance.goal_test(node.id))
            @assert isInTree(algo, node.id)
            #println("found goal")
            push!(algo.frontier, node)
            return
        end

        succ = getSucc(algo, node.id)
        #algo.instance.succ!(succ, node.id)
        #println("succ is $(succ)")
        for v in succ
            #print(v)
            leval = lazyEval(algo, node.id, v)
            if (leval == Inf)
                continue
            end

            newChildNode = getNewNode(algo, node, v)

            if (isInTree(algo, v))
                childNode = getNode(algo, v)
                if (childNode <= newChildNode)
                    continue
                end

                @assert isInTree(algo, node.id)
                if (newChildNode.parent == childNode.parent)
                    @timeit to "regular update" x = 1
                end
                @timeit to "takeout" rmNodeLst = takeOut!(algo, v, node.id)
                # check what nodes are removed
                global start_time
                global plt
                if (time() - start_time) > 2.0
                @show length(rmNodeLst)
                @show childNode
                @show newChildNode
                @show childNode == newChildNode
                readline()
                     @show length(rmNodeLst)
                     #println(length(alg.update))
                     #println(length(alg.rewire))
                     #println(length(alg.extend))
                     #node = first(alg.frontier)
                     #@show alg.instance.goal_test(node.id)
                     #@show first(alg.frontier)
                    scatter!(
                        unique(
                            (s->tuple(s.e.q...)).(rmNodeLst)); alpha=0.1, color=:red)
                     display(plt)
                     start_time = time()
                end
                # take out the subtree of root target(e)
                @assert isInTree(algo, node.id) "$(node.id) should be in tree$(oldNode)"
            end

            updateTreeNode!(algo, newChildNode)
            if (newChildNode.lazy > algo.alpha)
                @assert isInTree(algo, newChildNode.id)
                push!(algo.frontier, newChildNode)
            else
                @assert isInTree(algo, newChildNode.id)
                push!(algo.extend, newChildNode)
            end
        end
    end
    # @show length(algo.extend)
    # @assert length(algo.extend) == 0
end

function VAMP.setup!(algo::LRAAlgorithm, start_state, start_cost)
    updateTreeNode!(algo, TNode(start_state, nothing, start_cost, 0.0, algo.instance.heuristic(start_state), 0))
    push!(algo.extend, getNode(algo, start_state))
    extend!(algo)
end

function VAMP.step!(algo::LRAAlgorithm)::Bool
    #println("stepping...")
    #println(algo.frontier)
    if (isempty(algo.frontier))
        println("exit empty")
        return true
    end

    node = pop!(algo.frontier)
    (success, treeRewire) = evaluate!(algo, node)
    # println("eval")
    if (success)
        println("exit here")
        return true
    end

    update!(algo)
    # println("upd")
    # println("rewire set is $(treeRewire)")
    rewire!(algo, Set(treeRewire))
    # println("after rew")
    # for (x, y) in algo.tree
    #     println(y)
    # end
    extend!(algo)
    # println("ext")
    return false
end

function main()
    (dept, algo) = getGraph()
    #h nmprintln("loading successful")
    setup!(algo, dept, 0.0)
    cnt = 0
    while (!step!(algo))
        cnt = cnt + 1
        # for (x, y) in algo.tree
        #     println(y)
        # end
        #sleep(3)
    end
    # println("finished")
    # # for (x, y) in algo.tree
    # #     println(y)
    # # end
    #
    global to
    println(to)
     println(getNode(algo, algo.goal_node[1]))
end

function timerInit()
    global to
    to = TimerOutput()
end
# main()
