#module VSPACE
import Plots
Plots.pyplot()
import DataStructures
using Parameters
using TimerOutputs
using Printf

using VAMP
println("`using VAMP` completed")

include("domain_robot.jl")
include("domain_world.jl")
include("domain_motion_planning_collision.jl")
println("`include(domain_*)` completed")

parameters = Dict{Symbol, Any}()
parameters[:domain1_goal] = :easy
make_robot(parameters)
make_robot_vis_target(parameters)
make_domain_world(parameters)
make_domain_motion_planning_collision(parameters)

VAMP.do_parameters(parameters)

@unpack robot_width = parameters
@unpack goal = parameters
@unpack mp = parameters
@unpack visibility_instance = parameters

vamp_pieces = VAMP.make_vamp_pieces(parameters)

@unpack get_visible_region_cached__Fworld = vamp_pieces
@unpack polys_obstacles__Fworld = parameters
@unpack polys_robot__Frobot = parameters

const SE2_MPState_t = SE2_MPState{robot_width/2} # reasonable scaling to measure distance

const θ_steps = 8;
const Δθ = 2π/(θ_steps)
const Δl =  1/(2^3) # carefully chosen to be exactly representable in floating point and also include the goal, because of equality testing
const Δx = Δl
const Δy = Δl

@assert(mp.q_valid_test(rand(goal)))

starting_visibility__Fworld = [
  DifferenceGeneral(
    ArcRegion(0.8, -1.0, Point(mp.q_start.e.q), Vec(1.0, 0.0)),
    UnionOfConvex(POLY[])
  )]

angle_to_step(θ) = Int(round(θ/Δθ))
step_to_angle(step) = Δθ * (step % θ_steps) # makes all floating point angles canonical
angle_p_step(θ, Δsteps) = step_to_angle(angle_to_step(θ) + Δsteps)

struct SearchState{MP, V}
  q::MP
  q_path::Vector{MP}
  visible_region::V
end

const SearchState_t = SearchState{SE2_MPState_t{Float64}, Vector{Any}}

function make_heuristic()
  # do uniform cost search starting from the motion planning goal.
  # this effectively solves all-source-single-goal shortest paths
  ai = LazyAStarInstance(
    (s_to, s_fro) -> distance(s_to, s_fro),
    (s) -> 0, # do uniform cost search
    (s) -> false, # no goal
    function (set, s_fro)
      x, y = s_fro.e.q
      θ = s_fro.θ.q.θ
      q_new = SE2_MPState_t(x, y, angle_p_step(θ, +1)); push!(set, q_new)
      q_new = SE2_MPState_t(x, y, angle_p_step(θ, -1)); push!(set, q_new)
      q_new = SE2_MPState_t(x, y + Δy, θ);    push!(set, q_new)
      q_new = SE2_MPState_t(x, y - Δy, θ);    push!(set, q_new)
      q_new = SE2_MPState_t(x + Δx, y, θ);    push!(set, q_new)
      q_new = SE2_MPState_t(x - Δx, y, θ);    push!(set, q_new)
      #=
      q_new = SE2_MPState_t(x + Δx, y + Δy, θ); push!(set, q_new)
      q_new = SE2_MPState_t(x - Δx, y - Δy, θ); push!(set, q_new)
      q_new = SE2_MPState_t(x + Δx, y - Δy, θ); push!(set, q_new)
      q_new = SE2_MPState_t(x - Δx, y + Δy, θ); push!(set, q_new)
      =#
      nothing
    end,
    (s_to, s_fro) -> mp.q_valid_test(s_to)  # assuming the Δs above are tiny, don't check the whole path.
  )

  alg = LazyAStarAlgorithm(ai, SE2_MPState_t{Float64}, Float64)

  setup!(alg, rand(mp.q_goal), 0.0)
  function _heuristic_debug(s::SearchState_t, ::Type{Val{do_debug}}) where {do_debug}
    if !mp.q_valid_test(s.q)
      # println("BAD HEURISTIC: ")
      # @show s.q
      if do_debug
        robot__Fworld = transform(VAMP.get_tx__Fworld__Frobot(s.q), visibility_instance.robot__Frobot)

        return (Inf, Dict(:invalid_q=>s.q,
                          :swept_volume=>robot__Fworld,
                          :swept_volume_uncovered=>robot__Fworld,
                          :swept_volume_covered=>robot__Fworld,
                          ))
      else
        return Inf
      end
    end
    # TODO if s is not reachable from mp.goal, then computation below won't terminate.

    n = length(alg.closed_parents)
    #println("in heuristic: $n")

    # round quantities to the search lattice, in case the heuristic is getting called by something other than A*
    Δxy = SVector(Δx, Δy)
    q = SE2_MPState_t(
      Δxy .* (round.(s.q.e.q ./ Δxy))...,
      step_to_angle(angle_to_step(s.q.θ.q.θ))
    )

    while !haskey(alg.closed_parents, q)
      step!(alg)
    end
    m = length(alg.closed_parents)
    (parent, move_cost) = alg.closed_parents[q]

    DO_VIZ_HEURISTIC = true

    swept_volume_uncovered = Set() # for debugging only
    swept_volume_covered = Set() # for debugging only
    if DO_VIZ_HEURISTIC
      #println("done $(m-n)")
      inflated_move_cost = (1 + 1e-8) * move_cost # break ties when there are multiple shortest paths.

      remaining_path = follow_path(alg.closed_parents, q)

      n_uncovered = 0
      path_used = remaining_path[[2:3:(end-1)..., end]] # always include end.


      for q in path_used
        this_uncovered = VAMP.get_uncovered_pieces__Fworld(visibility_instance, q, s.visible_region)
        n_uncovered += length(this_uncovered)
        if do_debug
          this_covered = VAMP.get_covered_pieces__Fworld(visibility_instance, q, s.visible_region) # for debugging only
          union!(swept_volume_uncovered, this_uncovered)
          union!(swept_volume_covered, this_covered)
        end
      end

      n_total = length(visibility_instance.robot__Frobot) * length(path_used)
    else
      remaining_path = []
      n_uncovered = 0
      n_total = 1
    end

    fraction_uncovered = n_uncovered / n_total

    move_component = (1 + 1e-8) * move_cost
    visibility_component = 30 * fraction_uncovered * (1.0 + 0.0*move_cost)
    h = move_component + visibility_component
    #h = fraction_uncovered
    #h = 0.9*move_cost

    if do_debug
      swept_volume = reduce(
        union,
        (transform(VAMP.get_tx__Fworld__Frobot(q), visibility_instance.robot__Frobot) for q in remaining_path),
        init=typeof(visibility_instance.robot__Frobot)() # allow reducing over empty set.
      )
    end

    if do_debug
      return h, (Dict(
        :remaining_path=>remaining_path,
        :swept_volume=>swept_volume,
        :swept_volume_uncovered=>swept_volume_uncovered,
        :swept_volume_covered=>swept_volume_covered,
        :move_cost=>move_cost,
        :fraction_uncovered=>fraction_uncovered,
        :move_component=>move_component,
        :visibility_component=>visibility_component,
        ))
      else
        return h
      end
  end

  function heuristic(s::SearchState_t)
    return _heuristic_debug(s, Val{false})
  end

  function heuristic_debug(s::SearchState_t)
    return _heuristic_debug(s, Val{true})
  end
  return (heuristic, heuristic_debug)
end


heuristic, heuristic_debug = make_heuristic()

# convenience because plotting stuff takes x and y separately usually. iterates twice, though.
xypoints(collection) = (getindex.(collection, 1), getindex.(collection, 2))

### test heuristic gui
function heuristic_tester(θ)
  s_fro = SearchState_t(mp.q_start, [], starting_visibility__Fworld)
  q_new = SE2_MPState_t(
    -1.0, -1.0,
    step_to_angle(angle_to_step(θ))
  )
  s = SearchState_t(q_new, vcat(s_fro.q_path, q_new), vcat(s_fro.visible_region, get_visible_region_cached__Fworld(q_new)) )

  return heuristic_debug(s)
end

let θs = 0:0.1:π
  p = Plots.plot(layout=Plots.@layout([a; b; c]))
  Plots.plot!(p[1], θs, [heuristic_tester(θ)[2][:fraction_uncovered] for θ in θs])
  Plots.plot!(p[2], θs, [heuristic_tester(θ)[2][:move_cost] for θ in θs])
  Plots.plot!(p[3], θs, [heuristic_tester(θ)[1] for θ in θs])
  display(p)
end


import PyPlot
PyPlot.plt[:ion]() # I didn't think this was needed if running from the julia REPL
ax_hs = PyPlot.plt[:figure]()[:gca]()
ax_hs[:set_title]("heuristic slider debug")
ax_hs[:set_aspect](1.0)
ax_hs[:set_xlim](-2.0, 3.0)
ax_hs[:set_ylim](-2.0, 1.5)


ax_s = PyPlot.plt[:figure]()[:gca]()
ax_s[:set_title]("slider")
slider = PyPlot.matplotlib[:widgets][:Slider](ax_s, "normang", 0.0, 1.0, valinit=0.0)

PyPlot.plt[:show]() # I didn't think this was needed if running from the julia REPL

odots = ax_hs[:plot]([], [], "go", alpha=0.7)[1]
xdots = ax_hs[:plot]([], [], "kx", alpha=1.0)[1]



slider[:on_changed]() do sliderval
  θ = sliderval * π
  h, debugdict = heuristic_tester(θ)

  odots[:set_data](xypoints(collect(debugdict[:swept_volume_covered]))...)
  xdots[:set_data](xypoints(collect(debugdict[:swept_volume_uncovered]))...)
  ax_hs[:set_title](
    "fraction_uncovered = $(@sprintf("%.3f", debugdict[:fraction_uncovered]))
    mc = $(@sprintf("%.3f", debugdict[:move_cost]))
    h = $(@sprintf("%.3f", h))")
end

### end test heuristic gui

#@assert(false) # good stopping place for debugging heuristic


global LEAK
function vsafe_test(s_to, s_fro)
  if !mp.q_valid_test(s_to.q) # assuming the Δs above are tiny, don't check the whole path.
    return false
  end

  # s_to.q is seen by s_fro.q_path
  uncovered_pieces__Fworld = VAMP.get_uncovered_pieces__Fworld(visibility_instance, s_to.q, s_fro.visible_region)
  if !isempty(uncovered_pieces__Fworld)
    return false
  end

  return true
end


#=
r = s_fro.visible_region[1]
plot!(plt, VAMP.render(r.positive, 30), fillcolor=:yellow, fillalpha=0.2)
for shadow in r.negative.pieces
  plot_polyhedron!(plt, shadow, fillcolor=:black, fillalpha=0.1)
end

=#

ai = LazyAStarInstance(
  (s_to, s_fro) -> distance(s_to.q, s_fro.q),
  (s) -> heuristic(s),
  (s) -> s.q ∈ goal,
  function (set, s_fro)
    x, y = s_fro.q.e.q
    θ = s_fro.q.θ.q.θ
    q_news = (
      SE2_MPState_t(x, y, angle_p_step(θ, +1)),
      SE2_MPState_t(x, y, angle_p_step(θ, -1)),
      SE2_MPState_t(x, y + Δy, θ),
      SE2_MPState_t(x, y - Δy, θ),
      SE2_MPState_t(x + Δx, y, θ),
      SE2_MPState_t(x - Δx, y, θ),
      #=
      SE2_MPState_t(x + Δx, y + Δy, θ),
      SE2_MPState_t(x - Δx, y - Δy, θ),
      SE2_MPState_t(x + Δx, y - Δy, θ),
      SE2_MPState_t(x - Δx, y + Δy, θ),
      =#
    )

    for q_new in q_news
      push!(set, SearchState_t(q_new, vcat(s_fro.q_path, q_new), vcat(s_fro.visible_region, get_visible_region_cached__Fworld(q_new)) ))
    end
    nothing
  end,
  vsafe_test
  #(s_to, s_fro) -> mp.q_valid_test(s_to.q) # just for testing, remove visibility constraint.
)

struct VisualDomination{SET}
  closed::SET
end


function Base.empty!(v::VisualDomination)
  empty!(v.closed)
end

import VAMP.domination_add!
import VAMP.is_dominated
function domination_add!(d::VisualDomination, search_state)
  if search_state.q ∈ d.closed
    #println("errrr")
    #@show search_state.q
  end
  push!(d.closed, search_state.q)
end

global count1 = 0
global count2 = 0
function is_dominated(d::VisualDomination, search_state)
  # retbool = search_state.q ∈ d.closed
  if length(search_state.q_path) < 2
    return (false, nothing)
  end
  retbool = !VAMP.visit_has_novelty(search_state.q_path[1:end-1], search_state.q_path[end])
  if retbool
    global count1 += 1
  else
    global count2 += 1
  end
  (retbool, nothing)
end


alg = LazyAStarAlgorithm(ai, SearchState_t, Float64, VisualDomination(Set{SE2_MPState_t{Float64}}()))

setup!(alg, SearchState_t(mp.q_start, [], starting_visibility__Fworld), 0.0)


plt = Plots.plot(;aspect_ratio=1.0)

for obstacle in polys_obstacles__Fworld.pieces
    plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
end

for p in VAMP.get_robot__Fworld(polys_robot__Frobot, mp.q_start).pieces
  plot_polyhedron!(plt, p; color=:green)
end


function debug_plot_conf(q)
  plt = Plots.plot(;aspect_ratio=1.0)

  for obstacle in polys_obstacles__Fworld.pieces
      plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
  end

  for p in VAMP.get_robot__Fworld(polys_robot__Frobot, q).pieces
    plot_polyhedron!(plt, p; color=:green)
  end
  return plt
end

function debug_plot_path!(plt, path)
  tp = map(s->s.q, path)

  flatten_path(path::Array{VAMP.SE2MotionPlanningState{SCALE, T}}) where {SCALE, T} = reshape(reinterpret(T, path),  3, length(path))'

  solvepath = flatten_path(tp);

  Plots.plot!(plt, solvepath[:,1], solvepath[:,2])
  return plt
end


for p in VAMP.get_robot__Fworld(polys_robot__Frobot, rand(mp.q_goal)).pieces
  plot_polyhedron!(plt, p; color=:red)
end
Plots.xlims!(plt, (-2.0, 3.0))
Plots.ylims!(plt, (-2.0, 1.5))
display(plt)

last_time = 0.0
last_n_closed = 0

interactive = true
axi = nothing
axh = nothing

halt = false
do_heuristic_vis = false
do_search_vis = false
do_sleeps = false

if interactive
  println("start interactive stuff")
  import PyPlot
  PyPlot.plt[:ion]() # I didn't think this was needed if running from the julia REPL
  axi = PyPlot.plt[:figure]()[:gca]()
  axi[:set_title]("search debug")
  axi[:set_aspect](1.0)
  axh = PyPlot.plt[:figure]()[:gca]()
  axh[:set_title]("heuristic debug")
  axh[:set_aspect](1.0)

  subplots_button = PyPlot.plt[:figure]()[:subplots](2, 2) # axes and figure just for button
  halt_button = PyPlot.matplotlib[:widgets][:Button](subplots_button[1], "Halt Search")
  pause_button = PyPlot.matplotlib[:widgets][:Button](subplots_button[2], "Step Search")
  check_buttons = PyPlot.matplotlib[:widgets][:CheckButtons](subplots_button[3], ["Do Heuristic Viz", "Do Search Viz", "slow"], [true, false, true])

  PyPlot.plt[:show]() # I didn't think this was needed if running from the julia REPL

  halt_button[:on_clicked]() do event
    println("Received halt button click")
    global halt = true
  end

  check_buttons[:on_clicked]() do checks
    global do_heuristic_vis = check_buttons[:get_status]()[1]
    global do_search_vis = check_buttons[:get_status]()[2]
    global do_sleeps = check_buttons[:get_status]()[3]
  end

end

const vertices_robot__Frobot = VAMP.convexhull_andrew(collect(points(polys_robot__Frobot.pieces[1])))
function get_vertices_robot__Fworld(q)
  tx__Fworld__Frobot = VAMP.get_tx__Fworld__Frobot(q)
  #TODO make transform work on an iterable of points.
  return [first(transform(tx__Fworld__Frobot, Set([Point(p...)]))) for p in vertices_robot__Frobot]
end



Profile.clear()

println("start search")
while isempty(alg.goal_node) && !halt
  @timeit "vspace_astar_step" step_result = step!(alg)
  if !step_result; break end

  if interactive
    sleep(0.0001) # just for yielding
  end
  if !interactive || !do_search_vis
    if time() - last_time > 1.0
      @show length(alg.agenda)
      this_n_closed = length(alg.closed_parents)
      @show this_n_closed - last_n_closed
      last_n_closed = this_n_closed
      last_time = time()
    end#
  else
    axi[:cla]()
    axi[:set_xlim](plt.o[:axes][1][:get_xlim]())
    axi[:set_ylim](plt.o[:axes][1][:get_ylim]())

    will_expand = first(DataStructures.peek(alg.agenda))
    #=
    for obstacle in polys_obstacles__Fworld.pieces
        plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
    end

    for p in VAMP.get_robot__Fworld(polys_robot__Frobot, will_expand.state.q).pieces
      plot_polyhedron!(plt, p; fillalpha=0.0)
    end
    =#
    path = map(q->tuple(q.e.q...), will_expand.state.q_path)
    axi[:plot](xypoints(path)...)

    vertices__Fworld = get_vertices_robot__Fworld(will_expand.state.q)
    axi[:plot](getindex.(vertices__Fworld, 1), getindex.(vertices__Fworld, 2))

    println("debug search")

    if do_heuristic_vis
      axh[:cla]()
      axh[:set_xlim](plt.o[:axes][1][:get_xlim]())
      axh[:set_ylim](plt.o[:axes][1][:get_ylim]())

      worst_f_first = sort(collect(alg.agenda), by=x->-getindex(x,2))
      h, debugdict = heuristic_debug(worst_f_first[end][1].state)
      axh[:plot](xypoints(collect(debugdict[:swept_volume_covered]))..., "bo", alpha=0.7, label="best-f")
      axh[:plot](xypoints(collect(debugdict[:swept_volume_uncovered]))..., "rx", alpha=1.0, label="best-f")

      @show length(debugdict[:swept_volume_covered])
      @show length(debugdict[:swept_volume_uncovered])

      compareo = axh[:plot]([], [], "go", alpha=0.7, label="notbest-f")[1]
      comparex = axh[:plot]([], [], "kx", alpha=1.0, label="notbest-f")[1]
      axh[:legend]()

      for agenda_element in worst_f_first[[1]]
        h_this, debugdict = heuristic_debug(agenda_element[1].state)

        compareo[:set_data](xypoints(collect(debugdict[:swept_volume_covered]))...)
        comparex[:set_data](xypoints(collect(debugdict[:swept_volume_uncovered]))...)
        axh[:set_title]("h_best = $(@sprintf("%.3f", h)), h = $(@sprintf("%.3f", h_this))")
        if do_sleeps sleep(0.01) end
        println("debug heuristic")
      end
      axh[:figure][:canvas][:draw]()
    end
  end
  if do_sleeps sleep(0.5) end

  if length(alg.agenda) > 100_000_000
    println("I am tired of searching.")
    break
  end
end

function flatten_SE2(q)
  (q.e.q..., q.θ.q.θ)
end


#@show sort(countall(values(alg.agenda)))
@show length(alg.closed_parents)

goal_node = nothing
if length(alg.goal_node) > 0
  goal_node = alg.goal_node[1]
else
  # didn't actually reach goal. how close did we get?
  cp = collect(keys(alg.closed_parents));
  _d, goal_i = findmin([heuristic(cp[i]) for i in LinearIndices(cp)])
  goal_node = cp[goal_i]
end

function plot_robot(q)
  for p in VAMP.get_robot__Fworld(polys_robot__Frobot, q).pieces
    plot_polyhedron!(plt, p)
  end
end


path_full = reverse(follow_path(alg.closed_parents, goal_node))
path = path_full[1:end]

tp = map(s->s.q, path)


flatten_path(path::Array{VAMP.SE2MotionPlanningState{SCALE, T}}) where {SCALE, T} = reshape(reinterpret(T, path),  3, length(path))'

solvepath = flatten_path(tp);

# take something near the end of the solution path and see how many different paths end up there.
using ProgressMeter
query = tp[end]
plot_robot(query)
response = []
@showprogress 0.5 "query" for node in keys(alg.closed_parents)
  #if contains(==, node.q_path, query)
  if node.q == query
    push!(response, node)
  end
end
@show length(response)
@show sort(collect(countall([length(r.q_path) for r in response])), by=x->x[1])

@showprogress 0.5 "plot" for r in response[1:10000:end]
  fp = flatten_path(r.q_path)
  Plots.plot!(plt, fp[:,1], fp[:,2]; linewidth=3.0, linealpha=0.2)
end

Plots.plot!(plt, solvepath[:,1], solvepath[:,2])

USE_GPU = false

if USE_GPU
  using CUDAnative
  using CuArrays
  using CUDAdrv
  const CuArray = CuArrays.CuArray
end

vvs = [v for v in path[end].visible_region]



uvvs = UnionOfGeometry(vvs)
pvv = VAMP.make_packed_view_volume(UnionOfGeometry(uvvs.pieces[:]))

window = (128, 128)

out = nothing
if USE_GPU
  out = CuArray(zeros(Int16, window...))
else
  out = zeros(Int16, window...)
end

spec = VAMP.RasterVolumeSpec(VAMP.BoundingBox([-2.0, -4.0], [6.0, 2.0]), window)
vout = VAMP.RasterVolume(spec, out)

if USE_GPU
  pvv = VAMP.PackedViewVolumes(CuArray(pvv.arc_regions), CuArray(pvv.shadow_halfspaces))
end
VAMP.render!(vout, pvv)
if USE_GPU
  synchronize()
end

# make 0 plot as white, and anything else yellow-orange
cmap = Plots.cgrad([:yellow, :orange])
cmap.values[1] = eps()
prepend!(cmap.values, [0.0])
prepend!(cmap.colors, [Plots.RGBA{Float64}(1.0,1.0,1.0,1.0)])

# need to transfer GPU->CPU before plotting, otherwise "Bus Error 10"
viewdata = (Array(vout.data))'

plt = Plots.plot(;aspect_ratio=1.0)

Plots.heatmap!(plt, VAMP.coordinates(spec)..., viewdata; aspect_ratio=1, c=cmap)


# DUPLICATE UNTIL FIGURE OUT Plots.jl z-ordering

for obstacle in polys_obstacles__Fworld.pieces
    plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
end

for p in VAMP.get_robot__Fworld(polys_robot__Frobot, mp.q_start).pieces
  plot_polyhedron!(plt, p; color=:green)
end

Plots.plot!(plt, solvepath[:,1], solvepath[:,2] ; aspect_ratio = 1)

import Base.convert
convert(::Type{VAMP.MoveAction}, state::SE2_MPState) = VAMP.MoveAction([state.e.q..., state.θ.q.θ])

p = [convert(VAMP.MoveAction, state) for state in reverse(tp)] #TODO why is it reversed again? FIXME oh because z-order
stuff__Fworld = VAMP.worldgrounded_path(p, polys_robot__Frobot, poly_frustum__Fcamera, polys_obstacles__Fworld)
VAMP.plot_path!(plt, stuff__Fworld)

Plots.scatter!(plt, unique((s->tuple(s.q.e.q...)).(collect(keys(alg.closed_parents)))); alpha=0.1, color=:red)
Plots.scatter!(plt, unique((n->tuple(n.state.q.e.q...)).(collect(keys(alg.agenda)))); alpha=0.1, color=:grey)
display(plt)


# calculate heuristic_debug along the path

heuristic_debug_on_path = [heuristic_debug(ss) for ss in path_full]

plt_hop = Plots.plot(;reuse=false)

Plots.plot!(plt_hop, [h[1] for h in heuristic_debug_on_path], label="h")
Plots.plot!(plt_hop, [h[2][:move_component] for h in heuristic_debug_on_path], label="move")
Plots.plot!(plt_hop, [h[2][:visibility_component] for h in heuristic_debug_on_path], label="vis")

#=
for i = 1:10
  random_path = map(s->tuple(s.q.e.q...), follow_path(alg.closed_parents, rand(collect(keys(alg.closed_parents)))))
  plot!(plt, random_path, color=:red, linewidth=2.0)
  sleep(0.2)
  display(plt)
  deleteat!(plt.series_list, length(plt.series_list)) # ??
end
=#

#end # module
#=

open("profile_info.txt", "w") do s
    Profile.print(IOContext(s, :displaysize => (24, 500)), sortedby=:count)
end

=#
