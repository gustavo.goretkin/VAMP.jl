using DataStructures

struct TNode{GNT, EWT}
    id::GNT
    parent
    cost::EWT
    lazy::EWT
    estm::EWT
    budget::Int
end

# successors! = successors!,
# heuristic = heuristic,
# vsafe_penalty = vsafe_penalty,
# motion_cost = motion_cost,
# collision_test = collision_test,
# vamp_start_state = vamp_start_state,
# goal_test = goal_test

struct LRAInstance{FLT,FRT,FH,FG,FP,FS}# dispatch <: AbstractAStarInstance
  lazy_transition_cost::FLT  # (state_to::STATE, state_from::STATE) ↦ edge_cost::COST
  real_transition_cost::FRT  # (state_to::STATE, state_from::STATE) ↦ edge_cost::COST
  heuristic::FH         # state::STATE ↦ cost_to_goal::COST #arguably belongs in Algorithm, not Instance
  goal_test::FG         # state::STATE ↦ ::Bool
  pred!::FP           # (Set{STATE}, state_from) ↦ ::Void
  succ!::FS           # (Set{STATE}, state_from) ↦ ::Void
end

struct LRAAlgorithm
    instance#::LRAInstance
    tree#::Dict{GNT, TNode}
    goal_node#::Array{GNT}
    alpha#::Int
    frontier#::SortedSet{TNode}
    rewire#::SortedSet{TNode}
    update#::SortedSet{TNode}
    extend#::SortedSet{TNode}
    rmEdgesFrom
    rmEdgesTo
    gnt2state
    possible_preds
    successors!
end

# successors! = successors!,
# heuristic = heuristic,
# vsafe_penalty = vsafe_penalty,
# motion_cost = motion_cost,
# collision_test = collision_test,
# vamp_start_state = vamp_start_state,
# goal_test = goal_test

# function ConstructLRAAlgorithm(instance::LRAInstance, nodeType, weightType, alpha::Int)
#     return LRAAlgorithm(
#         instance,
#         Dict{nodeType, TNode{nodeType, weightType}}(),
#         nodeType[],
#         alpha,
#         SortedSet{TNode{nodeType, weightType}}(),
#         SortedSet{TNode{nodeType, weightType}}(),
#         SortedSet{TNode{nodeType, weightType}}(),
#         SortedSet{TNode{nodeType, weightType}}(),
#         Dict{nodeType, Set}(),
#         Dict{nodeType, Set}()
#     )
# end

function ConstructLRAAlgorithm(successors!,
                            heuristic,
                            vsafe_penalty,
                            motion_cost,
                            collision_test,
                            goal_test,
                            alpha = 5)
    gnt2state = Dict() # gnt ↦ state(gnt, vis)
    possible_preds = Dict()  # gnt ↦ Set(gnt)
    check(gn) = @assert haskey(gnt2state, gn) "gnt2state has no key $(gn)"
    convert(gn) = begin
        check(gn)
        return gnt2state[gn]
    end
    lra_successors!(res, from) = begin
        state = convert(from)
        succ = Vector()
        successors!(succ, state)
        if !haskey(possible_preds,from)
            possible_preds[from] = Set()
        end
        for p in Set(map(x->x.q, succ)) push!(res, p) end
        foreach(x -> push!(possible_preds[from], x), res)
        return
    end
    lra_predecessors!(res, to) = begin
        for q in possible_preds(to) push!(res, q) end
        return
    end
    lra_heuristic(node) = begin
        state = convert(node)
        return heuristic(state)
    end
    lra_lazy_cost(to, from) = begin
        state_to = convert(to)
        state_from = convert(from)
        return motion_cost(state_to, state_from) +
                vsafe_penalty(state_to, state_from)
    end
    lra_real_cost(to, from) = begin
        state_to = convert(to)
        state_from = convert(from)
        if collision_test(state_to, state_from)
            return Inf
        else
            return lra_lazy_cost(to, from)
        end
    end
    lra_goal_test(node) = begin
        state = convert(node)
        return goal_test(state)
    end

    instance = LRAInstance(
        lra_lazy_cost,
        lra_real_cost,
        lra_heuristic,
        lra_goal_test,
        lra_predecessors!,
        lra_successors!
    )

    return LRAAlgorithm(
        instance,
        Dict(),
        [],
        alpha,
        SortedSet(),
        SortedSet(),
        SortedSet(),
        SortedSet(),
        Dict(),
        Dict(),
        gnt2state,
        possible_preds,
        successors!
    )
end

function orderToken(x::TNode)
    return (x.cost + x.lazy + x.estm, x.budget, hash(x.id))
end

function Base.isequal(x::TNode, y::TNode)
    return orderToken(x) == orderToken(y)
end

function Base.isless(x::TNode, y::TNode)
    return orderToken(x) < orderToken(y)
end

# get tree node of graph vertex id
function getNode(algo::LRAAlgorithm, id)
    @assert isInTree(algo, id) "id:$(id) is not in tree"
    return algo.tree[id]
end

function getState(algo::LRAAlgorithm, id)
    @assert isInTree(algo, id)
    return algo.gnt2state[id]
end

# get parent node of id
function getParNode(algo::LRAAlgorithm, id)
    parNode = getNode(algo, id).parent
    #@assert getNode(algo, parNode).budget == 0 || getNode(algo,id).budget == getNode(algo,parNode).budget + 1
    # if (parEdg == nothing)
    #     return TNode(0, nothing, 0, 0, 0)
    # end
    return getNode(algo, parNode)
end

# return whether a tnode is succecssfully removed from a list
function remove!(set::SortedSet, node::TNode)::Bool
    if (in(node, set))
        delete!(set, node)
        return true
    end
    return false
end

function rmFromQueues(algo::LRAAlgorithm, id)
    if (isInTree(algo, id))
        oldNode = getNode(algo, id)
        remove!(algo.frontier, oldNode)
        remove!(algo.update, oldNode)
        remove!(algo.extend, oldNode)
        remove!(algo.rewire, oldNode)
    end
end

function isInTree(algo::LRAAlgorithm, id)
    #print(algo.tree)
    return haskey(algo.tree, id)
end

# every time we update a node, we need to assure it's old existences are removed.
function updateTreeNode!(algo::LRAAlgorithm, node::TNode)
    @assert algo.instance.goal_test(node.id) || (node.parent == nothing  || isInTree(algo, node.parent))
    @assert node.parent == nothing || getNode(algo, node.parent).budget == 0 || node.budget == getNode(algo, node.parent).budget + 1

    rmFromQueues(algo, node.id)
    #println(node)
    algo.tree[node.id] = node

    # update state
    if node.parent != nothing
        succ = Set()
        algo.successors!(succ, getState(algo, node.par))
        for s in succ
            if (s.q == node.id)
                algo.gnt2state[node.id] = s
                break
            end
        end
    end
end

# note that this function only need an graph vertex, which is a little bit
# different from updateTree.
function rmFromTree!(algo::LRAAlgorithm, id)
    if (isInTree(algo, id))
        rmFromQueues(algo, id)
        delete!(algo.tree, id)
        delete!(algo.gnt2state, id)
    end
end

# delete subtree rooted at id. and return an array containing all id in the subtree.
function takeOut!(algo::LRAAlgorithm, id, par)
    # println("taking out... $(id) $(par)")
    #sleep(0.1)
    if (!isInTree(algo, id))
        return []
    end

    # rmFromQueues(algo, id)

    ret = [id]
    succ = Set()
    algo.instance.succ!(succ, id)

    for v in succ
        if (isInTree(algo, v) &&
            v != par &&
            getNode(algo,  v).parent == id)
            append!(ret, takeOut!(algo, v, id))
        end
    end

    rmFromTree!(algo, id)
    return ret
end

function getSucc(algo::LRAAlgorithm, id)
    rm = haskey(algo.rmEdgesFrom, id) ? algo.rmEdgesFrom[id] : Set()
    succ = Set()
    algo.instance.succ!(succ, id)
    return setdiff(succ, rm)
end

function getPred(algo::LRAAlgorithm, id)
    rm = haskey(algo.rmEdgesTo, id) ? algo.rmEdgesTo[id] : Set()
    pred = Set()
    algo.instance.pred!(pred, id)
    return setdiff(pred, rm)
end

function realEval(algo::LRAAlgorithm, from, to)
    real = algo.instance.real_transition_cost(to, from)
    if (real == Inf)
        if (!haskey(algo.rmEdgesFrom, from))
            algo.rmEdgesFrom[from] = Set()
        end
        if (!haskey(algo.rmEdgesTo, to))
            algo.rmEdgesTo[to] = Set()
        end
        push!(algo.rmEdgesFrom[from], to)
        push!(algo.rmEdgesTo[to], from)
        #println("removed $(from) $(to)")
    end
    return real
end

function lazyEval(algo::LRAAlgorithm, from, to)
    return algo.instance.lazy_transition_cost(to, from)
end

# return a new Tnode by a parent node and an edge.
function getNewNode(algo::LRAAlgorithm, parNode::TNode, id)
    @assert parNode.budget < algo.alpha "node $(id) budget exceeded"
    # @assert lazyEval(graph, edge) != Inf
    return TNode(id,
                parNode.id,
                parNode.cost,
                parNode.lazy + lazyEval(algo, parNode.id, id),
                algo.instance.heuristic(id),
                parNode.budget + 1)
end

function getGraph()
    open("input.txt") do file
        nV = parse(Int, readline(file))
        nE = parse(Int, readline(file))
        dept = parse(Int, readline(file))
        dest = parse(Int, readline(file))
        alpha = parse(Int, readline(file))
        lazyWgt = Dict()
        realWgt = Dict()
        edges = Dict()

        for i in 1:nE
            from = parse(Int, readline(file))
            to = parse(Int, readline(file))
            real = parse(Float64, readline(file))
            lazy = parse(Float64, readline(file))
            if (Int(real) == 0x3f3f3f3f)
                real = Inf
            end
            lazyWgt[(from, to)] = lazy
            lazyWgt[(to, from)] = lazy
            realWgt[(from, to)] = real
            realWgt[(to, from)] = real
            if (!haskey(edges, from))
                #println(from)
                edges[from] = Set()
            end
            if (!haskey(edges, to))
                #println(to)
                edges[to] = Set()
            end
            push!(edges[from], to)
            push!(edges[to], from)
            assert(haskey(edges, from))
            assert(haskey(edges, to))
        end

        edgeFunc = (y,x) -> map(z -> push!(y, z), edges[x])
        lrai = LRAInstance(
            (from, to) -> lazyWgt[(from, to)],
            (from, to) -> realWgt[(from, to)],
            x -> 0,
            x -> x == dest,
            edgeFunc,
            edgeFunc
        )
        algo = ConstructLRAAlgorithm(
            lrai,
            Int,
            Float64,
            alpha
        )
        return (dept, algo)
    end
end
