using LinearAlgebra: norm
#=
struct LazyAStarInstance{FT,FH,FG,FE,FV}# dispatch <: AbstractAStarInstance
  transition_cost::FT   # (state_to::STATE, state_from::STATE) ↦ edge_cost::COST
  heuristic::FH         # state::STATE ↦ cost_to_goal::COST #arguably belonds in Algorithm, not Instance
  goal_test::FG         # state::STATE ↦ ::Bool
  expand!::FE           # (Set{STATE}, from_state) ↦ ::Void
  valid::FV             # (state_to::STATE, state_from::STATE) ↦ ::Bool
end
=#

# get to 10, starting from 0.

ai = LazyAStarInstance(
  (s_to, s_fro) -> abs(s_fro - s_to),
  (s) -> float(abs(10-s)),
  (s) -> s == 10,
  function (set, s_fro)
    push!(set, s_fro + 1)
    push!(set, s_fro - 1)
  end,
  (s_to, s_fro) -> true
)


alg = LazyAStarAlgorithm(ai, Int64, Float64)

setup!(alg, 0, 0.0)

while isempty(alg.goal_node)
  if !step!(alg) break end
end

@test reverse(follow_path(alg.closed_parents, alg.goal_node[1])) == collect(0:10)

using StaticArrays
# grid world
goal = SVector(10,10)

transition_cost(s_to, s_fro) = norm(s_fro - s_to)

ai = LazyAStarInstance(
  transition_cost,
  (s) -> norm(goal-s),
  (s) -> s == [10, 10],
  function (set, s_fro)
    for dx=-1:1, dy=-1:1
      if !(dx==0 && dy==0)
        push!(set, s_fro + SVector(dx,dy))
      end
    end
  end,
  (s_to, s_fro) -> ifelse(s_to == [5, 5] || s_to == [5, 6], false, true)
)


alg = LazyAStarAlgorithm(ai, SVector{2,Int64}, Float64)

setup!(alg, SVector(0, 0), 0.0)

while isempty(alg.goal_node)
  if !step!(alg) break end
end

function get_path_length(path)
  return sum(transition_cost(to, fro) for (fro, to) in zip(path[1:end-1], path[2:end]))
end

reference_solution = [ [0, 0], [1, 1], [2, 2], [3, 3], [4, 4],  [5, 4],  [6, 5],  [7, 6],  [8, 7],  [9, 8],  [10, 9], [10, 10] ]
this_solution = reverse(follow_path(alg.closed_parents, alg.goal_node[1]))
@test this_solution[1] == reference_solution[1]
@test this_solution[end] == reference_solution[end]
@test get_path_length(this_solution) ≈ get_path_length(reference_solution)
@test length(alg.valid_cache) > 0


# Eager A*

ai = LazyAStarInstance(
  transition_cost,
  (s) -> norm(goal-s),
  (s) -> s == [10, 10],
  function (set, s_fro)
    for dx=-1:1, dy=-1:1
      if !(dx==0 && dy==0)
        s_to = s_fro + SVector(dx,dy)
        if !(s_to == [5, 5] || s_to == [5, 6])
          push!(set, s_to)
        end
      end
    end
  end,
  VAMP.AlwaysValid() # signal not to do this lazy stuff.
)

alg = LazyAStarAlgorithm(ai, SVector{2,Int64}, Float64)

setup!(alg, SVector(0, 0), 0.0)

while isempty(alg.goal_node)
  if !step!(alg) break end
end

this_solution = reverse(follow_path(alg.closed_parents, alg.goal_node[1]))
@test this_solution[1] == reference_solution[1]
@test this_solution[end] == reference_solution[end]
@test get_path_length(this_solution) ≈ get_path_length(reference_solution)
@test length(alg.valid_cache) == 0
