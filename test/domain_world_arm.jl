function make_domain_world_arm(parameters)
  p = parameters
  @setparam p robot_width = nothing
  @setparam p arm_length = 1.0
  @setparam p frustum_depth = nothing
  @setparam p hallway_inner_width = .5 * sqrt(2) * robot_width + .5 * robot_width
  @setparam p wall_width = robot_width/20
  @setparam p hallway_length = frustum_depth + robot_width # so that the robot has to enter the hallway to see it.
  @setparam p key_hallway_length = hallway_length
  @setparam p peephole_size = hallway_inner_width/2
  @setparam p peephole_wall = (hallway_inner_width - peephole_size) / 2
  @setparam p box_width = .5 * arm_length
  @setparam p box_height = .5 * arm_length
  @setparam p barrier_height = 2.5 * robot_width


  function wall_maker(p1, p2, width=wall_width)
    # specify p1 and p2, the medial line segment of the wall.
    w = width/2
    rot90 = [0 -1; 1 0]
    x = normalize(p2 - p1)
    y = normalize(rot90 * x)
    return [
      p1 - x * w - y * w,
      p1 - x * w + y * w,
      p2 + x * w + y * w,
      p2 + x * w - y * w
    ]
  end


  obstacles_points = [
    wall_maker(Point(0.0, 0.0), Point(box_width, 0.0)),
    wall_maker(Point(box_width, 0.0), Point(box_width, box_height)),
    wall_maker(Point(0.0, 0.0), Point(0.0, box_height)),

    wall_maker(Point(0.0, barrier_height), Point(2*robot_width, barrier_height))
  ]

  polys_obstacles__Fworld = UnionOfConvex(
    [
      polyhedron(
        vrep(VAMP.array([Point(p...) for p in obstacle_points]))) for obstacle_points in obstacles_points])

  VAMP.compute!.(polys_obstacles__Fworld.pieces)

  function cspace_collision(q_se2)
    return false
  end

  @setparam parameters polys_obstacles__Fworld
  @setparam parameters cspace_collision

  return nothing
end
