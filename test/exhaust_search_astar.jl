module Exhaust
import Plots
Plots.pyplot()
import DataStructures
using Printf

using VAMP
println("`using VAMP` completed")

include("domain_robot.jl")
include("domain_world.jl")
include("domain_world2.jl")
include("domain_motion_planning_collision.jl")
println("`include(domain_*)` completed")

parameters = Dict()

make_robot(parameters)
make_robot_vis_target(parameters)
make_domain_world2(parameters)
make_domain_motion_planning_collision(parameters)

@setparam parameters polys_robot__Frobot = nothing
@setparam parameters poly_frustum__Fcamera = nothing
@setparam parameters polys_obstacles__Fworld = nothing
@setparam parameters is_collision_free = nothing
@setparam parameters workspace_points__Fworld = nothing

@setparam parameters visibility_instance = let
    @setparam parameters tx__Frobot__Fcamerabase = nothing
    @setparam parameters poly_frustum__Fcamera = nothing
    @setparam parameters all_view__Fcamerabase = nothing
    @setparam parameters polys_obstacles__Fworld = nothing
    @setparam parameters points_robot__Frobot = nothing

    camera = PanSteerableCamera(tx__Frobot__Fcamerabase, poly_frustum__Fcamera)
    potential_camera = VAMP.PotentialCamera(camera, all_view__Fcamerabase)

    VisibilityInstance(
      Set(points_robot__Frobot),
      polys_obstacles__Fworld,
      potential_camera,
      q->UnionOfConvex([]));
end;

@setparam parameters frustum_depth = nothing
@setparam parameters frustum_width = nothing

const SE2_MPState_t = SE2_MPState{parameters[:robot_width]/2} # reasonable scaling to measure distance

const θ_steps = 16;
const Δθ = 2π/(θ_steps)
const Δl =  1/(2^3) # carefully chosen to be exactly representable in floating point and also include the goal, because of equality testing
const Δx = Δl
const Δy = Δl

function on_grid(q)
    all(q.e.q .% [Δx, Δy] .== 0) && (q.θ.q.θ % Δθ == 0)
end

@setparam parameters q_start = nothing
@setparam parameters goal = nothing
@setparam parameters starting_visibility__Fworld = nothing
@setparam parameters boundingbox_workspace__Fworld = nothing

q_start = convert(SE2_MPState_t, q_start)
goal = PointEpsilonGoal(convert(SE2_MPState_t, rand(goal)), goal.ϵ) # TODO clunky

mp = HolonomicMotionPlanningInstance(
  q_start,
  goal,
  q->is_collision_free(q) && contains(boundingbox_workspace__Fworld, Point(q.e.q...)), # cheaply bound the workspace to help have a finite search space.
  (state1,state2)->is_path_free(state1,state2)
)

for q in [rand(goal), q_start]
    @assert mp.q_valid_test(q)
    @assert on_grid(q)
end

angle_to_step(θ) = Int(round(θ/Δθ))
step_to_angle(step) = Δθ * (step % θ_steps) # makes all floating point angles canonical
angle_p_step(θ, Δsteps) = step_to_angle(angle_to_step(θ) + Δsteps)

# convenience because plotting stuff takes x and y separately usually. iterates twice, though.
xypoints(collection) = (getindex.(collection, 1), getindex.(collection, 2))


# two search types. one tries to achieve goal, other tries to achieve looks
move_safe_preference = 0.001
move_ai = LazyAStarInstance(
  (s_to, s_fro) -> distance(s_to, s_fro),
  (s) -> 0.0,
  (s) -> false && s ∈ goal,
  function (set, s_fro)
    x, y = s_fro.e.q
    θ = s_fro.θ.q.θ
    q_news = (
      SE2_MPState_t(x, y, angle_p_step(θ, +1)),
      SE2_MPState_t(x, y, angle_p_step(θ, -1)),
      SE2_MPState_t(x, y + Δy, θ),
      SE2_MPState_t(x, y - Δy, θ),
      SE2_MPState_t(x + Δx, y, θ),
      SE2_MPState_t(x - Δx, y, θ),
    )

    for q_new in q_news
      push!(set, q_new)
    end
    nothing
  end,
  (s_to, s_fro) -> mp.q_valid_test(s_to) # motions are not constrained to visible region alone.
)

move_alg = LazyAStarAlgorithm(move_ai, SE2_MPState_t{Float64}, Float64)

setup!(move_alg, mp.q_start, 0.0)

move_start_time = last_time = time()
last_n_closed = 0
while true
    if !step!(move_alg) break end
    if time() - last_time > 1.0
      @show length(move_alg.agenda)
      @show length(move_alg.closed_parents)
      this_n_closed = length(move_alg.closed_parents)
      @show this_n_closed - last_n_closed
      last_n_closed = this_n_closed
      last_time = time()
      println("Runtime: $(@sprintf("%.2f", time() - move_start_time))")
      sleep(0.01)
    end
end
end
