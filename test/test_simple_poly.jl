using Plots
using Polyhedra
using GeometryTypes

include("util.jl")

# test how it handles redundant points and halfspaces

square_points = [Point(-1.0, -1.0), Point(-1.0, 1.0), Point(1.0, -1.0), Point(1.0, 1.0)]

poly_sq = polyhedron(vrep(array(square_points)))
poly_sq_big = polyhedron(vrep(array(2 .* square_points)))


#vcat(collect(hreps(poly_sq)), collect(hreps(poly_sq_big)))
# I don't see any way to construct Polyhedron from Vector of HalfSpaces.
# works
## SimpleHRepresentation{2, Float64}(hreps(poly_sq))

redundant = vrep(polyhedron(hrep(poly_sq) ∩ hrep(oly_sq_big)))
#or
# redundant = polyhedron(intersect(hrep(poly_sq), hrep(poly_sq_big)))
removehredundancy!(redundant)
