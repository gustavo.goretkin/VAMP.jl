#=
robot_width = 1.0

# narrow_view = nothing
narrow_view_half_fov = nothing

if narrow_view_half_fov == nothing
  frustum_width = 2.0 * robot_width
else
  frustum_width = frustum_depth * 2 * tand(narrow_view_half_fov)
end
=#

# robot shape and camera definition

function make_narrow_cone(parameters)
  p = parameters
  @setparam p robot_width = 1.0
  @setparam p tx__Frobot__Fcamerabase = Translation(0.0, 0.0)
  @setparam p all_field_of_view = 30.0
  @setparam p frustum_depth = 2.5 * robot_width
  @setparam p frustum_width = tand(all_field_of_view) * frustum_depth
  @setparam p camera_is_fixed = true
end

function make_robot(parameters)
  #=let=# p = parameters
    @setparam p robot_width = 1.0
    @setparam p frustum_depth = 2.5 * robot_width
    @setparam p frustum_width = 2.0 * robot_width
    @setparam p tx__Frobot__Fcamerabase = Translation(robot_width/4, 0.0)
    @setparam p all_field_of_view = 200.0
    @setparam p camera_is_fixed = false
  #end


  polys_robot__Frobot = let half = robot_width/2, width=robot_width
    points_robot = [
      Point(-half, -half),
      Point(-half + 0.8width, -half),
      Point(half, -half + 0.2width),
      Point(half, -half + 0.8width),
      Point(half - 0.2width, half),
      Point(-half, half)
    ]
    UnionOfConvex([polyhedron(vrep(VAMP.array(points_robot)))])
  end

  poly_frustum__Fcamera = polyhedron(vrep(VAMP.array([
    Point(0.0, 0.0), Point(frustum_depth, frustum_width/2), Point(frustum_depth, -frustum_width/2)
  ])))

  VAMP.compute!.(polys_robot__Frobot.pieces)
  VAMP.compute!(poly_frustum__Fcamera)

  # imagine the camera is on a gimbal, so that the camera point (the focal? point of the frustum) is fixed given a robot configuration.
  half_fov = deg2rad(all_field_of_view) / 2
  all_view__Fcamerabase = ArcRegion(frustum_depth, cos(half_fov), Point(0.0, 0.0), Vec(1.0, 0.0))

  #=let=# p = parameters
    @setparam p polys_robot__Frobot
    @setparam p all_view__Fcamerabase
    @setparam p poly_frustum__Fcamera
  #end
  return nothing
end


function make_robot_vis_target(parameters, refine_max_distance=0.1)
  # only refine the border of a 2D robot. On a 3D robot, will add some points on facets too.
  @setparam parameters polys_robot__Frobot = nothing

  points_robot__Frobot = Point{2, Float64}[]
  for piece in polys_robot__Frobot.pieces
    these_points = collect(points(piece))
    append!(points_robot__Frobot, these_points)

    for (a, b) in VAMP.vertex_pairs_on_face(piece)
      append!(points_robot__Frobot, VAMP.convex_interior_refine([these_points[a], these_points[b]], refine_max_distance, true))
    end
  end
  @setparam parameters points_robot__Frobot
end
