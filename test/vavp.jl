import Plots
Plots.pyplot()
import Parameters: @pack!, @unpack, @with_kw
using VAMP

println("`using VAMP` completed")

include("domain_robot.jl")
include("domain_world2.jl")
include("domain_motion_planning_collision.jl")
println("`include(domain_*)` completed")

parameters = Dict{Symbol, Any}()

make_robot(parameters)
make_robot_vis_target(parameters)
make_domain_world2(parameters)
make_domain_motion_planning_collision(parameters)

VAMP.do_parameters(parameters)

vamp_pieces = make_vamp_pieces(parameters)

@unpack polys_obstacles__Fworld = parameters
@unpack polys_robot__Frobot = parameters
@unpack robot_width = parameters
@unpack q_start = parameters
@unpack SE2_MPState_t = parameters
@unpack SearchState_t = parameters
@unpack get_visible_region_cached__Fworld = vamp_pieces

starting_visibility__Fworld = [
  # circle around starting pose. Bigger than the domain default to get rid of a spurious point
  DifferenceGeneral(
    ArcRegion(1.3 * robot_width, -1.0, Point(q_start.e.q), Vec(1.0, 0.0)),
    UnionOfConvex(POLY[])
  )]

s1 = SearchState_t(q_start, [q_start],
  vcat(
      VAMP.make_packed_view_volume(UnionOfGeometry(starting_visibility__Fworld)),
       get_visible_region_cached__Fworld(q_start)
       ))

vavpi = VAVPInstance(vamp_pieces=vamp_pieces, start_state=s1)
vavpa = VAVPAlgorithm(instance=vavpi)


import Serialization
dir = "outs/vavp_regression"
trajectory_saved = joinpath(dir, "paths.jl_serialization")

coverage_goal = Set(GeometryTypes.Point{2,Float64}[Point(0.625, 2.125), Point(1.025, 2.5), Point(0.3249999999999999, 2.25), Point(0.42500000000000004, 2.0), Point(0.925, 2.375), Point(0.22499999999999992, 2.375), Point(1.125, 2.35), Point(0.825, 2.125), Point(0.525, 2.375), Point(0.22499999999999992, 2.5), Point(1.025, 2.25), Point(0.125, 2.3), Point(0.525, 2.125), Point(0.625, 2.0), Point(0.3249999999999999, 2.5), Point(0.525, 2.25), Point(1.125, 2.325), Point(0.825, 2.5), Point(0.625, 2.25), Point(0.3249999999999999, 2.0), Point(0.125, 2.375), Point(0.525, 2.5), Point(0.925, 2.125), Point(1.025, 2.375), Point(0.125, 2.325), Point(0.825, 2.375), Point(0.22499999999999992, 2.25), Point(0.125, 2.45), Point(1.125, 2.2), Point(0.125, 2.2), Point(0.125, 2.35), Point(0.125, 2.425), Point(0.125, 2.5), Point(0.125, 2.0), Point(0.725, 2.125), Point(1.125, 2.475), Point(0.125, 2.125), Point(0.725, 2.5), Point(0.725, 2.375), Point(1.125, 2.225), Point(0.42500000000000004, 2.375), Point(1.125, 2.5), Point(1.125, 2.4), Point(0.42500000000000004, 2.25), Point(0.42500000000000004, 2.125), Point(0.125, 2.25), Point(0.125, 2.1), Point(0.3249999999999999, 2.125), Point(0.125, 2.225), Point(1.125, 2.45), Point(0.825, 2.0), Point(1.125, 2.25), Point(1.125, 2.375), Point(0.125, 2.4), Point(1.125, 2.3), Point(1.025, 2.125), Point(0.925, 2.0), Point(1.025, 2.0), Point(1.125, 2.1), Point(0.825, 2.25), Point(0.725, 2.0), Point(0.625, 2.375), Point(0.725, 2.25), Point(0.42500000000000004, 2.5), Point(0.525, 2.0), Point(0.125, 2.475), Point(0.3249999999999999, 2.375), Point(0.925, 2.25), Point(1.125, 2.425), Point(1.125, 2.125), Point(0.22499999999999992, 2.0), Point(1.125, 2.0), Point(0.925, 2.5), Point(0.22499999999999992, 2.125)])

@unpack visibility_instance = parameters
@unpack boundingbox_obstacles__Fworld = parameters
V(q_robot) = VAMP.get_visible_region__Fworld(visibility_instance, q_robot)


initial_vis_p = VAMP.LibGEOSInterface.LibGEOS.Polygon(parameters[:starting_visibility__Fworld][1].positive)

if isfile(trajectory_saved)
  println("loading saved path")
  _nt = Serialization.deserialize(open(trajectory_saved))
  q_path1 = _nt.q_path1
  q_path2 = _nt.q_path2
  coverage_sub_goal = _nt.coverage_sub_goal
else
  setup!(vavpa, coverage_goal)
  while true
      r = step!(vavpa)
      if !r.keepgoing
          break
      end
  end

  println("vavp search is complete")

  if length(vavpa.stack) != 2
    @warn("I thought this was going to be level 2")
  end
  if vavpa.path == nothing
    @warn("expecting vavpa.path")
  end


  path1 = VAMP.debugshit[:vavp_stack_provenance][2]  # an undocumented interface...
  path2 = vavpa.path
  q_path1 = unlift_q(path1)
  q_path2 = unlift_q(path2)

  Serialization.serialize(open(trajectory_saved, "w"),
    (
      q_path1=q_path1,
      q_path2=q_path2,
      coverage_sub_goal = vavpa.stack[2]))
end


bb_empty = BoundingBox([NaN, NaN], [NaN, NaN])

boundingbox_swept__Fworld = reduce(union,
    (BoundingBox(get_robot__Fworld(polys_robot__Frobot, q)) for q in vcat(q_path1, q_path2)), init=bb_empty)

bb_view__Fworld = union(boundingbox_obstacles__Fworld, boundingbox_swept__Fworld)
(xlim, ylim) = dimlimits(bb_view__Fworld)


plt1 = Plots.plot(;aspect_ratio=1.0, reuse=false)
plt2 = Plots.plot(;aspect_ratio=1.0, reuse=false)

for plt = [plt1, plt2]
  Plots.plot!(plt; xlim=xlim, ylim=ylim)
end


for plt = [plt1, plt2]
  Plots.plot!(plt, initial_vis_p; color=:green, alpha=0.7, linealpha=0.0)
end

for q in q_path1
  Plots.plot!(plt1, VAMP.LibGEOSInterface.Polygon(V(q)), color=:green, fillalpha=0.7, linealpha=0.0)
end

for q in q_path2
  Plots.plot!(plt2, VAMP.LibGEOSInterface.Polygon(V(q)), color=:green, fillalpha=0.7, linealpha=0.0)
end


for plt = [plt1, plt2]
  plot_polyhedron!(plt, polys_obstacles__Fworld, fillcolor=:grey, linealpha=0.0)
end

scatter_points!(plt1, coverage_goal; markerstrokealpha=0.0, #=markerstrokewidth=0.0,=# color=:red)
scatter_points!(plt2, coverage_sub_goal; markerstrokealpha=0.0, #=markerstrokewidth=0.0,=# color=:blue)


for q in q_path1
    robot = VAMP.get_robot__Fworld(polys_robot__Frobot, q)
    plot_polyhedron!(plt1, robot; linecolor=:black, fillalpha=0.0)
end

for q in q_path2
    robot = VAMP.get_robot__Fworld(polys_robot__Frobot, q)
    plot_polyhedron!(plt2, robot; linecolor=:black, fillalpha=0.0)
end

for plt = [plt1, plt2]
  # during a session, I got, I guess, Plots, or matplotlib into a weird state where these were changing between plots.
  plt.o[:set_dpi](100.0)
  plt.o[:set_size_inches](12.0, 8.0)
  display(plt)
end


for (plt_i, plt) in enumerate([plt1, plt2])
  for ext in ["png", "svg", "pdf"]
    mkpath(dir)
    plt.o[:savefig](joinpath(dir, "figure_$(plt_i).$(ext)"), bbox_inches="tight")
  end
end
