using VAMP
using DataStructures
include("domain_robot.jl")
include("domain_world.jl")
include("domain_motion_planning_collision.jl")

parameters = Dict()

make_robot(parameters)
make_domain_world(parameters)
make_domain_motion_planning_collision(parameters)

@setparam parameters polys_robot__Frobot = nothing
@setparam parameters polys_obstacles__Fworld = nothing
@setparam parameters is_collision_free = nothing
@setparam parameters is_path_free = nothing
@setparam parameters robot_width = nothing
@setparam parameters backwall_position = nothing
@setparam parameters poly_frustum__Fcamera = nothing

const SE2_MPState_t = SE2_MPState{robot_width/2} # reasonable scaling to measure distance

const θ_steps = 20;
const Δθ = 2π/(θ_steps)
const Δl = 0.125 # carefully chosen to be exactly representable in floating point and also include the goal, because of equality testing

robot_x_far_back = backwall_position + 0.5 * robot_width + Δl
robot_x_far_back = round(robot_x_far_back / Δl) * Δl # TODO warning, rounding might make this goal not feasible
goal = PointEpsilonGoal(
  #SE2_MPState_t(robot_x_far_back, 0.625,π),
  SE2_MPState_t(robot_x_far_back, 0.625,0),
  0.01
)

mp = HolonomicMotionPlanningInstance(
  #SE2_MPState_t(-1.0,-1.0,0.0),
  SE2_MPState_t(-10.0,0.625,0.0),
  goal,
  state->is_collision_free(state),
  (state1,state2)->is_path_free(state1,state2)
)

angle_to_step(θ) = Int(round(θ/Δθ))
step_to_angle(step) = Δθ * (step % θ_steps) # makes all floating point angles canonical
angle_p_step(θ, Δsteps) = step_to_angle(angle_to_step(θ) + Δsteps)

function distance_l1(a, b)
    sum(abs.(a.e.q - b.e.q))
end

ai = LazyAStarInstance(
  (s_to, s_fro) -> distance(s_to, s_fro),
  (s) -> distance_l1(rand(goal), s),#distance(rand(goal), s),
  (s) -> s ∈ goal,
  function (set, s_fro)
    x, y = s_fro.e.q
    θ = s_fro.θ.q.θ
    Δx = Δy = Δl
    #push!(set, SE2_MPState_t(x, y, angle_p_step(θ, +1)))
    #push!(set, SE2_MPState_t(x, y, angle_p_step(θ, -1)))
    push!(set, SE2_MPState_t(x, y + Δy, θ))
    push!(set, SE2_MPState_t(x, y - Δy, θ))
    push!(set, SE2_MPState_t(x + Δx, y, θ))
    push!(set, SE2_MPState_t(x - Δx, y, θ))
    nothing
  end,
  (s_to, s_fro) -> is_collision_free(s_to)  # assuming the Δs above are tiny, don't check the whole path.
)


alg = LazyAStarAlgorithm(ai, SE2_MPState_t{Float64}, Float64)

setup!(alg, mp.q_start, 0.0)



using Plots
gr()

plt = plot(;aspect_ratio=1.0, title="astar")

for obstacle in polys_obstacles__Fworld.pieces
    plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
end

for p in VAMP.get_robot__Fworld(polys_robot__Frobot, mp.q_start).pieces
  plot_polyhedron!(plt, p; color=:green)
end

for p in VAMP.get_robot__Fworld(polys_robot__Frobot, rand(mp.q_goal)).pieces
  plot_polyhedron!(plt, p; color=:red)
end

display(plt)

println("start search")
start_time = time()
while isempty(alg.goal_node)
  if !step!(alg) break end
  if (time() - start_time) > 5.0 break end
end
println("done searching")

if length(alg.goal_node) > 0
  end_node = alg.goal_node[1]
else
  end_node = DataStructures.peek(alg.agenda).first.parent_state
end

tp = follow_path(alg.closed_parents, end_node)
solvepath = reshape(reinterpret(Float64, tp),  3, length(tp))';


plot!(plt, solvepath[:,1], solvepath[:,2] ; aspect_ratio = 1)

import Base.convert
convert(::Type{VAMP.MoveAction}, state::SE2_MPState) = VAMP.MoveAction([state.e.q..., state.θ.q.θ])

p = [convert(VAMP.MoveAction, state) for state in reverse(tp)]
stuff__Fworld = VAMP.worldgrounded_path(p, polys_robot__Frobot, poly_frustum__Fcamera, polys_obstacles__Fworld)
VAMP.plot_path!(plt, stuff__Fworld)

scatter!( unique((s->tuple(s.e.q...)).(collect(keys(alg.closed_parents)))); alpha=0.1, color=:red)
scatter!( unique((n->tuple(n.state.e.q...)).(collect(keys(alg.agenda)))); alpha=0.1, color=:grey)
