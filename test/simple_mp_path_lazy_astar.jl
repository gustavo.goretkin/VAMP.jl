using VAMP
using TimerOutputs
using PyPlot
import Random

include("domain_robot.jl")
include("domain_world.jl")
include("domain_motion_planning_collision.jl")
# include("VAMPlragraphNewLookAhead.jl")
# include("VAMPastarNewLookAhead.jl")
include("VAMPlragraph.jl")
include("VAMPastar.jl")

parameters = Dict()

make_robot(parameters)
make_domain_world(parameters)
make_domain_motion_planning_collision(parameters)

@setparam parameters polys_robot__Frobot = nothing
@setparam parameters polys_obstacles__Fworld = nothing
@setparam parameters is_collision_free = nothing
@setparam parameters is_path_free = nothing
@setparam parameters robot_width = nothing
@setparam parameters backwall_position = nothing
@setparam parameters poly_frustum__Fcamera = nothing

const SE2_MPState_t = SE2_MPState{robot_width/2} # reasonable scaling to measure distance

const θ_steps = 20;
const Δθ = 2π/(θ_steps)
const Δl = 0.125 # carefully chosen to be exactly representable in floating point and also include the goal, because of equality testing

robot_x_far_back = backwall_position + 0.5 * robot_width + Δl
robot_x_far_back = round(robot_x_far_back / Δl) * Δl # TODO warning, rounding might make this goal not feasible
goal = PointEpsilonGoal(
  #SE2_MPState_t(robot_x_far_back, 0.625,#=π=# 0),
  #SE2_MPState_t(robot_x_far_back, 0.625,π),
  #SE2_MPState_t(-3, -3, 0.0),
  SE2_MPState_t(robot_x_far_back, 0.625,0),
  0.01
)

mp = HolonomicMotionPlanningInstance(
  #SE2_MPState_t(-3.0,-3.0,0.0),
  SE2_MPState_t(-5.0,0.625,0.0),
  goal,
  state->is_collision_free(state),
  (state1,state2)->is_path_free(state1,state2)
)

angle_to_step(θ) = Int(round(θ/Δθ))
function step_to_angle(step)
    res = Δθ * mod(step, θ_steps) # makes all floating point angles canonical
    @assert res >= 0
    return res
end
angle_p_step(θ, Δsteps) = step_to_angle(angle_to_step(θ) + Δsteps)

mutable struct Counting{TF}
    f::TF
    counter::Int
end

function (c::Counting)(args...)
    c.counter += 1
    c.f(args...)
end

function distance_l1(a, b)
    sum(abs.(a.e.q - b.e.q))
end

lazy_transition_cost = (s_to, s_fro) -> distance(s_to, s_fro)
#heuristic = (s) -> distance(rand(goal), s)
heuristic = (s) -> distance_l1(rand(goal), s)
goal_test = (s) -> s ∈ goal
expand! =  function (set, s_fro)
    x, y = s_fro.e.q
    θ = s_fro.θ.q.θ
    Δx = Δy = Δl
    #push!(set, SE2_MPState_t(x, y, angle_p_step(θ, +1)))
    #push!(set, SE2_MPState_t(x, y, angle_p_step(θ, -1)))
    # push!(set, SE2_MPState_t(x, y, angle_p_step(θ, +1)))
    # push!(set, SE2_MPState_t(x, y, angle_p_step(θ, -1)))
    push!(set, SE2_MPState_t(x, y + Δy, θ))
    push!(set, SE2_MPState_t(x, y - Δy, θ))
    push!(set, SE2_MPState_t(x + Δx, y, θ))
    push!(set, SE2_MPState_t(x - Δx, y, θ))
    # push!(set, SE2_MPState_t(x + Δx, y + Δy, θ))
    # push!(set, SE2_MPState_t(x + Δx, y - Δy, θ))
    # push!(set, SE2_MPState_t(x - Δx, y + Δy, θ))
    # push!(set, SE2_MPState_t(x - Δx, y - Δy, θ))
    nothing
  end
real_transition_cost = (s_to, s_fro) -> if is_collision_free(s_to)
    lazy_transition_cost(s_to, s_fro)
else
    Inf
end

import Base.convert
result = Dict()
benchmarkStat = []
check_this_state = SE2_MPState_t(-1.75,-2.25,deg2rad(324.0))
#for α in 0.25:0.25:0.25*10
#α = 2.0
α = 5
    cRealCost = Counting(real_transition_cost, 0)
    cLazyCost = Counting(lazy_transition_cost, 0)
    # struct LRAInstance{FLT,FRT,FH,FG,FE}# dispatch <: AbstractAStarInstance
    #   lazy_transition_cost::FLT  # (state_to::STATE, state_from::STATE) ↦ edge_cost::COST
    #   real_transition_cost::FRT  # (state_to::STATE, state_from::STATE) ↦ edge_cost::COST
    #   heuristic::FH         # state::STATE ↦ cost_to_goal::COST #arguably belongs in Algorithm, not Instance
    #   goal_test::FG         # state::STATE ↦ ::Bool
    #   successors!::FE           # (Set{STATE}, state_from) ↦ ::Void
    #   predecessors!::FE           # (Set{STATE}, state_to) ↦ ::Void
    # end

    using Plots
    pyplot()
    #gr()

    plt = Plots.plot(;aspect_ratio=1.0,reuse=false)
    title!(plt, string("alpha=$(α), rewireWhenEqual=$(rewireWhenEqual)"))
    for obstacle in polys_obstacles__Fworld.pieces
        plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
    end

    for p in VAMP.get_robot__Fworld(polys_robot__Frobot, mp.q_start).pieces
      plot_polyhedron!(plt, p; color=:green)
    end

    for p in VAMP.get_robot__Fworld(polys_robot__Frobot, rand(mp.q_goal)).pieces
      plot_polyhedron!(plt, p; color=:red)
    end

    display(plt)
    start_time = time()
    lrai = LRAInstance(cLazyCost, cRealCost, heuristic, goal_test, expand!, expand!)
    # insert LRA setup call here. e.g.

    alg = ConstructLRAAlgorithm(lrai, SE2_MPState_t{Float64}, Float64, α)
    timerInit()
    setup!(alg, mp.q_start, 0.0)

    println("start search")
    counter = 0

    @show check_this_state ∈ keys(alg.tree)
    @show length(alg.frontier)
    #fst_node = 0
    while isempty(alg.goal_node) #&& counter < 100
        global counter
        counter += 1
        # if (check_this_state ∈ keys(alg.tree))
        #     check_this_node = alg.tree[check_this_state]
        #     @show check_this_node
        #     subtree_states = takeOut!(alg, check_this_state, check_this_node.parent, false)
        #     subtree_nodes = [alg.tree[state] for state in subtree_states]
        #     @show length(subtree_nodes)
        #     minnode = minimum(subtree_nodes)
        #     @show minnode
        #     @show orderToken(minnode)
        #     @show first(alg.frontier)
        #     @show orderToken(first(alg.frontier))
        #
        #     for node in subtree_nodes
        #         plot!([tuple(node.id.e.q...), tuple(node.parent.e.q...)], color=:blue)
        #     end
        #     display(plt)
        #     readline(stdin)
        # end

        res = step!(alg)

        global inspectSwitch
        if (inspectSwitch && check_this_state ∈ keys(alg.tree))
            check_this_node = alg.tree[check_this_state]
            @show check_this_node
            subtree_states = takeOut!(alg, check_this_state, check_this_node.parent, false)
            subtree_nodes = [alg.tree[state] for state in subtree_states]
            @show length(subtree_nodes)
            frt_nodes = [node for node in subtree_nodes if node.budget == α]
            if (isempty(frt_nodes))
                println("no frt nodes")
                readline()
            end
            minnode = minimum(frt_nodes)
            @show minnode
            @show tok = orderToken(minnode)
            global fst_node
            if (abs(tok[1]-5.7892457419320325)<1e-8 && tok[2] == α && tok[3] == 0x73ba88f5c1737b3f && first(alg.frontier) == fst_node)
                println("last round")
                return
                #readline()
            end

            @show first(alg.frontier)
            @show orderToken(first(alg.frontier))

            for node in subtree_nodes
              plot!([tuple(node.id.e.q...), tuple(node.parent.e.q...)], color=:blue)
            end
            #return
            if (first(alg.frontier) ∈ subtree_nodes)
                display(plt)
                println("to extend")
                #readline(stdin)
            end
        end
        # if (check_this_state ∉ keys(alg.tree))
        #     println("state not in tree")
        #     #readline()
        # end
        if res break end
        global start_time

        # if true #(time() - start_time) > 2.0
        #      @show length(alg.frontier)
        # #      #println(length(alg.update))
        # #      #println(length(alg.rewire))
        # #      #println(length(alg.extend))
        # #      node = first(alg.frontier)
        # #      @show alg.instance.goal_test(node.id)
        # #      #@show first(alg.frontier)
          # scatter!(
          #     unique(
          #         (s->tuple(s.id.e.q...)).(collect(alg.frontier))); alpha=0.1, color=:red)
        # #      display(plt)
        #      start_time = time()
        # end
        end
        println("done searching")
        println("alpha is $(α)\n")
        println("distance to target is $(getNode(alg,alg.goal_node[1]).cost)")
        print(to)
        println()
        @show length(collect([thing.id for thing in values(alg.tree) if thing.budget == 0]))
        append!(benchmarkStat, [to])
        print("\n\n\n")
#end

function plotBenchmarkRes(benchmarkStat)
    totTime = []
    totSpace = []
    ncalls = []
    extPct = []
    evalPct = []
    rewirePct = []
    updPct = []

    for to in benchmarkStat
        append!(totTime, TimerOutputs.tottime(to))
        append!(totSpace, TimerOutputs.totallocated(to))
        append!(ncalls, TimerOutputs.ncalls(to["extend!"]))
        append!(extPct, TimerOutputs.time(to["extend!"]) / totTime[end])
        append!(evalPct, TimerOutputs.time(to["evaluate!"]) / totTime[end])
        append!(rewirePct, TimerOutputs.time(to["rewire!"]) / totTime[end])
        append!(updPct, TimerOutputs.time(to["DataStructures.update!"]) / totTime[end])
    end
    totTime /= 1000^3 # convert to seconds
    totSpace /= 1024^3 # convert to GiB
    ncalls /= 1000 # convert to kilocalls
    updPct *= 100 # convert to percentage
    rewirePct *= 100
    evalPct *= 100
    extPct *= 100

    println(ncalls)
    gr()

    p1 = plot(totTime, xlabel="alpha", ylabel="sec", label="runtime")

    p2 = plot(totSpace, xlabel="alpha", ylabel="GiB", label="space", dpi=300)

    p3 = plot(ncalls, xlabel="alpha", ylabel="kilocalls", label="#calls", dpi=300)

    p4 = plot([updPct, rewirePct, evalPct, extPct],xlabel="alpha", ylabel="time percentage(%)", label=["update" "rewire" "evaluate" "extend"], dpi=300, size=(1000, 700))

    #plt.figure(figsize=(8,8))
    plot(p1, p2, p3, p4, layout=(2,2))

end
using Plots
plotBenchmarkRes(benchmarkStat)

# println(length(alg.frontier))
# println(length(alg.rewire))
# println(length(alg.update))
# println(length(alg.extend))

# the following block depends on an `alg`

if length(alg.goal_node) > 0
  end_node = alg.goal_node[1]
else
  end_node = DataStructures.peek(alg.agenda).first.parent_state
end

function follow_path2(tree::Dict, end_)  # TODO any Associative
  reached = Set()
  r = []
  next_ = end_
  while next_ ∉ reached && next_ != nothing
    push!(reached, next_)
    push!(r, next_)
    next_ = tree[next_].parent
  end
  return r
end

tp = follow_path2(alg.tree, end_node)
tp = [x for x in tp]
solvepath = reshape(reinterpret(Float64, tp),  3, length(tp))';


plot!(plt, solvepath[:,1], solvepath[:,2] ; aspect_ratio = 1)

convert(::Type{VAMP.MoveAction}, state::SE2_MPState) = VAMP.MoveAction([state.e.q..., state.θ.q.θ])

p = [convert(VAMP.MoveAction, state) for state in reverse(tp)]
stuff__Fworld = VAMP.worldgrounded_path(p, polys_robot__Frobot, poly_frustum__Fcamera, polys_obstacles__Fworld)
VAMP.plot_path!(plt, stuff__Fworld)

scatter!(
    unique(
        (s->tuple(s.id.e.q...)).(collect(alg.frontier))); alpha=0.1, color=:red)
scatter!(
    unique(
        (s->tuple(s.e.q...)).(collect(
                                [thing.id for thing in values(alg.tree) if thing.budget == 0]
                                ))); alpha=0.1, color=:red)

@show (alg.alpha)
@show (cRealCost.counter)
@show (cLazyCost.counter)
@show (getNode(alg,end_node).cost)

display(plt)
#result[alg.alpha] = @NT(real=cRealCost.counter, lazy=cLazyCost.counter, dist=getNode(alg,end_node).cost)
#scatter!( unique((n->tuple(n.state.e.q...)).(collect(keys(alg.agenda)))); alpha=0.1, color=:grey)
#end
