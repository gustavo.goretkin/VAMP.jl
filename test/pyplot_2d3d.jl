using VAMP
using Plots
pyplot()

plt = plot3d(rand(10), rand(10), rand(10))
plot3d!(;xlim=(-3,3), ylim=(-3,3), zlim=(0.0, 360.0))
display(plt) # must be displayed

include("domain_robot.jl")
include("domain_world.jl")

for poly in polys_obstacles__Fworld.pieces
  VAMP.pyplot_polyhedron3d!(plt.subplots[1].o, plt, poly)
end
