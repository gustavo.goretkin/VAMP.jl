function make_domain_world(parameters)
  p = parameters
  @setparam p robot_width = nothing
  @setparam p robot_diameter = sqrt(2) * robot_width
  @setparam p frustum_depth = nothing
  @setparam p hallway_inner_width = .5 * robot_diameter + .5 * robot_width
  @setparam p wall_width = robot_width/20
  @setparam p hallway_length = frustum_depth + robot_width # so that the robot has to enter the hallway to see it.
  @setparam p peephole_size = 0.0 * robot_width/3
  @setparam p peephole_wall = (hallway_inner_width - peephole_size) / 2

  mirror_hallway_center(ps) = [[0.0, hallway_inner_width]] .+ map(x->[1,-1] .* x, ps)

  bottom_wall_points = [[0.0, 0.0], [hallway_length, 0.0], [hallway_length, -wall_width], [0.0, -wall_width]]
  top_wall_points = mirror_hallway_center(bottom_wall_points)
  bottom_peep_points = [[0.0, 0.0], [wall_width, 0.0], [wall_width, peephole_wall], [0.0, peephole_wall]]
  top_peep_points = mirror_hallway_center(bottom_peep_points)

  obstacles_points_0 = [bottom_wall_points, top_wall_points, bottom_peep_points, top_peep_points]

  @setparam p backwall_position = -2.0

  obstacles_points = [[x + [backwall_position, 0.0] for x in points] for points in obstacles_points_0]


  polys_obstacles__Fworld = UnionOfConvex(
    [
      polyhedron(
        vrep(VAMP.array([Point(p...) for p in obstacle_points]))) for obstacle_points in obstacles_points])

  VAMP.compute!.(polys_obstacles__Fworld.pieces)

  function cspace_collision(q_se2)
    return false
  end

  @setparam parameters polys_obstacles__Fworld
  @setparam parameters cspace_collision

  @setparam parameters domain1_goal = :hard
  if domain1_goal == :hard
      @setparam parameters goal = PointEpsilonGoal(
        SE2_MPState(-1.0, 0.625, 0.0),
        0.01
    )
  elseif domain1_goal == :easy
    @setparam parameters goal = PointEpsilonGoal(
      SE2_MPState(0.0, 0.625, deg2rad(180)),
      0.01
    )
  else
    @warn("unrecognized")
  end

  @setparam parameters q_start = SE2_MPState(-1.0,-1.0,0.0)

  @setparam parameters starting_visibility__Fworld = [
    # circle around starting pose
    DifferenceGeneral(
      ArcRegion(0.8 * robot_width, -1.0, Point(q_start.e.q), Vec(1.0, 0.0)),
      UnionOfConvex(POLY[])
    )]
end
