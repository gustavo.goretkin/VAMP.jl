import JLD2
import Plots
Plots.pyplot()
import DataStructures
using Printf

using VAMP
println("`using VAMP` completed")

include("domain_robot.jl")
include("domain_world2.jl")
include("domain_motion_planning_collision.jl")
println("`include(domain_*)` completed")

parameters = Dict()

make_robot(parameters)
make_robot_vis_target(parameters)
make_domain_world2(parameters)
make_domain_motion_planning_collision(parameters)

@setparam parameters polys_robot__Frobot = nothing
@setparam parameters poly_frustum__Fcamera = nothing
@setparam parameters polys_obstacles__Fworld = nothing
@setparam parameters is_collision_free = nothing
@setparam parameters boundingbox_workspace__Fworld = nothing

@setparam parameters visibility_instance = let
    @setparam parameters tx__Frobot__Fcamerabase = nothing
    @setparam parameters poly_frustum__Fcamera = nothing
    @setparam parameters all_view__Fcamerabase = nothing
    @setparam parameters polys_obstacles__Fworld = nothing
    @setparam parameters points_robot__Frobot = nothing

    camera = PanSteerableCamera(tx__Frobot__Fcamerabase, poly_frustum__Fcamera)
    potential_camera = VAMP.PotentialCamera(camera, all_view__Fcamerabase)

    VisibilityInstance(Set(points_robot__Frobot), polys_obstacles__Fworld, potential_camera, q->UnionOfConvex([]));
end;

@setparam parameters frustum_depth = nothing
@setparam parameters frustum_width = nothing

include("../src/animate_path.jl")
const SE2_MPState_t = SE2_MPState{parameters[:robot_width]/2} # reasonable scaling to measure distance

const θ_steps = 16;
const Δθ = 2π/(θ_steps)
const Δl =  1/(2^3) # carefully chosen to be exactly representable in floating point and also include the goal, because of equality testing
const Δx = Δl
const Δy = Δl

function on_grid(q)
    all(q.e.q .% [Δx, Δy] .== 0) && (q.θ.q.θ % Δθ == 0)
end

@setparam parameters subgoals = nothing
@setparam parameters goal = nothing
@setparam parameters q_start = nothing

q_start = convert(SE2_MPState_t, q_start)
goal = PointEpsilonGoal(convert(SE2_MPState_t, rand(goal)), goal.ϵ) # TODO clunky
subgoals = map(q->convert(SE2_MPState_t, q), subgoals)

mp = HolonomicMotionPlanningInstance(
  q_start,
  goal, # dummy. #TODO do separate single query MP instance and MP collision definition
  state->is_collision_free(state),
  (state1,state2)->is_path_free(state1,state2)
)

# make sure all goals are motion-planning feasible
for q in [q_start, subgoals..., rand(goal)]
  @assert mp.q_valid_test(q)
  @assert on_grid(q)
end

@setparam parameters starting_visibility__Fworld = nothing

  # const ViewVolume = DifferenceGeneral{}
  # vcache = Dict{SE2_MPState_t, ViewVolume}()
  vcache = Dict{Any, Any}()

  function make_get_visible_region_cached__Fworld(vcache, visibility_instance)
    function get_visible_region_cached__Fworld(q_robot)
      if !haskey(vcache, q_robot)
        vcache[q_robot] = VAMP.get_visible_region__Fworld(visibility_instance, q_robot)
      end
      return vcache[q_robot]
    end
    return get_visible_region_cached__Fworld
  end

  get_visible_region_cached__Fworld = make_get_visible_region_cached__Fworld(vcache, visibility_instance)



angle_to_step(θ) = Int(round(θ/Δθ))
step_to_angle(step) = Δθ * (step % θ_steps) # makes all floating point angles canonical
angle_p_step(θ, Δsteps) = step_to_angle(angle_to_step(θ) + Δsteps)

struct SearchState{MP, V}
  q::MP
  q_path::Vector{MP}
  visible_region::V
end

struct SearchStateExplore{MP, V, NTS}
  q::MP
  q_path::Vector{MP}
  visible_region::V
  need_to_see::NTS
end

const SearchState_t = SearchState{SE2_MPState_t{Float64}, Vector{Any}}
const SearchStateExplore_t = SearchStateExplore{SE2_MPState_t{Float64}, Vector{Any}, Set{Any}}

function make_heuristic(mp, q_goal)
  # do uniform cost search starting from the motion planning goal.
  # this effectively solves all-source-single-goal shortest paths
  ai = LazyAStarInstance(
    (s_to, s_fro) -> distance(s_to, s_fro),
    (s) -> 0, # do uniform cost search
    (s) -> false, # no goal
    function (set, s_fro)
      x, y = s_fro.e.q
      θ = s_fro.θ.q.θ
      q_new = SE2_MPState_t(x, y, angle_p_step(θ, +1)); push!(set, q_new)
      q_new = SE2_MPState_t(x, y, angle_p_step(θ, -1)); push!(set, q_new)
      q_new = SE2_MPState_t(x, y + Δy, θ);    push!(set, q_new)
      q_new = SE2_MPState_t(x, y - Δy, θ);    push!(set, q_new)
      q_new = SE2_MPState_t(x + Δx, y, θ);    push!(set, q_new)
      q_new = SE2_MPState_t(x - Δx, y, θ);    push!(set, q_new)
      nothing
    end,
    (s_to, s_fro) -> mp.q_valid_test(s_to)  # assuming the Δs above are tiny, don't check the whole path.
  )

  alg = LazyAStarAlgorithm(ai, SE2_MPState_t{Float64}, Float64)

  setup!(alg, q_goal, 0.0)
  function heuristic(s::SearchState_t)
    print_time = time()
    if !mp.q_valid_test(s.q)
        return Inf
    end
    # TODO if s is not reachable from q_goal, then computation below won't terminate.
    # TODO round s to the discretization used in heuristic.
    n = length(alg.closed_parents)
    #println("in heuristic: $n")

    # round quantities to the search lattice, in case the heuristic is getting called by something other than A*
    Δxy = SVector(Δx, Δy)
    q = SE2_MPState_t(
      Δxy .* (round.(s.q.e.q ./ Δxy))...,
      step_to_angle(angle_to_step(s.q.θ.q.θ))
    )

    while !haskey(alg.closed_parents, q)
      step!(alg)
      if time() - print_time > 1.0
        println("Heuristic Search Function")
        @show length(alg.agenda)
        print_time = time()
      end
    end
    m = length(alg.closed_parents)
    (parent, move_cost) = alg.closed_parents[q]


    move_component = (1 + 1e-8) * move_cost
    h = move_component
    return h
  end

  return heuristic
end


# convenience because plotting stuff takes x and y separately usually. iterates twice, though.
xypoints(collection) = (getindex.(collection, 1), getindex.(collection, 2))

function vsafe_violations(s_to, s_fro)
  # s_to.q is seen by s_fro.q_path
  uncovered_pieces__Fworld = VAMP.get_uncovered_pieces__Fworld(visibility_instance, s_to.q, s_fro.visible_region)
  n = length(uncovered_pieces__Fworld)
  #=
  if n > 0
    println("vsafe returned: $n")
  end
  =#
  return n
end

function vsafe_test(s_to, s_fro)
  if !mp.q_valid_test(s_to.q) # assuming the Δs above are tiny, don't check the whole path.
    return false
  end

  return vsafe_violations(s_to, s_fro) == 0
end


# two search types. one tries to achieve goal, other tries to achieve looks

## two search tpyes within move. strict and not strict.

function move_vis_successors(set, s_fro)
  x, y = s_fro.q.e.q
  θ = s_fro.q.θ.q.θ
  q_news = (
    SE2_MPState_t(x, y, angle_p_step(θ, +1)),
    SE2_MPState_t(x, y, angle_p_step(θ, -1)),
    SE2_MPState_t(x, y + Δy, θ),
    SE2_MPState_t(x, y - Δy, θ),
    SE2_MPState_t(x + Δx, y, θ),
    SE2_MPState_t(x - Δx, y, θ),
  )

  for q_new in q_news
    push!(set, SearchState_t(q_new, vcat(s_fro.q_path, q_new), vcat(s_fro.visible_region, get_visible_region_cached__Fworld(q_new)) ))
  end
  nothing
end


struct ConfigurationDomination{SET} <: VAMP.AbstractDomination
  closed::SET
end

import VAMP.domination_add!
import VAMP.is_dominated
import Base.empty!

empty!(d::ConfigurationDomination) = empty!(d.closed)

function domination_add!(d::ConfigurationDomination, search_state)
  if search_state.q ∈ d.closed
    #println("errrr")
    #@show search_state.q
  end
  push!(d.closed, search_state.q)
end

function is_dominated(d::ConfigurationDomination, search_state)
  if search_state.q ∈ d.closed
    return (true, nothing)
  end
  return (false, nothing)
end


function make_move_alg(mp, goal, transition_cost, successors, edge_test,
  SearchState_t=SearchState_t,
  cost_t=Float64,
  domination=ConfigurationDomination(Set{SE2_MPState_t{Float64}}()),)
  heuristic=make_heuristic(mp, rand(goal))

  # for "relaxed" calls:
  # transition cost could include v-violation and edge_test could include just obstacle checking

  # presumably uses a transition_cost or edge_test that is different from the one used in the heuristic.
  ai = LazyAStarInstance(
    transition_cost,
    (s) -> heuristic(s),
    (s) -> s.q ∈ goal,
    successors,
    edge_test #
  )

  alg = LazyAStarAlgorithm(ai, SearchState_t, cost_t, domination)
  return alg
end

path = [SearchState_t(mp.q_start, [], starting_visibility__Fworld)]
path_segments = []

println("start searches on subgoal sequence")
flush(STDOUT)
for subgoal in subgoals
  # greedy search, alternate beteen move and look
  move_alg = make_move_alg(
    mp, PointEpsilonGoal(subgoal, 0.1),
    (s_to, s_fro) -> distance(s_to.q, s_fro.q),
    move_vis_successors,
    vsafe_test
  )
  setup!(move_alg, path[end], 0.0)

  last_time = 0.0
  last_n_closed = 0

  move_start_time = time()

  fall_through = false
  last_phase = :move
  println("Move search.")
  while isempty(move_alg.goal_node)
    if !step!(move_alg) break end

    if time() - last_time > 1.0
      @show length(move_alg.agenda)
      this_n_closed = length(move_alg.closed_parents)
      @show this_n_closed - last_n_closed
      last_n_closed = this_n_closed
      last_time = time()
      println("Runtime: $(@sprintf("%.2f", time() - move_start_time))") ; flush(STDOUT)
    end

    if time() - move_start_time > 60.0
      println("move search timeout"); flush(STDOUT)
    end

  end
  if fall_through
    break
  end

  println("\n\nMove search terminated. Time: $(@sprintf("%.2f", time() - move_start_time))")
  @show length(move_alg.closed_parents)
  @show length(move_alg.agenda)

  if isempty(move_alg.goal_node)
    error("move search didn't get to goal.")
  end

  move_path = reverse(follow_path(move_alg.closed_parents, move_alg.goal_node[1]))
  append!(path, move_path[2:end])
  push!(path_segments, move_path)

end
println("subgoal search is done.")

function check_visual_constraint(q_path, visibility_instance, starting_visibility__Fworld)
  swept_path = [transform(VAMP.get_tx__Fworld__Frobot(q_robot), visibility_instance.robot__Frobot) for q_robot in q_path]
  potential_view_path = [VAMP.get_visible_region__Fworld(visibility_instance, q_robot) for q_robot in q_path]

  n = length(q_path)
  violations = []
  for i = 1:n
    for point in swept_path[i]
      if !isinside(UnionOfGeometry(starting_visibility__Fworld), point) && !isinside(UnionOfGeometry(potential_view_path[1:i]), point)
        push!(violations, (i, point))
      end
    end
  end
  return violations
end

function check_visual_feasibility(q_path, discretized_views, starting_visibility__Fworld)
  swept_path = [transform(VAMP.get_tx__Fworld__Frobot(q_robot), visibility_instance.robot__Frobot) for q_robot in q_path]
  potential_view_path = [VAMP.get_visible_region__Fworld(visibility_instance, q_robot) for q_robot in q_path]

  n = length(q_path)
  violations = []
  for i = 1:n
    for point in swept_path[i]
      if !isinside(UnionOfGeometry(starting_visibility__Fworld), point) &&
          !isinside(UnionOfGeometry(
            vcat(discretized_views[1:i]...)), point)
        push!(violations, (i, point))
      end
    end
  end
  return violations
end


do_plan_views = false

if do_plan_views
  println("plan views")
  q_path = map(s->s.q, path)
  swept_points_path = [transform(VAMP.get_tx__Fworld__Frobot(q_robot), visibility_instance.robot__Frobot) for q_robot in q_path]
  discretized_views = VAMP.get_discretized_views(q_path, visibility_instance, frustum_depth, frustum_width, poly_frustum__Fcamera, force_single_center=false)
  println("make model")
  plan_view = VAMP.make_plan_views_model(swept_points_path, discretized_views, starting_visibility__Fworld)
  println("solve model")
  solve_status = JuMP.solve(plan_view[:model])

  if solve_status == :Optimal
    no_view_plan_path = path

    path = Any[no_view_plan_path[1]]
    for i = 1:length(no_view_plan_path)
      q = no_view_plan_path[i].q
      q_path = map(s->s.q, path)

      push!(path, SearchState_t(q, q_path, path[end].visible_region))

      for j in find(JuMP.getvalue(plan_view[:view_taken])[i,:])
        visible_region_prefix = path[end].visible_region
        new_view = discretized_views[i][j]
        visible_region = vcat(visible_region_prefix, [new_view])
        push!(path, SearchState_t(q, q_path, visible_region))
      end
    end
  else
    println("\n\nplan view solver status: $(solve_status)")
  end
end

tp = map(s->s.q, path)
flatten_path(path::Array{VAMP.SE2MotionPlanningState{SCALE, T}}) where {SCALE, T} = reshape(reinterpret(T, path),  3, length(path))'
solvepath = flatten_path(tp);

obstacles_bbox = BoundingBox(Point(NaN, NaN))

for poly in polys_obstacles__Fworld.pieces
  for p in points(poly)
    obstacles_bbox = union(obstacles_bbox, BoundingBox(Point(p...)))
  end
end

robot = polys_robot__Frobot


robot_path_bbox = BoundingBox(Point(NaN, NaN))

for q in tp
  for poly in VAMP.get_robot__Fworld(robot, q).pieces
    for p in points(poly)
      robot_path_bbox = union(robot_path_bbox, BoundingBox(Point(p...)))
    end
  end
end

view_bounding_box = let bbox = union(robot_path_bbox, obstacles_bbox), margin = parameters[:robot_width]/20
  BoundingBox(minlimit(bbox) .- margin, maxlimit(bbox) .+ margin)
end

println("will start plotting.")
plt = Plots.plot(;aspect_ratio=1.0)
for obstacle in polys_obstacles__Fworld.pieces
    plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
end

for path_segment in path_segments
  for q = [path_segment[1].q, path_segment[end].q]
    for p in VAMP.get_robot__Fworld(polys_robot__Frobot, q).pieces
      plot_polyhedron!(plt, p; color=:green, fillalpha=0.0)
    end
  end
end
display(plt)

make_frame_visible_region!(plt, path[end].visible_region,
  VAMP.RasterVolumeSpec(view_bounding_box, (256, 256)))
# DUPLICATE UNTIL FIGURE OUT Plots.jl z-ordering

for obstacle in polys_obstacles__Fworld.pieces
    plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
end

for p in VAMP.get_robot__Fworld(polys_robot__Frobot, mp.q_start).pieces
  plot_polyhedron!(plt, p; color=:green)
end

Plots.plot!(plt, solvepath[:,1], solvepath[:,2] ; aspect_ratio = 1)

import Base.convert
convert(::Type{VAMP.MoveAction}, state::SE2_MPState) = VAMP.MoveAction([state.e.q..., state.θ.q.θ])

p = [convert(VAMP.MoveAction, state) for state in tp]
stuff__Fworld = VAMP.worldgrounded_path(p, polys_robot__Frobot, poly_frustum__Fcamera, polys_obstacles__Fworld)
VAMP.plot_path!(plt, stuff__Fworld)


display(plt)

path_full = path
nothing; # for not printing the last line if it's noisy.
