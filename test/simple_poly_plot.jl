using Plots
using Polyhedra
using GeometryTypes

import VAMP
pyplot()

square_points = [Point(-1.0, -1.0), Point(-1.0, 1.0), Point(1.0, -1.0), Point(1.0, 1.0)]

poly_sq = polyhedron(vrep(VAMP.array(square_points)))


plot(poly_sq)
