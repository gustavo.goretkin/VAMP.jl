using Printf
using VAMP

include("domain_robot.jl")
include("domain_world.jl")
include("domain_motion_planning_collision.jl")

camera = PanSteerableCamera(tx__Frobot__Fcamerabase, poly_frustum__Fcamera)
visibility_instance = VisibilityInstance(polys_robot__Frobot, polys_obstacles__Fworld, camera, q->UnionOfConvex([]))


# BEGIN COPY'd from simple_mp_path.jl but LIFTED TO VAMP
q_goal = SE2_MPState(1.0,0.6,0.0; SCALE=.2)
#q_goal = SE2_MPState(-1.0,1.0,0.0; SCALE=.2) # easy problem

goal = PointEpsilonGoal(
  VampState(q_goal, VISIBILITY_S[]), # lift to vamp
  0.01
)

# starting_visibility__Frobot

mp = HolonomicMotionPlanningInstance(
  VampState(SE2_MPState(-1.0,-1.0,0.0; SCALE=.2), VISIBILITY_S[]), # lift to vamp
  goal,
  state->is_collision_free(state.robot),  # lift to vamp
  (state1,state2)->is_path_free(state1.robot,state2.robot) # lift to vamp
)

rrt_instance = RRTInstance(
  mp,
  0.5,
  ()->if (rand() < .1) rand(mp.q_goal) else VampState(SE2_MPState(6*(rand()-.5), 6*(rand()-.5), 2π*rand(); SCALE=.2), VISIBILITY_S[]) end
)

rrt_algorithm = RRTAlgorithm(rrt_instance, VAMP.RRTNodeAction{VAMPSTATE, VAMPACTION})
# END COPY

vamp_algorithm = VampRRTAlgorithm(rrt_algorithm, visibility_instance)

setup!(vamp_algorithm.rrt, 10_000)

# slow
using Plots
using JLD2
unique_id = hex(rand(UInt))[1:6]

setup!(vamp_algorithm.rrt, 10_000)
last_l = -1
frame_count = 1
tic()
for i = 1:50_000
  q_rand = rrt_instance.q_sampler()
  @show i length(vamp_algorithm.rrt.tree)
  if i%50 == 1
    println("plot time")
    plt = plot(;xlim=(-4,4), ylim=(-4,4), aspectratio=1.0, legend=false)
  else
    plt = nothing
  end

  step!(vamp_algorithm, q_rand, plt)

  if last_l != length(vamp_algorithm.rrt.tree)
    last_l = length(vamp_algorithm.rrt.tree)

    if plt != nothing
      for diff_ in vamp_algorithm.rrt.tree[end].state.viz
       for p in explicit(diff_).pieces
         plot_polyhedron!(plt, p; color=:yellow, alpha=0.2)
       end
      end

      for p in polys_obstacles__Fworld.pieces
        plot_polyhedron!(plt, p; color=:brown)
      end

      for p in VAMP.get_robot__Fworld(polys_robot__Frobot, vamp_algorithm.rrt.tree[end].state.robot).pieces
        plot_polyhedron!(plt, p; color=:purple)
      end

      path = getpath(vamp_algorithm.rrt.tree, vamp_algorithm.rrt.tree[end])
      plot!(plt, [tuple(node.robot.e.q...) for node in path]; linewidth=4.0, color=:black, marker=:o)

      frame_str = @sprintf("%05d", frame_count)
      filename = "animation/$(unique_id)_$(frame_str).png"

      savefig(plt, filename)
      #display(plt)
      frame_count += 1
    end

    if length(rrt_algorithm.reached_goals) > 0
      println("FOUND GOAL")
      break
    end

    if i%2000 == 1
      println("save time")
      @save "animation/visrun_$(Dates.now()).jld2" vamp_algorithm
    end
  end
end
toc()
