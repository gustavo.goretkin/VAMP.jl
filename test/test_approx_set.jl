r_ = float.(rand(1:1000, 10))
r = vcat(r_, r_)
noise = ((rand(length(r)) .- 0.5) * sqrt(eps())) .+ 1 # multiplicative noise keeping relative accuracy well within sqrt(eps)
rn = r .* noise
bigvalue = 10 # something > 1
bignoise = ([rand(Set([-bigvalue, bigvalue])) for _ in 1:length(r)] .* sqrt(eps())) .+ 1  # will never be isapprox with default rel accuracy
rbign = r .* bignoise

@test isapprox(Set(r_), Set(r_))
@test isapprox(Set(r_), Set(r))
@test isapprox(Set(r), Set(rn))
@test isapprox(Set(r), Set(rbign)) == false
@test isapprox(Set(r), Set(rbign); rtol=2*bigvalue*sqrt(eps()))
