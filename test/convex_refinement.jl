# show what it looks like to cover the space. ideally no black should show through the circles, but this isn't guaranteed
using VAMP
using Plots
include("domain_robot.jl")

v = map(x->(Point(x...)), collect(points(polys_robot__Frobot.pieces[1])))
v = v[randperm(length(v))]
max_distance = 0.05
@inline isclose(a,b) = sum((a-b).^2) ≤ max_distance^2
@inline ishalfclose(a,b) = sum((a-b).^2) ≤ (0.5* max_distance)^2
@inline center(a,b) = (a + b) ./ 2


import GR
f(x,y) =  Point(GR.wctondc(x, y)...)
wctondc_scale = mean(f(1.0, 1.0) - f(0.0, 0.0))
msize = wctondc_scale * sum(Plots.gr_plot_size) # this is the markersize to use so that marker Shape units match data units
circle_marker = let r=1.0; Shape([(r*cos(x), r*sin(x)) for x in range(0, stop=2π, length=80)]) end


plt = plot(;aspect_ratio=1.0, legend=nothing)
plot_polyhedron!(plt, polys_robot__Frobot.pieces[1], fillcolor=:black)
scatter!(plt, map(x->tuple(x...), v))
display(plt)

function cb(point)
  scatter!(plt, map(x->tuple(x...), [point]))
  display(plt)
end
vr = VAMP.convex_interior_refine(v, isclose, ishalfclose, center, cb)

#scatter!(plt, map(x->tuple(x...), vr))
