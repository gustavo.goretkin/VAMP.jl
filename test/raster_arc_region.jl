using Plots
using VAMP

include("domain_robot.jl")
include("domain_world.jl")


parameters = Dict{Symbol, Any}()

make_robot(parameters)
make_domain_world(parameters)
make_robot_vis_target(parameters)
VAMP.make_visibility_instance(parameters)

@setparam parameters all_view__Fcamerabase = nothing
@setparam parameters polys_obstacles__Fworld = nothing

tx__Fworld__Fcamerabase = VAMP.get_tx__Fworld__Frobot(SE2_MPState(0.0,-0.3,0.0))

vis = VAMP.compute_visibility(tx__Fworld__Fcamerabase, all_view__Fcamerabase, polys_obstacles__Fworld)

@inline f(x, old) = old || isinside(vis, Point(x...))

rv = VAMP.RasterVolume(
  VAMP.RasterVolumeSpec(
    VAMP.BoundingBox([-3.0, -3.0], [3.0, 3.0]),
    (1000, 1000)),
  BitArray(1000,1000)
  )

rv.data .= 0

VAMP.render!(rv, f)

plt = plot(;aspect_ratio=1)

heatmap!(plt, VAMP.coordinates(rv.spec)..., rv.data')

for poly in polys_obstacles__Fworld.pieces
  plot!(plt, poly, color=:brown)
end

display(plt)
