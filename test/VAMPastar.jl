using VAMP
using TimerOutputs
using Printf
using PyPlot
include("VAMPlragraph.jl")

to = TimerOutput()

debug = Dict()
@timeit to function evaluate!(algo::LRAAlgorithm, node::TNode)
    global debug
    debug[:algo] = algo
    debug[:node] = node
    #print("...")
    parNode = getParNode(algo, node.id)
    while (parNode.budget != 0)
        #println(node)
        node = parNode
        parNode = getParNode(algo, node.id)
        debug[:parNode] = parNode
    end

    realEdgCost = realEval(algo, node.parent, node.id)

    # @show parNode
    # readline()

    if (realEdgCost != Inf)
        newChildNode = TNode(node.id, node.parent, parNode.cost+realEdgCost, 0.0, best_heuristic(algo, node.id), 0)
        updateTreeNode!(algo, newChildNode)
        push!(algo.update, newChildNode)
        succ = false
        if (algo.instance.goal_test(newChildNode.id))
            push!(algo.goal_node, newChildNode.id)
            succ=true
        end
        return (succ, [])
    else
        #println(algo.tree)
        treeRewire = takeOut!(algo, node.id, node.parent)
        return (false, treeRewire)
    end
    return (false, [])
end

function check_heuristic(queue, algo)
    if (length(queue) == 0) return end
    node = pop!(queue)
    while (node.estm != (best = best_heuristic(algo, node.id)))
        newNode = TNode(node.id,
                        node.parNode,
                        node.cost,
                        node.lazy,
                        best,
                        node.budget)
        push!(queue, newNode)
        node = pop!(queue)
    end
    push!(queue, node)
end

@timeit to function DataStructures.update!(algo::LRAAlgorithm)
    while (!isempty(algo.update))
        check_heuristic(algo.update, algo)
        node = pop!(algo.update)
        @assert isInTree(algo, node.id)

        succ = getSucc(algo,node.id)
        childVtxs = [v for v in succ
                        if (isInTree(algo, v) &&
                            getNode(algo, v).parent == node.id)]

        if (length(childVtxs) == 0 || algo.instance.goal_test(node.id))
            @assert isInTree(algo, node.id)
            push!(algo.extend, node)
        else
            for v in childVtxs
                childNode = getNode(algo, v)
                if (childNode.budget == algo.alpha)
                    remove!(algo.frontier, childNode)
                end
                newChildNode = getNewNode(algo, node, v)
                updateTreeNode!(algo, newChildNode)
                if (newChildNode.budget < algo.alpha)
                    push!(algo.update, newChildNode)
                end
            end
        end
        #displayTree(algo, "finished update")
    end
end

@timeit to function rewire!(algo::LRAAlgorithm, treeRewire::Set)
    # if you run this part, you won't get any error.
    # this part is used to testify that ignore rewiring is feasible.
    # for v in treeRewire
    #     pred = getPred(algo, v)
    #     for p in pred
    #         if !isInTree(algo, p) continue end
    #         pNode = getNode(algo, p)
    #         if pNode.budget < algo.alpha
    #             push!(algo.extend, pNode)
    #         end
    #     end
    # end

    # below is a regular rewiring procedure.
    for v in treeRewire
        pred = getPred(algo, v)
        #algo.instance.pred!(pred, v)
        newNodes = [getNewNode(algo, getNode(algo, u), v)
                    for u in pred
                    if (isInTree(algo, u) &&
                        !in(u, treeRewire) &&
                        lazyEval(algo, u, v) != Inf &&
                        getNode(algo, u).budget < algo.alpha)]

        if (length(newNodes) == 0)
            continue
        end
        minNewNode = minimum(newNodes)
        if (!isInTree(algo, v) || minNewNode < getNode(algo, v))
            #println("rewired node $(minNewNode)")
            updateTreeNode!(algo, minNewNode)
            push!(algo.rewire, minNewNode)
        end
        #displayTree(algo)
    end

    while (!isempty(algo.rewire))
        check_heuristic(algo.update, algo)
        node = pop!(algo.rewire)

        if (node.budget == algo.alpha || algo.instance.goal_test(node.id))
            @assert isInTree(algo, node.id)
            push!(algo.frontier, node)
            continue
        end

        if (node.budget < algo.alpha)
            push!(algo.extend, node)
        else
            succ = getSucc(algo, node.id)
            for v in succ
                if (lazyEval(algo, node.id, v) == Inf || !in(v, treeRewire))
                    continue
                end
                newNode = getNewNode(algo, node, v)
                inTree = isInTree(algo, v)
                if (!inTree || newNode < getNode(algo, v))
                    updateTreeNode!(algo, newNode)
                    push!(algo.rewire, newNode)
                end
            end
        end
        #displayTree(algo, "finished rewiring")
    end
end

# should be defined somewhere else
# rewireWhenEqual = false

@timeit to function extend!(algo::LRAAlgorithm)
    loop_counter = 0    # this does nothing except for prevent an issue like https://github.com/JuliaLang/julia/issues/30093
    while (!isempty(algo.extend))
        loop_counter += 1
        check_heuristic(algo.update, algo)
        node = pop!(algo.extend)
        #@show node
        #readline()
        #println(node)
        if (algo.instance.goal_test(node.id))
            @assert isInTree(algo, node.id)
            push!(algo.frontier, node)
            return
        end

        succ = getSucc(algo, node.id)
        #algo.instance.succ!(succ, node.id)
        #println("succ is $(succ)")
        for v in succ
            #print(v)
            leval = lazyEval(algo, node.id, v)
            if (leval == Inf)
                continue
            end

            newChildNode = getNewNode(algo, node, v)

            if (isInTree(algo, v))
                childNode = getNode(algo, v)
                # if (childNode == newChildNode)
                #     @show childNode
                #     @show orderToken(childNode)
                #     @show newChildNode
                #     @show orderToken(newChildNode)
                #     sleep(1)
                # end
                if (childNode < newChildNode)
                    continue
                end

                # this part is for debugging only
                global rewireWhenEqual
                if (!rewireWhenEqual && childNode == newChildNode)
                    continue
                end

                @assert isInTree(algo, node.id)
                # if (newChildNode.parent == childNode.parent)
                #     @show childNode == newChildNode
                #     @show newChildNode < childNode
                #     @timeit to "regular update" x = 1
                # end
                #debug part ends here.

                if (orderToken(childNode) != orderToken(newChildNode))
                    @timeit to "takeout" rmNodeLst = takeOut!(algo, v, node.id)
                end
                # for debugging only, check what nodes are removed
                global start_time
                global plt
                if (time() - start_time) > 1
                      # @show length(rmNodeLst)
                      # @show childNode
                      # @show newChildNode
                      # @show childNode == newChildNode
                      # readline()
                     # @show length(alg.update)
                     # @show length(alg.rewire)
                     # @show length(alg.extend)
                     # @show length(alg.frontier)
                #      #node = first(alg.frontier)
                #      #@show alg.instance.goal_test(node.id)
                #      #@show first(alg.frontier)
                    # scatter!(
                    #     unique(
                    #         (s->tuple(s.e.q...)).(rmNodeLst)); alpha=0.1, color=:red)
                    #  display(plt)
                    #start_time = time()
                 end
                #debugging ends here.

                # take out the subtree of root target(e)
                @assert isInTree(algo, node.id) "$(node.id) should be in tree$(oldNode)"
            end

            updateTreeNode!(algo, newChildNode)
            if (newChildNode.budget == algo.alpha)
                @assert isInTree(algo, newChildNode.id)
                push!(algo.frontier, newChildNode)
            else
                @assert isInTree(algo, newChildNode.id)
                push!(algo.extend, newChildNode)
            end
            displayTree(algo, "extending")
        end
        displayTree(algo, "finished extending node")
    end
end

function VAMP.setup!(algo::LRAAlgorithm, start_state, start_cost)
    updateTreeNode!(algo, TNode(start_state, nothing, start_cost, 0.0, best_heuristic(algo, start_state), 0))
    push!(algo.extend, getNode(algo, start_state))
    extend!(algo)
    displayTree(algo)
end

function VAMP.step!(algo::LRAAlgorithm)::Bool
    # println("stepping...")
    #println(algo.frontier)
    if (isempty(algo.frontier))
        println("exit empty")
        return true
    end

    check_heuristic(algo.update, algo)
    printHeuristic(algo, first(algo.frontier).id, "before eval")

    node = pop!(algo.frontier)
    (success, treeRewire) = evaluate!(algo, node)

    displayTree(algo, "after eval")

    # println("eval")
    if (success)
        println("exit here")
        return true
    end

    update!(algo)
    # println("upd")
    # println("rewire set is $(treeRewire)")
    rewire!(algo, Set(treeRewire))
    # println("after rew")
    # for (x, y) in algo.tree
    #     println(y)
    # end
    extend!(algo)
    displayTree(algo, "after extend")
    # println("ext")
    return false
end

framecounter = 0

function printHeuristic(algo::LRAAlgorithm, state, caption::String="")
    global start_time
    global evalSet
    global framecounter
    if true || time() - start_time > 0.05

        p = deepcopy(plt)
        title!(p, string("alpha=$(α), rewireWhenEqual=$(rewireWhenEqual), $(caption)"))
        for nd in values(algo.tree)
            if (nd.parent == nothing)
                continue
            end
            (f, t) = minmax(tuple(nd.id.e.q...), tuple(nd.parent.e.q...))
            if (nd.budget > 0)
                plot!(p, [f,t], color=:blue)
            else
                plot!(p, [f,t], color=:yellow)
            end
        end

        path = []
        best_heuristic(algo, state, true, path)
        #plotting heuristic
        for i in 1:length(path)-1
            (f, t) = minmax(tuple(path[i].e.q...), tuple(path[i+1].e.q...))
            plot!(p, [f,t], color=:green)
        end

        cnt = 0
        for nd in algo.frontier
            if (cnt == 0)
                scatter!(p, nd.id.e.q, color=:red)
            elseif cnt <= 9
                scatter!(p, nd.id.e.q, color=:blue)
            else
                scatter!(p, nd.id.e.q, color=:purple, alpha=0.1)
            end
            cnt += 1
        end
        #display(p)
        framecounter += 1
        name = @sprintf("%05d.png",framecounter)
        path = "./rewireWhenEqual=$(rewireWhenEqual)/photos/"
        mkpath(path)
        Plots.savefig(p,  joinpath(path,name))
        #scatter!([tuple(from.e.q...), tuple(to.e.q...)]; alpha=0.1, color=:red)
        start_time = time()
    end
end

function displayTree(algo::LRAAlgorithm, caption::String="")
    global start_time
    global evalSet
    global framecounter
    if true || time() - start_time > 0.05
        p = deepcopy(plt)
        title!(p, string("alpha=$(α), rewireWhenEqual=$(rewireWhenEqual), $(caption)"))
        for nd in values(algo.tree)
            if (nd.parent == nothing)
                continue
            end
            (f, t) = minmax(tuple(nd.id.e.q...), tuple(nd.parent.e.q...))
            if (nd.budget > 0)
                plot!(p, [f,t], color=:blue)
            else
                plot!(p, [f,t], color=:yellow)
            end
        end
        cnt = 0
        for nd in algo.frontier
            if (cnt == 0)
                scatter!(p, nd.id.e.q, color=:red)
            elseif cnt <= 9
                scatter!(p, nd.id.e.q, color=:blue)
            else
                scatter!(p, nd.id.e.q, color=:purple, alpha=0.1)
            end
            cnt += 1
        end
        #display(p)
        framecounter += 1
        name = @sprintf("%05d.png",framecounter)
        path = "./rewireWhenEqual=$(rewireWhenEqual)/photos/"
        mkpath(path)
        Plots.savefig(p,  joinpath(path,name))
        #scatter!([tuple(from.e.q...), tuple(to.e.q...)]; alpha=0.1, color=:red)
        start_time = time()
    end
end

function main()
    (dept, algo) = getGraph()
    #h nmprintln("loading successful")
    setup!(algo, dept, 0.0)
    cnt = 0
    while (!step!(algo))
        cnt = cnt + 1
        # for (x, y) in algo.tree
        #     println(y)
        # end
        #sleep(3)
    end
    # println("finished")
    # # for (x, y) in algo.tree
    # #     println(y)
    # # end
    #
    global to
    println(to)
    #assert(isInTree(algo, algo.goal_node[1]))
    println(getNode(algo, algo.goal_node[1]))
end

function timerInit()
    global to
    to = TimerOutput()
end
# main()
