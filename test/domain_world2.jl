function make_domain_world2(parameters)
  #=let=# p = parameters
    @setparam p robot_width = nothing
    @setparam p frustum_depth = nothing
    @setparam p hallway_inner_width = .5 * sqrt(2) * robot_width + .5 * robot_width
    @setparam p wall_width = robot_width/20
    @setparam p hallway_length = frustum_depth + robot_width # so that the robot has to enter the hallway to see it.
    @setparam p key_hallway_length = hallway_length
    @setparam p peephole_size = hallway_inner_width/2
    @setparam p peephole_wall = (hallway_inner_width - peephole_size) / 2
  #end


  function wall_maker(p1, p2, width=wall_width)
    # specify p1 and p2, the medial line segment of the wall.
    w = width/2
    rot90 = [0 -1; 1 0]
    x = normalize(p2 - p1)
    y = normalize(rot90 * x)
    return [
      p1 - x * w - y * w,
      p1 - x * w + y * w,
      p2 + x * w + y * w,
      p2 + x * w - y * w
    ]
  end

  obstacles_points = [
    wall_maker(Point(0.0, 0.0), Point(hallway_length, 0.0), wall_width),
    wall_maker(Point(peephole_wall + peephole_size, hallway_inner_width + wall_width), Point(hallway_length, hallway_inner_width + wall_width)),
    wall_maker(Point(0.0, hallway_inner_width + wall_width), Point(peephole_wall, hallway_inner_width + wall_width)),
    wall_maker(Point(0.0, 0.0), Point(0.0, hallway_inner_width + wall_width + key_hallway_length)),
    wall_maker(Point(hallway_inner_width + wall_width, hallway_inner_width + wall_width), Point(hallway_inner_width + wall_width, hallway_inner_width + wall_width + key_hallway_length))
  ]

  polys_obstacles__Fworld = UnionOfConvex(
    [
      polyhedron(
        vrep(VAMP.array([Point(p...) for p in obstacle_points]))) for obstacle_points in obstacles_points])

  VAMP.compute!.(polys_obstacles__Fworld.pieces)

  function cspace_collision(q_se2)
    x = q_se2.e.q[1]
    y = q_se2.e.q[2]
    θ = q_se2.θ.q.θ

    in_hallway = (0.0 ≤ x ≤ hallway_inner_width) &&
      (hallway_inner_width ≤ y ≤ hallway_inner_width + key_hallway_length + robot_width/2)

    rightway = deg2rad(0+45) ≤ mod2pi(θ) ≤ deg2rad(180-45)

    return in_hallway && !rightway
  end

  @setparam parameters polys_obstacles__Fworld
  @setparam parameters cspace_collision

  @setparam parameters q_start = SE2_MPState(3.0, 3.0, 0.0)
  @setparam parameters goal = PointEpsilonGoal(SE2_MPState(0.625, 2.5, deg2rad(90.0)), 0.01)

  # hardcoded subgoals
  @setparam parameters subgoals = [
    SE2_MPState(2.75,0.625,deg2rad(180.0)),
    SE2_MPState(0.625,0.625,deg2rad(90.0)),
    SE2_MPState(0.625,5.375,deg2rad(270.0)),
    SE2_MPState(0.625,2.5,deg2rad(90.0))
  ]

  @setparam parameters starting_visibility__Fworld = [
    # circle around starting pose
    DifferenceGeneral(
      ArcRegion(0.8 * robot_width, -1.0, Point(q_start.e.q), Vec(1.0, 0.0)),
      UnionOfConvex(POLY[])
    )]

  return nothing
end
