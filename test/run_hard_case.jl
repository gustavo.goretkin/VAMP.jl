include("experiment_setup.jl")

results_root = "vamp_out/experiments"
mkpath(results_root)

# run_many_experiments(make_all_setparams(), results_root)

result_dir = joinpath(results_root, "the_hard_one")
mkdir(result_dir)
run_one_experiment(
  #Dict(:all_field_of_view=>200.0,:domain1_goal=>:easy,:domain_id=>:world1,:algorithm_id=>:two_phase_vavp),
  Dict(:all_field_of_view=>200.0,:domain_id=>:world2,:algorithm_id=>:two_phase_vavp),
  result_dir
)