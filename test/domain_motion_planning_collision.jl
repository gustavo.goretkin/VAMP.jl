import EnhancedGJK

# TODO not necessarily correct if T isn't closed under sqrt. But correct at least for AbstractFloatingPoint types
_distance_type(::EnhancedGJK.GJKResult{M, N, T}) where {M, N, T} = T

function _separation_distance(gjk_result)
  if gjk_result.in_collision
      return zero(_distance_type(gjk_result))
  else
      return EnhancedGJK.separation_distance(gjk_result)
  end
end


function make_domain_motion_planning_collision(parameters)
  @setparam parameters polys_robot__Frobot = nothing
  @setparam parameters poly_frustum__Fcamera = nothing
  @setparam parameters cspace_collision = nothing
  @setparam parameters polys_obstacles__Fworld = nothing


  to_flexible_convex_hull(uoc::UnionOfConvex) = [FlexibleConvexHull(VAMP.convexhull_andrew( map(x->Point(x...), points(poly)) )) for poly in uoc.pieces]
  #GJK geometry
  fch_obstacles__Fworld = to_flexible_convex_hull(polys_obstacles__Fworld)
  fch_robot__Frobot = to_flexible_convex_hull(polys_robot__Frobot)

  function is_collision_free(state)
    if cspace_collision(state) # obstacles that are easy to specify in C-space
      return false
    end

    tx__Fworld__Frobot = VAMP.get_tx__Fworld__Frobot(state)

    for robot_piece in fch_robot__Frobot
      for world_peace in fch_obstacles__Fworld # ;)
        if _separation_distance(EnhancedGJK.gjk(robot_piece, world_peace, tx__Fworld__Frobot)) <= 100eps()
          return false
        end
      end
    end
    return true
  end

  function is_path_free(state1, state2)
    # should assume both endpoints have been collision-checked
    ϵ = 0.01
    d = distance(state1,state2)
    if d < ϵ
      return true
    end
    k =  Int(ceil(log2(d/ϵ)))

    a = action(state2, state1)
    @assert k < 16
    for i = 1:(2^k)
        α = parse(Int, reverse(bits(Int16(i)))[1:k], 2) / 2^k #weird binary subdivision.
        check = state(state1, fragment(α, a))
        if !is_collision_free(check)
          return false
        end
    end

    return true
  end

  @setparam parameters is_collision_free
  @setparam parameters is_path_free

  obstacles_bbox = BoundingBox(Point(NaN, NaN))

  for poly in polys_obstacles__Fworld.pieces
    for p in points(poly)
      obstacles_bbox = union(obstacles_bbox, BoundingBox(Point(p...)))
    end
  end

  @setparam parameters boundingbox_obstacles__Fworld = obstacles_bbox
  @setparam parameters robot_width = nothing

  @setparam parameters boundingbox_workspace__Fworld = BoundingBox(
    minlimit(boundingbox_obstacles__Fworld) .- 2robot_width,
    maxlimit(boundingbox_obstacles__Fworld) .+ 2robot_width
  )

  # generate discretization of workspace for checking coverage

  @setparam parameters workspace_points_Δl = 0.125

  boundingbox_interior_workspace__Fworld = BoundingBox(
    minlimit(boundingbox_workspace__Fworld) .+ workspace_points_Δl / 2,
    maxlimit(boundingbox_workspace__Fworld) .- workspace_points_Δl / 2
  )

  workspace_points__Fworld = Point{2, Float64}[]

  let Δl = workspace_points_Δl, bb =  boundingbox_interior_workspace__Fworld
    for x = minlimit(bb)[1]:Δl:maxlimit(bb)[1], y = minlimit(bb)[2]:Δl:maxlimit(bb)[2]
      push!(workspace_points__Fworld, Point(x,y))
    end
  end


  workspace_points_raster_spec__Fworld = let Δl = workspace_points_Δl, bb =  boundingbox_interior_workspace__Fworld
    axes = [minlimit(bb)[i]:Δl:maxlimit(bb)[i] for i=[1, 2]]
    window = length.(axes)
    box =  BoundingBox(minimum.(axes), maximum.(axes))

    VAMP.RasterVolumeSpec(box, tuple(window...))
  end

  @setparam parameters workspace_points_raster_spec__Fworld

  # remove points that lie inside obstacles, TODO should remove points that are eps close to boundaries too.
  workspace_points__Fworld = collect(difference(
    Set(workspace_points__Fworld), polys_obstacles__Fworld.pieces))

  @setparam parameters workspace_points__Fworld
end
