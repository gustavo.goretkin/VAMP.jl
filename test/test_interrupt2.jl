
import PyPlot
#PyPlot.plt[:ion]()
PyPlot.plt[:close]("all")
axi = PyPlot.plt[:figure]()[:gca]()
axi[:set_title]("search debug")
axi[:set_aspect](1.0)


axb = PyPlot.plt[:figure]()[:gca]()
halt_button = PyPlot.matplotlib[:widgets][:Button](axb, "Halt Search")
halt = false
halt_button[:on_clicked]() do event
  println("Received halt button click")
  global halt = true
end


#PyPlot.plt[:show]()



while !halt
  axi[:clear]()
  r = rand(10)
  axi[:plot](r ./ maximum(r))
  println("sleep 1.0")
  sleep(1.0)
  println("Done sleep.")
end
