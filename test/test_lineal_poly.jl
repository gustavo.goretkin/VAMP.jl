using Plots
using Polyhedra

include("../src/plot.jl")

# make an unbounded polygon, that can contain a line
shr = hrep(
        -float.([  1 1;
                  -1 -1;]),
        [2, 2,])

plot(;xlim=(-5,5), ylim=(-5,5), aspect_ratio=1, legend=false)
for hs in allhalfspaces(shr)
  halfspace!(hs.a..., hs.β)
end

display(current())
