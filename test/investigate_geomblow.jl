# difference is making thousands of pieces.
using VAMP

using FileIO
using Printf

l = load("difference_329_2018-01-02T03:29:46.147.jld2")

A = l["A"]
B = l["B"]

plt = plot(;aspectratio=1, xlim=(-6,6), ylim=(-6,6))

for p in A.pieces
  plot_polyhedron!(plt, p; color=:purple, alpha=0.5)
end


for d in B
  for p in d.positive.pieces
    plot_polyhedron!(plt, p; color=:yellow, alpha=0.1)
  end

  for p in d.negative.pieces
    plot_polyhedron!(plt, p; color=:black, alpha=0.1)
  end
end

display(plt)





result = copy(A.pieces)
frame_i = 1
for B_i in B
  plt = plot(;aspectratio=1, xlim=(-6,6), ylim=(-6,6))
  for p in B_i.positive.pieces
    plot_polyhedron!(plt, p; color=:yellow, alpha=0.1)
  end

  for p in B_i.negative.pieces
    plot_polyhedron!(plt, p; color=:black, alpha=0.1)
  end

  new_result = []
  @show length(result)


  for p in result
    plot_polyhedron!(plt, p; color=:purple, alpha=0.5)
  end
  display(plt)
  frame_str = @sprintf("%05d", frame_i)
  savefig(plt, "animation/diff_$frame_str.png")
  frame_i += 1

  for p in result
    append!(new_result, difference(p, B_i).pieces)
  end
  result = new_result
end




for p in difference(A.pieces[1], B[1]).pieces
  plot_polyhedron!(plt, p; color=:purple, alpha=0.5)
end
