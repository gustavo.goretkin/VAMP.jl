p = polyhedron(vrep(VAMP.array([Point(rand(), rand()) for _ in 1:20])))
removevredundancy!(p)

# check roundtrippability of hrep and vrep

v1 = collect(points(p))
v2 = collect(points(vrep(polyhedron(hrep(p)))))

@test isapprox(Set(v1), Set(v2))

h1 = collect(allhalfspaces(p))
h2 = collect(allhalfspaces(hrep(polyhedron(vrep(p)))))

@test isapprox(Set(h1), Set(h2))


m = AffineMap(rand(2,2), rand(2))
tp = transform(m, p)
@test !isnothing(p.hrep)
@test !isnothing(p.vrep)
@test !isnothing(tp.hrep)
@test !isnothing(tp.vrep)

vtp = transform(m, polyhedron(vrep(p)))
htp = transform(m, polyhedron(hrep(p)))

@test isnothing(vtp.hrep)
@test !isnothing(vtp.vrep)
@test !isnothing(htp.hrep)
@test isnothing(htp.vrep)


v1 = collect(points(tp))
v2 = collect(points(vtp))
v3 = collect(points(htp))

@test isapprox(Set(v1), Set(v2))
@test isapprox(Set(v1), Set(v3))
@test isapprox(Set(v2), Set(v3))

h1 = collect(allhalfspaces(tp))
h2 = collect(allhalfspaces(htp))
h3 = collect(allhalfspaces(vtp))

@test isapprox(Set(h1), Set(h2))
@test isapprox(Set(h1), Set(h3))
@test isapprox(Set(h2), Set(h3))
