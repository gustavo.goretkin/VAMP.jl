# compare two sets approximately

function approxin(needle, haystack; approx_kwargs...)
  for stalk in haystack
    if isapprox(needle, stalk; approx_kwargs...)
      return true
    end
  end

  return false
end

import Base.isapprox

"A ≈ B iff ∀a∈A approxin(a, B) and ∀b∈B approxin(b, A).
A ≈ B does not imply length(A)==length(B)
(A and B may have different cardinalities and still be approximately equal)"
function isapprox(A::Set, B::Set; approx_kwargs...)
  # we only use the iteration behavior of A and B, but we want to mark that the order is not important,
  # so it's wrapped in a set. TODO is there another way to design it without using a different function or kwargs?
  for a in A
    if !approxin(a, B; approx_kwargs...)
      return false
    end
  end

  for b in B
    if !approxin(b, A; approx_kwargs...)
      return false
    end
  end

  return true
end
