include("../perf/figure_maker.jl")
root = "vamp_out/experiments/single"
file_output_path = "vamp_out/experiments/single/vis"
do_figure_maker_path(root, file_output_path; view_plan_kind = :none, plot_all_frames=false)