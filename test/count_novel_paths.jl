include("../src/util.jl")

agenda = [[(1, 1)]]

function succ!(children, s::T) where {T}
  x, y = s
  for t = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
    if all((1,1) .≤ t .≤ (3,3)) # 4 by 4 chokes. 3 by 3 chokes without goal testing
      push!(children, t)
    end
  end
end

children = eltype(eltype(agenda))[]
closed = []
goals = []

lasttime = 0.0
while length(agenda) > 0
  if time() - lasttime > 0.5
    @show length(agenda)
    lasttime = time()
  end
  node = pop!(agenda)
  push!(closed, node)

  if last(node) == (2,2) # goal test
    push!(goals, node)
    continue
  end

  empty!(children)
  succ!(children, last(node))
  for child in children
    if visit_has_novelty(node, child)
      push!(agenda, [node..., child])
    end
  end
end

_j, i = findmax(length.(goals))
goals[i]
