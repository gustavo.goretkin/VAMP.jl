include("experiment_setup.jl")

function do_just_move_search(;fov=50.0, move_safe_preference=0.05)
  setparam = Dict(:all_field_of_view=>fov,:domain1_goal=>:hard,:domain_id=>:world1,:algorithm_id=>:two_phase_vavp)

  parameters = make_complete_parameters(setparam)

  tpi = VAMP.TwoPhaseInstance(
    vamp_pieces=make_vamp_pieces(parameters),
    domain_parameters=parameters,
    parameters = Dict(:execute_move_prefix=>false, :move_safe_preference=>move_safe_preference #=,:do_vavp=>false=#)
    )

  tpa = VAMP.TwoPhaseAlgorithm(instance=tpi)
  setup!(tpa)

  @unpack mp = parameters
  @unpack starting_visibility__Fworld = parameters
  @unpack get_visible_region_cached__Fworld = tpi.vamp_pieces
  @unpack SearchState_t = parameters

  move_alg = tpa.move_alg

  move_start_state = SearchState_t(
    mp.q_start,
    [mp.q_start],
    vcat(
      VAMP.make_packed_view_volume(UnionOfGeometry(starting_visibility__Fworld)),
      get_visible_region_cached__Fworld(mp.q_start))
  )

  move_search_result = move_search!(move_alg, move_start_state)

  move_path = reverse(follow_path(move_alg.closed_parents, move_alg.goal_node[1]))

  q_path = unlift_q(move_path)

  return (setparam=setparam, q_path=q_path)
end

function do_full_tpa_search(;fov=50.0, move_safe_preference=0.05)
  setparam = Dict(:all_field_of_view=>fov,:domain1_goal=>:hard,:domain_id=>:world1,:algorithm_id=>:two_phase_vavp)

  parameters = make_complete_parameters(setparam)

  tpi = VAMP.TwoPhaseInstance(
    vamp_pieces=make_vamp_pieces(parameters),
    domain_parameters=parameters,
    parameters = Dict(:execute_move_prefix=>false, :move_safe_preference=>move_safe_preference, :do_vavp=>true)
    )

  tpa = VAMP.TwoPhaseAlgorithm(instance=tpi)
  setup!(tpa)

  while true
    if !step!(tpa).keepgoing
        break
    end
  end

  q_path = vcat([unlift_q(segment.path) for segment in tpa.path_segments]...)

  return (setparam=setparam, q_path=q_path)
end

include("../perf/figure_maker.jl")

function do_ffmpeg(filepath)
  frames_filepath = joinpath(filepath, "frames")
  let frames = joinpath(frames_filepath, "png"), output_no_ext = joinpath(filepath , "from_png")
    run(`ffmpeg -i $(frames)/%04d.png -c:v qtrle $(output_no_ext).mov`)
    run(`ffmpeg -i $(frames)/%04d.png -tune animation -pix_fmt yuv420p $(output_no_ext).mp4`)
  end
end

function do_movie_from_result(result, filepath)
  q_path = result.q_path

  parameters = make_complete_parameters(result.setparam)
  v_path = VAMP.get_visible_region_geos_path__Fworld(parameters, q_path)
  viol_path = VAMP.get_incremental_violations__Fworld(parameters, q_path; visibility_path__Fworld=v_path)

  frames_filepath = joinpath(filepath, "frames")
  plot_frames_viol(parameters, q_path, v_path, viol_path, frames_filepath)

  do_ffmpeg(filepath)
end


result_narrow_lo_penalty = do_just_move_search(;fov=50.0, move_safe_preference=0.001)
do_movie_from_result(result_narrow_lo_penalty, "vamp_out/v-safe_examples/narrow_lo_penalty")

result_narrow_hi_penalty = do_just_move_search(;fov=50.0, move_safe_preference=0.05)
do_movie_from_result(result_narrow_hi_penalty, "vamp_out/v-safe_examples/narrow_hi_penalty")

result_narrow_full = do_full_tpa_search(;fov=50.0, move_safe_preference=0.05)
do_movie_from_result(result_narrow_full, "vamp_out/v-safe_examples/narrow_hi_penalty_full")

result_narrow_lo_penalty_full = do_full_tpa_search(;fov=50.0, move_safe_preference=0.005)
do_movie_from_result(result_narrow_lo_penalty_full, "vamp_out/v-safe_examples/narrow_lo_penalty_full")

result_wide_full = do_full_tpa_search(;fov=200.0, move_safe_preference=0.005)
do_movie_from_result(result_wide_full, "vamp_out/v-safe_examples/wide_lo_penalty_full")


#=
allpaths = [
  "vamp_out/v-safe_examples/narrow_lo_penalty",
  "vamp_out/v-safe_examples/narrow_hi_penalty",
  "vamp_out/v-safe_examples/narrow_hi_penalty_full",
  "vamp_out/v-safe_examples/narrow_lo_penalty_full",
  "vamp_out/v-safe_examples/wide_lo_penalty_full"
]

map(do_ffmpeg, allpaths)
=#

import DataStructures: DefaultOrderedDict, OrderedSet
function find_groupby(data; by=identity)
  T = eltype(data) # TODO infer through by
  IT = Int64 # TODO index type.
  d = DefaultOrderedDict{T, OrderedSet{IT}}(OrderedSet{IT})
  for (i, x) in enumerate(data)
    push!(d[by(x)], i)
  end
  return d
end


function find_good_revisit_example(q_path)
  # find the pair of indices (i1, i2) into q_path such that q_path[i1] == q_path[i2] and (i2 - i1) is maximized
  revisits = sort(
      collect(find_groupby(q_path)),
      by = p -> maximum(last(p)) - minimum(last(p)))

  return last(revisits[end])
end

function do_same_configuration_stills(result, filepath)
  q_path = result.q_path

  parameters = make_complete_parameters(result.setparam)
  v_path = VAMP.get_visible_region_geos_path__Fworld(parameters, q_path)
  viol_path = VAMP.get_incremental_violations__Fworld(parameters, q_path; visibility_path__Fworld=v_path)

  path_idxs = collect(find_good_revisit_example(q_path))
  if length(path_idxs) == 1
    @warn("no configurations revisited")
    return
  end
  frames_filepath = joinpath(filepath, "revisit")
  plot_frames_viol(parameters, q_path, v_path, viol_path, frames_filepath; path_i_frames=path_idxs)
end

do_same_configuration_stills(result_narrow_lo_penalty, "vamp_out/v-safe_examples/narrow_lo_penalty")
do_same_configuration_stills(result_narrow_hi_penalty, "vamp_out/v-safe_examples/narrow_hi_penalty")
do_same_configuration_stills(result_narrow_full, "vamp_out/v-safe_examples/narrow_hi_penalty_full")
do_same_configuration_stills(result_narrow_lo_penalty_full, "vamp_out/v-safe_examples/narrow_lo_penalty_full")
do_same_configuration_stills(result_wide_full, "vamp_out/v-safe_examples/wide_lo_penalty_full")