include("experiment_setup.jl")

results_root = "vamp_out/experiments"
mkpath(results_root)

run_many_experiments(make_all_setparams(), results_root)
