using VAMP
using Plots

robot_width = 1.0

polys_robot__Frobot = let half = robot_width/2, width=robot_width
  points_robot = [
    Point(-half, -half),
    Point(-half + 0.8width, -half),
    Point(half, -half + 0.2width),
    Point(half, -half + 0.8width),
    Point(half - 0.2width, half),
    Point(-half, half)
  ]
  UnionOfConvex([polyhedron(vrep(VAMP.array(points_robot)))])
end

frustum_depth = 2.5 * robot_width
frustum_width = 2.0 * robot_width

poly_frustum__Fcamera = polyhedron(vrep(VAMP.array([
  Point(0.0, 0.0), Point(frustum_depth, frustum_width/2), Point(frustum_depth, -frustum_width/2)
])))



@inline function get_tx(x, y, θ)
  t = Translation(x, y)
  r = convert(LinearMap, VAMP.Rot(θ)) # already a Rot
  return compose(t, r)
end


# take random frustum chunks out. of the robot polygon
POLY = typeof(poly_frustum__Fcamera)

remaining = polys_robot__Frobot
# remaining = UnionOfConvex(fill(polys_robot__Frobot.pieces[1], 10)) # stress for profiling
r = 3
while length(remaining.pieces) > 0
  plt = plot(;aspect_ratio=1.0, xlim=(-r,r), ylim=(-r,r))

  p_frustum = transform(
    get_tx(rand()-0.5, rand()-0.5, 2*π*rand()),
    poly_frustum__Fcamera)

  for piece in remaining.pieces
    plot_polyhedron!(plt, piece; color=:green)
    if overlaps(VAMP.BoundingBox(p_frustum), VAMP.BoundingBox(piece))
      plot_polyhedron!(plt, piece; color=:red)
    end
  end
  plot_polyhedron!(plt, p_frustum; color=:yellow, alpha=0.5)
  plot!(plt,
    Shape(map(x->tuple(x...), VAMP.ordered_vertices(VAMP.BoundingBox(p_frustum))));
    color=nothing,
    linecolor=:black
    )

  display(plt)
  sleep(0.3)

  remaining = difference(remaining,
    [Difference(UnionOfConvex([p_frustum]), UnionOfConvex(POLY[]))])

  plot_polyhedron!(plt, p_frustum; color=:yellow, alpha=0.5)
  for p in remaining.pieces
    plot_polyhedron!(plt, p; color=:brown)
  end

  plot!(plt; aspect_ratio=1.0)
  display(plt)
  sleep(0.3)

  @show length(remaining.pieces)
end
