# pretty sure this file should be deleted in favor of do_algs
# paths for two-phase are not looking optimal anymore. Even for the HallwayHard domain, they dip low before going toward the hallway. Let us investigate why.
import Plots
Plots.pyplot() # GR, I think, causes SystemError too many open files.
import DataStructures
import TimerOutputs: @timeit
import Parameters: @pack!, @unpack, @with_kw

using VAMP

results_path = "outs/junk"
mkpath(results_path)

println("`using VAMP` completed")

include("domain_robot.jl")
include("domain_world.jl")
include("domain_motion_planning_collision.jl")
include("do_algs.jl")
println("`include(domain_*)` completed")


parameters = Dict{Symbol, Any}()
make_robot(parameters)
make_robot_vis_target(parameters)
make_domain_world(parameters)
make_domain_motion_planning_collision(parameters)

VAMP.do_parameters(parameters)

search_parameters=Dict(:do_vavp=>true)

mkpath(joinpath(results_path, "summary"))
mkpath(joinpath(results_path, "calls"))

vamp_pieces = make_vamp_pieces(parameters)

@unpack goal = parameters

tpi = VAMP.TwoPhaseInstance(vamp_pieces=vamp_pieces, domain_parameters=parameters)
merge!(tpi.parameters, search_parameters)
@show tpi.parameters
tpa = VAMP.TwoPhaseAlgorithm(instance=tpi)
setup!(tpa)

while true
  VAMP.make_summary_plots(tpa.path_segments, parameters; q_goal=rand(goal), file_prefix=joinpath(results_path, "summary/"))
  if !step!(tpa).keepgoing
      break
  end

  if tpa.state == :explore_violation
    let move_process_result = tpa.move_process_result
      @unpack polys_obstacles__Fworld = parameters
      @unpack polys_robot__Frobot = parameters

      plt = Plots.plot(;aspect_ratio=1)

      #= let path = move_process_result.move_path_prefix
        vrs = VAMP.LibGEOSInterface.to_geos_with_no_holes(
          UnionOfGeometry(
              map(q->VAMP.get_visible_region__Fworld(visibility_instance, q), path[end].q_path)),
          [LibGEOS.Polygon(UnionOfGeometry(starting_visibility__Fworld))]) #= if starting visibilty has holes, this will not work =#

        for vr in vrs; Plots.plot(plt, vr; color=:yellow, linealpha=0.0); end

      end=#
      plot_polyhedron!(plt, polys_obstacles__Fworld; linealpha=0.0, color=:grey)

      for (kind, path) in zip([:safe, :unsafe], [move_process_result.move_path_prefix, move_process_result.move_path_unsafe])
        for q in unlift_q(path)
          p = VAMP.get_robot__Fworld(polys_robot__Frobot, q)

          plot_polyhedron!(plt, p, fillalpha=0.0, linecolor=Dict(:safe=>:grey, :unsafe=>:red)[kind], linealpha=0.5)
        end
      end

      scatter_points!(plt, tpa.coverage_goal; color=:red)
      Plots.savefig(plt, joinpath(results_path, "calls", "move_call_$(length(tpa.path_segments)).svg"))
      display(plt)
    end
  end
end
println("two-phase search is done.")
