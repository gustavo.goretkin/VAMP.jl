using VAMP

include("domain_robot.jl")
include("domain_world.jl")
include("domain_motion_planning_collision.jl")

goal = PointEpsilonGoal(
  SE2_MPState(1.0,0.6,0.0),
  0.01
)

mp = HolonomicMotionPlanningInstance(
  SE2_MPState(-1.0,-1.0,0.0),
  goal,
  state->is_collision_free(state),
  (state1,state2)->is_path_free(state1,state2)
)

instance = RRTInstance(
  mp,
  .1,
  ()->if (rand() < .1) rand(mp.q_goal) else SE2_MPState(6*(rand()-.5), 6*(rand()-.5), 2π*rand()) end
)

algorithm = RRTAlgorithm(instance)

using Plots

plt = plot(;aspect_ratio=1.0)

for obstacle in polys_obstacles__Fworld.pieces
    plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
end

for p in VAMP.get_robot__Fworld(polys_robot__Frobot, algorithm.instance.mp.q_start).pieces
  plot_polyhedron!(plt, p; color=:green)
end

for p in VAMP.get_robot__Fworld(polys_robot__Frobot, rand(algorithm.instance.mp.q_goal)).pieces
  plot_polyhedron!(plt, p; color=:red)
end

display(plt)


println("start search")
while true
  setup!(algorithm, 15_000)
  steps!(algorithm, 15_000)

  if length(algorithm.reached_goals) > 0
    println("found goal")
    break
  end
  println("restarting search")
end

rrt_path = reverse(getpath(algorithm.tree, algorithm.tree[algorithm.reached_goals[1]]))
solvepath = reshape(reinterpret(Float64, rrt_path),  3, length(rrt_path))';

import Base.convert
convert(::Type{VAMP.MoveAction}, state::SE2_MPState) = VAMP.MoveAction([state.e.q..., state.θ.q.θ])

move_path = [convert(VAMP.MoveAction, state) for state in rrt_path]
stuff__Fworld = VAMP.worldgrounded_path(move_path, polys_robot__Frobot, poly_frustum__Fcamera, polys_obstacles__Fworld)
VAMP.plot_path!(plt, stuff__Fworld)

plot!(plt, solvepath[:,1], solvepath[:,2] ; aspect_ratio = 1, linewidth=6.0, color=:black, marker=:o)

shortcut_alg = shortcutting_algorithm(rrt_path, is_path_free)

shortcut_path_i = shortcut(shortcut_alg)
shortcut_path = rrt_path[shortcut_path_i]

_path = reshape(reinterpret(Float64, shortcut_path),  3, length(shortcut_path))';
plot!(plt, _path[:,1], _path[:,2] ; aspect_ratio = 1, linewidth=3.0, color=:grey, marker=:o)
