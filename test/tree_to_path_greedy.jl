if !isdefined(Main, :SCRIPT_RERUN_FLAG_TREE_TO_PATH_GREEDY)
using VAMP
using FileIO
using CUDAnative
using CuArrays
using CUDAdrv
using Printf
const CuArray = CuArrays.CuArray

d = load("migrated_retrovamprrt_goals_2018-02-08T13:03:14.589.jld2")
g = d["goal_found_ones"][1]
visibility_instance = g.visibility_instance
tree = g.rrt.tree

starting_visibility__Fworld = g.visible_region[2] # FIXME, need to treat starting visibility specially


println("Compute all visible regions")
visibility_cache = Dict([Pair(i, VAMP.get_visible_region__Fworld(visibility_instance, tree[i].state)) for i in LinearIndices(tree)])
println("Done")

function which_cover(pieces__Fworld)
  return [length(difference(pieces__Fworld, visibility_cache[i])) for i in LinearIndices(tree)]
end
SCRIPT_RERUN_FLAG_TREE_TO_PATH_GREEDY = true
end

tree_goal_index = g.rrt.reached_goals[1]
tree_start_index = 1

visited_indices = Set([tree_start_index])
agenda = [tree_goal_index]
path = [(tree_start_index, tree_start_index)]
cumulative_visibility__Fworld = [starting_visibility__Fworld, VAMP.get_visible_region__Fworld(visibility_instance, tree[tree_start_index].state)]
path_debug = []
path_debug_goal = Dict{Any, Any}()
path_debug_uncovered = Dict{Any, Any}()

function find_first_new(visited, path)
  for i in LinearIndices(visited)
    if path[i] ∉ visited
      return i
    end
  end
  return nothing
end

while length(agenda) > 0
  this_goal = pop!(agenda)
  sofar_start = first(path[end])
  sofar = last(path[end])
  lca, p1, p2 = VAMP.find_lowest_common_ancestor_and_paths(tree, sofar, this_goal)
  # try to make a little progress towards this_goal
  i_tentative = p2[find_first_new(visited_indices, p2)]
  q_tentative = tree[i_tentative].state
  uncovered__Fworld = VAMP.get_uncovered_pieces__Fworld(visibility_instance, q_tentative, cumulative_visibility__Fworld)
  if length(uncovered__Fworld) == 0
    # visibly-safe extension
    if lca == sofar
      path[end] = (sofar_start, i_tentative)
    else
      push!(path, (lca, i_tentative))
    end
    push!(cumulative_visibility__Fworld, VAMP.get_visible_region__Fworld(visibility_instance, q_tentative))
    push!(visited_indices, i_tentative)
    if i_tentative != this_goal
      push!(agenda, this_goal)
    end
  else
    push!(agenda, this_goal) # still need to go here
    # need to make uncovered__Fworld visible (or more visible)
    coverage = which_cover(uncovered__Fworld)
    push!(path_debug, (length(path), :uncovered, copy(uncovered__Fworld)))
    path_debug_uncovered[(length(path), last(path[end]))] = deepcopy(uncovered__Fworld)
    #println("Hit deadend while trying to go after: $(this_goal).")
    #println("Path: $(path)")
    # out of all the nodes that best cover this new segment, which one is closest to where we are now?

    best_length = typemax(Int)
    best_coverage = typemax(Int)
    best_i = nothing
    for i in sortperm(coverage)
      if i in agenda || i in visited_indices
        continue  # we're already trying to go here, or we've already been here. need another goal that can achieve visibility
      end
      l = VAMP.length_path(tree, sofar, i)
      if coverage[i] > best_coverage
        break
      end
      if l < best_length && coverage[i] < best_coverage
        best_coverage = coverage[i]
        best_length = l
        best_i = i
      end
    end

    new_goal = best_i
    push!(path_debug, (length(path), :new_goal, new_goal))
    path_debug_goal[(length(path), last(path[end]))] = new_goal

    push!(agenda, new_goal)
    @show agenda

  end
end


# get true robot geometry (should store in the jld)
include("domain_robot.jl")



paired_compressed_tree_path = path
compressed_tree_path = vcat(map(x->[x...], paired_compressed_tree_path)...)

segmented_tree_path = []
segmented_tree_path_provenance = [] # indices to paired_compressed_tree_path

debug_uncovered = nothing
debug_goal = nothing

for i in 1:(length(compressed_tree_path)-1)
  # has repeated nodes, but taken care of later.
  segment = VAMP.path(tree, compressed_tree_path[i], compressed_tree_path[i+1])
  push!(segmented_tree_path, segment)
  push!(segmented_tree_path_provenance, (i+1)÷2)
end

segmented_tree_path_provenance_repeated = [[segmented_tree_path_provenance[j] for l in 1:length(segmented_tree_path[j])] for j in LinearIndices(segmented_tree_path)]

flat_tree_path = vcat(segmented_tree_path[1][1], map(x->x[2:end], segmented_tree_path)...)
flat_tree_path_provenance = vcat(segmented_tree_path_provenance_repeated[1][1], map(x->x[2:end], segmented_tree_path_provenance_repeated)...)

using Plots
pyplot()

unique_id = hex(rand(UInt))[1:6]
@show unique_id
flat_tree_path_ = flat_tree_path
for i = 1:length(flat_tree_path) # make animation
  println("make frame $i of $(length(flat_tree_path_))")
  flat_tree_path = flat_tree_path_[1:i]


qs = [tree[i].state for i in flat_tree_path]
xy = [tuple(tree[i].state.e.q...) for i in flat_tree_path]

# make labels for scatter plot (not that useful, impossible to read)
idict = Dict()
for i=1:length(flat_tree_path)
  ti = flat_tree_path[i]
  idict[ti] = get(idict, ti, "") * " $i,"
end

tolabel = unique(flat_tree_path)
xylabel = [tuple(tree[i].state.e.q...) for i in tolabel]
labels = [idict[i] for i in tolabel]


vvs = collect(VAMP.get_generator_visible_region__Fworld(g.visibility_instance, unique(qs)))
push!(vvs, starting_visibility__Fworld)
uvvs = UnionOfGeometry(vvs)
pvv = VAMP.make_packed_view_volume(UnionOfGeometry(uvvs.pieces[:]))


window = (1024, 1024)
out = CuArray(zeros(Int16, window...))
spec = VAMP.RasterVolumeSpec(VAMP.BoundingBox([-4.0, -4.0], [4.0, 4.0]), window)
vout = VAMP.RasterVolume(spec, out)

gpu_pvv = VAMP.PackedViewVolumes(CuArray(pvv.arc_regions), CuArray(pvv.shadow_halfspaces))
VAMP.render!(vout, gpu_pvv)
synchronize()


# need to transfer GPU->CPU before plotting, otherwise "Bus Error 10"
plt = plot(aspect_ratio=1, dpi = 150, size = (1800, 1200))
cmap = cgrad([:yellow, :orange])
cmap.values[1] = eps()
prepend!(cmap.values, [0.0])
prepend!(cmap.colors, [RGBA{Float64}(1.0,1.0,1.0,1.0)])

heatmap!(plt, VAMP.coordinates(spec)..., (Array(vout.data))'; aspect_ratio=1, c=cmap)


for piece in g.visibility_instance.occluders__Fworld.pieces
  plot_polyhedron!(plt, piece, fillcolor=:brown)
end

plot!(plt, xy)
#scatter!(plt, xylabel, aspect_ratio=1, series_annotations=labels)

for p in VAMP.get_robot__Fworld(polys_robot__Frobot, qs[end]).pieces
  plot_polyhedron!(plt, p; color=:purple, alpha=0.6)
end

paired_i = flat_tree_path_provenance[i]
debug_key = (paired_i, flat_tree_path[end])
if haskey(path_debug_uncovered, debug_key)
  debug_uncovered = map(x->tuple(x...), collect(path_debug_uncovered[debug_key]))
end

if debug_uncovered != nothing
  scatter!(plt, debug_uncovered, fillcolor=:red, linecolor=:black)
end

if haskey(path_debug_goal, debug_key)
  this_goal = path_debug_goal[debug_key]

  q_this_goal = tree[this_goal].state

  debug_goal = q_this_goal
end

if debug_goal != nothing
  for p in VAMP.get_robot__Fworld(polys_robot__Frobot, debug_goal).pieces
    plot_polyhedron!(plt, p; color=:brown, fillalpha=0.0)
  end
end


frame_count = i
frame_str = @sprintf("%05d", frame_count)
filename = "animation/$(unique_id)_$(frame_str).png"

savefig(plt, filename)
# display(plt)

end
