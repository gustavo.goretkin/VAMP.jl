using Revise
using Plots

using GeometryTypes

max_distance = 0.05

vertices_start = [Point((rand(2) .- 0.5)...) for i = 1:3]
vertices = copy(vertices_start)
pairs = [[1,2], [1,3], [2,3]]

for pair in pairs
  vpair = vertices[pair]
  d = vpair[2] - vpair[1]
  l = norm(d)
  n = Int(ceil(l / max_distance))
  inbetween = vcat([(1 - (k/n)) * vpair[1] +  (k/n) * vpair[2] for k in 1:(n-1)])
  append!(vertices, inbetween)
end

using VAMP

PLOT3D = false


function simplexradius(V)
  N = length(V)
  r = 0.0 # TODO want eltype of eltype if you have array of static arrays
  for i=1:N, j=(i+1):N
    r = max(r, norm(V[i]-V[j]))
  end
  return r
end

function simplexcenter(V)
  return mean(V)
end

while true
  if PLOT3D
  vertices_lifted = [Point(p..., norm(p)^2) for p in vertices]
  vertices_unlifted = [Point(p..., 0.0) for p in vertices]
  plt = plot3d(;legend=nothing)
  scatter3d!(plt, map(x->tuple(x...), vertices_lifted))
  scatter3d!(plt, map(x->tuple(x...), vertices_unlifted))
  else
  plt = plot(;aspect_ratio=1, legend=nothing)
  scatter!(plt, map(x->tuple(x...), vertices))
  end
  display(plt)

  @show length(vertices)
  println("start computation")
  tris = VAMP.delaunay(vertices, true, false, true)
  println("end computation")

  for tri in tris
    tricycle = vertices[[tri...]][vcat(collect(1:end), 1)]
    if PLOT3D
    tricycle_lifted = vertices_lifted[[tri...]][vcat(collect(1:end), 1)]
    tricycle_unlifted = vertices_unlifted[[tri...]][vcat(collect(1:end), 1)]
    plot3d!(plt, map(x->tuple(x...), tricycle_lifted))
    plot3d!(plt, map(x->tuple(x...), tricycle_unlifted), alpha=0.5, linecolor=:brown)
    else
    plot!(plt, map(x->tuple(x...), tricycle))
    end
  end
  display(plt)

  new_points = []
  for tri in tris
    simplex = vertices[[tri...]]
    if simplexradius(simplex) > max_distance
      push!(new_points, simplexcenter(simplex))
    end
  end
  @show length(new_points)
  if length(new_points) == 0
    break
  end
  append!(vertices, new_points)
  scatter!(plt, map(x->tuple(x...), new_points), markersize=10.0)
  display(plt)
  sleep(0.5)

end
