using Test

include("approx_set.jl")
include("test_approx_set.jl")

using VAMP
# write your own tests here
include("transform_poly.jl")
include("a_star.jl")
include("param.jl")

#=
I guess these tests never worked.
@test VAMP.find_novel_prefix([1,2,3,4,3,2,1,2,3], Set([])) == 1:9
@test VAMP.find_novel_prefix([1,2,3,4,3,2,1,2,3], Set([1])) == 1:6
@test VAMP.find_novel_prefix([1,2,3,4,3,2,1,2,3], Set([2])) == 1:7
@test VAMP.find_novel_prefix([1,2,3,4,3,2,1,2,3], Set([3])) == 1:8
@test VAMP.find_novel_prefix([1,2,3,4,3,2,1,2,3], Set([1,2,3])) == 1:4
=#

include("lazy_search_pieces.jl")
test_lazy_search_pieces()
