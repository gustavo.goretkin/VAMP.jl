import Plots
Plots.pyplot()
import Parameters: @pack!, @unpack, @with_kw
using VAMP


println("`using VAMP` completed")

include("domain_robot.jl")
include("domain_world.jl")
include("domain_motion_planning_collision.jl") # Not needed in this test.
println("`include(domain_*)` completed")

parameters = Dict{Symbol, Any}()
#parameters[:frustum_depth] = 1.0
parameters[:boundingbox_workspace__Fworld] = BoundingBox([-2, -2], [10.0, 1.0])

make_robot(parameters)
make_robot_vis_target(parameters)
make_domain_world(parameters)
make_domain_motion_planning_collision(parameters)

VAMP.do_parameters(parameters)

vamp_pieces = make_vamp_pieces(parameters)

@unpack SE2_MPState_t = parameters
@unpack SearchState_t = parameters

@unpack polys_robot__Frobot = parameters
@unpack get_visible_region_cached__Fworld = vamp_pieces
@unpack vsafe_split_path = vamp_pieces
@unpack vsafe_violations = vamp_pieces
@unpack visibility_violations_path = vamp_pieces


@unpack visibility_instance = parameters

V(q_robot) = VAMP.get_visible_region__Fworld(visibility_instance, q_robot)
@unpack get_visible_region_cached__Fworld = vamp_pieces


waypoints = [
    SE2_MPState_t(0.0,-1.0,0.0),
    SE2_MPState_t(3.0, -1.0, 0.0),
    SE2_MPState_t(3.0, -1.0, deg2rad(180)),
    SE2_MPState_t(6.0, -1.0, deg2rad(180)),
    SE2_MPState_t(6.0, -1.0, deg2rad(0)),
    SE2_MPState_t(9.0, -1.0, deg2rad(0))]


path = vcat([[VAMP.state(q1, VAMP.fragment(α, VAMP.action(q2, q1))) for α = 0.0:0.04:1.0] for (q1, q2) in VAMP.iter2(waypoints)]...)

plt = Plots.plot(; aspect_ratio=1.0, reuse=false)




starting_visibility__Fworld = [
  # circle around starting pose
  DifferenceGeneral(
    ArcRegion(0.8 * parameters[:robot_width], -1.0, Point(waypoints[1].e.q), Vec(1.0, 0.0)),
    UnionOfConvex(POLY[])
  )]


function induce_visibility(initial_visibility, q_path)
  s1 = SearchState_t(q_path[1], [q_path[1]], VAMP.make_packed_view_volume(UnionOfGeometry(initial_visibility)))

  function succ(s1, q2)
    SearchState_t(q2, vcat(s1.q_path, q2), vcat(s1.visible_region, get_visible_region_cached__Fworld(q2)))
  end

  ss = [s1]

  for q in q_path[2:end]
    push!(ss, succ(ss[end], q))
  end
  return ss
end

s_path = induce_visibility(starting_visibility__Fworld, path)

view_bounding_box = BoundingBox([-2, -2], [12, 2])
spec = VAMP.RasterVolumeSpec(view_bounding_box, (256, 256))

VAMP.make_frame_visible_region!(plt, s_path[2].visible_region, spec)

for q in path
    plot_polyhedron!(plt, VAMP.get_robot__Fworld(polys_robot__Frobot, q), linecolor=:green, fillalpha=0.0, linestyle=:solid)
end

#uses vsafe_test, which uses mp collision check, which uses boundingbox_workspace__Fworld
safe, unsafe = vsafe_split_path(s_path);

for q in unlift_q(unsafe)
    plot_polyhedron!(plt, VAMP.get_robot__Fworld(polys_robot__Frobot, q), linecolor=:red, linealpha=0.5, fillalpha=0.0, linestyle=:solid, linewidth=2.0)
end

viol_all = visibility_violations_path(s_path)
viol_unsafe = visibility_violations_path(unsafe)
viol_safe = visibility_violations_path(safe)

scatter_points!(plt, viol_all)

# should be no points.
scatter_points!(plt, setdiff(viol_all, viol_unsafe))

display(plt)

@assert length(viol_safe) == 0
@assert length(viol_all) == length(viol_unsafe) # fails. looks like off-by-one error
