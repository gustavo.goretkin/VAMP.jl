using Polyhedra
using CDDLib



struct Cdd_SetFamily
  famsize::CDDLib.Cdd_bigrange
  setsize::CDDLib.Cdd_bigrange
  # `dd(f)_SetVector set` is the next field
  # dd(f)_SetVector is a typedef for *set_type, defined in setoper.h
  # set_type is a typedef for *unsigned long
  set::CDDLib.Cdd_SetVector
end

# pattern matched with https://github.com/JuliaPolyhedra/CDDLib.jl/blob/9ca656754071da773d514470044a9c85a4ac7a6e/src/polyhedra.jl#L76
function dd_compute_a_inc(poly::Ptr{CDDLib.Cdd_PolyhedraData{Cdouble}})
  CDDLib.@ddf_ccall ComputeAinc Void (Ptr{CDDLib.Cdd_PolyhedraData{Cdouble}},) poly
end

function dd_copy_incidence(poly::Ptr{CDDLib.Cdd_PolyhedraData{Cdouble}})
  CDDLib.@ddf_ccall CopyIncidence Ptr{Cdd_SetFamily} (Ptr{CDDLib.Cdd_PolyhedraData{Cdouble}},) poly
end

function dd_copy_input_incidence(poly::Ptr{CDDLib.Cdd_PolyhedraData{Cdouble}})
  CDDLib.@ddf_ccall CopyInputIncidence Ptr{Cdd_SetFamily} (Ptr{CDDLib.Cdd_PolyhedraData{Cdouble}},) poly
end

function unsafe_setfamily(F::Ptr{Cdd_SetFamily})
  F = unsafe_load(F)
  family = IntSet[]
  sizehint!(family, F.famsize)
  for i = 1:F.famsize
    set = unsafe_load(F.set, i)
    maxel = unsafe_load(set)
    intset = convert(IntSet, CDDLib.CDDSet(set, maxel ))
    @assert CDDLib.dd_set_card(set)==length(intset)
    push!(family, intset)
  end
  family
end

function unsafe_array(a::Ptr{T}, n) where T
  o = Array{T, 1}(n)
  for i=1:n
    o[i] = unsafe_load(a, i)
  end
  return o
end

function unsafe_array(a::Ptr{Ptr{T}}, n1, n2) where T
  o = Array{T, 2}(n1, n2)
  for i=1:n1, j=1:n2
    o[i, j] = unsafe_load(unsafe_load(a, i), j)
  end
  return o
end


# square pyramid
vertices = [
  -1.0 -1.0 0.0;
  +1.0 -1.0 0.0;
  -1.0 +1.0 0.0;
  +1.0 +1.0 0.0;
   0.0  0.0 1.0;
  ]

vertices = rand(2, 2)

in_cdd_v = polyhedron(SimpleVRepresentation(vertices), CDDLib.CDDLibrary(:float))
in_cdd_h = polyhedron(hrep(in_cdd_v), CDDLib.CDDLibrary(:float))

for in_cdd = [in_cdd_v, in_cdd_h]
  # force computation
  hrep(in_cdd);
  vrep(in_cdd);

  @show unsafe_load(in_cdd.poly.value.poly).AincGenerated
  dd_compute_a_inc(in_cdd.poly.value.poly)
  @show unsafe_load(in_cdd.poly.value.poly).AincGenerated

  poly = unsafe_load(in_cdd.poly.value.poly)

  println("H or V rep:")
  @show poly.representation == CDDLib.dd_Generator
  @show poly.representation == CDDLib.dd_Inequality

  #V = unsafe_load(CDDLib.dd_copygenerators(in_cdd.poly.value.poly))
  #H = unsafe_load(CDDLib.dd_copyinequalities(in_cdd.poly.value.poly))

  V = copygenerators(in_cdd.poly.value)
  H = copyinequalities(in_cdd.poly.value)

  VV = unsafe_load(V.matrix)
  HH = unsafe_load(H.matrix)
  Hmat = unsafe_array(HH.matrix, HH.rowsize, HH.colsize)
  Vmat = unsafe_array(VV.matrix, VV.rowsize, VV.colsize)
  @show Hmat
  @show Vmat
  incidence = unsafe_setfamily(dd_copy_incidence(in_cdd.poly.value.poly))
  #if CDD representation is H, indices(incidence) indexes into Hmat (input)
  #if CDD representation is V, indices(incidence) indexes into Vmat (input)
  #if CDD representation is H, incidence[i] elements indexes into Vmat (output)
  #if CDD representation is V, incidence[i] elements indexes into Hmat (output)
  @show incidence

  input_incidence = unsafe_setfamily(dd_copy_input_incidence(in_cdd.poly.value.poly))
  # input_incidence is the opposite
  @show input_incidence
end
