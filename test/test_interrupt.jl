using Gtk

b = GtkButton("stop search")
win = GtkWindow(b, "Callbacks")
showall(win)

interrupt = false

id = signal_connect(b, "clicked") do widget
    global interrupt = true
end

while !(interrupt)
  println(interrupt)
  println("step 1")
  sleep(0.3)
  println("step 2")
  sleep(0.4)
  println("step 3")
  sleep(0.3)
end
