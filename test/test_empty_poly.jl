using Polyhedra

# test how it handles redundant points and halfspaces
square_points = [-1.0 -1.0; -1.0 1.0; 1.0 -1.0; 1.0 1.0]

poly_sq = polyhedron(vrep(square_points))
poly_sq_right = polyhedron(vrep(10 .+ square_points))


#vcat(collect(hreps(poly_sq)), collect(hreps(poly_sq_big)))
# I don't see any way to construct Polyhedron from Vector of HalfSpaces.
# works
## SimpleHRepresentation{2, Float64}(hreps(poly_sq))

empty_poly = SimplePolyhedron{2,Float64}(intersect(hrep(poly_sq), hrep(poly_sq_right)))
removehredundancy!(empty_poly)
