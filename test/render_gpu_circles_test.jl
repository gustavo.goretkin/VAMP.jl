using StaticArrays
using CuArrays, CUDAnative


"Axis-aligned bounding box. Constructor doesn't check all(minimum .<= maximum)"
struct BoundingBox{N,T}
  minimum::SVector{N,T}
  maximum::SVector{N,T}
end

BoundingBox(mi, ma) = BoundingBox(promote(SVector(mi...), SVector(ma...))...)

struct RasterVolumeSpec{N, T}
  limits::BoundingBox{N, T}
  size::NTuple{N, Int64}
end

struct RasterVolume{S, A}
  spec::S
  data::A
end

function coordinates(v::RasterVolumeSpec{N}) where {N}
  return tuple([range(v.limits.minimum[i], stop=v.limits.maximum[i], length=v.size[i]) for i=1:N]...)
end

@inline function coordinates(v::RasterVolumeSpec{N}, I) where {N}
  extent__c = v.limits.maximum - v.limits.minimum
  x__f = (SVector(I...) .- 1) ./ (SVector(v.size...) .- 1)
  x__c = x__f .* extent__c + v.limits.minimum
  return x__c
end

struct Sphere{C, R}
  center::C
  radius::R
end

circles = [Sphere(SVector(rand(), rand()), rand()/10) for i=1:1000]


window = (1024, 1024)
out = CuArray(zeros(Int16, window...))
cu_circles = CuArray(circles)
spec = RasterVolumeSpec(BoundingBox([0.0, 0.0], [1.0, 1.0]), window)
vout = RasterVolume(spec, out)


function kernel_render(out, spec, circles)
  #i = (blockIdx().x-1) * blockDim().x + threadIdx().x
  I = CuArrays.@cuindex(out)
  xy = coordinates(spec, I)
  for circle in circles
    out[I...] += ifelse(norm((xy .- circle.center)) < circle.radius, 1, 0)
    end
  return
end

@cuda (1024, 1024) kernel_render(vout.data, vout.spec, cu_circles)
vout.data
# @code_llvm coordinates(spec, (1,1))
using Plots
pyplot()
# need to transfer GPU->CPU before plotting, otherwise "Bus Error 10"
heatmap(coordinates(spec)..., Array(vout.data); aspect_ratio=1)
