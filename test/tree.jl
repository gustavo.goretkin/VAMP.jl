struct TreeNode
  parent::Int
end

import VAMP.findparent
findparent(t::TreeNode) = t.parent

function make_linear_tree(n)
  return vcat([TreeNode(1)], [TreeNode(i) for i =1:(n-1)])
end

# these probably must be topologically sorted
linear_tree = make_linear_tree(7)
linear_tree_branch_off_end = vcat(make_linear_tree(7), [TreeNode(7), TreeNode(7)])
linear_tree_branch_off_3 = vcat(make_linear_tree(7), [TreeNode(3)])
linear_tree_branch_off_3_5 = vcat(make_linear_tree(7), [TreeNode(3), TreeNode(5)])

function make_complete_k_ary_tree(k, h)
  if k == 1
    return make_linear_tree(h+1)
  end
  nodes = (k^(h + 1) - 1 ) / (k-1)
  return vcat([TreeNode(1)], [TreeNode(Int(floor((i-1)/k))+1) for i in 1:(nodes-1)])
end

# https://oeis.org/A000522
function count_arrangements(n)
  a = zero(n)
  for k = zero(n):n
    a += factorial(n) ÷ factorial(k)
  end
  return a
end

function doit(tree, goal)
  tsi = VAMP.TreeSearchInstance(tree, goal, (state_from, new_edge) -> true)
  asi = VAMP.make_a_star_instance(tsi)
  asa = VAMP.make_a_star_algorithm(tsi, asi)
  while step!(asa) end
  return asa
end

function get_sorted_goal_element(asa)
  paths = getfield.(asa.goal_node, :path)
  paths = sort_paths(paths)
  return paths
end

"sort paths by length, and then lexicographically along path. each pair element (tuple) is itself sorted lexicographically"
sort_paths(paths) = sort(paths,
  lt=(a,b)->(length(a) < length(b) || (length(a) == length(b) &&
    tuple(a...) < tuple(b...))) )  # turn array to tuple to use lexicographic sort


impossible_goal = -1 # force all paths to be explored
for k = 1:6
  asa = doit(make_complete_k_ary_tree(k, 1), impossible_goal)
  @test length(asa.closed_parents) == count_arrangements(k)
end

asa = doit(make_complete_k_ary_tree(2, 1), impossible_goal)
@test length(asa.closed_parents) == 5 # Don't know enumerative combinatorics

asa = doit(make_complete_k_ary_tree(2, 2), impossible_goal)
@test length(asa.closed_parents) == 225 # Don't know enumerative combinatorics


goal = 7

asa = doit(linear_tree, goal)
@test get_sorted_goal_element(asa) == [[(1,7)]]

asa = doit(linear_tree_branch_off_end, goal)
@test get_sorted_goal_element(asa) == [[(1,7)]]

asa = doit(linear_tree_branch_off_3, goal)
@test get_sorted_goal_element(asa) == [
  [(1, 7)],
  [(1, 8), (3, 7)],
  [(1, 4), (3, 8), (3, 7)],
  [(1, 5), (3, 8), (3, 7)],
  [(1, 6), (3, 8), (3, 7)]
]

asa = doit(linear_tree_branch_off_3_5, goal)
@test get_sorted_goal_element(asa) == [
  [(1, 7)],
  [(1, 8), (3, 7)],
  [(1, 9), (5, 7)],
  [(1, 4), (3, 8), (3, 7)],
  [(1, 5), (3, 8), (3, 7)],
  [(1, 6), (3, 8), (3, 7)],
  [(1, 6), (5, 9), (5, 7)],
  [(1, 8), (3, 9), (5, 7)],
  [(1, 9), (3, 8), (3, 7)],
  [(1, 4), (3, 8), (3, 9), (5, 7)],
  [(1, 5), (3, 8), (3, 9), (5, 7)],
  [(1, 6), (3, 8), (3, 9), (5, 7)],
  [(1, 6), (5, 9), (3, 8), (3, 7)],
  [(1, 8), (3, 6), (5, 9), (5, 7)],
  [(1, 9), (5, 6), (3, 8), (3, 7)],
  [(1, 4), (3, 8), (3, 6), (5, 9), (5, 7)],
  [(1, 5), (3, 8), (3, 6), (5, 9), (5, 7)]
]
# at the time of writing, the implementation achieves 50. Just raise a flag if more nodes get expanded
@test length(asa.closed_parents) <= 50
