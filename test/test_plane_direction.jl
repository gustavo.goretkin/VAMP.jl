# do normals point in or out?
## assuming β ≥ 0 (the plane "c" / offset), then the normals of the halfspaces point OUT of the convex region
## the rows of hrep(poly).A point IN
## double check. the `show` method in Polyhedra does some negation.

#=
function LiftedHRepresentation{N, T}(it::HRepIterator{N, T}) where {N, T}
    A = Matrix{T}(length(it), N+1)
    linset = IntSet()
    for (i, h) in enumerate(it)
        A[i,2:end] = -h.a
        A[i,1] = h.β
        if islin(h)
            push!(linset, i)
        end
    end
    LiftedHRepresentation{N, T}(A, linset)
end


function SimpleHRepresentation{N, T}(it::HRepIterator{N, T}) where {N, T}
    A = Matrix{T}(length(it), N)
    b = Vector{T}(length(it))
    linset = IntSet()
    for (i, h) in enumerate(it)
        A[i,:] = h.a
        b[i] = h.β
        if islin(h)
            push!(linset, i)
        end
    end
    SimpleHRepresentation{N, T}(A, b, linset)
end
=#

using Polyhedra

vertices = float.([0 0; 0 1; 1 0; 1 1])
center = mean(vertices,1)

svr = vrep(vertices)

poly = polyhedron(svr, SimplePolyhedraLibrary{Float64}())

for halfspace in allhalfspaces(poly)
  @show halfspace
  @show dot(halfspace.a, center)
  @show halfspace.β
  signed_distance_ = (dot(halfspace.a, center) - halfspace.β) / norm(halfspace.a)
  @show signed_distance_
end

h = LiftedHRepresentation{2, Float64}(poly)
hAb = MixedMatHRep{2, Float64}(poly)
@show -h.A[:,2:end] * center' - h.A[:,1]
@show hAb.A * center' - hAb.b
