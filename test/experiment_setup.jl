import VAMP.Plots
Plots.pyplot() # GR, I think, causes SystemError too many open files.
import Parameters: @pack!, @unpack

using VAMP

import Serialization

println("`using VAMP` completed")

include("domain_robot.jl")
include("domain_world.jl")
include("domain_world2.jl")
include("domain_motion_planning_collision.jl")
include("do_algs.jl")
println("`include(domain_*)` completed")

function make_domain_world_generic(parameters)
  @unpack domain_id = parameters
  if domain_id == :world1
    make_domain_world(parameters)
  elseif domain_id == :world2
    make_domain_world2(parameters)
  end
end

function make_generic_fov(parameters)
  if parameters[:all_field_of_view] == :narrow
    delete!(parameters, :all_field_of_view)
    make_narrow_cone(parameters)
  end
end

function make_all_setparams()
  parameters = Dict{Symbol, Any}()
  last_parameters = nothing

  setparams = [Dict{Symbol, Any}()]

  new_setparams = []
  for domain_id in [:world1, :world2]
    for old_setparam in setparams
      old_setparam = copy(old_setparam)
      @pack! old_setparam = domain_id
      push!(new_setparams, old_setparam)
    end
  end
  setparams = new_setparams

  new_setparams = []
  for fov in [#=360.0,=# 350.0, 200.0, 50.0, #=:narrow=#]
    for old_setparam in setparams
      if old_setparam[:domain_id] == :world2 && fov == :narrow
        continue # this instance isn't feasible.
      end
      old_setparam = copy(old_setparam)
      old_setparam[:all_field_of_view] = fov
      push!(new_setparams, old_setparam)
    end
  end
  setparams = new_setparams

  new_setparams = []
  for old_setparam in setparams
    if old_setparam[:domain_id] == :world1
      for g in [:easy, :hard]
        old_setparam = copy(old_setparam)
        old_setparam[:domain1_goal] = g
        push!(new_setparams, old_setparam)
      end
    else
      push!(new_setparams, old_setparam)
    end
  end
  setparams = new_setparams


  new_setparams = []
  for algorithm_id in [:gstar_treevis, :two_phase_vavp, :two_phase_none]
    for old_setparam in setparams
      old_setparam = copy(old_setparam)
      @pack! old_setparam = algorithm_id
      push!(new_setparams, old_setparam)
    end
  end
  setparams = new_setparams
  return setparams
end


# parameter names in the file name are abbreviated
abbr = Dict("all_field_of_view"=>"fov", "domain1_goal"=>"goal", "algorithm_id"=>"alg")
function getabbr(s)
  if haskey(abbr, string(s))
    return abbr[string(s)]
  else
    return string(s)
  end
end

function make_filename_params(setparam)
  return join(["$(getabbr(string(k)))-$(v)" for (k,v) in sort(setparam)], "_")
end

function make_complete_parameters(setparam)
  parameters = copy(setparam)

  make_generic_fov(parameters)
  make_robot(parameters)
  make_robot_vis_target(parameters)
  make_domain_world_generic(parameters)
  make_domain_motion_planning_collision(parameters)

  VAMP.do_parameters(parameters)

  return parameters
end

function run_one_experiment(setparam, results_path)
  try
    parameters = make_complete_parameters(setparam)
    JLD2.@save joinpath(results_path, "parameters.jld2") setparam parameters
    JLD2.@save joinpath(results_path, "setparam_parameters.jld2") setparam
    Serialization.serialize(
        open(joinpath(results_path, "parameters.jl_serialize"), "w"),
        parameters)

    if setparam[:algorithm_id] == :two_phase_vavp
      do_two_phase(results_path, parameters, search_parameters=Dict(:do_vavp=>true))
    elseif setparam[:algorithm_id] == :two_phase_none
      do_two_phase(results_path, parameters, search_parameters=Dict(:do_vavp=>false))
    elseif setparam[:algorithm_id] == :gstar_treevis
      do_g_star_treevis(results_path, parameters)
    else
      error("unknown algorithm_id: $(setparam[:algorithm_id])")
    end

  catch err
    rethrow()
    if isa(err, InterruptException)
      rethrow()
    end
    @warn("catching error: $(err)")
    open(joinpath(results_path, "error.txt"), "a") do f
      write(f, "Error\n")
      write(f, string(err))
      write(f, experiment_name)
    end
  finally
  end
  return nothing
end

function run_many_experiments(setparams, results_root)
  for setparam in setparams
    if VAMP.ishalted()
      error("I have been halted.")
    end

    experiment_name = make_filename_params(setparam)

    if any(startswith(dir, experiment_name) for dir in readdir(results_root))
      @warn("Skipping: $(experiment_name), because directory already exists.")
      continue
    end

    results_path = joinpath(results_root, "$(experiment_name)_$(string(rand(UInt), base=16)[1:6])")
    mkpath(results_path)
    println("Results are in: $results_path")

    run_one_experiment(setparam, results_path)
  end
end