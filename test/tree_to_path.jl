using VAMP
using FileIO

d = load("retrovamprrt_goals_2018-02-08T13:03:14.589.jld2")
g = d["goal_found_ones"][1]

tree_goal = g.rrt.reached_goals[1]
tree = g.rrt.tree

starting_visibility = g.visible_region[2] # FIXME, need to treat starting visibility specially
function edge_validator(tree_search_node, new_edge)
  i_new = last(new_edge)
  q_new = tree[i_new].state

  qs_covering = [tree[i].state for i in tree_search_node.visited] # TODO check in smarter order
  uncovered_pieces__Fworld = VAMP.get_uncovered_pieces__Fworld_from_qs_covering(g.visibility_instance, q_new, qs_covering, [starting_visibility])
  return length(uncovered_pieces__Fworld) == 0
end

tsi = VAMP.TreeSearchInstance(tree, tree_goal, edge_validator)
asi = VAMP.make_a_star_instance(tsi)
asa = VAMP.make_a_star_algorithm(tsi, asi)
println("start")

t = 0.0
while true
  step!(asa)
  if (time() - t) > 3.0
    println((time() - t))
    t = time()
    println(length(asa.closed_parents))
  end
  if length(asa.goal_node) > 0
    println("a goal was found")
    break
  end
end

paired_compressed_tree_path = asa.goal_node[1].path
compressed_tree_path = vcat(map(x->[x...], paired_compressed_tree_path)...)

segmented_tree_path = []
for i in 1:(length(compressed_tree_path)-1)
  # TODO shave off end of segment except for last segment, to avoid repeated nodes
  segment = VAMP.path(tree, compressed_tree_path[i], compressed_tree_path[i+1])
  push!(segmented_tree_path, segment)
end

flat_tree_path = vcat(segmented_tree_path[1][1], map(x->x[2:end], segmented_tree_path)...)

qs = [tree[i].state for i in flat_tree_path]
xy = [tuple(tree[i].state.e.q...) for i in flat_tree_path]

idict = Dict()
for i=1:length(flat_tree_path)
  ti = flat_tree_path[i]
  idict[ti] = get(idict, ti, "") * " $i,"
end

tolabel = unique(flat_tree_path)
xylabel = [tuple(tree[i].state.e.q...) for i in tolabel]
labels = [idict[i] for i in tolabel]


vvs = collect(VAMP.get_generator_visible_region__Fworld(g.visibility_instance, qs))

uvvs = UnionOfGeometry(vvs)
pvv = make_packed_view_volume(UnionOfGeometry(uvvs.pieces[:]))


window = (1024, 1024)
out = CuArray(zeros(Int16, window...))
spec = VAMP.RasterVolumeSpec(VAMP.BoundingBox([-4.0, -4.0], [4.0, 4.0]), window)
vout = VAMP.RasterVolume(spec, out)

gpu_pvv = PackedViewVolumes(CuArray(pvv.arc_regions), CuArray(pvv.shadow_halfspaces))
render!(vout, gpu_pvv)
synchronize()


using Plots
pyplot()
# need to transfer GPU->CPU before plotting, otherwise "Bus Error 10"
plt = plot(aspect_ratio=1)
cmap = cgrad([:yellow, :orange])
cmap.values[1] = eps()
prepend!(cmap.values, [0.0])
prepend!(cmap.colors, [RGBA{Float64}(1.0,1.0,1.0,1.0)])

heatmap!(plt, VAMP.coordinates(spec)..., (Array(vout.data))'; aspect_ratio=1, c=cmap)


for piece in g.visibility_instance.occluders__Fworld.pieces
  plot_polyhedron!(plt, piece, fillcolor=:brown)
end

plot!(plt, xy)
scatter!(plt, xylabel, aspect_ratio=1, series_annotations=labels)

display(plt)
