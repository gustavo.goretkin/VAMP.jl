import Plots
Plots.pyplot()
import DataStructures

using VAMP
println("`using VAMP` completed")

include("domain_robot.jl")
include("domain_world.jl")
include("domain_motion_planning_collision.jl")
println("`include(domain_*)` completed")

const SE2_MPState_t = SE2_MPState{robot_width/2} # reasonable scaling to measure distance

const θ_steps = 20;
const Δθ = 2π/(θ_steps)
const Δx =  1/(2^3) # carefully chosen to be exactly representable in floating point and also include the goal, because of equality testing
const Δy =  1/(2^3)

angle_to_step(θ) = Int(round(θ/Δθ))
step_to_angle(step) = Δθ * (step % θ_steps) # makes all floating point angles canonical
angle_p_step(θ, Δsteps) = step_to_angle(angle_to_step(θ) + Δsteps)


goal = PointEpsilonGoal(
  SE2_MPState_t(1.0,0.625,π),
  0.01
)

#=
goal = PointEpsilonGoal(
  SE2_MPState_t(1.0,0.625,0.0),
  0.01
)
=#

mp = HolonomicMotionPlanningInstance(
  SE2_MPState_t(-1.0,-1.0,0.0),
  goal,
  state->is_collision_free(state),
  (state1,state2)->is_path_free(state1,state2)
)

nonholo_succ =  function (set, s_fro)
    x, y = s_fro.e.q
    θ = s_fro.θ.q.θ
    # rotate in place
    q_new = SE2_MPState_t(x, y, angle_p_step(θ, +1)); push!(set, q_new)
    q_new = SE2_MPState_t(x, y, angle_p_step(θ, -1)); push!(set, q_new)
    Δx = Int(round(cos(θ)))
    Δy = Int(round(sin(θ)))

    # go forward
    q_new = SE2_MPState_t(x + Δx, y + Δy, θ);    push!(set, q_new)
    nothing
end

holo_succ =  function (set, s_fro)
    x, y = s_fro.e.q
    θ = s_fro.θ.q.θ
    # rotate in place
    q_new = SE2_MPState_t(x, y, angle_p_step(θ, +1)); push!(set, q_new)
    q_new = SE2_MPState_t(x, y, angle_p_step(θ, -1)); push!(set, q_new)
    q_new = SE2_MPState_t(x, y + Δy, θ);    push!(set, q_new)
    q_new = SE2_MPState_t(x, y - Δy, θ);    push!(set, q_new)
    q_new = SE2_MPState_t(x + Δx, y, θ);    push!(set, q_new)
    q_new = SE2_MPState_t(x - Δx, y, θ);    push!(set, q_new)
    nothing
end




function make_astar_alg(succ)
  ai = LazyAStarInstance(
    (s_to, s_fro) -> distance(s_to, s_fro),
    (s) -> distance(rand(goal), s),
    (s) -> s ∈ goal,
    succ,
    (s_to, s_fro) -> mp.q_valid_test(s_to)  # assuming the Δs above are tiny, don't check the whole path.
  )

  alg = LazyAStarAlgorithm(ai, SE2_MPState_t{Float64}, Float64)
  return alg
end

alg
setup!(alg, mp.q_start, 0.0)



using Plots

plt = plot(;aspect_ratio=1.0)

for obstacle in polys_obstacles__Fworld.pieces
    plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
end

for p in VAMP.get_robot__Fworld(polys_robot__Frobot, mp.q_start).pieces
  plot_polyhedron!(plt, p; color=:green)
end

for p in VAMP.get_robot__Fworld(polys_robot__Frobot, rand(mp.q_goal)).pieces
  plot_polyhedron!(plt, p; color=:red)
end

display(plt)

println("start search")
start_time = time()
while isempty(alg.goal_node)
  if !step!(alg) break end
  if time() - start_time > 5.0
    println("time out")
    break
  end
end


goal_node = nothing
if length(alg.goal_node) > 0
  goal_node = alg.goal_node[1]
else
  # didn't actually reach goal. how close did we get?
  cp = collect(keys(alg.closed_parents));
  _d, goal_i = findmin([heuristic(cp[i]) for i in LinearIndices(cp)])
  goal_node = cp[goal_i]
end



tp = follow_path(alg.closed_parents, goal_node)
solvepath = reshape(reinterpret(Float64, tp),  3, length(tp))';

plot!(plt, solvepath[:,1], solvepath[:,2] ; aspect_ratio = 1)

import Base.convert
convert(::Type{VAMP.MoveAction}, state::SE2_MPState) = VAMP.MoveAction([state.e.q..., state.θ.q.θ])

p = [convert(VAMP.MoveAction, state) for state in reverse(tp)]
stuff__Fworld = VAMP.worldgrounded_path(p, polys_robot__Frobot, poly_frustum__Fcamera, polys_obstacles__Fworld)
VAMP.plot_path!(plt, stuff__Fworld)

scatter!( unique((s->tuple(s.e.q...)).(collect(keys(alg.closed_parents)))); alpha=0.1, color=:red)
scatter!( unique((n->tuple(n.state.e.q...)).(collect(keys(alg.agenda)))); alpha=0.1, color=:grey)
