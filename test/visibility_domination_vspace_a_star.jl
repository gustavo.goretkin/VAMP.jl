# run after vspace_a_star.jl to check if visibility-based domination test makes any difference.
using NamedTuples

dom_test = DataStructures.DefaultDict(Set)
for s in keys(alg.closed_parents)
  push!(dom_test[s.q], s)
end

closed_children_q = DataStructures.DefaultDict(Set)

for (child, (parent, cost)) in alg.closed_parents
  push!(closed_children_q[parent.q], child)
end

succesor_counts = Dict((k, length(v)) for (k,v) in closed_children_q)
box = BoundingBox([k.e.q for k in keys(succesor_counts)])
cells = (Int.(widths(box) ./ [Δx, Δy]) .+ 1)
spec = VAMP.RasterVolumeSpec(box, (cells...))
grid = zeros(Int16, cells...)

for (k,v) in succesor_counts
  Iu = VAMP.coordinates_inv(spec, k.e.q)
  I = Int.(round.(Iu)) # should only ever round by ~eps()
  grid[I...] += v
end

plt = Plots.plot(;aspect_ratio=1.0)

Plots.heatmap!(plt, VAMP.coordinates(spec)..., log10.(grid' + 1); aspect_ratio=1, c=cmap)

for obstacle in polys_obstacles__Fworld.pieces
    plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
end

display(plt)


bottleneck = SE2_MPState{0.5}(2.0,-0.625,0.0)

if false
  dt = collect(dom_test);
  visitation_and_paths = [(length(value), Base.maximum(length(node.q_path) for node in value)) for (key, value) in dt]
   _, i = findmax(visitation_and_paths)
  keydt,valdt = dt[i]

  plt = debug_plot_conf(keydt)

  for node in dom_test[keydt]
    path = reverse(follow_path(alg.closed_parents, node))
    debug_plot_path!(plt, path)
  end
  display(plt)

  @assert(false)
end



function renderbox(visible_region)
  spheres = (HyperSphere(v.positive.center, v.positive.radius) for v in visible_region)
  BoundingBox(UnionOfGeometry(spheres))
end

renderboxes = DataStructures.DefaultDict(Vector)
println("Compute rasters")

@showprogress 0.5 "raster1" for (q, s) in dom_test
  if q != bottleneck
    continue
  end

  window = reduce(union, (renderbox(node.visible_region) for node in s))
  rvs = VAMP.RasterVolumeSpec(
    window,
    ((Int.(round.(widths(window))/0.05))...)
  )

  @showprogress 0.5 "raster2" for node in s
    pvv = VAMP.make_packed_view_volume(UnionOfGeometry([v for v in node.visible_region]))
    out = zeros(Int16, rvs.size...)
    vout = VAMP.RasterVolume(rvs, out)
    VAMP.render!(vout, pvv)

    push!(renderboxes[q], (raster=vout, searchnode=node))
  end

end


"r1 dominates r2 if when r1 contains visibility, r2 also."
function raster_dominates(r1, r2)
  for idx in eachindex(r1, r2)
    if !(r1[idx] > 0) && (r2[idx] > 0)
      return false
    end
  end
  return true
end


println("Compute domination matrices")

domination_matrices = Dict()
for (q, list) in renderboxes
  n = length(list)
  domination_matrix = zeros(Bool, n, n)
  for i = 1:n, j = 1:n
    if i == j
        domination_matrix[i,j] = false  # technically should be true, but not interested in self-domination
        continue
    end
    continue
    (ri, si) = (list[i].raster, list[i].searchnode)
    (rj, sj) = (list[j].raster, list[j].searchnode)
    domination_matrix[i,j] = (length(si.q_path) <= length(sj.q_path)) && raster_dominates(ri.data, rj.data)
  end
  domination_matrices[q] = domination_matrix
end

@show any(domination_matrices[bottleneck])

items = renderboxes[bottleneck]

plt = Plots.plot(;aspect_ratio=1.0)

for obstacle in polys_obstacles__Fworld.pieces
    plot_polyhedron!(plt, obstacle, color=:grey, linecolor=nothing)
end


for i = 1:20
  data = items[i].raster.data
  Plots.contour!(plt, VAMP.coordinates(spec)..., 1.0*(data' .> 0); aspect_ratio=1, c=Plots.cgrad([:yellow, :orange]))
end
display(plt)

cmap_div = Plots.cgrad([:blue, :green, :white, :yellow, :orange])
gap = 1e-1
cmap_div.values[:] = [0.0, 0.5-gap, 0.5, 0.5+gap, 1.0]
data = (items[2].raster.data .> 0) - (items[1].raster.data .> 0)
spec = items[2].raster.spec
l = Base.maximum(abs.(data))

Plots.heatmap!(plt, VAMP.coordinates(spec)..., data'; aspect_ratio=1, c=cmap_div, clims=(-l, l))
