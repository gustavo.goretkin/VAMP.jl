import JLD2
#import BSON
import FileIO
using Printf
import TimerOutputs
import TimerOutputs: @timeit
using Nullables

function do_two_phase(results_path, parameters; search_parameters=Dict())
  mkpath(joinpath(results_path, "summary"))
  mkpath(joinpath(results_path, "calls"))

  vamp_pieces = make_vamp_pieces(parameters)

  @unpack goal = parameters

  tpi = VAMP.TwoPhaseInstance(vamp_pieces=vamp_pieces, domain_parameters=parameters)
  merge!(tpi.parameters, search_parameters)
  @show tpi.parameters
  tpa = VAMP.TwoPhaseAlgorithm(instance=tpi)
  setup!(tpa)

  while true
      VAMP.make_summary_plots(tpa.path_segments, parameters; q_goal=rand(goal), file_prefix=joinpath(results_path, "summary/"))
      if !step!(tpa).keepgoing
          break
      end

      if tpa.state == :explore_violation
        let move_process_result = tpa.move_process_result
          @unpack polys_obstacles__Fworld = parameters
          @unpack polys_robot__Frobot = parameters

          plt = Plots.plot(;aspect_ratio=1)

          #= let path = move_process_result.move_path_prefix
            vrs = VAMP.LibGEOSInterface.to_geos_with_no_holes(
              UnionOfGeometry(
                  map(q->VAMP.get_visible_region__Fworld(visibility_instance, q), path[end].q_path)),
              [LibGEOS.Polygon(UnionOfGeometry(starting_visibility__Fworld))]) #= if starting visibilty has holes, this will not work =#

            for vr in vrs; Plots.plot(plt, vr; color=:yellow, linealpha=0.0); end

          end=#
          plot_polyhedron!(plt, polys_obstacles__Fworld; linealpha=0.0, color=:grey)

          for (kind, path) in zip([:safe, :unsafe], [move_process_result.move_path_prefix, move_process_result.move_path_unsafe])
            for q in unlift_q(path)
              p = VAMP.get_robot__Fworld(polys_robot__Frobot, q)

              plot_polyhedron!(plt, p, fillalpha=0.0, linecolor=Dict(:safe=>:grey, :unsafe=>:red)[kind], linealpha=0.5)
            end
          end

          scatter_points!(plt, tpa.coverage_goal; color=:red)
          Plots.savefig(plt, joinpath(results_path, "calls", "move_call_$(length(tpa.path_segments)).png"))
          display(plt)
        end
      end
  end
  println("two-phase search is done.")
  path_segments = tpa.path_segments

  path = vcat(map(x->x.path, path_segments)...)
  path_length = VAMP.get_path_length(unlift_q(path))

  timer = tpa.timer
  total_search_time_secs = TimerOutputs.time(timer["step"]) / 1e9

  closed_used = sum([segment.search_result.search_summaries[end].summary.closed_size for segment in path_segments[2:end]])
  closed_total = sum(summary.closed_size for summary in tpa.final_search_summaries)

  q_path_segments = [unlift_q(segment.path) for segment in path_segments]
  kind_segments = [segment.kind for segment in path_segments]

  results_dict = Dict{Symbol, Any}()
  @pack! results_dict = q_path_segments, kind_segments, path_length, total_search_time_secs, closed_used, closed_total
  results_dict[:hostname] = gethostname()
  results_dict[:n_path_segments] = length(path_segments)

  # add possibly more complicated objects that might be harder to JLD2-load
  results_dict2 = copy(results_dict)
  @pack! results_dict2 = parameters, timer, path, path_segments


  FileIO.save(joinpath(results_path, "results_full.jld2"), Dict(string(k)=>v for (k, v) in results_dict2))
  FileIO.save(joinpath(results_path, "results_dict.jld2"), Dict("results_dict"=>results_dict))
  println("JLD2 saved in: $(results_path)")

  #=
  # https://github.com/MikeInnes/BSON.jl/issues/3 can't save many kinds of dicts
  BSON.bson(joinpath(results_path, "results.bson"), results_dict)
  println("BSON saved in: $(results_path)")
  =#



  open(joinpath(results_path, "info.txt"), "w") do f
      write(f, "$(@__FILE__)\n")
      write(f, string(timer))
      write(f,"\n")
      write(f, "path_length: $(path_length)\n")
      write(f, "closed_used: $(closed_used)\n")
      write(f, "closed_total: $(closed_total)\n")
      write(f, "total_search_time_secs: $(total_search_time_secs)")
  end

  VAMP.make_summary_plots(path_segments, parameters; q_goal=rand(goal), file_prefix=joinpath(results_path, "summary/"))
  return tpa
end

import VAMP.GStar
function do_g_star_treevis(results_path, parameters)
  vamp_pieces = make_vamp_pieces(parameters)

  gstar_pieces = VAMP.make_g_star_vis(parameters, vamp_pieces)

  tree_vis_successors! = gstar_pieces.tree_vis_successors!
  tree_vis_valid = gstar_pieces.tree_vis_valid

  gi = GStar.GStarInstance(
    push_successors! = tree_vis_successors!,
    transition_valid = tree_vis_valid,
    goal_test = function goal_test(s); s ∈ goal; end
    )

  @unpack SE2_MPState_t = parameters
  @unpack mp = parameters
  @unpack goal = parameters

  ga = GStar.GStarAlgorithm{SE2_MPState_t{Float64}, Float64, typeof(gi)}(instance=gi)

  function make_g_star_summary(alg)
    if length(alg.open) > 0
        (node, f_val) = DataStructures.peek(alg.open)
    else
      f_val = nothing
    end

    return (
      open_size=length(alg.open),
      closed_size=length(alg.closed),
      preopen_size=length(alg.preopen),
      agenda_top = (f_val=f_val,)
      )
  end

  function report_search!(search_summaries, this_time, search_alg; interval=1.0, header="")
    if length(search_summaries) == 0 || this_time - search_summaries[end].time > interval
      summary = make_g_star_summary(search_alg)
      print(header)
      println(summary)
      if length(search_summaries) > 0
        d = summary.closed_size - search_summaries[end].summary.closed_size
        println("New Closed: $(d)")
      end

      push!(search_summaries, (time=this_time, summary=summary))
      if length(search_summaries) > 0
        runtime = this_time - first(search_summaries).time
        println("Runtime: $(@sprintf("%.2f", runtime))")
      end
      sleep(0.001)
    end
  end


  GStar.setup!(ga, mp.q_start)

  to = TimerOutputs.TimerOutput()

  search_summaries = []

  @timeit to "gstar" while true
    while true
      @timeit to "step" result = GStar.step!(ga)
      if !result.keepgoing
        @show result
        break
      end
      report_search!(search_summaries, time(), ga)
    end

    if !(isnull(ga.goal_state[]))
      println("found goal")
      break
    end
    @timeit "step_preopen" GStar.step_preopen!(ga)

    if length(ga.open) == 0
      println("after preopen, no news.")
      break
    end
  end

  total_search_time_secs = TimerOutputs.time(to["gstar"]) / 1e9
  timer = to

  closed_total = length(ga.closed)

  results_dict = Dict{Symbol, Any}()

  @pack! results_dict = total_search_time_secs, closed_total
  results_dict[:hostname] = gethostname()

  results_dict2 = copy(results_dict)
  @pack! results_dict2 = parameters, timer


  FileIO.save(joinpath(results_path, "results.jld2"), Dict(string(k)=>v for (k, v) in results_dict2))
  FileIO.save(joinpath(results_path, "results_dict.jld2"), Dict("results_dict"=>results_dict))
  println("JLD2 saved in: $(results_path)")

end
