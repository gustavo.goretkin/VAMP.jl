using DataStructures

struct TNode{GNT, EWT}
    id::GNT
    parent
    cost::EWT
    lazy::EWT
    estm::EWT
    budget::Int
end

struct LRAInstance{FLT,FRT,FH,FG,FE}# dispatch <: AbstractAStarInstance
  lazy_transition_cost::FLT  # (state_to::STATE, state_from::STATE) ↦ edge_cost::COST
  real_transition_cost::FRT  # (state_to::STATE, state_from::STATE) ↦ edge_cost::COST
  heuristic::FH         # state::STATE ↦ cost_to_goal::COST #arguably belongs in Algorithm, not Instance
  goal_test::FG         # state::STATE ↦ ::Bool
  succ!::FE           # (Set{STATE}, state_from) ↦ ::Void
end

struct LRAAlgorithm
    instance#::LRAInstance
    tree#::Dict{GNT, TNode}
    goal_node#::Array{GNT}
    alpha#::Int
    frontier#::SortedSet{TNode}
    rewire#::SortedSet{TNode}
    update#::SortedSet{TNode}
    extend#::SortedSet{TNode}
    rmEdgesFrom
    rmEdgesTo
    possible_preds
end

function ConstructLRAAlgorithm(successors!,
                            heuristic,
                            vsafe_penalty,
                            motion_cost,
                            collision_test,
                            goal_test,
                            alpha = 1)
    possible_preds = Dict()  # gnt ↦ Set(gnt)
    lra_successors!(res, from) = begin
        successors!(res, from)
        for x in res
            if !haskey(possible_preds,aliasVss(x))
                possible_preds[aliasVss(x)] = Set()
            end
            push!(possible_preds[aliasVss(x)], from)
        end
        #println("size of succs of $(from) is $(length(res))")
    end
    lazy_cost(to, from) = begin
        return motion_cost(to, from) + vsafe_penalty(to, from)
        #return motion_cost(to, from)
        # if !collision_test(to, from)
        #     return Inf
        # else
        #     return motion_cost(to, from)
        # end
    end
    real_cost(to, from) = begin
        if !collision_test(to, from)
            return Inf
        else
            return lazy_cost(to, from)
        end
        # if !collision_test(to, from)
        #     return Inf
        # else
        #     return motion_cost(to, from) + vsafe_penalty(to, from)
        # end
        return lazy_cost(to, from) + vsafe_penalty(to, from)
    end

    instance = LRAInstance(
        lazy_cost,
        real_cost,
        heuristic,
        goal_test,
        lra_successors!
    )

    return LRAAlgorithm(
        instance,
        Dict(),
        [],
        alpha,
        SortedSet(),
        SortedSet(),
        SortedSet(),
        SortedSet(),
        Dict(),
        Dict(),
        possible_preds
    )
end

function aliasVss(x)
    return x.q
end

function orderToken(x::TNode)
    return (x.cost + x.lazy + x.estm, x.budget, hash(aliasVss(x.id)))
end

function Base.isequal(x::TNode, y::TNode)
    return orderToken(x) == orderToken(y)
end

function Base.:(==)(x::TNode, y::TNode)
    return orderToken(x) < orderToken(y)
end

# get tree node of graph vertex id
function getNode(algo::LRAAlgorithm, id)
    # if !isInTree(algo, id)
    #     for (x, y) in algo.tree
    #         println(x)
    #     end
    # end
    @assert isInTree(algo, id) "id:$(id.q) is not in tree"
    return algo.tree[aliasVss(id)]
end

# get parent node of id
function getParNode(algo::LRAAlgorithm, id)
    #println("finding parent of $(id.q)")
    parNode = getNode(algo, id).parent
    #@assert getNode(algo, parNode).budget == 0 || getNode(algo,id).budget == getNode(algo,parNode).budget + 1
    # if (parEdg == nothing)
    #     return TNode(0, nothing, 0, 0, 0)
    # end
    return getNode(algo, parNode)
end

# return whether a tnode is succecssfully removed from a list
function remove!(set::SortedSet, node::TNode)::Bool
    if (in(node, set))
        delete!(set, node)
        return true
    end
    return false
end

function rmFromQueues(algo::LRAAlgorithm, id)
    if (isInTree(algo, id))
        oldNode = getNode(algo, id)
        remove!(algo.frontier, oldNode)
        remove!(algo.update, oldNode)
        remove!(algo.extend, oldNode)
        remove!(algo.rewire, oldNode)
    end
end

function isInTree(algo::LRAAlgorithm, id)
    #print(algo.tree)
    return haskey(algo.tree, aliasVss(id))
end

# every time we update a node, we need to assure it's old existences are removed.
function updateTreeNode!(algo::LRAAlgorithm, node::TNode)
    @assert algo.instance.goal_test(node.id) || (node.parent == nothing  || isInTree(algo, node.parent))
    @assert node.parent == nothing || getNode(algo, node.parent).budget == 0 || node.budget == getNode(algo, node.parent).budget + 1
    @assert node.lazy != Inf && node.cost != Inf

    rmFromQueues(algo, node.id)
    #println(node)
    #println("node to update is $(node.id.q)")
    algo.tree[aliasVss(node.id)] = node
end

# note that this function only need an graph vertex, which is a little bit
# different from updateTreeNode!.
function rmFromTree!(algo::LRAAlgorithm, id)
    if (isInTree(algo, id))
        #println("node to remove is $(id.q)")
        rmFromQueues(algo, id)
        delete!(algo.tree, aliasVss(id))
    end
end

# delete subtree rooted at id. and return an array containing all id in the subtree.
function takeOut!(algo::LRAAlgorithm, id, par)
    # println("taking out... $(id) $(par)")
    #sleep(0.1)
    if (!isInTree(algo, id))
        return []
    end

    # rmFromQueues(algo, id)

    ret = [id]
    succ = getSucc(algo, id)

    for v in succ
        if (isInTree(algo, v) &&
            v != par &&
            getNode(algo, v).parent == id)
            append!(ret, takeOut!(algo, v, id))
        end
    end

    rmFromTree!(algo, id)
    return ret
end
tid = 0
function getSucc(algo::LRAAlgorithm, id)
    rm = haskey(algo.rmEdgesFrom, aliasVss(id)) ? algo.rmEdgesFrom[aliasVss(id)] : Set()
    succ = Set()
    #println("config in getSucc is $(id)")
    global tid
    tid = id
    algo.instance.succ!(succ, id)
    #return setdiff(succ, rm)
    #println("using getsucc $(length(succ))")
    return filter(x->!in(x.q, rm), succ)
end

function getPred(algo::LRAAlgorithm, id)
    rm = haskey(algo.rmEdgesTo, aliasVss(id)) ? algo.rmEdgesTo[aliasVss(id)] : Set()
    res = Set()
    if haskey(algo.possible_preds, aliasVss(id))
        pred = algo.possible_preds[aliasVss(id)]
        for p in pred
            if isInTree(algo, p)
                push!(res, getNode(algo, p).id)
            end
        end
    end
    #algo.instance.pred!(pred, id)
    #return setdiff(pred, rm)
    return filter(x->!in(x.q, rm), res)
end


edge_to_plot = Tuple{Float64,Float64}[]
evalSet = Set{Tuple{SE2MotionPlanningState{0.5,Float64},SE2MotionPlanningState{0.5,Float64}}}()
function realEval(algo::LRAAlgorithm, from, to)
    real = algo.instance.real_transition_cost(to, from)
    if (real == Inf)
        if (!haskey(algo.rmEdgesFrom, aliasVss(from)))
            algo.rmEdgesFrom[aliasVss(from)] = Set()
        end
        if (!haskey(algo.rmEdgesTo, aliasVss(to)))
            algo.rmEdgesTo[aliasVss(to)] = Set()
        end
        push!(algo.rmEdgesFrom[aliasVss(from)], aliasVss(to))
        push!(algo.rmEdgesTo[aliasVss(to)], aliasVss(from))
        #println("removed $(from) $(to)")
    end

    global plt
    global edge_to_plot
    global evalSet
    #@show [tuple(from.e.q...), tuple(to.e.q...)]
    #@show from
    f = from.q
    t = to.q

    if ((f,t) ∉ evalSet && (t,f) ∉ evalSet)
        if real == Inf
            plot!(plt, [tuple(f.e.q...), tuple(t.e.q...)], color=:red)
        end
        push!(evalSet, (f,t))
        push!(evalSet, (t,f))
    end

    return real
end

function lazyEval(algo::LRAAlgorithm, from, to)
    lazy = algo.instance.lazy_transition_cost(to, from)
    if (lazy == Inf)
        if (!haskey(algo.rmEdgesFrom, aliasVss(from)))
            algo.rmEdgesFrom[aliasVss(from)] = Set()
        end
        if (!haskey(algo.rmEdgesTo, aliasVss(to)))
            algo.rmEdgesTo[aliasVss(to)] = Set()
        end
        push!(algo.rmEdgesFrom[aliasVss(from)], aliasVss(to))
        push!(algo.rmEdgesTo[aliasVss(to)], aliasVss(from))
        #println("removed $(from) $(to)")
    end
    return lazy
end

# return a new Tnode by a parent node and an edge.
function getNewNode(algo::LRAAlgorithm, parNode::TNode, id)
    @assert parNode.budget < algo.alpha "node $(id) budget exceeded"
    # @assert lazyEval(graph, edge) != Inf
    return TNode(id,
                parNode.id,
                parNode.cost,
                parNode.lazy + lazyEval(algo, parNode.id, id),
                algo.instance.heuristic(id),
                parNode.budget + 1)
end
