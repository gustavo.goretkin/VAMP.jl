# no-obstacle test of RRT

using VAMP

goal = PointEpsilonGoal(
  SE2_MPState(1.0,0.0,0.0),
  0.01
)

mp = HolonomicMotionPlanningInstance(
  SE2_MPState(0.0,0.0,0.0),
  goal,
  x->true,
  (x,y)->true
)

instance = RRTInstance(
  mp,
  .1,
  ()->if (rand() < .1) rand(mp.q_goal) else SE2_MPState(rand(), rand(), rand()) end
)

algorithm = RRTAlgorithm(instance)
setup!(algorithm, 1000)
step!(algorithm)
@show length(getpath(algorithm.tree, algorithm.tree[algorithm.reached_goals[1]]))
