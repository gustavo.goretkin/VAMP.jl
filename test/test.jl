include("lazy_search_pieces.jl")

global plt
lsp = make_lazy_search_pieces()
alg = ConstructLRAAlgorithm(lsp.successors!,
                            lsp.heuristic,
                            lsp.vsafe_penalty,
                            lsp.motion_cost,
                            lsp.collision_test,
                            lsp.goal_test,
                            3)

timerInit()
setup!(alg, lsp.vamp_start_state, 0.0)
plt = Plots.plot(;aspect_ratio=1.0,reuse=true)
plot_polyhedron!(plt, lsp.parameters[:polys_obstacles__Fworld], color=:grey, linecolor=nothing)
display(plt)

println("start search")
start_time = time()
while isempty(alg.goal_node)
  res = step!(alg)
  vis_lra_tree!(plt, alg)
  display(plt)
  if res break end

  global start_time
  if (time() - start_time) > 1.0
      println("frontier size is $(length(alg.frontier))")
      println("tree size is $(length(alg.tree))")
      start_time = time()
  end
end
println("done searching")
println("alpha is $(alg.alpha)\n")
println("tree size is $(length(alg.tree))")
println(lsp.vamp_start_state.q)

q_path = reverse(follow_path_general(alg.tree, alg.goal_node[1].q; v_to_k=(v->v.parent.q)))
@show length(q_path)

show_path!(plt, q_path, lsp.parameters)
plot_q_trail!(plt, q_path, lsp.parameters[:polys_robot__Frobot])

vis_lra_tree!(plt, alg)
display(plt)
println("make movie")
do_movie(q_path, lsp.parameters, "lazy_vamp_movie")


#head = keys(alg.tree).head
# res = Set()
# lsp.successors!(res, lsp.vamp_start_state)
# println(length(res))
# for x in res
#   println("collision is $(lsp.collision_test(x, lsp.vamp_start_state))")
# end

println("distance to target is $(getNode(alg,alg.goal_node[1]).cost)")
print(to)
# append!(benchmarkStat, [to])
print("\n\n\n")
