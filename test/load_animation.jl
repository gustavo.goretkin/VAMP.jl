using Revise
using VAMP
using FileIO

println("start loading jld2")
@time l = load("animation/visrun_2018-01-05T11:34:08.473.jld2")

vamp_algorithm = l["vamp_algorithm"]

gi = vamp_algorithm.rrt.reached_goals[1]
path = VAMP.get_state_action_path(vamp_algorithm.rrt.tree, vamp_algorithm.rrt.tree[gi])

using Plots
VAMP.make_animation(vamp_algorithm.visibility_instance, map(reverse, path)...)
