using LinearAlgebra
using PyPlot
using GeometryTypes
using Setfield # https://github.com/jw3126/Setfield.jl
import Base.contains
using VAMP

include("domain_robot.jl")
include("domain_world.jl")


parameters = Dict()

make_robot(parameters)
make_domain_world(parameters)

@setparam parameters polys_obstacles__Fworld = nothing


mutable struct Pacman{T}
  segment::LineSegment{Point{2, T}}
  box::BoundingBox{2, T}
  sector::ArcRegion{2, T}
end

struct RenderedSegment
  line
end

struct RenderedArcRegion
  line
end

struct RenderedBox
  line
end

function make_viz!(ax, s::Type{LineSegment{Point{2, T}}}) where {T<:Real}
  RenderedSegment(ax[:plot](zeros(0))[1])
end

function render!(rs::RenderedSegment, s::LineSegment{Point{2, T}}) where {T}
  rs.line[:set_data]([[s[i][xy] for i=1:2] for xy=1:2]...)
end


function make_viz!(ax, s::Type{ArcRegion{2, T}}) where {T<:Real}
  RenderedArcRegion(ax[:plot](zeros(0))[1])
end

function render!(rar::RenderedArcRegion, ar::ArcRegion{2, T}) where {T}
  θl = acos(ar.cos_half_fov)
  θc = atan(ar.direction[2], ar.direction[1])
  xyc = ar.center
  r = ar.radius
  points = [xyc, [r * Point(cos(θ+θc), sin(θ+θc)) + xyc for θ = range(-θl, stop=θl, length=50)]..., xyc]
  rar.line[:set_data]([[point[xy] for point in points] for xy=1:2]...)
end


function make_viz!(ax, s::Type{BoundingBox{2, T}}) where {T<:Real}
  RenderedBox(ax[:plot](zeros(0))[1])
end

function render!(rb::RenderedBox, box::BoundingBox{2, T}) where {T}
  vs = ordered_vertices(box)
  vs = [vs..., vs[1]]
  rb.line[:set_data]([[vs[i][xy] for i=1:length(vs)] for xy=1:2]...)
end

function drag_update!(pacman, pacman_clicked, clicked::Point{2, T}, dragged::Point{2,T}) where {T}
  # should measure all distances in screen space (pixels), not data
  (distance_s, selection_s) = gui_distance(pacman_clicked.segment, clicked, nothing)
  (distance_b, selection_b) = gui_distance(pacman_clicked.box, clicked, nothing)

  drag = dragged - clicked

  if distance_s < distance_b
    drags = [(i in selection_s) ? drag : [0.0, 0.0] for i = 1:2]
    pacman.segment = LineSegment((pacman_clicked.segment[i] + drags[i] for i = 1:2)...)
  else
    mask = [[0.0, 0.0], [0.0, 0.0]]
    for maskel in selection_b
      mask[maskel[1]][maskel[2]] = 1
    end
    masked_drag = [maski .* drag for maski in mask]
    new_mima = [minlimit(pacman_clicked.box), maxlimit(pacman_clicked.box)] + masked_drag
    # make sure mins < maxs
    newmi = min.(new_mima...)
    newma = max.(new_mima...)
    pacman.box = BoundingBox(newmi, newma)
  end
end

function gui_distance(segment::LineSegment{Point{2, T}}, clicked::Point{2, T}, context::Any) where {T}
  s1 = norm(segment[1] - clicked)
  s2 = norm(segment[2] - clicked)
  s = norm(findnearest(segment, clicked) - clicked)  # distance to segment

  l = norm(segment[2] - segment[1])

  vmin, imin = findmin([s1, s2])
  if vmin > max(l/10, 0.01) && vmin > s
    # segment is selected. Drag both endpoints
    return (s, [1, 2])
  else
    return (vmin, [imin])
  end
end

function gui_distance(box::BoundingBox{2, T}, clicked::Point{2, T}, context::Any) where {T}
  vis = GeometryTypes.standard_cube_vertices(Val{2}) .+ 1
  mima = [box.minimum, box.maximum]
  verts = [Point((mima[vi[coord_i]][coord_i] for coord_i = 1:2)...)  for vi in vis]
  vmin, imin = findmin(norm.([vert - clicked for vert in verts]))
  if vmin > max(minimum(widths(box))/10, 0.01) && contains(box, clicked)
    # drag whole box
    return (0.0, [vis...])
  else
    return (vmin, [[vis[imin][1], 1], [vis[imin][2], 2]])
  end
end


pacman = Pacman(
  LineSegment(Point(-1.0, -1.0), Point(1.0, 1.0)),
  BoundingBox([0.0, 0.0], [0.1, 0.1]),
  ArcRegion(1.0, cos(deg2rad(170.0)), Point(-0.1, -0.1), Vec(1.0, 0.0))
)

fig, ax = plt[:subplots]()

ax[:set_aspect](1.0)
ax[:set_xlim](-2.0, 2.0)
ax[:set_ylim](-2.0, 2.0)


#GeometryTypes.register_ax_debug(ax)

mutable struct GUIClosure
  clicked::Bool
  click_point::Any
  items::Any
  freeze::Any
  segment_viz::Any
  sector_viz::Any
  box_viz::Any
end

gui = GUIClosure(false, nothing, nothing, nothing, nothing, nothing, nothing)
gui.items = pacman
gui.segment_viz = make_viz!(ax, LineSegment{Point{2, Float64}})
gui.sector_viz = make_viz!(ax, ArcRegion{2, Float64})
gui.box_viz = make_viz!(ax, BoundingBox{2, Float64})

render!(gui.segment_viz, gui.items.segment)
render!(gui.sector_viz, gui.items.sector)
render!(gui.box_viz, gui.items.box)

function onclick!(gui, event)
    gui.clicked = true
    gui.freeze = deepcopy(gui.items) # how to copy! into a mutable struct?
    gui.click_point = Point(event[:xdata], event[:ydata])
end

function onunclick!(gui, event)
    gui.clicked = false
end



function negate_status(s)
  if s == :full
    return :empty
  elseif s == :empty
    return :full
  end
  return :mixed
end

function add_status(pos1, pos2)
  if pos1 == :full && pos2 == :full
    return :full
  end

  if pos1 == :empty || pos2 == :empty
    return :empty
  end

  return :mixed
end


function onmove!(gui, event)
    if gui.clicked && event[:xdata] != nothing && event[:ydata] != nothing
      #@show event[:xdata] event[:ydata]
      drag_update!(gui.items, gui.freeze, gui.click_point, Point(event[:xdata], event[:ydata]))
      render!(gui.segment_viz, gui.items.segment)
      render!(gui.box_viz, gui.items.box)

      if contains(gui.items.sector, gui.items.segment)
        gui.segment_viz.line[:set_linestyle]("-")
      else
        gui.segment_viz.line[:set_linestyle]("--")
      end

      status_sector = cellstatus(gui.items.sector, gui.items.box)
      status = status_sector
      for shadow in VAMP.compute_visibility(gui.items.sector, polys_obstacles__Fworld).negative.pieces
        status_shadow = cellstatus(shadow, gui.items.box)
        status = add_status(status, negate_status(status_shadow))
      end

      if status == :full
        gui.box_viz.line[:set_linestyle]("solid")
      elseif status == :empty
        gui.box_viz.line[:set_linestyle]("dotted")
      else
        gui.box_viz.line[:set_linestyle]("dashed")
      end
    end
end

for poly in polys_obstacles__Fworld.pieces
  ((xhull, yhull), _junk) = VAMP.renderclip((-2.0,2.0),(-2.0,2.0), poly)
  ax[:add_patch](PyPlot.matplotlib[:patches][:Polygon]([xhull yhull]))
end

for shadow in VAMP.compute_visibility(gui.items.sector, polys_obstacles__Fworld).negative.pieces
  ((xhull, yhull), _junk) = VAMP.renderclip((-2.0,2.0),(-2.0,2.0), shadow)
  ax[:add_patch](PyPlot.matplotlib[:patches][:Polygon]([xhull yhull], color="grey", alpha=0.2))
end

cid = fig[:canvas][:mpl_connect]("button_press_event", e->onclick!(gui, e))
cid = fig[:canvas][:mpl_connect]("button_release_event", e->onunclick!(gui, e))
cid = fig[:canvas][:mpl_connect]("motion_notify_event", e->onmove!(gui, e))
