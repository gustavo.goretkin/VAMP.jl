s1 = SE2_MPState(0.0,0.0,0.0)
s2 = SE2_MPState(1.0,2.0,3.0)

state(s1, action(s2,s1)) === s2

half = fragment(.5, action(s2,s1))
state(state(s1, half), half) === s2
