using VAMP
using BenchmarkTools

println("`using VAMP` completed")

include("../test/domain_robot.jl")
include("../test/domain_world.jl")
include("../test/domain_motion_planning_collision.jl")
println("`include(domain_*)` completed")

parameters = Dict()

make_robot(parameters)
make_robot_vis_target(parameters)
make_domain_world(parameters)
make_domain_motion_planning_collision(parameters)

println("made robot paramters")

@setparam parameters polys_robot__Frobot = nothing
@setparam parameters poly_frustum__Fcamera = nothing
@setparam parameters polys_obstacles__Fworld = nothing
@setparam parameters is_collision_free = nothing
@setparam parameters is_path_free = nothing

@setparam parameters visibility_instance = let
    @setparam parameters tx__Frobot__Fcamerabase = nothing
    @setparam parameters poly_frustum__Fcamera = nothing
    @setparam parameters all_view__Fcamerabase = nothing
    @setparam parameters polys_obstacles__Fworld = nothing
    @setparam parameters points_robot__Frobot = nothing

    camera = PanSteerableCamera(tx__Frobot__Fcamerabase, poly_frustum__Fcamera)
    potential_camera = VAMP.PotentialCamera(camera, all_view__Fcamerabase)

    VisibilityInstance(
      Set(points_robot__Frobot),
      polys_obstacles__Fworld,
      potential_camera,
      q->UnionOfConvex([]));
end;

println("built visibility_instance")

@setparam parameters frustum_depth = nothing
@setparam parameters frustum_width = nothing


SE2_MPState_t = SE2_MPState{parameters[:robot_width]/2} # reasonable scaling to measure distance
ROBOT_S = SE2_MPState_t{Float64}
ROBOT_A = SE2MotionPlanningAction{parameters[:robot_width]/2, Float64}

using JLD2
