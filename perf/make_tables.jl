using CSV
using DataFrames
using Query
using LaTeXStrings
using LaTeXTabulars

df = CSV.read("outs/experiments/aggregate.csv")

function to_domain_name(domain_id, domain1_goal)
    if string(domain_id) == "world2"
        return "TwoHallway"
    end

    if string(domain_id) == "world1"
        "Hallway" * ucfirst(string(domain1_goal))
    end
end

function to_algorithm_name(algorithm_id)
    return Dict("two_phase_none"=>"TP1", "two_phase_vavp"=>"TPV", "gstar_treevis"=>"Tree")[string(algorithm_id)]
end

df[:domain] = [to_domain_name(arg...) for arg in zip(df[:domain_id], df[:domain1_goal])]
df[:algorithm] = map(to_algorithm_name, df[:algorithm_id])

function do_pivot(df, fieldname; reduction=minimum, format=((x)->x))
    rownames = ["HallwayEasy", "HallwayHard", "TwoHallway"]
    colnames =  ["TP1", "TPV"]
    rowfield = :domain
    colfield = :algorithm

    result = Array{Any}(length(rownames), length(colnames))

    for (irow, row) in enumerate(rownames)
        for (icol, col) in enumerate(colnames)
            selected = df[(df[rowfield] .== row) .& (df[colfield] .== col), :]
            v = reduction(selected[fieldname])
            result[irow, icol] = format(v)
        end
    end

    return result
end

megatable = Array{Any}(3, 3)

field_format = [x->@sprintf("%02.1f", x), x->@sprintf("%02.1f", x), x->x]

for (iout, fov) in enumerate([50, 200, 350])
    for (jout, field_number) in enumerate([:total_search_time_secs, :path_length, :closed_total])

        # table elements

        query = @eval @from i in df begin
            @where i.all_field_of_view == $(fov) &&  i.algorithm_id in ["two_phase_vavp", "two_phase_none"]
            @select {i.algorithm, i.domain, i.$(field_number)}
            @collect DataFrame
        end
        @show field_number
        @show fov
        println(query)
        megatable[jout, iout] = #=ones(3,3) * fov=# do_pivot(query, field_number, format=field_format[jout])
    end
end

row_in_header = ["{\\sc HallwayEasy}", "{\\sc HallwayHard}", "{\\sc TwoHallway}"]
row_out_header = ["Search time (s)", "Path length (m)", "Closed nodes"]
n_row_out = 3
n_row_in = 3
n_col_in = 2
n_col_out = 3

megarows = []
for row_out_i = 1:n_row_out
  for row_in_i = 1:n_row_in
  megarow = vcat([
    [
    megatable[row_out_i, col_out_i][row_in_i, col_in_i]
    #"$(row_out_i),$(col_out_i),$(row_in_i),$(col_in_i)"
    for col_in_i = 1:n_col_in]
      for col_out_i = 1:n_col_out]...)

  row_out_header_ = ""

  if row_in_i == 1
    row_out_header_ = row_out_header[row_out_i]
    #@show row_out_header_
  end
  #@show vcat([row_out_header_], [row_in_header[row_in_i]])
  push!(megarows, vcat([row_out_header_], [row_in_header[row_in_i]], megarow) )
  end
  push!(megarows, Rule(:mid))
end

n_algs = 2
col_in_header = ["VB1", "VB\$\\infty\$"]

col_out_header = [
  Rule(:top),
  vcat(["", ""],  [MultiColumn(n_algs, :c, "fov=\\ang{$(fov)}") for fov in [50, 200, 350]]),
  ["\\cmidrule(lr){3-4} \\cmidrule(lr){5-6} \\cmidrule(lr){7-8}"], # TODO make sure there is no \\ after this row.
  vcat(["", ""], col_in_header, col_in_header, col_in_header)  ]



megarows_label = vcat(col_out_header,
  megarows, [Rule(:bottom)])

latex_tabular("/home/goretkin/vamp/vampwafr/table.tex",
  Tabular("llrrrrrr"),
  megarows_label)


#==========#
