using VAMP
using BenchmarkTools

println("`using VAMP` completed")

include("../test/domain_robot.jl")
include("../test/domain_world.jl")
include("../test/domain_motion_planning_collision.jl")
println("`include(domain_*)` completed")

parameters = Dict()

make_robot(parameters)
make_robot_vis_target(parameters)
make_domain_world(parameters)
make_domain_motion_planning_collision(parameters)

@setparam parameters polys_robot__Frobot = nothing
@setparam parameters poly_frustum__Fcamera = nothing
@setparam parameters polys_obstacles__Fworld = nothing
@setparam parameters is_collision_free = nothing
@setparam parameters is_path_free = nothing

@setparam parameters visibility_instance = let
    @setparam parameters tx__Frobot__Fcamerabase = nothing
    @setparam parameters poly_frustum__Fcamera = nothing
    @setparam parameters all_view__Fcamerabase = nothing
    @setparam parameters polys_obstacles__Fworld = nothing
    @setparam parameters points_robot__Frobot = nothing

    camera = PanSteerableCamera(tx__Frobot__Fcamerabase, poly_frustum__Fcamera)
    potential_camera = VAMP.PotentialCamera(camera, all_view__Fcamerabase)

    VisibilityInstance(
      Set(points_robot__Frobot),
      polys_obstacles__Fworld,
      potential_camera,
      q->UnionOfConvex([]));
end;

@setparam parameters frustum_depth = nothing
@setparam parameters frustum_width = nothing


SE2_MPState_t = SE2_MPState{parameters[:robot_width]/2} # reasonable scaling to measure distance
ROBOT_S = SE2_MPState_t{Float64}
ROBOT_A = SE2MotionPlanningAction{parameters[:robot_width]/2, Float64}

# BEGIN COPY'd from simple_mp_path.jl but UNUNUNUNUNLIFTED TO VAMP
q_goal = SE2_MPState_t(1.0,0.6,0.0)
# q_goal = SE2_MPState_t(-1.0,1.0,0.0) # easy problem

goal = PointEpsilonGoal(q_goal, 0.01)
start = SE2_MPState_t(-1.0,-1.0,0.0)
# starting_visibility__Frobot

mp = HolonomicMotionPlanningInstance(
  start,
  goal,
  state->is_collision_free(state),
  (state1,state2)->is_path_free(state1,state2)
)

q_viz_goal = SE2_MPState_t(1.0,0.6,π) # not really

extension_length = frustum_depth/2 # arbitrary and wrongitrary
rrt_instance = RRTInstance(
  mp,
  extension_length,  # insert some heavy domain-specific knowledge
  ()->if (rand() < .05) begin if rand() < .5 rand(mp.q_goal) else q_viz_goal end end else SE2_MPState_t(8*(rand()-.5), 8*(rand()-.5), 2π*rand()) end
)

rrt_algorithm = RRTAlgorithm(rrt_instance, VAMP.RRTNode{ROBOT_S})
# END COPY

starting_visibility__Fworld = [
  DifferenceGeneral(
    ArcRegion(0.8, -1.0, Point(start.e.q), Vec(1.0, 0.0)),
    UnionOfConvex(POLY[])
  )]

vamp_algorithm = VAMP.RetroVampRRTAlgorithm(rrt_algorithm, visibility_instance, eltype(starting_visibility__Fworld)[])

setup!(vamp_algorithm.rrt, 100)
step!(vamp_algorithm, rrt_instance.q_sampler(), nothing)


function plot_algorithm!(plt, vamp_algorithm)
  plot!(plt; aspect_ratio=1.0)
  # INCREDIBLY SLOW

  for (i, region) in enumerate(vamp_algorithm.visible_region)
    for p in explicit(region,15).pieces
      plot_polyhedron!(plt, p; fillalpha=1.0, linealpha=0.0, fillcolor=:yellow)
    end
    println("$i of $(length(vamp_algorithm.visible_region))")
    display(plt) # incremental plotting because this is slow...
  end

  for piece in vamp_algorithm.visibility_instance.occluders__Fworld.pieces
    plot_polyhedron!(plt, piece, fillcolor=:brown)
  end

  scatter!(plt, collect(map(x->tuple(x...), transform(
    VAMP.get_tx__Fworld__Frobot(vamp_algorithm.rrt.tree[1].state),
    vamp_algorithm.visibility_instance.robot__Frobot))), color=:green, linealpha=0.0)

  for node in vamp_algorithm.rrt.tree
    to_state = node.state
    from_state = vamp_algorithm.rrt.tree[node.parent_index].state
    # tree edge
    plot!(plt, map(x->tuple(x...), [from_state.e.q, to_state.e.q]), color=:black)
  end
  # tree node
  scatter!(plt, map(x->tuple(x...), node.state.e.q for node in vamp_algorithm.rrt.tree), color=:blue, alpha=0.2)

  scatter!(plt, collect(map(x->tuple(x...), transform(
    VAMP.get_tx__Fworld__Frobot(vamp_algorithm.rrt.instance.mp.q_goal.q),
    vamp_algorithm.visibility_instance.robot__Frobot))), color=:red, linealpha=0.0)

end

using Plots
pyplot()
plt = plot()
plot_algorithm!(plt, vamp_algorithm)
display(plt)


n = 15_000

using JLD2

println("start benchmark")
date = Dates.now()
goal_found_ones = []
gave_up_ones = []
tried_so_far = 0
while true
  tried_so_far += 1
  @show tried_so_far
  last_time = 0.0
#=b = @benchmark=# for i = 1:n
  # q_rand = q_rands[i]
  q_rand = rrt_instance.q_sampler()
  step!(vamp_algorithm, q_rand, nothing)
  if (time() - last_time > 3.0)
    last_time = time()
    println("iteration: $i")
    sleep(0.5) # try to allow breaking with Ctrl-C
  end
  if !isempty(vamp_algorithm.rrt.reached_goals)
    println("FOUND GOAL after $i iterations")
    push!(goal_found_ones, deepcopy(vamp_algorithm))
    @save "retrovamprrt_goals_$date.jld2" goal_found_ones tried_so_far
    break
  end
end
push!(gave_up_ones, deepcopy(vamp_algorithm))
@save "retrovamprrt_gaveup_$date.jld2" gave_up_ones tried_so_far
#end setup= begin
  if false
    @show length(vamp_algorithm.rrt.tree)
    plt = plot(;reuse=false)
    plot_algorithm!(plt, vamp_algorithm)
    display(plt)
  end

  if false
    plt3d = plot3d(;reuse=false)
    VAMP.plot_tree_se2!(plt3d, vamp_algorithm.rrt.tree)

    flatten(state) = tuple(state.e.q..., rad2deg(state.θ.q.θ))
    scatter3d!(plt3d, [flatten(vamp_algorithm.rrt.instance.mp.q_goal.q)], color=:red)

    display(plt3d)
    sleep(0.5) #???
    # will get overwritten by any other plot updates.
    for poly in vamp_algorithm.visibility_instance.occluders__Fworld.pieces
      VAMP.pyplot_polyhedron3d!(plt3d.subplots[1].o, plt3d, poly)
    end
  end
  setup!(vamp_algorithm, n)
  # TODO next lines should be in setup!, and have a VisibilityMotionPlanningInstance?
  append!(vamp_algorithm.visible_region, starting_visibility__Fworld) # note that there is already a region in visible_region corresponding to the root
  #srand(0)
  #q_rands = [rrt_instance.q_sampler() for i=1:n]
  #last_time = 0.0
end #seconds = 0.1

@show b
