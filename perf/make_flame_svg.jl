import ProfileView
import JLD2
function profile_svg_from_jld2(filename)
  j = JLD2.jldopen(filename)
  li = j["li"]
  lidict = j["lidict"]
  ProfileView.svgwrite("$(filename).svg", li, lidict)
end
