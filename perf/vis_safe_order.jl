using VAMP
using BenchmarkTools

include("../test/domain_robot.jl")
include("../test/domain_world.jl")

path = [SE2_MPState(.6 + x , hallway_inner_width/2, π) for x in range(5.0, stop=0.0, length=50)]
check_me = path[end]

using Plots

plt = plot(;aspect_ratio=1.0)
for p in polys_obstacles__Fworld.pieces
  plot_polyhedron!(plt, p; fillcolor=:brown)
end

function plot_q_robot!(plt, q_robot)
  robot__Fworld = VAMP.get_robot__Fworld(polys_robot__Frobot, q_robot)
  for p in robot__Fworld.pieces
    plot_polyhedron!(plt, p; fillcolor=:purple)
  end
  return plt
end

function plot_viz!(plt, viz::Difference)
  for p in explicit(viz).pieces
    plot_polyhedron!(plt, p, fillcolor=:yellow, fillalpha=0.2)
  end
  return plt
end

function plot_viz!(plt, viz::ArcRegion)
  shape = VAMP.render(viz, 35)
  plot!(plt, shape, fillcolor=:yellow, fillalpha=0.2, linealpha=0.0)
  return plt
end

function plot_viz!(plt, viz::DifferenceGeneral{ARC, UnionOfConvex{POLY}}) where {ARC, POLY}
  for p in explicit(viz, 35).pieces
    plot!(plt, p, fillcolor=:yellow, fillalpha=0.2, linealpha=0.0)
  end
  return plt
end



plot_q_robot!(plt, path[1])
plot_q_robot!(plt, path[end])

visible_regions = []
for q_robot in path
  tx__Fworld__Frobot = VAMP.get_tx__Fworld__Frobot(q_robot)

  tx__Fworld__Fcamerabase = tx__Fworld__Frobot ∘ tx__Frobot__Fcamerabase
  #visible_region = VAMP.compute_visibility(tx__Fworld__Fcamerabase, poly_all_view__Fcamera, polys_obstacles__Fworld)
  visible_region = VAMP.compute_visibility(tx__Fworld__Fcamerabase, all_view__Fcamerabase, polys_obstacles__Fworld)
  push!(visible_regions, visible_region) # should be AbstractVector{Difference}, in this case
end

plot_q_robot!(plt, path[1])
plot_q_robot!(plt, path[end])
plot_viz!(plt, visible_regions[1])
plot_viz!(plt, visible_regions[end])

#= for poly positive
difference(polys_robot__Frobot, visible_regions)
=#

plt = plot(;aspect_ratio=1.0)
plot_viz!(plt, visible_regions[27])

#= for poly positive
for p in difference(polys_robot__Frobot, visible_regions[27:27]).pieces
  plot_polyhedron!(plt, p)
end
=#
scatter!(plt, collect(map(x->tuple(x...), difference(
  transform(VAMP.get_tx__Fworld__Frobot(path[27+5]), Set(points_robot__Frobot)), visible_regions[27:27]))))

vis_check = reverse(visible_regions[:]) # or don't reverse
@benchmark begin
safe = difference(
  transform(VAMP.get_tx__Fworld__Frobot(path[end]), Set(points_robot__Frobot)),
  vis_check
  )
end

for p in visible_regions[27].negative.pieces
  plot_polyhedron!(plt, p, fillcolor=:black ,fillalpha=0.1)
end

display(plt)
