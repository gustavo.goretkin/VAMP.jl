using VAMP
import JLD2
import Plots
using Printf

Plots.pyplot()
function depict_goal!(plt, goal::PointEpsilonGoal; kwargs...)
    q = rand(goal)
    r = get_robot__Fworld(polys_robot__Frobot, q)
    plot_polyhedron!(plt, r; fillalpha=0.0, linewidth=6.0, kwargs...)
end

function depict_goal!(plt, goal::VAMP.CoverageGoal; kwargs...)
    scatter_points!(plt, goal.region; kwargs...)
end


function do_subgoal_figure(root)
    subgoals_fn = "subgoals"
    search_results = JLD2.jldopen(joinpath(root, "results_full.jld2"))
    goals_meta = search_results["goals_meta"]

    for (i, goal_meta) in enumerate(goals_meta)
        plt = Plots.plot(;aspect_ratio=1.0)
        plot_polyhedron!(plt, polys_obstacles__Fworld, color=:grey, linealpha=0.0)


        for (kind, path) in zip([:safe, :unsafe], [goal_meta.provenance.path_prefix, goal_meta.provenance.path_unsafe])
          for q in path
            p = VAMP.get_robot__Fworld(polys_robot__Frobot, q)

            plot_polyhedron!(plt, p, fillalpha=0.0, linecolor=Dict(:safe=>:grey, :unsafe=>:red)[kind], linealpha=0.5)
          end
        end

        depict_goal!(plt, goal_meta.goal; color=:green, linecolor=:green)
        depict_goal!(plt, goal_meta.regress; color=:red)

        for ext in ["png", "svg", "pdf"]
            dir = joinpath(root, subgoals_fn, ext)
            mkpath(dir)
            Plots.savefig(plt, joinpath(dir, "$(i).$(ext)"))
        end
    end
end

"""
Find bounding box that keeps obstacles and swept volume in view.
"""
function get_figure_bounding_box__Fworld(parameters, q_path)
    @setparam parameters boundingbox_obstacles__Fworld = nothing
    @setparam parameters polys_robot__Frobot = nothing

    bb_empty = BoundingBox([NaN, NaN], [NaN, NaN])

    boundingbox_swept__Fworld = reduce(union,
        (BoundingBox(get_robot__Fworld(polys_robot__Frobot, q)) for q in q_path), init=bb_empty)

    bb_view__Fworld = union(boundingbox_obstacles__Fworld, boundingbox_swept__Fworld)
    return bb_view__Fworld
end

function do_figure_maker(root; view_plan_kind=:none, plot_all_frames=true)
    view_seq_fn = "view_sequence_viewplan-$(view_plan_kind)-paf$(plot_all_frames)"
    file_output_path = joinpath(root, view_seq_fn)

    if !safe_make_path(file_output_path)
        println("$(file_output_path) already exists")
        if isfile(joinpath(file_output_path, "finished_and_did_not_crash.txt"))
            println("some animations already made")
            return
        end
    end
    do_figure_maker_path(root, file_output_path; view_plan_kind=view_plan_kind, plot_all_frames=plot_all_frames)
end

function do_figure_maker_path(root, file_output_path; view_plan_kind=:none, plot_all_frames=true)
    view_plan_fn = "view_plan.jld2"
    search_results = JLD2.jldopen(joinpath(root, "results_full.jld2"))

    path = search_results["path"]
    path_segments = search_results["path_segments"]

    path_i_transitions = cumsum(map(x->length(x.path), path_segments))
    path_i_to_segment_i(path_i) = first(searchsorted(path_i_transitions, path_i))

    parameters = search_results["parameters"]

    views_path = [ [] for _ in path] # all views, indexed by path_i
    path_i_views = [] # path_i's that have views

    if view_plan_kind == :full
        views_for_plots = nothing
        if !isfile(joinpath(root, view_plan_fn))
            println("Did not find $(view_plan_fn). Generating it.")
            q_path = unlift_q(path)
            view_plan_results = VAMP.greedy_view_plan(q_path, parameters, path[1].visible_region)

            views_for_plots = let r = view_plan_results
                Dict(k=>VAMP.vamp_geom_to_plots(r.discrete_views[k.path_i][k.view_i]) for k in r.view_plan)
            end

            JLD2.@save joinpath(root, view_plan_fn) view_plan_results views_for_plots
        else
            view_plan_results = JLD2.jldopen(joinpath(root, view_plan_fn))
            views_for_plots = view_plan_results["views_for_plots"]
        end

        path_i_views = sort(unique(map(b->b.path_i, keys(views_for_plots))))

        for (k, v) in sort(collect(views_for_plots), by=(x)->(first(x).path_i, first(x).view_i))
            push!(views_path[k.path_i], v)
        end

    end
    if view_plan_kind == :none
        #V = VAMP.make_vamp_pieces(search_results["parameters"])[:get_visible_region_cached__Fworld]
        visibility_instance = search_results["parameters"][:visibility_instance]
        V(q_robot) = VAMP.get_visible_region__Fworld(visibility_instance, q_robot)
        for i = 1:length(path)
            push!(views_path[i], VAMP.LibGEOSInterface.Polygon(V(path[i].q)))
            push!(path_i_views, i)
        end
    end


    # at these frames either we move between two segments, or we take a view.
    path_i_frames = sort(unique(vcat(path_i_views, path_i_transitions)))

    if plot_all_frames
        path_i_frames = collect(1:length(path))
    end

    plot_frames(parameters, path, path_segments, views_path, path_i_views, path_i_frames, file_output_path)
    open(joinpath(file_output_path, "finished_and_did_not_crash.txt"), "w") do f
        write(f, "Good thing.\n")
    end
end

"""
Plot a snail trail of the robot from q_path[1] to q_path[end].
with end filled in, with outlines dropping off in the past.
"""
function plot_fading_trail!(plt, q_path, polys_robot__Frobot)
    N = length(q_path)
    for i in 1:N
        robot = VAMP.get_robot__Fworld(polys_robot__Frobot, q_path[i])
        α = ( 1.5*((i/N) - 1) + 1)
        if i == N
          plot_polyhedron!(plt, robot; color=:blue, fillalpha=0.7)
        else
          plot_polyhedron!(plt, robot; color=:grey, fillalpha=0.0, linealpha=min(1.0, max(0.0, α)))
        end
    end
end

"""
Plot projection of configuration path in workspace
"""
function plot_center_path!(plt, q_path)
    xs = [q.e.q[1] for q in q_path]
    ys = [q.e.q[2] for q in q_path]
    Plots.plot!(plt, xs, ys, color=:green, linestyle=:dash)
end

"""
Note, Plots might not draw until you try to save or display the plot via Plots.
"""
function save_frame(plt, file_output_path, fig_i; exts=["png", "svg", "pdf"], save_via=[:Plots, :mpl])
    for ext in exts
        frame_str = @sprintf("%04d", fig_i)
        dir = joinpath(file_output_path, ext)
        dirmpl = joinpath(file_output_path, ext * "mpl")

        if :Plots in save_via
            mkpath(dir)
            Plots.savefig(plt, joinpath(dir, "$(frame_str).$(ext)"))
        end

        if :mpl in save_via
            mkpath(dirmpl)
            plt.o[:savefig](joinpath(dirmpl, "$(frame_str).$(ext)"), bbox_inches="tight")
        end
    end
end

function plot_frames(parameters, path, path_segments, views_path, path_i_views, path_i_frames, file_output_path)
    @setparam parameters polys_robot__Frobot = nothing
    @setparam parameters polys_obstacles__Fworld = nothing

    bb_view__Fworld = get_figure_bounding_box__Fworld(parameters, unlift_q(path))

    path_i_transitions = cumsum(map(x->length(x.path), path_segments))
    path_i_to_segment_i(path_i) = first(searchsorted(path_i_transitions, path_i))

    for (fig_i, path_i) in enumerate(path_i_frames)
        (xlim, ylim) = VAMP.dimlimits(bb_view__Fworld)
        segment_i = path_i_to_segment_i(path_i)
        path_kind = path_segments[segment_i].kind
        title = "seg:$(segment_i), kind:$(path_kind)"

        plt = Plots.plot(;aspect_ratio=1, xlim=xlim, ylim=ylim, title=title)

        @show fig_i
        initial_visibility = let xys = VAMP.explicitcontour(parameters[:starting_visibility__Fworld][1].positive, 360)
            [Plots.Shape([xy[1] for xy in xys], [xy[2] for xy in xys])]
        end

        visible_shapes = vcat(initial_visibility, views_path[1:path_i]...)

        for v in visible_shapes
            Plots.plot!(plt, v; color=:yellow, linealpha=0.0, fillalpha=0.3)
        end

        q_goal = path[end].q
        if q_goal != nothing
            plot_polyhedron!(plt, VAMP.get_robot__Fworld(polys_robot__Frobot, q_goal), color=:green, fillalpha=0.0, linestyle=:dash)
        end

        segment = path[1:path_i]

        plot_center_path!(plt, unlift_q(segment))

        plot_polyhedron!(plt, polys_obstacles__Fworld, color=:grey, linecolor=nothing)

        plot_fading_trail!(plt, unlift_q(segment), polys_robot__Frobot)

        if path_i in path_i_views
            # emphasize new views
            for v in views_path[path_i]
                Plots.plot!(plt, v; color=:orange, linealpha=0.3, fillalpha=0.3)
            end
        end
        Plots.plot!(plt; size=(600, 400))
        save_frame(plt, file_output_path, fig_i, exts=["png"]; save_via=[:Plots])
    end
    return nothing
end

import Serialization
function do_movie_maker(root)
    search_results = JLD2.jldopen(joinpath(root, "results_full.jld2"))

    path = search_results["path"]
    path_segments = search_results["path_segments"]
    # parameters = search_results["parameters"]

    parameters = Serialization.deserialize(open(joinpath(root, "parameters.jl_serialize")))

    FileIO.load(joinpath(root, "parameters.jld2"))["setparam"]

    VAMP.make_animation(path, parameters; q_goal=nothing, root=joinpath(root, "animation"))
end



function plot_frame_viol!(plt, parameters, q_path, v_path, viol_path, path_i)
    @setparam parameters polys_robot__Frobot = nothing
    @setparam parameters polys_obstacles__Fworld = nothing

    @assert length(parameters[:starting_visibility__Fworld]) == 1
    initial_visibility = let xys = VAMP.explicitcontour(parameters[:starting_visibility__Fworld][1].positive, 360)
        [Plots.Shape([xy[1] for xy in xys], [xy[2] for xy in xys])]
    end

    visible_shapes = vcat(initial_visibility, v_path[1:path_i]...)

    for v in visible_shapes
        Plots.plot!(plt, v; color=:yellow, linealpha=0.0, fillalpha=0.3)
    end

    q_goal = q_path[end]
    if q_goal != nothing
        plot_polyhedron!(plt, VAMP.get_robot__Fworld(polys_robot__Frobot, q_goal), color=:green, fillalpha=0.0, linestyle=:dash)
    end

    # plot all past violations
    all_violation = VAMP.LibGEOSInterface.union_no_hole(UnionOfGeometry([v for v in viol_path[1:path_i]]))
    for v in all_violation
        Plots.plot!(plt, v; color=:red, linealpha=0.0, fillalpha=0.8)
    end


    plot_center_path!(plt, q_path[1:path_i])

    plot_polyhedron!(plt, polys_obstacles__Fworld, color=:grey, linecolor=nothing)

    plot_fading_trail!(plt, q_path[1:path_i], polys_robot__Frobot)

    # emphasize current view
    Plots.plot!(plt, v_path[path_i]; color=:orange, linealpha=0.3, fillalpha=0.3)

    # plot new violations most saliently
    for v in [viol_path[path_i]]
        Plots.plot!(plt, v; color=:red, linealpha=0.0, fillalpha=0.9)
    end
end

function plot_frames_viol(parameters, q_path, v_path, viol_path, file_output_path; path_i_frames = 1:length(q_path))
    @assert length(q_path) == length(v_path) == length(viol_path)

    bb_view__Fworld = get_figure_bounding_box__Fworld(parameters, q_path)

    for (fig_i, path_i) in enumerate(path_i_frames)
        (xlim, ylim) = VAMP.dimlimits(bb_view__Fworld)

        plt = Plots.plot(;aspect_ratio=1, xlim=xlim, ylim=ylim)

        @show fig_i path_i

        plot_frame_viol!(plt, parameters, q_path, v_path, viol_path, path_i)
        save_frame(plt, file_output_path, fig_i, exts=["png"]; save_via=[:Plots])
    end
    return nothing
end