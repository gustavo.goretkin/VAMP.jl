using VAMP
using BenchmarkTools

include("../test/domain_robot.jl")
include("../test/domain_world.jl")
include("../test/domain_motion_planning_collision.jl")
println("`include(domain_*)` completed")

parameters = Dict()

make_robot(parameters)
make_robot_vis_target(parameters)
make_domain_world(parameters)
make_domain_motion_planning_collision(parameters)

@setparam parameters polys_robot__Frobot = nothing
@setparam parameters poly_frustum__Fcamera = nothing
@setparam parameters polys_obstacles__Fworld = nothing
@setparam parameters is_collision_free = nothing
@setparam parameters is_path_free = nothing

@setparam parameters visibility_instance = let
    @setparam parameters tx__Frobot__Fcamerabase = nothing
    @setparam parameters poly_frustum__Fcamera = nothing
    @setparam parameters all_view__Fcamerabase = nothing
    @setparam parameters polys_obstacles__Fworld = nothing
    @setparam parameters points_robot__Frobot = nothing

    camera = PanSteerableCamera(tx__Frobot__Fcamerabase, poly_frustum__Fcamera)

    VisibilityInstance(
      polys_robot__Frobot,
      polys_obstacles__Fworld,
      camera,
      q->UnionOfConvex([]));
end;

ROBOT_S = SE2MotionPlanningState{1.0, Float64}
VISIBILITY_S = Difference{POLY}

ROBOT_A = SE2MotionPlanningAction{1.0, Float64}
VISIBILITY_A = SO2LookAction{VAMP.Rot{Float64}}

VAMPSTATE = VampState{ROBOT_S, VISIBILITY_S}
VAMPACTION = VampAction{ROBOT_A, VISIBILITY_A}


# BEGIN COPY'd from simple_mp_path.jl but LIFTED TO VAMP
#q_goal = SE2_MPState(1.0,0.6,0.0)
q_goal = SE2_MPState(-1.0,1.0,0.0) # easy problem

goal = PointEpsilonGoal(
  VampState(q_goal, VISIBILITY_S[]), # lift to vamp
  0.01
)

# starting_visibility__Frobot

mp = HolonomicMotionPlanningInstance(
  VampState(SE2_MPState(-1.0,-1.0,0.0), VISIBILITY_S[]), # lift to vamp
  goal,
  state->is_collision_free(state.robot),  # lift to vamp
  (state1,state2)->is_path_free(state1.robot,state2.robot) # lift to vamp
)

rrt_instance = RRTInstance(
  mp,
  1.0,
  ()->if (rand() < .1) rand(mp.q_goal) else VampState(SE2_MPState(6*(rand()-.5), 6*(rand()-.5), 2π*rand()), VISIBILITY_S[]) end
)

rrt_algorithm = RRTAlgorithm(rrt_instance, VAMP.RRTNodeAction{VAMPSTATE, VAMPACTION})
# END COPY

vamp_algorithm = VampRRTAlgorithm(rrt_algorithm, visibility_instance)

setup!(vamp_algorithm.rrt, 100)
step!(vamp_algorithm, rrt_instance.q_sampler(), nothing)

println("start benchmark")
b = @benchmark for i = 1:n
  q_rand = q_rands[i]
  step!(vamp_algorithm, q_rand, nothing)
end setup= begin
  setup!(vamp_algorithm.rrt, 100)
  srand(0)
  n = 50
  q_rands = [rrt_instance.q_sampler() for i=1:n]
end seconds = 30
