using Glob
using VAMP

include("figure_maker.jl")
for dir in glob("outs/experiments/alg-two*")
    try
        println("process: $(dir)")
        #do_figure_maker(dir; view_plan_kind = :none)

        if isdir(joinpath(dir, "animation"))
            println("already have animation")
        else
            do_movie_maker(dir)
        end
    catch err
        @warn(err)
        open(joinpath(dir, "figure_maker_error.txt"), "a") do f
          write(f, "Error\n")
          write(f, string(err))
        end
    end
end
