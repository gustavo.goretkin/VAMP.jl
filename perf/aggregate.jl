using JLD2
using DataFrames
using NamedTuples
using CSV
rows = []
root_dir = "outs/experiments"
for dir in readdir(root_dir)
    if startswith(dir, "skipme")
        println("$(dir) is being skipped")
        continue
    end
    this_dir = joinpath(root_dir, dir)
    println(this_dir)
    if !isdir(this_dir); continue; end
    results_path = joinpath(this_dir, "results_dict.jld2")
    parameters_path = joinpath(this_dir, "parameters.jld2")

    if !isfile(results_path)
        warn("$(dir) has no results file")
        continue
    end
    parameters_jld = JLD2.jldopen(parameters_path)
    results_jld = JLD2.jldopen(results_path)
    setparam = parameters_jld["setparam"] # this is not too slow to load.
    results_dict = results_jld["results_dict"]
    row = merge(setparam, results_dict, Dict(:file=>dir))
    push!(rows, row)
end

defacto_cols = reduce(union, map(x->collect(keys(x)), rows))
column_order = [:file, :domain_id, :domain1_goal, :all_field_of_view, :algorithm_id, :total_search_time_secs, :closed_total, :path_length, :n_path_segments]

missing_cols = setdiff(Set(defacto_cols), Set(column_order))
if length(missing_cols) > 0
    warn("Missing: $(missing_cols)")
end

# list of row
table = [[get(row, col, missing) for col in column_order] for row in rows]

# list of columns
tableT = map(x->[x...], collect(zip(table...)))

df = DataFrame(tableT, column_order)
df[:all_field_of_view] = [if d == :narrow 15.0 else d end for d in df[:all_field_of_view]]


sort!(df, (:domain_id, :domain1_goal, :all_field_of_view, :algorithm_id))

CSV.write("outs/experiments/aggregate.csv", df)
